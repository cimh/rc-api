# -*- coding: utf-8 -*-
import sys
from api_engine import s
from api_engine.enums import USER_TYPE, USER_STATUS, USER_DELETE_STATUS, \
                             VIDEO_DELETE_STATUS, VIDEO_STATUS, VIDEO_PERMISSIONS

ES_SUCCESS_RESPONSE = {'acknowledged': True}
ES_CHUNK_SIZE = 10000
ES_REFRESH_INTERVAL = '1s'


def write(text=None):
    """

    :param text:
    :return:
    """
    sys.stdout.write(text)
    sys.stdout.flush()

default_template = {
    'template': '*-index-*',
    'settings': {
        'number_of_shards': str(s().elastic.nodes_count),
        'number_of_replicas': str(2 if s().environment == 'PRODUCTION' else 0),
        'index.cache.query.enable': True,
        'index.auto_expand_replicas': False,
    },
    'mappings': {
        '_default_': {
            'dynamic_templates': [
                {
                    'string_fields': {
                        'match': '*',
                        'match_mapping_type': 'string',
                        'mapping': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                    },
                },
                {
                    'date_fields': {
                        'mapping': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'match': '*',
                        'match_mapping_type': 'date',
                    },
                },
            ],
            '_source': {
                'enabled': True,
                'compress': False
            },
            '_all': {
                'enabled': False,
            },
            'properties': {
                '__time_created': {
                    'type': 'date',
                    'format': 'yyyy-MM-dd HH:mm:ss',
                },
                'scope': {
                    'type': 'string',
                    'index': 'not_analyzed',
                },
                'modify_ts': {
                    'type': 'date',
                    'format': 'yyyy-MM-dd HH:mm:ss',
                },
                'create_ts': {
                    'type': 'date',
                    'format': 'yyyy-MM-dd HH:mm:ss',
                },
                'delete_ts': {
                    'type': 'date',
                    'format': 'yyyy-MM-dd HH:mm:ss',
                },
                'is_deleted': {
                    'type': 'boolean',
                },
            },
        },
    }
}

# @TODO: date_fields dynamic template not match format
# @TODO: string_fields not working
default_warmer_setup = {
    'warmers': {

    }
}

system_user = {
    "birthday": "1980-01-01",
    "country": "EU",
    "joined_ts": "2014-12-27 12:12:12",
    "date_joined": "2014-12-27",
    "default_video_permission": VIDEO_PERMISSIONS.all,
    "email": "system@hellobot.com",
    "follow_approve": True,
    "fullname": "System",
    "gender": "female",
    "gps_incognito": False,
    "gps_location": {
        "lat": 10.84569,
        "lon": 52.418059
    },
    "is_active": True,
    "is_deleted": False,
    "delete_status": USER_DELETE_STATUS.not_deleted,
    "language": "en",
    "language2": "en",
    "last_login": "2014-01-01 19:46:25",
    "modify_ts": "2014-04-02 05:59:33",
    "password": "$pbkdf2-sha256$200000$0/r/nxMCIESodc7Zey9FiA$F6g.2wHXrjMnOPqt0TGdFeYZGLlY9w7DvKMQXOZCvsI",
    "photo_keys": None,
    "region": "Planet Earth",
    "status": USER_STATUS.idle,
    "user_type": USER_TYPE.system,
    "is_online": True,
    "gadget": False
}

proxy_user = {
    "birthday": "1984-08-04",
    "country": "EU",
    "joined_ts": "2014-12-27 12:12:13",
    "date_joined": "2014-12-27",
    "default_video_permission": VIDEO_PERMISSIONS.all,
    "email": "proxy@hellobot.com",
    "follow_approve": True,
    "fullname": "Proxy",
    "gender": "female",
    "gps_incognito": False,
    "gps_location": {
        "lat": 10.84569,
        "lon": 52.418059
    },
    "is_active": True,
    "is_deleted": False,
    "delete_status": USER_DELETE_STATUS.not_deleted,
    "language": "en",
    "language2": "en",
    "last_login": "2014-01-01 19:46:25",
    "modify_ts": "2014-04-02 05:59:33",
    "password": "$pbkdf2-sha256$200000$0/r/nxMCIESodc7Zey9FiA$F6g.2wHXrjMnOPqt0TGdFeYZGLlY9w7DvKMQXOZCvsI",
    "photo_keys": None,
    "region": "Planet Earth",
    "status": USER_STATUS.idle,
    "user_type": USER_TYPE.proxy,
    "is_online": True,
    "gadget": False
}

locust_user = {
    "rating": 0,
    "gps_incognito": False,
    "photo": "",
    "count_likes": 0,
    "user_type": 1,
    "photo_keys": {
        "origin": "avatar/AU1IY0QuTG6xZ0Lt7Yj2/e77710f08766f003f8213dac2ffb0f6e.png",
        "big": "avatar/AU1IY0QuTG6xZ0Lt7Yj2/baa98ed67c7fce5e3037a1ec920403da.png",
        "small": "avatar/AU1IY0QuTG6xZ0Lt7Yj2/597dbfc365613a55c292e82907d041bc.png",
        "medium": "avatar/AU1IY0QuTG6xZ0Lt7Yj2/df1b52062b9fb5be58c70096a618ad0d.png",
        "profile": "avatar/AU1IY0QuTG6xZ0Lt7Yj2/e65a01cebfed50c947ace5e294bea6a1.png"
    },
    "is_staff": False,
    "country_code": "",
    "count_bans": 0,
    "date_joined": "2015-05-12",
    "sms_codes": [
        {
            "send_ts": "2015-05-12 13:49:34",
            "value": "2655"
        }
    ],
    "last_login": "2015-05-12 13:49:33",
    "subscription_id": "",
    "modify_ts": "2015-07-06 11:25:31",
    "email": "",
    "status": 1,
    "gps_location": {
        "lat": -27.11124,
        "lon": -109.35053
    },
    "photo_keys_old": {
        "small": "b1/ReelCam/avatar/AU1IY0QuTG6xZ0Lt7Yj2/597dbfc365613a55c292e82907d041bc.png",
        "big": "b1/ReelCam/avatar/AU1IY0QuTG6xZ0Lt7Yj2/baa98ed67c7fce5e3037a1ec920403da.png",
        "medium": "b1/ReelCam/avatar/AU1IY0QuTG6xZ0Lt7Yj2/df1b52062b9fb5be58c70096a618ad0d.png",
        "profile": "b1/ReelCam/avatar/AU1IY0QuTG6xZ0Lt7Yj2/e65a01cebfed50c947ace5e294bea6a1.png",
        "origin": "b1/ReelCam/avatar/AU1IY0QuTG6xZ0Lt7Yj2/e77710f08766f003f8213dac2ffb0f6e.png"
    },
    "follow_approve": False,
    "gadget": True,
    "social": None,
    "is_active": True,
    "push_token": "",
    "phone": "70000000000",
    "birthday": None,
    "is_sms_validated": True,
    "is_online": True,
    "password": "$pbkdf2-sha256$200000$m5Py3jvH2FsL4XyP8f4/hw$PBr83vTqmWACw.pohYqcTfORC7Up2EM236bK.B1qhos",
    "count_dislikes": 0,
    "delete_status": 1,
    "default_video_permission": 0,
    "is_deleted": False,
    "language": "",
    "gender": "",
    "region": "",
    "social_auth": None,
    "language2": "",
    "total_video_duration": 0,
    "country": "",
    "fullname": "Icinga",
}

hbq_operator_user = {
    "birthday": "1980-01-01",
    "country": "EU",
    "joined_ts": "2014-12-27 12:12:12",
    "date_joined": "2014-12-27",
    "default_video_permission": VIDEO_PERMISSIONS.all,
    "email": "hbq_operator@hellobot.com",
    "follow_approve": True,
    "fullname": "Operator",
    "gender": "male",
    "gps_incognito": True,
    "gps_location": {
        "lat": 10.84569,
        "lon": 52.418059
    },
    "is_active": True,
    "is_deleted": False,
    "delete_status": USER_DELETE_STATUS.not_deleted,
    "language": "ru",
    "language2": "en",
    "last_login": "2014-01-01 19:46:25",
    "modify_ts": "2014-04-02 05:59:33",
    "password": "$pbkdf2-sha256$200000$GiNECCEkhHAupfS.FyIkhA$c9Gc/IfgIiCHi6UXqS5Rrgj2AXBLfD/1N24ePWybRbQ",  # "6d09ipxbsm"
    "photo_keys": None,
    "region": "Planet Earth",
    "status": USER_STATUS.idle,
    "user_type": USER_TYPE.hbq_operator,
    "is_online": True,
    "gadget": False,
    "phone": "333000333",
    "is_sms_validated": True,
}

default_application_settings = {
    'help_email': 'support@reelcam.com',
    'company_tel': '+3726682822',
    'site_url': 'http://reelcam.com',
    'tutorial_video': 'http://help.reelcam.com:9090/get/b1/stage/media/HELPER8ByVzGTVPOAWKLO/HELPERch6nikWoMYOP9XM/ri.flv',
    'company_name': 'Hellobot OÜ',
    'video_alias': '/#/video/'
}

default_background_settings = {
    'limit': 3,
    'shows': 50,
    'expire': 1434326400  # 15-06-2015
}

default_help_video = {
    'reporter_id': None,
    'access_observer': False,
    'count_bans': 0,
    'count_dislikes': 0,
    'count_likes': 0,
    'count_views': 0,
    'is_deleted': False,
    'delete_status': VIDEO_DELETE_STATUS.not_deleted,
    'duration': 58,
    'gps_incognito': True,
    'gps_location': {
        'lat': 52.3747158,
        'lon': 4.8986142
    },
    'last_pause_time': '2015-01-14 13:17:10',
    'master_id': None,
    'modify_ts': '2014-07-24 21:01:17',
    'observer_id': None,
    'observer_start_ts': None,
    'observer_token': None,
    'owner_id': None,
    'proxy_id': None,
    'reporter_start_ts': '2015-01-14 13:15:10',
    'reporter_token': '7281b97f-e4ee-4de9-bb17-c007cd4e4286',
    'status': VIDEO_STATUS.finished,
    'subject': 'English language',
    'visible': True,
    'permission': VIDEO_PERMISSIONS.all,
    'record_uploaded': True,
    'tags': '#help #en',
    'is_reanimated': False,
    'screenshot_keys': {
    },
    'filesize': 524288,
    'media_urls': {
        'r': '',
        'ri': '',
        'o': ''
    }
}
