# -*- coding: utf-8 -*-
from .static import *
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan, bulk
from api_engine import s
from .static.data import default_template, write, ES_SUCCESS_RESPONSE, ES_CHUNK_SIZE, ES_REFRESH_INTERVAL
TIME_CREATED_ERA_START = 1454025600  # 2016-01-29 00:00:00


class ESIndex(object):
    """
    ESManager uses this class to manipulate indices.
    """
    es = Elasticsearch(hosts=[s().elastic.host], port=s().elastic.host.split(':')[1], timeout=300, max_retries=10)

    new_name = None
    old_name = None
    mapping_names = None
    alias_name = None
    _mappings = None
    _settings = None
    _alias = None
    dry = False

    def make_new_name(self):
        """

        :return:
        """
        import random, string
        new_name = self.alias_name + "-v_" + ''.join(random.choice(string.ascii_lowercase) for i in range(4))
        return new_name

    def __init__(self, old_name=None, alias_name=None, mappings=None, settings=None, mapping_names=None, dry=False, **kwargs):
        """

        :param old_name:
        :param alias_name:
        :param mappings:
        :param settings:
        :param mapping_names:
        :param kwargs:
        :return:
        """
        try:
            self.old_name = old_name
            self.alias_name = alias_name if alias_name else old_name if old_name else None
            self.new_name = self.make_new_name()
            self._mappings = mappings if mappings else {}
            self._settings = settings if settings else {}
            self.mapping_names = mapping_names if mapping_names else []
            self.dry = True if dry else False
        except Exception as e:
            raise ValueError("WTF [%s]" % e)
        super(ESIndex, self).__init__()

    def create(self):
        """
        Creates index.

        :return:
        """
        write("Creating index %s.." % magenta(self.new_name))

        m = {'mappings': self._mappings.copy()}
        m.update({'settings': default_template['settings']})
        m.update({'settings': self._settings})

        if not self.dry:
            if self.es.indices.create(index=self.new_name, body=m) != ES_SUCCESS_RESPONSE:
                raise Exception(red("Failed to create index %s.\n" % magenta(self.new_name)))

        write(green("OK.\n"))

    def alias(self):
        """
        Creates alias.

        :return:
        """
        write("Creating alias %s to %s.." % (blue(self.alias_name), magenta(self.new_name)))

        if self.old_name:
            args = dict(actions=[
                dict(remove=dict(alias=self.alias_name, index=self.old_name)) \
                    if self.es.indices.exists(self.old_name) else None,
                dict(add=dict(alias=self.alias_name, index=self.new_name))
            ])
        else:
            args = dict(actions=[
                dict(add=dict(alias=self.alias_name, index=self.new_name))
            ])
        if not self.dry:
            self.es.indices.update_aliases(args)
        write(green("OK.\n"))

    def reindex(self, old_name, new_name):
        """
        Reindex data from <old_name> to <new_name>

        :param old_name:
        :param new_name:
        :return:
        """
        from elasticsearch.helpers import scan, bulk
        from utils.datetimemixin import DatetimeMixin
        if self.es.indices.exists(old_name) and self.es.count(index=old_name)['count'] > 0:
            if not self.dry:
                self.es.indices.put_settings({'index': {'refresh_interval': -1}}, index=self.old_name)
                write("Reindexing %s to %s.." % (blue(self.alias_name), magenta(self.new_name)))
                search = scan(self.es, index=self.old_name, size=ES_CHUNK_SIZE)
                actions = []
                for r in search:
                    r['_index'] = self.new_name
                    if '__time_created' not in r['_source']:
                        r['_source']['__time_created'] = DatetimeMixin.YmdHMS_from_ts(TIME_CREATED_ERA_START)
                    actions.append(r)
                docs_count, errors = bulk(self.es, index=self.new_name, doc_type=self.mapping_names, actions=actions)
                # r = reindex(self.es, self.old_name, new_name, chunk_size=ES_CHUNK_SIZE)
                # There were more than 0 errors
                # if r[1] > 0:
                #     raise Exception(color("Failed to reindex %s data (%s errors).\n" % (color(old_name, MAGENTA), str(r[1])), RED))
                self.es.indices.put_settings({'index': {'refresh_interval': ES_REFRESH_INTERVAL}}, index=self.old_name)
                self.refresh(new_name)
                write(green('OK (%s/%s docs).\n' % (len(actions), docs_count)))
        else:
            pass

    def refresh(self, index_name=None):
        """
        Refreshes index.

        :return:
        """
        if self.es.indices.exists(index_name):
            if not self.dry:
                self.es.indices.refresh(index=index_name)
                # write("Refreshed index %s.\n" % index_name)

    def delete(self, index_name=None):
        """
        Deletes index.

        :return:
        """
        if self.es.indices.exists(index_name):
            if not self.dry:
                write('Deleted index %s.\n' % magenta(index_name))
                self.es.indices.delete(index_name)

    def delete_field(self, field_name=None, doc_type=None):
        """
        Deletes a field from index.

        :param field_name:
        :param doc_type:
        :return:
        """
        if not self.dry:
            txt_fname = cyan(field_name)
            txt_iname = magenta(self.old_name)
            txt_doctype = cyan(doc_type)
            search = scan(self.es, index=self.old_name, doc_type=doc_type, size=ES_CHUNK_SIZE)
            actions = []
            for r in search:
                    r['_source'].pop(field_name, None)
                    actions.append(r)
            docs_count, errors = bulk(self.es, index=self.old_name, doc_type=doc_type, actions=actions)
            write("Deleted field %s [%s.%s] (%s)..\n" % (txt_fname, txt_iname, txt_doctype, docs_count))
            if errors:
                write(red("%s errors occured while deleting field %s." % (errors, txt_fname)))

    def add_field(self, field_name=None, doc_type=None):
        """
        Adds a field to index.

        :param field_name:
        :param doc_type:
        :return:
        """
        if not self.dry:
            from utils.datetimemixin import DatetimeMixin
            txt_fname = cyan(field_name)
            txt_iname = magenta(self.old_name)
            txt_doctype = cyan(doc_type)
            search = scan(self.es, index=self.old_name, doc_type=doc_type, size=ES_CHUNK_SIZE)
            actions = []
            for r in search:
                    if '__time_created' not in r['_source']:
                        r['_source']['__time_created'] = DatetimeMixin.YmdHMS_from_ts(TIME_CREATED_ERA_START)
                    if field_name not in r['_source']:
                        r['_source'][field_name] = None
                    actions.append(r)
            docs_count, errors = bulk(self.es, index=self.old_name, doc_type=doc_type, actions=actions)
            write("Added field %s [%s.%s] (%s)..\n" % (txt_fname, txt_iname, txt_doctype, docs_count))
            if errors:
                write(red("%s errors occured while deleting field %s." % (errors, txt_fname)))

    @classmethod
    def video_scope_bulk(cls, scope=None):
        """

        :param scope:
        :return:
        """
        INDEX_NAME = 'video-index'
        DOC_TYPE = 'video-entry'

        if not scope:
            scope = s().get_scope('reelcam')

        write("Writing scope %s to %s.." % (blue(scope), magenta(INDEX_NAME)))

        videos = scan(cls.es, index=INDEX_NAME, doc_type=DOC_TYPE, size=ES_CHUNK_SIZE)
        actions = []
        citydoom_scope = s().get_scope('citydoom')

        for r in videos:
            if 'scope' in r['_source']:
                if (r['_source']['subject'].find('Охота') != -1 or
                    r['_source']['subject'].find('Quest Map') != -1 or
                    r['_source']['subject'].find('[Промо игра]') != -1 or
                    r['_source']['subject'].find('Балканский ТРЭШ') != -1) and \
                                r['_source']['scope'] != citydoom_scope:

                    r['_source']['scope'] = citydoom_scope
                    actions.append(r)
                elif r['_source']['scope'] == '' and \
                    (('parts' in r['_source'] and r['_source']['parts'] == []) or
                        ('parts' not in r['_source'])) and \
                        not (r['_source']['subject'].find('Охота') != -1 or
                             r['_source']['subject'].find('Quest Map') != -1 or
                             r['_source']['subject'].find('[Промо игра]') != -1 or
                             r['_source']['subject'].find('Балканский ТРЭШ') != -1):
                    r['_source']['scope'] = scope
                    actions.append(r)
        docs_count, errors = bulk(cls.es, index=INDEX_NAME, doc_type=DOC_TYPE, actions=actions)
        write(green("OK (%s docs).\n" % docs_count))
        if errors:
            write(red("%s errors occurred while scoping data in %s. (%s)" % (errors, INDEX_NAME, docs_count)))

    def setup(self):
        """
        Setups index.

        :return:
        """
        self.create()
        if self.old_name:
            self.refresh(self.old_name)
            self.reindex(self.old_name, self.new_name)
            self.refresh(self.new_name)
            self.alias()
            self.delete(self.old_name)
        else:
            self.alias()
