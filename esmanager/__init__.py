# -*- coding: utf-8 -*-
import logging
from collections import defaultdict
from os import devnull
from tornado import gen
from tornado.escape import json_decode, json_encode
from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from tornado.ioloop import IOLoop
from api_engine import s
from api_engine.enums import USER_TYPE
from reelcam.mappers.base import BaseMapper
from utils import all_subclasses
from .esindex import ESIndex
from .static.data import *
from .static import *

logger = logging.getLogger('mainLog')
es_logger = logging.getLogger('elasticsearch')
# es_tracer = logging.getLogger('elasticsearch.trace')


class ESManager(object):
    """
    ES Manager is a tool for deploying database mappings (indices, warmers, aliases, templates).
    """

    indices = []
    warmers = defaultdict(lambda: {'warmers': {}})
    aliases = []
    dry = False

    def __init__(self, *args, **kwargs):
        """
        Collects all mappings and fills self.indices with list of ESIndex instances.

        :param args:
        :param kwargs:
        :return:
        """
        if 'dry' in kwargs:
            self.dry = True
        self._init_models()
        super(ESManager, self).__init__()

    def delete(self, mask):
        """

        :param mask:
        :return:
        """
        write("Deleting %s.." % magenta(mask))
        if not self.dry:
            s().get_es().indices.delete(mask)
        write(green("OK.\n"))

    def delete_all(self):
        """

        :return:
        """
        write("Deleting %s indices.." % cyan('_all'))
        if not self.dry:
            res = s().get_es().indices.delete('_all')
        write(green("OK.\n"))

    def import_mappers(self):
        """

        :return:
        """
        from reelcam.mappers.bookmark import BookmarkEntryMappingType
        from reelcam.mappers.chat import ChatEntryMappingType
        from reelcam.mappers.comment import CommentEntryMappingType
        from reelcam.mappers.feedback import FeedbackEntryMappingType
        from reelcam.mappers.follow import FollowEntryMappingType
        from reelcam.mappers.history.bookmark import BookmarkHistoryEntryMappingType
        from reelcam.mappers.history.chat import ChatHistoryEntryMappingType
        from reelcam.mappers.history.comment import CommentHistoryEntryMappingType
        from reelcam.mappers.history.follow import FollowHistoryEntryMappingType
        from reelcam.mappers.history.video import VideoHistoryEntryMappingType
        from reelcam.mappers.history.vlike import VideoLikeHistoryEntryMappingType
        from reelcam.mappers.message import MessageEntryMappingType
        from reelcam.mappers.proxy import ProxyEntryMappingType
        from reelcam.mappers.smslog import SmsLogEntryMappingType
        from reelcam.mappers.subscription import SubscriptionEntryMappingType
        from reelcam.mappers.upload import UploadEntryMappingType
        from reelcam.mappers.user import UserEntryMappingType
        from reelcam.mappers.video.ban import VideoBanEntryMappingType
        from reelcam.mappers.video.gps import VideoGpsEntryMappingType
        from reelcam.mappers.video.like import VideoLikeEntryMappingType
        from reelcam.mappers.video.livemsg import LiveMsgEntryMappingType
        from reelcam.mappers.video.rtview import RTViewEntryMappingType
        from reelcam.mappers.video.users import VideoUsersEntryMappingType
        from reelcam.mappers.video.video import VideoEntryMappingType
        from reelcam.mappers.video.view import VideoViewEntryMappingType
        from reelcam.mappers.observe import ObserveEntryMappingType
        from reelcam.mappers.h import HEntryMappingType
        from reelcam.mappers.promo import PromoEntryMappingType

    def make_indices(self):
        """
        Collects all index names from reelcam.mappers.

        :return:
        """
        self.import_mappers()
        indices = {}
        for cls in all_subclasses(BaseMapper):
            if cls.__name__ in ["BaseHistoryMapper", "ObserveHistoryEntryMappingType"]:
                continue

            index = cls().get_index()

            if cls().get_mapping() is not None:
                _mappings = cls().get_mapping().get('mappings', {})
                _settings = cls().get_mapping().get('settings', {})

                if index not in indices:
                    indices[index] = dict(mappings=_mappings, settings=_settings, mapping_names=[])

                if 'mappings' in indices[index]:
                    indices[index]['mappings'].update(_mappings)
                else:
                    indices[index]['mappings'] = _mappings

                if 'settings' in indices[index]:
                    indices[index]['settings'].update(_settings)
                else:
                    indices[index]['settings'] = _settings

                indices[index]['mapping_names'].append(cls.get_mapping_type_name())

            if index not in self.__class__.aliases:
                self.aliases.append(index)

        return indices

    def _init_models(self):
        """

        :return:
        """
        indices = self.make_indices()

        for i in indices:
            index_exists = s().get_es().indices.exists(index=i, ignore_unavailable=True)
            old_name = s().get_es().indices.get_alias(i).popitem()[0] if index_exists else ''
            self.indices.append(ESIndex(old_name=old_name,
                                        alias_name=i,
                                        mappings=indices[i]['mappings'],
                                        settings=indices[i]['settings'],
                                        mapping_names=indices[i]['mapping_names'],
                                        dry=self.dry))

        self.warmers = self.make_warmers()

    def make_warmers(self):
        """
        Collects all warmers for each index.

        :return:
        """
        for cls in all_subclasses(BaseMapper):
            index = cls().get_index()

            if self.warmers[index]['warmers'] and cls.get_warmers().get('warmers'):
                self.warmers[index]['warmers'].update(cls.get_warmers().get('warmers'))
            else:
                current = cls.get_warmers()
                self.warmers[index].update(current)

        return self.warmers

    @gen.coroutine
    def insert_system_user(self):
        """
        Inserts system user profile in user-index if not exists.

        :return:
        """
        if self.dry:
            return

        User = s().M.model('User')
        u = yield User.get_system()
        if not u:
            write("Inserting system user..")
            request = HTTPRequest(url="%s/%s/%s" %
                            (
                                s().elastic.host,
                                User.mapper.get_index(),
                                User.mapper.get_mapping_type_name()
                            ),
                            body=json_encode(system_user),
                            method="POST")
            AsyncHTTPClient().fetch(request, None)
            write(green("OK.\n"))
            raise gen.Return(request)

    @gen.coroutine
    def insert_proxy_user(self):
        """
        Inserts proxy user profile in user-index if not exists.

        :return:
        """
        if self.dry:
            return

        User = s().M.model('User')
        u = yield User.get_one(user_type=USER_TYPE.proxy)
        user = proxy_user
        if not u:
            write("Inserting proxy user..")
            request = HTTPRequest(url="%s/%s/%s" %
                                  (
                                      s().elastic.host,
                                      User.mapper.get_index(),
                                      User.mapper.get_mapping_type_name()
                                  ),
                                  body=json_encode(user),
                                  method="POST")
            AsyncHTTPClient().fetch(request, None)
            write(green("OK.\n"))
            raise gen.Return(request)

    def insert_default_template(self):
        """
        Inserts default template.

        :return:
        """
        return s().get_es().indices.put_template(
            name='default',
            body=default_template
        )

    def setup(self, verbose=False):
        """
        @TODO

        :return:
        """
        from api_engine import s
        if not verbose:
            sys.stdout = open(devnull, 'w')

        if self.dry:
            write(yellow("DRY RUN..\n"))

        cluster_status = s().get_es().cluster.health()['status']
        if cluster_status not in ['yellow', 'green']:
            write(red("Cannot run on %s status cluster.\n" % cluster_status))
            sys.exit(-1)
            # raise Exception("Cannot run on %s status cluster." % cluster_status)

        # @TODO:
        if self.insert_default_template() != ES_SUCCESS_RESPONSE:
            raise Exception("default template not loaded!")

        # self.__class__.aliases = self.make_aliases()
        indices_names = [i.alias_name for i in self.indices]
        if s().environment not in ['TESTING', 'PRODUCTION']:
            for i in s().get_es().cat.indices(h='index').split('\n'):
                if i:
                    idx = i.strip().split('-v_')[0]
                    if idx and idx not in indices_names:
                        write('Deleted index %s.\n' % magenta(idx))
                        s().get_es().indices.delete(idx)

        for index in self.indices:
            # if s().environment == 'TESTING' and index.alias_name in ['backgroundsettings-testing-index',
            #                                                          'background-testing-index',
            #                                                          'subscription-testing-index']:
            #     continue
            alias_exists = s().get_es().indices.exists_alias(index=index.alias_name, name=index.alias_name)
            mappings = s().get_es().indices.get_mapping(index=index.alias_name).popitem()[1] if alias_exists else []

            # Indices can have multiple mappings, so we should mark to write instead of write
            update_needed = True if not alias_exists else False

            #SETTINGS
            # #TODO: check other settings too.
            if index.old_name:
                settings = s().get_es().indices.get_settings(index.alias_name)
                for _s in ['number_of_replicas', 'number_of_shards']:
                    if settings[index.old_name]['settings']['index'][_s] != default_template['settings'][_s]:
                        update_needed = True

            diff = None

            # MAPPINGS
            for m in index.mapping_names:
                if update_needed:
                    break
                # Merging mapping from mapper class with default template fields to compare with existing mapping
                m_comp = index._mappings[m]['properties'].copy()
                m_comp.update(default_template['mappings']['_default_']['properties'])

                if m in mappings['mappings']:
                    if m_comp != mappings['mappings'][m]['properties']:
                        diff = mappings['mappings'][m]['properties'].copy()
                        for k in m_comp:
                            if k in diff:
                                del diff[k]
                        inv_diff = set(m_comp) - set(mappings['mappings'][m]['properties'].copy())
                        if len(inv_diff):
                            for k in inv_diff:
                                index.add_field(k, m)
                            update_needed = True
                        if len(diff):
                            for f in diff:
                                index.delete_field(f, m)
                            update_needed = True
                        if len(m_comp) and len(diff) and len(m_comp) != len(diff):
                            update_needed = True

            if update_needed:
                index.setup()

        # WARMERS
        # Setting up warmers @TODO: move to ESIndex method
        for index in self.warmers:
            if len(self.warmers[index]['warmers']):
                warmer_name = list(self.warmers[index]['warmers'].keys())[0]
                if not s().get_es().indices.get_warmer(name=warmer_name):
                    write("Creating warmer %s [%s].." % (cyan(warmer_name), blue(index)))
                    try:
                        data = self.warmers[index]['warmers'][warmer_name]
                        if not self.dry:
                            s().get_es().indices.put_warmer(name=warmer_name, body=data, index=index)
                        # r = requests.put("%s/%s/_warmer/%s" % (s().elastic.host, index, warmer_name ),
                        #                  data=json_encode(data))
                    except Exception as e:
                        raise Exception("FAIL (%s).\n" % e)
                    write(green("OK.\n"))

        indices = '_all'
        write("Optimizing indices [%s].." % (indices))
        try:
            s().get_es().indices.optimize(indices)
        except Exception as e:
            write("FAIL [%s]\n" % e)
        write(green("OK.\n"))
        IOLoop.current().run_sync(self.insert_proxy_user)
        IOLoop.current().run_sync(self.insert_system_user)
        if s().environment not in ['TESTING', 'DEVELOPMENT']:
            ESIndex.video_scope_bulk()
        write(yellow("COMPLETED.\n"))


def convert(data):
    """
    Converts to string.

    :param data:
    :return:
    """
    import collections
    if isinstance(data, str):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(list(map(convert, iter(list(data.items())))))
    elif isinstance(data, collections.Iterable):
        return type(data)(list(map(convert, data)))
    else:
        return data
