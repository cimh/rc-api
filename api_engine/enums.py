# -*- coding: utf-8 -*-


def enum(*sequential, **named):
    """

    :param sequential:
    :param named:
    :return:
    """
    enums = dict(list(zip(sequential, list(range(len(sequential))))), **named)
    return type('Enum', (), enums)


"""Follow status"""
FOLLOW_STATUS = enum(
    '',
    'follow',
    'not_follow',
    'request',
    'disfollow'
)

"""Video status"""
VIDEO_STATUS = enum(
    '',
    'paused',
    'active',
    'finished',
    'lost_finished'
)

"""Video delete status"""
VIDEO_DELETE_STATUS = enum(
    '',
    'not_deleted',
    'deleted',
    'blocked',
    'user_deleted'
)

"""Video permissions"""
VIDEO_PERMISSIONS = enum(
    'all',
    'followers',
    'list',
    'followers_and_list'
)

"""Observe request status"""
OBSERVE_REQUEST_STATUS = enum(
    '',
    'request',
    'accept',
    'reject',
    'ban'
)

"""User type"""
USER_TYPE = enum(
    '',
    'user',
    'bot',
    'system',
    'proxy',
    'moderator',
    'support',
    'hbq_operator',
    'wedcam_operator',
    'wedcam_trend',
    'invite'
)

"""User status"""
USER_STATUS = enum(
    '',
    'idle',
    'report',
    'observe',
    'work'
)

"""User delete status"""
USER_DELETE_STATUS = enum(
    '',
    'not_deleted',
    'deleted',  # deleted by self
    'blocked',  # blocked by moderator
)

"""Proxy status"""
PROXY_STATUS = enum(
    '',
    'offline',
    'online'
)

"""GPS types"""
GPS_TYPES = enum(
    'network',
    'gps'
)

"""Quest item types"""
ITEM_TYPE = enum(
    '',
    'ammo'
)

"""Quest status"""
QUEST_STATUSES = enum(
    'not_started',
    'active',
    'finished',
    'paused'
)

"""Observe status"""
OBSERVE_STATUSES = enum(
    '',
    'request',
    'accept',
    'reject',
)

"""Wedcam Invite status"""
INVITE_STATUSES = enum(
    'sent',
    'ack',
    'decline'
)

"""Wedcam Wedding status"""
WEDDING_STATUSES = enum(
    'sent',
    'ack',
    'divorce'
)

"""Wedcam Wedding subscribe types"""
WEDDING_SUBSCRIBE_TYPES = enum(
    'time',
    'distance',
    'age'
)

"""Video name min length"""
VIDEO_SUBJECT_MIN_LENGTH = 3

"""CityDOOM"""
BOT = "bot"
HUNTER = "hunter"
RUNAWAY = "runaway"

"""HBQuest Item ADD distance threshold (meters)"""
ITEM_ADD_DISTANCE_THRESHOLD = 1

"""HBQuest Item PICK distance threshold (meters)"""
ITEM_PICK_DISTANCE_THRESHOLD = 100000

OFFICE_LOCATION = {'lat': 59.925392, 'lon': 30.343531}
SAN_FRANCISCO_LOCATION = {'lat': 37.775310, 'lon': -122.419684}

"""Platforms"""
ANDROID = "ANDROID"
IOS = "IOS"
UNKNOWN = "<unknown>"

"""App names"""
REELCAM = "reelcam"
CITYDOOM = "citydoom"
WEDCAM = "wedcam"
APP_NAMES = (REELCAM, CITYDOOM, WEDCAM)

"""Events"""
TRY2CONNECT = "user:2_gadget_connect"
