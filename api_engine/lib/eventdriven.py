# -*- coding: utf-8 -*-


class EventDriven(object):
    """
    Subclass this for providing event-driven ability.
    """

    def emit_event(self, event):
        """
        Emits ``event`` by current instance(emitter)

        :param event:
        :return:
        """
        raise NotImplementedError

    def notify(self, emitter, event):
        """
        Notifies instance that ``emitter`` emits ``event``

        :param emitter:
        :param event:
        :return:
        """
        raise NotImplementedError

    @classmethod
    def event_emitted(cls, emitter, event):
        """
        Use this method for notifies class that ``emitter`` emits ``event``
        (model class for example).

        :param emitter:
        :param event:
        :return:
        """
        raise NotImplementedError

    def get_event_type(self):
        """
        Returns type of all instance events.

        :return:
        """
        raise NotImplementedError
