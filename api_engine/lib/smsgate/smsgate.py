# -*- coding: utf-8 -*-
from urllib.parse import urlencode
from tornado import gen
from tornado.escape import json_decode
from tornado.httputil import url_concat
from tornado.httpclient import AsyncHTTPClient, HTTPError


class SMSGate(object):
    """
    SMSGate base class.
    """

    # SMS gateway base URL.
    BASE_ENDPOINT = ""
    # SMS gateway message URL.
    MESSAGE_ENDPOINT = ""

    def __init__(self):
        """

        :return:
        """
        self.client = AsyncHTTPClient()

    @gen.coroutine
    def request(self, url, method="GET", headers=None, params=None, **kwargs):
        """
        Perform asynchronous HTTP request.

        :param: url: str URL to fetch.
        :param: method: str HTTP method.
        :param: headers: `~tornado.httputil.HTTPHeaders` or `dict`.
        :param: params: dict HTTP request params.
        :param: kwargs: other `~tornado.httpclient.HTTPRequest` args.
        :return: `~tornado.httpclient.HTTPResponse` object.


        :param url:
        :param method:
        :param headers:
        :param params:
        :param kwargs:
        :return:
        """
        body = None
        if params is not None:
            if method == "GET":
                url = url_concat(url, params)
            else:
                body = urlencode(params)
        elif 'body' in kwargs:
            body = kwargs['body']

        try:
            response = yield self.client.fetch(url, method=method, headers=headers, body=body, **kwargs)
        except HTTPError as e:
            # log.http_error()
            response = e.response
        # else:
            # log.ok()
        raise gen.Return(response)

    @staticmethod
    def _parse_response(response):
        """
        Parse response json.

        :param: response: `~tornado.httpclient.HTTPRequest` object.
        :return:
        """
        try:
            r = json_decode(response.body)
        except (TypeError, ValueError):
            r = None
        return r

    def send(self, receivers, sender, text):
        """
        Send SMS.

        :param: receiver: str message receiver.
        :param: sender: str message sender.
        :param: text: str message text.
        :return: str message ID.
        """
        raise NotImplementedError()
