# -*- coding: utf-8 -*-
from urllib.parse import urljoin

from tornado import gen
from tornado.escape import utf8

from .smsgate import SMSGate


class Streamtelecom(SMSGate):
    """
    SMSGate implementation for Stream-Telecom.
    """

    BASE_ENDPOINT = "http://gateway.api.sc/rest/"
    MESSAGE_ENDPOINT = urljoin(BASE_ENDPOINT, "Send/SendSms/")
    SESSION_ENDPOINT = urljoin(BASE_ENDPOINT, "Session/session.php")

    _sessionid = ""

    def __init__(self, login, password):
        """

        :param login:
        :param password:
        :return:
        """
        SMSGate.__init__(self)
        self.login = login
        self.password = password

    @gen.coroutine
    def set_sessionid(self):
        """

        :return:
        """
        params = dict(
            login=self.login,
            password=self.password
        )
        resp = yield self.request(self.SESSION_ENDPOINT, "POST", params=params)
        if resp.code == 200:
            self.__class__._sessionid = self._parse_response(resp)

    @gen.coroutine
    def send(self, src, dst, data):
        """

        :param src:
        :param dst:
        :param data:
        :return:
        """
        if not self._sessionid:
            yield self.set_sessionid()

        params = dict(
            sessionId=self._sessionid,
            destinationAddress=dst,
            data=utf8(data),
            sourceAddress=src
        )
        resp = yield self.request(self.MESSAGE_ENDPOINT, "POST", params=params)
        if resp.code == 200:
            data = self._parse_response(resp)
            sms_id = data[0] if data and isinstance(data, list) else None
            raise gen.Return(sms_id)
        else:
            return
