# -*- coding: utf-8 -*-
from urllib.parse import urljoin
from tornado import gen
from tornado.escape import json_encode
from api_engine import s
from .smsgate import SMSGate
from tornado.httpclient import AsyncHTTPClient, HTTPRequest


class Plivo(object):
    """
    SMSGate implementation for Plivo
    """

    BASE_ENDPOINT = "https://api.plivo.com/v1/"
    MESSAGE_ENDPOINT = "Account/%s/Message/"

    def __init__(self, auth_id, auth_token):
        """

        :param login:
        :param password:
        :return:
        """
        self.auth_id = auth_id
        self.auth_token = auth_token
        self.MESSAGE_ENDPOINT = urljoin(self.BASE_ENDPOINT, self.MESSAGE_ENDPOINT % self.auth_id)

    def send(self, src, dst, data):
        """
        Send SMS.

        :param src:
        :param dst:
        :param data:
        :return:
        """
        if not all([src, dst, data]):
            return None
        params = dict(
            dst=dst,
            text=data
        )
        if src:
            params['src'] = src
        request = HTTPRequest(url=self.MESSAGE_ENDPOINT,
                              auth_username=self.auth_id,
                              auth_password=self.auth_token,
                              headers={'content-type': 'application/json'},
                              body=json_encode(params),
                              method="POST")
        return AsyncHTTPClient().fetch(request)
