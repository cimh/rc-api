# -*- coding: utf-8 -*-
from pydispatch import dispatcher
from utils.kvs import get_key
from api_engine import s


EVENT = 'EVENT'


class PubSub(object):
    """
    PubSub class.
    """

    periodic = None
    deadline = 100  # In milliseconds.

    def __init__(self, client, pubsub_client):
        """

        :param client: ``tornadis.Client`` object.
        :param pubsub_client: ``tornadis.PubSubClient`` object.
        """
        self.pub = client
        self.sub = pubsub_client

    def start(self):
        """

        :return:
        """
        if self.periodic:
            return

        from tornado import gen
        from tornado.ioloop import PeriodicCallback

        @gen.coroutine
        def periodic():
            """

            :return:
            """
            message = yield self.sub.pubsub_pop_message(deadline=self.deadline)
            if message is not None:
                self.read_message(message)

        self.periodic = PeriodicCallback(periodic, self.deadline)
        self.periodic.start()

    def read_message(self, message):
        """

        :param message:
        :return:
        """
        if not isinstance(message, list):
            return
        try:
            _, key, data = message[0], message[1], message[2]
        except IndexError:
            return
        else:
            dispatcher.send(key, message=data)

    def subscribe(self, key, callback):
        """

        :param key: str Subscribe key.
        :param callback: callable object with one 'message' argument.
        """
        def cb(sender, message):
            """

            :param sender:
            :param message:
            :return:
            """
            del sender
            callback(message)
        dispatcher.connect(cb, signal=key, weak=False)
        self.sub.pubsub_subscribe(key)
        if not self.periodic:
            self.start()

    def publish(self, key, message):
        """

        :param key: str Subscribe key.
        :param message: str Message.
        """
        self.pub.async_call("PUBLISH", key, message)

    def unsubscribe(self, key):
        """

        :param key: str Subscribe key.
        """
        self.sub.pubsub_unsubscribe(key)

    @staticmethod
    def get_key(prefix, suffix=''):
        """

        :param prefix:
        :param suffix:
        :return:
        """
        return get_key('PUBSUB', prefix, suffix, s().get_scope())
