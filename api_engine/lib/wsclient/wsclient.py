# -*- coding: utf-8 -*-


class WsClient(object):
    """
    A websocket client class.
    Subclass this for providing websocket client functionality.
    """

    def connection_opened(self, connection):
        """

        :param connection: (WSConnection) a websocket connection instance
        :return:
        """
        pass

    def connection_closed(self, connection):
        """

        :param connection: (WSConnection) a websocket connection instance
        :return:
        """
        pass

    def get_sessions(self):
        """
        Returns all active(has at least one opened connection) sessions

        :return: list of WsSession objects
        """
        pass

    def get_connections(self):
        """
        Returns all opened connections

        :return: list of WSConnection objects
        """
        pass

    def send(self, data):
        """
        Send data to all opened client connections

        :param data: a JSON-serializable object
        :return:
        """
        pass
