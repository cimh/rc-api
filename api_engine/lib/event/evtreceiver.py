# -*- coding: utf-8 -*- 
from tornado.escape import json_decode, json_encode
from tornado.ioloop import IOLoop
from api_engine import s
from api_engine.lib.pubsub import EVENT


class EvtReceiver(object):
    """
    Subclass to react to receive events on Redis PUBSUB.
    """

    @classmethod
    def on_redis_event(cls, msg):
        """

        :param msg:
        :return:
        """
        try:
            msg = json_decode(msg)
        except (TypeError, ValueError):
            return
        event, session_ids, connection_ids = msg.get('event'), msg.get('session_ids'), msg.get('connection_ids')
        if event and (session_ids or connection_ids):
            cls.dispatch_event(event, session_ids, connection_ids)

    @classmethod
    def dispatch_event(cls, event, session_ids=None, connection_ids=None):
        """

        :param event:
        :param session_ids:
        :param connection_ids:
        :return:
        """
        try:
            event = json_encode(event)
        except (TypeError, ValueError):
            return

        connections = {}
        if session_ids:
            list([connections.update(cls.sessions.get(sess_id, {})) for sess_id in session_ids])

        if connection_ids:
            connection_ids = [_id for _id in connection_ids if _id not in connections]
            connections.update({_id: cls.connections.get(_id) for _id in connection_ids if _id in cls.connections})

        list([IOLoop.current().add_callback(c.write_message, event) if isinstance(c, cls) else None for c in list(connections.values())])

    @classmethod
    def subscribe(cls):
        """
        Subscribes to redis event channel.

        :return:
        """
        pubsub = s().pubsub
        pubsub.subscribe(pubsub.get_key(EVENT), cls.on_redis_event)
