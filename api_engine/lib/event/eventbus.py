# -*- coding: utf-8 -*- 
from pydispatch import dispatcher
from reelcam.utils.gcm import GCM
from api_engine import s
from collections import deque
import weakref


def handle(evt):
    """

    :return:
    """
    s().evt_bus.q_events.append(evt)


class EventBus(object):
    """
    Event Bus class. Accumulates events to send them later in periodic callback.
    Can send Push messages via GCM or emit the event directly to WebSocketConnection via WS message
    or both.
    """

    gcm = None
    q_events = deque()
    q_push = deque()
    connections = weakref.WeakValueDictionary()

    def __init__(self):
        """

        :return:
        """
        dispatcher.connect(handle)
        self.gcm = GCM(s().gcm_keys.api_key)

    def add_connection(self, connection):
        """

        :param connection:
        :return:
        """
        self.connections.update({connection.id: connection})

    # def run(self):
    #     """
    #
    #     :return:
    #     """
    #     for e in list(self.q_events):
    #         if e.push:
    #             self.gcm.send_push(e['destination'], e['event'], e['event']['method'], e['sender'])
    #         if e.ws:
    #             dispatcher.send(e.event.method, e.sender, e.event)
