# -*- coding: utf-8 -*-
import logging
from json import dumps
from pydispatch import dispatcher
from tornado import gen
from tornado.escape import json_encode
from api_engine import s
from api_engine.enums import UNKNOWN, TRY2CONNECT, CITYDOOM, REELCAM, WEDCAM
from api_engine.lib.pubsub import EVENT

log = logging.getLogger(__name__)


class EVT(object):
    """
    Event Helper class.
    """

    connection_ids = None
    push = False
    websocket = True
    e_notification = None

    def __init__(self, name=None, sender=None, notify_sender=False, connection_ids=None, users=None, data=None,
                 notification=None, push=False, body_loc_key=None, body_loc_args=None, user_img=None, websocket=True,
                 app_name=None):
        """

        :param name:
        :param sender:
        :param connection_ids:
        :param users:
        :param data:
        :param push:
        :param body_loc_key:
        :param body_loc_args:
        :param user_img:
        :return:
        """
        connection_ids = connection_ids or []
        users = users or []
        self.sender = sender if sender else UNKNOWN
        self.websocket = websocket

        self.connection_ids = set(connection_ids)
        self.users = set(users)
        self.users.discard(None)
        if notify_sender:
            self.users.add(sender)
        event_user_id = self.sender.id if self.sender not in ['SYS', UNKNOWN] else self.sender
        data['event_user_id'] = event_user_id
        if self.sender is not 'SYS':
            data = dict(data=data)
        data['method'] = name if name else UNKNOWN
        data['event_user_id'] = event_user_id
        self.data = data if data else dict(data=dict())
        self.app_name = app_name

        self.push = push
        if self.push:
            self.body_loc_key = body_loc_key
            self.body_loc_args = body_loc_args
            self.e_user_img = user_img
        if notification:
            self.e_notification = notification
        self.send()

    def send(self):
        """

        :param self:
        :return:
        """
        if self.websocket:
            self.send_websocket()

        ctx = getattr(self.sender, 'ctx', None)
        if self.push and ctx:
            if ctx.app_name in [CITYDOOM, REELCAM, WEDCAM]:
                api_key = getattr(s().gcm_keys, ctx.app_name)
            else:
                api_key = s().gcm_keys.api_key

            gcm = s().gcm(api_key)
            gcm.send_push(e_name=self.data['method'], e_data=self.data, e_notification=self.e_notification,
                          e_user_img=self.e_user_img, users=list(set(self.users)),
                          body_loc_key=self.body_loc_key, body_loc_args=self.body_loc_args, app_name=self.app_name)

        if s().environment == 'TESTING':
            dispatcher.send(self.data['method'], self.sender, self)

        if log.isEnabledFor(logging.INFO) and self.data['method'] != 'user:new_session':
            try:
                self.connections_count = len(self.connection_ids)
                del self.sender
                del self.connection_ids
                del self.users
                # log.info('EVT sent: %s', dumps(self.__dict__, sort_keys=True, indent=3))
            except:
                pass

    @gen.coroutine
    def send_websocket(self):
        """

        :return:
        """
        users = list(self.users)
        sessions = set()
        connections = set()

        if len(users) == 1 and self.data['method'] == TRY2CONNECT:
            sessions, connections = yield users[0].try_2_connect()
        else:
            for user in users:
                user_sessions = yield user.get_sessions()
                if user_sessions:
                    sessions.update(user_sessions)

            if self.connection_ids:
                connections.update(self.connection_ids)

        message = {'event': self.data.copy(),
                   'session_ids': [k.decode("utf-8") if not isinstance(k, str) else k for k in sessions],
                   'connection_ids': [c.decode("utf-8") if not isinstance(c, str) else c for c in connections]}
        pubsub = s().pubsub
        pubsub.publish(pubsub.get_key(EVENT), json_encode(message))
