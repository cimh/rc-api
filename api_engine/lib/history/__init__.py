# -*- coding: utf-8 -*- 
from api_engine import s
from tornado.ioloop import IOLoop
# from tornado import gen


class _H(object):
    """
    Private Shortcut for History, to use H model in settings.
    """

    def __init__(self, method=None, e_model=None, e_user_id=None, is_visible=True, user_id=None):
        """
        Use it to save H record from anywhere.

        :param: method: `method` field in H mapping
        :param: data: `data` field, needs Model instance
        :param: user_id: `e_user_id` in H mapping
        :param: is_visible: `is_visible` field in H mapping
        :return:
        """
        H = s().M.model('H')
        if "get_data" in dir(e_model):
            data = e_model.get_data()
        else:
            data = dict(e_model)
        h = H({
            'method': method,
            'data': data,
            'e_user_id': e_user_id if e_user_id else str(),
            'e_object_id': data.get('id'),
            'is_visible': is_visible,
            'scope': data.get('scope') or s().elastic.default_scope,
            'seen': False,
            'user_id': user_id,
            'create_ts': H.now_YmdHMS(),
        })

        # @gen.coroutine
        # def callback(h):
        #     existing = yield H.get(e_object_id=h.e_object_id)
        #     yield [e.delete(e.id) for e in existing]
        #     yield h.save()

        IOLoop.current().spawn_callback(h.save)
