# -*- coding: utf-8 -*-
from tornado import gen

from api_engine import s


# Sender names.
CITYDOOM = "CityDOOM"
REELCAM = "ReelCam"
WEDCAM = "WedCam"


def send(src, dst, data):
    """

    :param src:
    :param dst:
    :param data:
    :return:
    """
    sms = SMS()
    sms.send(src, dst, data)
    return sms


class SMS(object):
    """
    SMS class.

    """

    gw = None
    rus_gw = None

    @gen.coroutine
    def send(self, src, dst, data):
        """

        :param src:
        :param dst:
        :param data:
        :return:
        """
        if s().environment in ["DEVELOPMENT", "TESTING"]:
            return

        if dst.startswith('7'):
            if not self.rus_gw:
                self.set_rus_gw()
            self.rus_gw.send(src, dst, data)

        else:
            dst = '+'+dst
            if dst.startswith('+1'):
                src = ''
            if not self.gw:
                self.set_gw()
            self.gw.send(src, dst, data)

    @classmethod
    def set_gw(cls):
        """

        :return:
        """
        from api_engine.lib.smsgate.plivo import Plivo
        cls.gw = Plivo(s().sms_gate.plivo.auth_id, s().sms_gate.plivo.auth_token)

    @classmethod
    def set_rus_gw(cls):
        """

        :return:
        """
        from api_engine.lib.smsgate.streamtelecom import Streamtelecom
        cls.rus_gw = Streamtelecom(s().sms_gate.stream_telecom.login, s().sms_gate.stream_telecom.password)
