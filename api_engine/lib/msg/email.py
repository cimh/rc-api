# -*- coding: utf-8 -*-
import logging

from api_engine import s


log = logging.getLogger(__name__)


# Sender names.
CITYDOOM = "CityDOOM"
REELCAM = "ReelCam"
WEDCAM = "WedCam"


def send(src, dst, data, name=None, subject=None, html=True, attachments=None):
    """

    :param src:
    :param dst:
    :param data:
    :param name:
    :param subject:
    :param html:
    :param attachments:
    :return:
    """
    if s().environment in ["DEVELOPMENT", "TESTING"]:
            return

    log.debug('Email MESSAGE was created: sender_name="%s", subject="%s"', name, subject)

    email = {'from_email': src, 'to': [dict(email=dst)]}
    if name:
        email['from_name'] = name
    if subject:
        email['subject'] = subject

    if html:
        email['html'] = data
        email['auto_html'] = True
    else:
        email['text'] = data

    if attachments:
        email['attachments'] = []
        for a in attachments:
            email['attachments'].append({'content': a.body, 'name': a.filename, 'type': a.content_type})

    if log.isEnabledFor(logging.INFO):
        from json import dumps
        log.info('Email MESSAGE was sent: sender="%s", sender_name="%s", receivers=%s, subject="%s", body="%s"',
                 dst, name, dumps(src), subject, data[:200])

    s().email_sender.messages.send(message=email, async=True)
