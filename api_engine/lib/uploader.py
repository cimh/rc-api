# -*- coding: utf-8 -*-


class Uploader(object):
    """
    An Uploader interface.
    """

    def upload(self, key, fp):
        """
        Upload data by ``key``.

        :param key:
        :param fp:
        :return:
        """
        raise NotImplementedError

    def download(self, key, fp):
        """

        :param key:
        :param fp:
        :return:
        """
        raise NotImplementedError

    def get(self, key):
        """

        :param key:
        :return:
        """
        raise NotImplementedError

    def delete(self, key):
        """
        Delete data by ``key``.

        :param key:
        :return:
        """
        raise NotImplementedError

    def initiate_multipart(self, key):
        """

        :param key:
        :return:  MultiPartUpload instance
        """
        raise NotImplementedError

    def resume_multipart(self, key, upload_id):
        """

        :param key:
        :param upload_id:
        :return:
        """
        raise NotImplementedError


class MultiPartUpload(object):
    """
    MultiPartUpload class.
    """

    def upload_part(self, fp, part_num):
        """
        Upload (append) chunk to ``key``.
        Require Uploader.complete_multipart call when last chunk uploaded.

        :param fp:
        :param part_num:
        :return:
        """
        raise NotImplementedError

    def complete_upload(self):
        """
        Call this when last chunk has been uploaded.

        :return:
        """
        raise NotImplementedError

    def cancel_upload(self):
        """

        :return:
        """
        raise NotImplementedError
