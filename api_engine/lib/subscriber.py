# -*- coding: utf-8 -*-


class Subscriber(object):
    """
    Subclass this to provide subscribe ability.
    """

    def subscribe(self, instance):
        """
        Subscribes to the instance.

        :param instance:
        :return:
        """
        raise NotImplementedError

    def unsubscribe(self, instance):
        """
        Unsubscribe from the instance.

        :param instance:
        :return:
        """
        raise NotImplementedError

    def is_subscribed(self, instance):
        """

        :param instance:
        :return:
        """
        raise NotImplementedError

    def clear_subscribes(self):
        """

        :return:
        """
        raise NotImplementedError
