# -*- coding: utf-8 -*-
from tornado import gen
from api_engine import s
from api_engine.auth.sessionmanager import sm, USER
from api_engine.enums import ANDROID, IOS, UNKNOWN, REELCAM, CITYDOOM, WEDCAM


class APIContext(object):
    """
    API context class.
    """

    def __init__(self, request):
        """

        :param request: HTTPServerRequest object.
        """
        self.request = request
        self.session = None
        self.connection = request.connection_id
        self.ws_conn = None
        self.request_user = None
        self.request_user_data = None
        self.platform = None
        self.app_name = None

    async def setup(self, session=None, platform=None, app_name=None):
        """
        Context setup method.

        :param session: str Session ID.
        :param platform: str Platform ID.
        :param app_name: str Application ID.
        :return: str Session ID.
        """
        if platform:
            platform = platform.upper()
            if platform in [ANDROID, IOS]:
                self.platform = platform
            else:
                self.platform = UNKNOWN

        if app_name:
            app_name = app_name.lower()
            if app_name in [REELCAM, CITYDOOM, WEDCAM]:
                self.app_name = app_name
            else:
                self.app_name = UNKNOWN

        if not session:
            self.session = sm.make_session()
        else:
            self.session = str(session)
            session_data = await sm.get_session(self.session)
            if not session_data:
                r = await self.setup(platform=platform, app_name=app_name)
                return r
            elif session_data.get(USER):
                User = s().M.model('User', ctx=self)
                self.request_user = await User.get_one(_id=session_data[USER])
                self.request_user_data = await self.request_user.get_data()

        from reelcam.models.wsconnection import WSConnection
        self.ws_conn = WSConnection({'id': self.connection_id, 'session_id': self.session})

        return self.session

    def remove_session(self):
        """

        :return:
        """
        if self.session:
            sm.delete_session(self.session)
            self.session = None
            self.request_user = None

    def set_user(self, request_user):
        """

        :param request_user: User object.
        """
        if self.session:
            self.request_user = request_user
            sm.add_user(self.session, request_user.id)

    @property
    def connection_id(self):
        """

        :return:
        """
        return self.request.connection_id

    @property
    def client_ip(self):
        """

        :return:
        """
        return self.request.remote_ip

    @property
    def wsconnection(self):
        """

        :return:
        """
        return self.ws_conn

    def get_request_info(self):
        """

        :return:
        """
        info = dict(session_id=self.session,
                    connection_id=self.connection_id,
                    client_ip=self.client_ip)
        if self.request_user:
            info['user_id'] = self.request_user.id
        return info
