# -*- coding: utf-8 -*-
from cerberus import Validator, errors
from collections import Mapping, Sequence


class CustomValidator(Validator):
    """
    Custom Validator class.
    """

    def _validate_empty_or_minlength(self, empty_or_minlength, field, value):
        """

        :param empty_or_minlength:
        :param field:
        :param value:
        :return:
        """
        if len(value) != 0 and len(value) < empty_or_minlength:
            self._error(field, "Must be empty or has 3 chars min")

    def _validate_maxlength(self, max_length, field, value):
        """
        Adds support of `maxlength` rule for Mapping type objects.

        :param max_length:
        :param field:
        :param value:
        :return:
        """
        if isinstance(value, (Mapping, Sequence)):
            if len(value) > max_length:
                self._error(field, errors.ERROR_MAX_LENGTH.format(max_length))

    def _validate_minlength(self, min_length, field, value):
        """
        Adds support of `minlength` rule for Mapping type objects.

        :param min_length:
        :param field:
        :param value:
        :return:
        """
        if isinstance(value, (Mapping, Sequence)):
            if len(value) < min_length:
                self._error(field, errors.ERROR_MIN_LENGTH.format(min_length))
