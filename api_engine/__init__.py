# -*- coding: utf-8 -*-
from .settings import Settings



def get_settings():
    """
    Returns a global settings object.

    :return:
    """
    _s = Settings()
    return _s

s = get_settings
