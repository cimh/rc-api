# -*- coding: utf-8 -*-
from tornado.escape import json_encode
from api_engine import s
from utils.datetimemixin import dtm
from pprint import pformat


class APILog(object):
    """
    APILog class.
    """

    MAX_LOG_STRING_SIZE = 8192
    EXC_TEXT_MAX_SIZE = 7000

    def __init__(self, logger):
        """

        :param logger:
        :return:
        """
        self.log = logger

    def log_call(self, request, response, exc_text=None, memprof=None):
        """

        :param request:
        :param response:
        :param exc_text:
        :return:
        """
        if 'photo' or 'files' in request.message.data:
            request.message.data.pop('photo', None)
            request.message.data.pop('files', None)
        # response = deepcopy(response)
        log_dict = {
            'request': {'info': request.ctx.get_request_info(),
                        'body': request.message},
            'response': {},
            'exception': exc_text,
            'ts': dtm.now_YmdHMSs(),
        }
        log_dict['response'].update(response if not isinstance(response, str)
                                    else {'plain_text': response})

        try:
            code = response['status']['code']
        except (KeyError, TypeError):
            code = 0

        if self.log_flag(s().logging.level, code):
            try:
                log_json = json_encode(log_dict)
                # log message size check for UDP log handlers
                if len(log_json) > self.MAX_LOG_STRING_SIZE:
                    log_dict = self.format_log(log_dict)
                extra = dict(log=log_dict)
                self.log.debug(pformat(log_dict), extra=extra)
                # self.log.info(memprof['heap'])
            except TypeError:
                pass

        return log_dict

    @classmethod
    def log_flag(cls, level, code):
        """

        :param level: str log level literal.
        :param code: int api response code.
        :return:
        """
        log_flag = False
        if level != 'ERR' or code and code >= 500:
            log_flag = True
        return log_flag

    @classmethod
    def format_log(cls, log_dict):
        """
        Put size of JSON-serialized data instead of real body for some parts into log_dict.

        :param log_dict:
        :return:
        """
        if log_dict['exception'] and len(log_dict['exception']) > cls.EXC_TEXT_MAX_SIZE:
            log_dict['exception'] = log_dict['exception'][-cls.EXC_TEXT_MAX_SIZE:]

        log_dict['json_length'] = len(json_encode(log_dict))
        log_dict['request']['body']['data'] = {
            'json_length': len(json_encode(log_dict['request']['body'].get('data', '')))
        }
        log_dict['response'].pop('data', None)
        return log_dict
