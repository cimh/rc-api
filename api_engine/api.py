# -*- coding: utf-8 -*-
import logging
import sys
from api_engine import apiresults
from api_engine.schema import APISchema
from api_engine import s
from tornado import gen
from tornado.util import ObjectDict
from .context import APIContext
from .exc import APIError, WrongMessageError, WrongAPIRequestError
from .request import APIRequest
from .log import APILog
# from StringIO import StringIO
# from guppy import hpy
# hp = hpy()

schema = APISchema()


def schema_to_http(schema=None, prefix=None, other=False):
    """

    :param schema:
    :param prefix:
    :param replace:
    :return:
    """
    result = dict()
    for k in schema.schema:
        if prefix and k.startswith(prefix):
            url = '/'+k.replace(':', '/')
            result[url] = k
        elif other:
            result['/'+k] = k
    return result


class API(object):
    """
    API class.
    """

    context_class = APIContext

    request_class = APIRequest

    log = APILog(logging.getLogger('api'))

    @classmethod
    async def call_method(cls, method_name, data, ctx):
        """
        Calls API method without WS connection.

        :param: method_name: APIMethod name
        :param: ctx: APIContext instance
        """
        # Try to get method from request and find handler for this method
        method = schema.get_method(name=method_name)
        if not method:
            return apiresults.bad_request()
        handler = method.get_handler(ctx)
        # Executing request with APIHandler.handle(handler_name, data)
        res = await handler.handle(method.name, data)
        return res

    @classmethod
    async def call(cls, request):
        """
        API entry point.

        :param: request: APIRequest object.
        """
        # hp.setref()
        # sio = StringIO()

        exc_text = None
        # Try to execute request with API handlers
        result = apiresults.bad_request()
        try:
            # Check request object type.
            if not cls.check_request_obj(request):
                raise WrongAPIRequestError()

            # Check client message.
            try:
                message = APIMessage(**request.message)
            except TypeError:
                raise WrongMessageError()
            from tornado.concurrent import Future
            result = Future()
            data = await cls.call_method(message.method, message.data, request.ctx)
            result.set_result(data)
            result = result.result()
        except (WrongAPIRequestError, APIError, Exception):
            exc_text = await cls.capture_exception()
            result.set_result(apiresults.error())

        finally:
            result = await result
            response = cls._make_rep(result, request)
            # sio.write(hp.heap())
            # sio.seek(0)
            # memprof = dict(heap=sio.read())
            # cls.log.log_call(request, response, exc_text, memprof)
            # if request.message.method not in ['health:get']:
            #     cls.log.log_call(request, response, exc_text)
            return response

    @classmethod
    def check_request_obj(cls, request):
        """
        Check API request and context objects types.

        :param: request: ``cls.request_class`` api request object.
        :param: request.ctx: ``cls.context_class`` api context object.
        :return: bool
        """
        return isinstance(request, cls.request_class) and isinstance(request.ctx, cls.context_class)

    @classmethod
    def _make_rep(cls, result, request):
        """

        :param result:
        :param request:
        :return:
        """
        if request.ctx.request.type == cls.request_class.HTTP:
            return result
        elif 'reqid' in request.message:
            result.update(reqid=request.message['reqid'])
        return result

    @classmethod
    async def capture_exception(cls):
        """
        API.call() exception handler

        :return:
        """
        import traceback
        from io import StringIO
        f = StringIO()
        traceback.print_exc(file=f)
        f.seek(0)
        exc_text = f.read()
        sys.stderr.write(exc_text+'\n')
        if s().sentry.on:
            await gen.Task(s().sentry_client.captureException, exc_info=True)
        return exc_text


class APIMessage(ObjectDict):
    """
    API request message class.
    """

    def __init__(self, method, data, **kwargs):
        """

        :param: method: (str) API method name
        :param: data: (dict) message data
        :param: reqid: (int, basestring) request ID
        """
        super(APIMessage, self).__init__()
        self.method = method
        self.data = data
        if 'reqid' in kwargs:
            self.reqid = kwargs.pop('reqid')
        self.update(kwargs)

