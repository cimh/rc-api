# -*- coding: utf-8 -*-
from uuid import uuid4
from tornado import gen

from api_engine import s
from utils import kvs

CONN_CNT = 'CONN_CNT'
CONN_IDS = 'CONN_IDS'
USER = 'USER'


class SessionManagerRedis(object):
    """
    SessionManager class.
    """

    @classmethod
    def get_anon_ttl(cls):
        """

        :return:
        """
        return s().ttl.anon_session

    @classmethod
    def get_ttl(cls):
        """
        
        :return:
        """
        return s().ttl.session

    @classmethod
    def make_session(cls, connection=None):
        """

        :param connection: str Connection ID.
        """
        session = uuid4().hex
        pp = s().kvspp
        pp.stack_call("HSET", cls.get_key(session), CONN_CNT, 0)
        pp.stack_call("EXPIRE", cls.get_key(session), cls.get_anon_ttl())
        if connection:
            connection = str(connection)
            pp.stack_call("SADD", cls.get_key(session, CONN_IDS), connection)
            pp.stack_call("EXPIRE", cls.get_key(session, CONN_IDS), cls.get_anon_ttl())
            pp.stack_call("HINCRBY", cls.get_key(session), CONN_CNT, 1)

        s().kvs.async_call(pp)
        return session

    @classmethod
    async def get_session(cls, session):
        """

        :param session: str Session ID.
        :return:
        """
        session = str(session)
        data = await s().kvs.call("HGETALL", cls.get_key(session))
        session_data = {}
        for i in range(0, len(data)):
            if i % 2:
                k = data[i-1]
                v = data[i]
                session_data[k] = v
        return dict(session_data)

    @classmethod
    def add_connection(cls, session, connection):
        """

        :param session: str Session ID.
        :param connection: str Connection ID.
        """
        pp = s().kvspp
        pp.stack_call("SADD", cls.get_key(session, CONN_IDS), connection)
        pp.stack_call("EXPIRE", cls.get_key(session, CONN_IDS), cls.get_anon_ttl())
        pp.stack_call("HINCRBY", cls.get_key(session), CONN_CNT, 1)
        s().kvs.async_call(pp)

    @classmethod
    def remove_connection(cls, session, connection):
        """

        :param session: str Session ID.
        :param connection: str Connection ID.
        """
        pp = s().kvspp
        pp.stack_call("SREM", cls.get_key(session, CONN_IDS), connection)
        pp.stack_call("EXPIRE", cls.get_key(session, CONN_IDS), cls.get_anon_ttl())
        pp.stack_call("HINCRBY", cls.get_key(session), CONN_CNT, -1)
        s().kvs.async_call(pp)

    @classmethod
    def add_user(cls, session, user_id):
        """

        :param session: str Session ID.
        :param user_id: str User ID.  # TODO: str Client ID.
        :return:
        """
        session = str(session)
        client = str(user_id)
        pp = s().kvspp
        pp.stack_call("HSET", cls.get_key(session), USER, client)  # TODO: USER -> CLIENT.
        pp.stack_call("EXPIRE", cls.get_key(session), cls.get_ttl())
        pp.stack_call("EXPIRE", cls.get_key(session, CONN_IDS), cls.get_ttl())
        s().kvs.async_call(pp)

    @classmethod
    def delete_user(cls, session):
        """

        :param session: str Session ID.
        """
        session = str(session)
        s().kvs.async_call("HDEL", cls.get_key(session), USER)

    @classmethod
    def delete_session(cls, session):
        """

        :param session: str Session ID.
        """
        session = str(session)
        s().kvs.async_call("DEL", cls.get_key(session), cls.get_key(session, CONN_IDS))

    @classmethod
    def expire(cls, session, _anon=False):
        """

        :param session: str Session ID.
        :param _anon: bool.
        """
        session = str(session)
        ttl = cls.get_ttl()
        if _anon:
            ttl = cls.get_anon_ttl()
        s().kvs.async_call("EXPIRE", cls.get_key(session), ttl)

    @staticmethod
    def get_key(session, suffix=''):
        """

        :param session: str Session ID.
        :param suffix:
        """
        session = str(session)
        return kvs.get_key('SESSION', session, suffix, s().get_scope())


sm = SessionManagerRedis
