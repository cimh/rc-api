# -*- coding: utf-8 -*-
import random
import string
from passlib.hash import pbkdf2_sha256


class Auth(object):
    """
    An authentication tools provider
    """

    @classmethod
    def authenticate(cls, user, password):
        """

        :param user:
        :param password:
        :return:
        """
        return cls.verify_password(user, password)

    @classmethod
    def verify_password(cls, user, password):
        """

        :param user:
        :param password:
        :return:
        """
        return pbkdf2_sha256.verify(password, user.password)

    @classmethod
    def make_password_hash(cls, raw_pass):
        """

        :param raw_pass:
        :return:
        """
        _hash = pbkdf2_sha256.encrypt(raw_pass, rounds=20000, salt_size=8)
        return _hash

    @classmethod
    def generate_random_password(cls):
        """

        :return:
        """
        return ''.join(random.sample(string.ascii_letters + string.digits + '!@#$?*', 10))
