# -*- coding: utf-8 -*-
import os
import shlex
from subprocess import Popen, PIPE
from io import StringIO
import xmltodict
from PIL import Image


class MediaHelper(object):
    """
    A MediaHelper class.
    Provides different media data operations (as ffmpeg, yamdi etc..) from high-level functions.

    """

    ASPECT = 16 / 9
    # FLV header
    FLV = 'FLV\x01\x05\x00\x00\x00\t\x00\x00\x00\x00'

    preview_sample_length = 60*5  # In seconds.

    @classmethod
    def set_meta(cls, path, seconds, target):
        """

        :param path:
        :param seconds:
        :param target:
        :return:
        """
        import subprocess
        yamdi = 'yamdi -i %s -o %s -w -x -' % (path, path+'_tmp')
        if target == 'o':
            yamdi += ' -a %d' % (seconds * 10)  # Time in milliseconds between keyframes if there is only audio. TODO:10
            ifsharp = ' -Sf'
            yamdi += ifsharp

        elif target == 'ri':
            ifsharp = ' -f'
            yamdi += ifsharp

        args = shlex.split(yamdi)
        p = subprocess.Popen(args, stdout=subprocess.PIPE)
        out, err = p.communicate()

        meta = {}
        if not err:
            try:
                meta = xmltodict.parse(out, dict_constructor=dict)['fileset']['flv']
            except Exception:
                return meta
            else:
                meta.pop('audiodelay')
                meta.pop('hasCuePoints')
                meta.pop('lastvideoframetimestamp')
                meta['filename'] = meta.pop('@name')
        return meta

    @classmethod
    def make_previews(cls, path, count=3):
        """

        :param path:
        :param count:
        :param duration:
        :return:
        """
        path_dict = dict()
        dirpath = os.path.split(path)[0]

        meta = cls.get_meta(path)
        if not meta:
            return path_dict
        duration = int(float(meta['duration']))
        intervals = []
        if int(duration / cls.preview_sample_length) in [0, 1]:
            intervals.append((0, duration))
        elif int(duration / cls.preview_sample_length) == 2:
            intervals.append((0, cls.preview_sample_length))
            intervals.append((duration-cls.preview_sample_length, duration))
        else:
            intervals.append((0, cls.preview_sample_length))
            left = int(duration / 2) - int(cls.preview_sample_length / 2)
            right = left + cls.preview_sample_length
            intervals.append((left, right))
            intervals.append((duration-cls.preview_sample_length, duration))

        command_pattern = 'ffmpeg -loglevel error -ss %d -t %d -i %s -vf "select=eq(pict_type\,I),%s" ' \
                          '-vsync vfr -qscale:v 2 thumbnails%d-%d-%%05d.jpeg'

        vf_pattern = 'crop=%d:%d:%d:%d'
        w, h = cls.get_width_height(path)
        if [i for i in [w, h] if not i]:
            return
        new_w, new_h = cls.get_wh_for_aspect(w, h, cls.ASPECT)
        x_box, y_box = int(abs(w - new_w) / 2), int(abs(h - new_h) / 2)
        vf_string = vf_pattern % (new_w, new_h, x_box, y_box)

        cwd = os.getcwd()
        os.chdir(dirpath)

        for left, right in intervals:
            command_line = command_pattern % (left, right, path, vf_string, left, right)
            p = Popen(shlex.split(command_line), stdout=PIPE, stderr=PIPE)
            out, err = p.communicate()
            if err:
                continue
            ifsharp = 'ifsharp -r -i %d' % count
            p = Popen(shlex.split(ifsharp), stdout=PIPE, stderr=PIPE)
            p.communicate()

        os.chdir(cwd)
        previews = [p for p in os.listdir(dirpath) if p.endswith('.jpeg')]
        num = 0
        for p in previews[:count]:
            num += 1
            path_dict['screen'+str(num)] = os.path.join(dirpath, p)
        return path_dict

    @classmethod
    def get_meta(cls, path):
        """
        Returns video metadata.

        :param path:
        :return:
        """
        command_line = 'yamdi -i %s -x -' % path
        p = Popen(shlex.split(command_line), stdout=PIPE, stderr=PIPE)
        out, err = p.communicate()
        if out:
            try:
                meta = xmltodict.parse(out, dict_constructor=dict)
            except Exception:
                return None
            else:
                return meta['fileset']['flv']

    @classmethod
    def get_width_height(cls, path):
        """

        :param path:
        :return:
        """
        meta = cls.get_meta(path)
        if not meta:
            return None, None
        return int(meta['width']), int(meta['height'])

    @classmethod
    def crop_preview(cls, file_like, out_format='JPEG'):
        """
        Returns new image file cropped without scaling for aspect 16:9.

        :param file_like:
        :param out_format:
        :return:
        """
        if isinstance(file_like, str):
            file_like = StringIO(file_like)
        file_like.seek(0)
        img = Image.open(file_like)
        w, h = img.size
        new_w, new_h = cls.get_wh_for_aspect(w, h, cls.ASPECT)
        x_box, y_box = int(abs(w - new_w) / 2), int(abs(h - new_h) / 2)
        box = (x_box, y_box, w - x_box, h - y_box)
        img = img.crop(box)
        out = StringIO()
        img.save(out, out_format)
        out.seek(0)
        return out

    @classmethod
    def get_wh_for_aspect(cls, w, h, aspect):
        """

        :param w:
        :param h:
        :param aspect:
        :return:
        """
        new_w, new_h = (w, int(w / aspect)) if float(w) / h <= aspect else (int(h * aspect), h)
        return int(new_w), int(new_h)

    @classmethod
    def get_image_format(cls, fp):
        """
        Returns either format name of an image or None if `fp` has not a valid image type of.

        :param fp:
        :return:
        """
        if isinstance(fp, str):
            fp = StringIO(fp)
        try:
            image = Image.open(fp)
            image.load()
        except IOError:
            return
        else:
            return image.format


class MetaData(object):
    """
    A video metadata registry class.
    """

    def __init__(self, duration, filesize, **kwargs):
        self.duration = float(duration)
        self.filesize = int(filesize)


"""
#!/usr/bin/env bash

ffmpeg -i video.flv -q:v 1 -r 1 -vframes 1 -vf "select=not(mod(n\,88))" -ss 00:00:00 -t 00:03:00 -y screen1.jpg


ffmpeg -i "video.flv" -vcodec copy -f rawvideo -y /dev/null 2>&1 | grep frame | awk '{split($0,a,"fps")}END{print a[1]}' | sed 's/.*= *//'

ffmpeg -i video.flv -q:v 1 -r 0.00033723021582733815 -vf "select=not(mod(n\,2965))"  -y screen%d.jpg

ffmpeg -ss 301.33 -i ri.flv -t 301.33 -vframes 1 -intra -qscale 0 -f image2 -y %09d.jpg
select=eq(pict_type\,PICT_TYPE_I),

ffmpeg  -ss 15.98 -i ri.flv -t 16.98 -vframes 1 -intra -vf "crop=256:144:0:24" -f image2 -y screen3.jpg

"""
