# -*- coding: utf-8 -*-
from api_engine.api import schema_to_http, schema

urls = {
    '/upload.php': 'upload:upload',
    '/size.php': 'upload:size',
    '/duration.php': 'upload:duration',
    '/feedback/submit': 'feedback:submit',
    '/background_upload': 'system:background_upload',
    '/paypal_ipn': 'paypal:paypal_ipn_listener',
    '/_health': 'health:get',
    '/rfi_post_payment': 'rfi:post_payment',
}

urls.update(schema_to_http(schema, 'upload'))
