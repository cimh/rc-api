# -*- coding: utf-8 -*-


class APIError(Exception):
    """A base API exception."""


class MethodNotFoundError(APIError):
    """API method not found exception."""


class WrongMessageError(APIError):
    """API wrong message format error."""


class WrongAPIRequestError(APIError):
    """API wrong request type error."""
