# -*- coding: utf-8 -*-
from tornado.util import ObjectDict


class APIRequest(object):
    """
    API request class.
    """

    HTTP, WS = "HTTP", "WS"

    def __init__(self, ctx, message):
        """

        :param: request.ctx: APIContext object.
        :param: message: dict client message.
        """
        self.ctx = ctx
        message = ObjectDict(message)
        if 'data' not in message:
            message.data = {}
        self.message = message
