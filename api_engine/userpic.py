# -*- coding: utf-8 -*-
from io import StringIO
from uuid import uuid4
from PIL import Image, ImageDraw
from api_engine import s
from reelcam.uploader import get_uploader

SMALL = "small"
MEDIUM = "medium"
BIG = "big"
PROFILE = "profile"


class Userpic(object):
    """
    Userpic class.
    """

    input_formats = ('JPEG', 'PNG',)

    userpic_format = 'PNG'

    size_set = {
        SMALL: (18, 18),
        MEDIUM: (32, 32),
        BIG: (72, 72),
        PROFILE: (200, 200),
    }

    def __init__(self, user_id):
        """

        :param user_id:
        :return:
        """
        self.user_id = user_id

    @classmethod
    def validate_userpic(cls, userpic):
        """

        :param userpic: str Userpic.
        :return: bool.
        """
        if len(userpic) > s().userpic.max_size:
            return False
        try:
            image = Image.open(StringIO(userpic))
        except IOError:
            return False
        if image.format not in cls.input_formats:
            return False
        return True

    def add_userpic(self, userpic):
        """

        :param userpic: str Userpic.
        :return: list of str keys.
        """
        if not self.validate_userpic(userpic):
            return None
        image = Image.open(StringIO(userpic))
        image = self.crop_image(image)
        image = self.round_image(image)

        resize_images = {}
        for name, size in list(self.size_set.items()):
            resize_images[name] = self.resize_image(image, size)

        keys = {}
        uploader = get_uploader()
        for name, img in list(resize_images.items()):
            key = '/'.join(['avatar', self.user_id, uuid4().hex[:16]+'.png'])
            fp = StringIO()
            img.save(fp, self.userpic_format)
            fp.seek(0)
            uploader.upload(key, fp)
            uploader.set_acl(key, 'public-read')
            keys[name] = key

        return keys

    @staticmethod
    def remove_userpic(key):
        """

        :param key: str Key.
        :return:
        """
        uploader = get_uploader()
        uploader.delete(key)

    @staticmethod
    def crop_image(image):
        """

        :param image: Image object.
        :return: Image object.
        """
        w, h = image.size
        delta = abs(w - h)
        if w > h:
            box = ((0 + delta / 2), 0, (w - delta / 2), h)
        else:
            box = (0, (0 + delta / 2), w, (h - delta / 2))
        return image.crop(box)

    @staticmethod
    def round_image(image):
        """

        :param image: Image object.
        :return: Image object.
        """
        round_scale = 4
        w, h = image.size
        w, h = (min(w, h), min(w, h))
        image = image.resize((w, h))
        empty = image.copy()
        empty.putalpha(Image.new('L', image.size, 0))
        mask = Image.new('L', (w*round_scale, h*round_scale), 0)
        draw = ImageDraw.Draw(mask)
        draw.ellipse((1, 1, w*round_scale-1, h*round_scale-1), 255)
        mask = mask.resize((w, h), Image.ANTIALIAS)
        return Image.composite(image, empty, mask)

    @staticmethod
    def resize_image(image, size):
        """

        :param image: Image object.
        :param size: tuple (int width, int height).
        :return: Image object.
        """
        return image.copy().resize(size)
