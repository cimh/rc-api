# -*- coding: utf-8 -*-
import os
from os.path import abspath, dirname, isfile, join, pardir

import mandrill

from .config import Config, ConfigValidationError
from .configgen import ConfigGen
from .validator import Validator
from api_engine.enums import REELCAM, CITYDOOM, WEDCAM


def _project_dir():
    """
    Returns an absolute project path.

    :return:
    """
    return abspath(join(dirname(__file__), pardir, pardir))


class Meta(type):
    """
    Config meta class.
    """

    def __new__(meta, name, bases, class_dict):
        """
        """
        if bases != (object, ):
            raise TypeError('Inheritance is not allowed!')
        cls = type.__new__(meta, name, bases, class_dict)
        return cls


class Section(object):
    """
    Config section class.
    """

    def __init__(self, name, data):
        """

        :param name:
        :param data:
        :return:
        """
        self.name = name

        def is_section(item):
            return isinstance(item[1], dict)
        for k, v in list(data.items()):
            # assert k.isalnum()  # @TODO: add key validation
            k = k.lower()
            if is_section((k, v)):
                v = self.__class__(k, v)
            setattr(self, k, v)

    def __setattr__(self, key, value):
        """

        :param key:
        :param value:
        :return:
        """
        if hasattr(self, key):
            raise TypeError('Can\'t reset section attribute.')
        else:
            self.__dict__[key] = value

    def __repr__(self):
        """

        :return:
        """
        return '%s(name=%s)' % (self.__class__.__name__, self.name)


class Settings(object, metaclass=Meta):
    """
    An application settings provider class.

    Single instance of this must be a global at application.
    Note: At module level it should be instantiated into application entry point only.
          Elsewhere use this like Settings().param, for example:

          ## entry.py
          from settings import Settings
          from lib import func

          def main():
              print Settings().param
              return func()

          if __name__ == '__main__':
              try:
                  settings = Settings()  # GOOD
              except Exception as e:
                  raise e
              else:
                  print main()

          ## lib.py
          from settings import Settings

          # settings = Settings()  # BAD
          # def func():
          #     return settings.param

          def func():
              return Settings().param  # GOOD

    """
    _instance = None

    def __new__(cls, *args, **kwargs):
        """
        A singleton implementation.

        :param more:
        :return:
        """
        if not cls._instance:
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance

    # Default config path
    config_path = join(_project_dir(), 'config.ini')
    # Default configspec path
    configspec_path = join(dirname(abspath(__file__)), 'configspec.ini')
    # Config root section name
    config_root_section = 'HELLOBOT'

    _config = None
    _celery_app = None
    _es = None
    _evt_bus = None
    _gcm_client = None
    _google_api = None
    _kvs = None
    _sentry_client = None
    _template_loader = None
    _pubsub = None

    def __init__(self):
        """
        Checks that a config and configspec files are exists.
        Loads a config object from config file and validate this via configspec.
        Sets up an own config sections attributes via ``_load_sections`` if
        config file was validated successfully, otherwise raise an exception.

        Finally we do some configuration jobs via ``_initialize``.

        :return:
        """
        if self._config:
            # This needs because the __new__ implicitly call
            # the __init__ of instance returned.
            return

        try:
            config = os.environ['CONFIG_FILE_PATH']
        except KeyError:
            config = self.config_path
        if not isfile(config):
            raise ValueError('config file is missing at: %s' % config)

        configspec = self.configspec_path
        if not isfile(configspec):
            raise ValueError('configspec file is missing at %s' % configspec)

        config = Config(config, configspec)
        try: 
            config._validate()
        except ConfigValidationError as e:
            raise e
        else:
            self._config = config
            self._load_sections(self._config)

        self._initialize()

    def _initialize(self):
        """
        Put there your configuration code that's executed after config was loaded successfully.

        :return:
        """
        from tornado.httpclient import AsyncHTTPClient
        import urllib3
        urllib3.disable_warnings()
        AsyncHTTPClient.configure("tornado.curl_httpclient.CurlAsyncHTTPClient")
        self.es_monkeypatch()
        self.setup_logging()

    def _load_sections(self, config):
        """

        :param config:
        :return:
        """
        try:
            config = config[self.config_root_section]
        except (TypeError, KeyError):
            raise ValueError('the root config section %s is missing' % self.config_root_section)

        for k, v in list(config.items()):
            k = k.lower()
            if isinstance(v, dict):
                v = Section(k, v)
            setattr(self, k, v)

    def __setattr__(self, key, value):
        """

        :param key:
        :param value:
        :return:
        """
        if key == "elastic":
            value.default_scope = self.get_scope()

        if self._config \
                and hasattr(self, key) \
                and any([key.lower() in self._config[self.config_root_section],
                         key.upper() in self._config[self.config_root_section],
                         key.lower() == self.config_root_section.lower()]):

            raise TypeError('Can\'t reset settings attribute: %s, was: %s, new: %s' %
                            (key, str(getattr(self, key, ''))[:32], str(value)[:32]))
        else:
            self.__dict__[key] = value

    def get_scope(self, app_name=None):
        """

        :param app_name:
        :return:
        """
        if not app_name:
            app_name = REELCAM
        return "%s_%s" % (app_name.upper(), self.environment.upper())

    @classmethod
    def generate_config_file(cls):
        """
        Generates a config file at ``Settings.config_path`` from configspec at ``Settings.configspec_path``.

        :return:
        """
        cgen = ConfigGen(configspec=cls.configspec_path)
        cgen.generate(outfile=cls.config_path)

    def get_es(self):
        """

        :return:
        """
        if not self._es:
            from elasticsearch import Elasticsearch
            self._es = Elasticsearch(hosts=[self.elastic.host],
                                     port=self.elastic.host.split(':')[1],
                                     timeout=self.elastic.timeout)
        return self._es

    @property
    def cwd(self):
        """
        Returns an absolute project path.

        :return:
        """
        return _project_dir()

    @staticmethod
    def e(event_name, sender=None, notify_sender=False, connection_ids=None, users=None, data=None,
          notification=None, push=False, body_loc_key=None, body_loc_args=None, user_img=None, websocket=True,
          app_name=REELCAM):
        """

        :param event_name:
        :param sender:
        :param notify_sender:
        :param connection_ids:
        :param users:
        :param data:
        :param push:
        :param body_loc_key:
        :param body_loc_args:
        :param user_img:
        :param websocket:
        :param app_name:
        :return:
        """
        from api_engine.lib.event import EVT
        EVT(name=event_name,
            sender=sender,
            notify_sender=notify_sender,
            connection_ids=connection_ids,
            users=users,
            data=data,
            notification=notification,
            push=push,
            body_loc_key=body_loc_key,
            body_loc_args=body_loc_args,
            user_img=user_img,
            websocket=websocket,
            app_name=app_name)

    @staticmethod
    def h(method, data, e_user_id, user_id=None):
        """

        :param method:
        :param data:
        :param e_user_id:
        :param user_id:
        :return:
        """
        from api_engine.lib.history import _H
        _H(method=method, e_model=data, e_user_id=e_user_id, user_id=user_id)

    @property
    def google_api(self):
        """
        Return Google API instance.
        """
        from reelcam.utils.google_api import GoogleAPI
        if not self._google_api:
            self._google_api = GoogleAPI()
        return self._google_api

    @property
    def evt_bus(self):
        """
        Return EventBus instance.
        """
        from api_engine.lib.event.eventbus import EventBus
        if not self._evt_bus:
            self._evt_bus = EventBus()
        return self._evt_bus

    @property
    def gcm(self):
        """

        :return:
        """
        from reelcam.utils.gcm import GCM
        if not self._gcm_client:
            self._gcm_client = GCM
        return self._gcm_client

    @property
    def system_user_id(self):
        """

        :return:
        """
        return '_'.join((self.app, 'system'))

    @property
    def M(self):
        """

        :return:
        """
        from reelcam.models import M
        return M

    @property
    def default_gps_point(self):
        """

        :return:
        """
        return {
            'lat': self.gps.default.lat,
            'lon': self.gps.default.lon
        }

    @property
    def logfile_path(self):
        """

        :return:
        """
        return join(self.cwd, self.logging.path)

    @property
    def upload_path(self):
        """

        :return:
        """
        return join(self.fs_root, 'upload')

    @property
    def geoip_path(self):
        """

        :return:
        """
        return join(self.fs_root, 'geoip/GeoLite2-City.mmdb')

    @property
    def email_sender(self):
        """
        Email sender

        :return:
        """
        return mandrill.Mandrill(self.mandrill.key)

    @property
    def kvs(self):
        """

        :return:
        """
        if not self._kvs:
            from tornadis import Client
            self._kvs = Client(host=self.redis.host, port=self.redis.port)
        return self._kvs

    @property
    def kvspp(self):
        """

        :return:
        """
        from tornadis import Pipeline
        return Pipeline()

    def set_environment(self, environment):
        """

        :param environment:
        :return:
        """
        self.__dict__['environment'] = environment
        self.elastic.__dict__['default_scope'] = self.get_scope()

    @property
    def celery_app(self):
        """
        Get Celery app.

        :return:
        """
        if not self._celery_app:
            from celery import Celery
            from reelcam.scheduler.celeryconf import CeleryConf
            redis_uri = 'redis://%s:%d/%d' % (self.redis.host, self.redis.port, self.redis.db)
            self._celery_app = Celery('reelcam.scheduler',
                                      broker=redis_uri, backend=redis_uri,
                                      include=['reelcam',
                                               'server',
                                               'reelcam.scheduler.tasks',
                                               'reelcam.scheduler.periodic',
                                               'utils'])

            self._celery_app.config_from_object(CeleryConf)

        return self._celery_app

    @property
    def sentry_client(self):
        """
        Get AsyncSentryClient object.

        :return:
        """
        if self._sentry_client:
            return self._sentry_client
        from raven.contrib.tornado import AsyncSentryClient
        from raven.transport import TornadoHTTPTransport
        self._sentry_client = AsyncSentryClient(
            dsn=self.sentry.dsn,
            transport=TornadoHTTPTransport,
            auto_log_stacks=True,
            processors=('raven.processors.RemovePostDataProcessor',),
            string_max_length=512,
        )
        return self._sentry_client

    @property
    def tloader(self):
        """

        :return:
        """
        if self._template_loader:
            return self._template_loader
        from tornado.template import Loader
        self._template_loader = Loader(self.template_path)
        return self._template_loader

    @property
    def template_path(self):
        """

        :return:
        """
        return join(self.cwd, 'templates')

    @property
    def es_settings(self):
        """

        :return:
        """
        es_settings = dict(urls=self.elastic.host,
                           timeout=self.elastic.timeout,
                           max_retries=3,
                           retry_on_timeout=True,
                           retry_on_status=(503, 504, 599))
        return es_settings

    def es_monkeypatch(self):
        """

        :param es_settings:
        :return:
        """
        def patch_get_es(init_package, S,  **default_settings):

            original_get_es = getattr(init_package, 'get_es')
            _urls = default_settings.pop('urls')
            _timeout = default_settings.pop('timeout')
            from functools import wraps

            @wraps(original_get_es)
            def get_es(urls=_urls, timeout=_timeout, force_new=False, **settings):
                settings.update(default_settings)
                return original_get_es(urls=urls, timeout=timeout, force_new=force_new, **settings)
            setattr(init_package, 'get_es', get_es)

            original_S_get_es = getattr(S, 'get_es')

            @wraps(original_S_get_es)
            def S_get_es(self, default_builder=get_es):
                return original_S_get_es(self, default_builder=default_builder)
            setattr(S, 'get_es', S_get_es)

        import asyncelasticutils
        import elasticutils
        patch_get_es(asyncelasticutils, asyncelasticutils.S, **self.es_settings)
        patch_get_es(elasticutils, elasticutils.S, **self.es_settings)

    def setup_logging(self):
        """
        Setup application logging.

        """
        import logging.handlers
        import logstash
        logstash_tags = [self.app.lower(), self.environment.lower()]
        loggers = ["api",
                   "api_engine",
                   "api_engine.lib.event.evt",
                   "elasticsearch",
                   "elasticsearch.trace",
                   "reelcam",
                   "reelcam.utils.gcm",
                   "server"]
        loggers = [logging.getLogger(logger_name) for logger_name in loggers]
        if self.logging.level == "DEBUG":
            loglevel = logging.DEBUG
        elif self.logging.level == "WARNING":
            loglevel = logging.WARNING
        elif self.logging.level == "ERROR":
            loglevel = logging.ERROR
        elif self.logging.level == "INFO":
            loglevel = logging.INFO
        else:
            loglevel = logging.NOTSET
        # '%(asctime)s - %(levelname)s - %(name)s || %(message)s',
        for logger in loggers:
            logger.propagate = False
            logger.setLevel(loglevel)
            if self.logstash.on:
                logstash_handler = logstash.TCPLogstashHandler(self.logstash.host,
                                                               port=self.logstash.port,
                                                               version=1,
                                                               tags=logstash_tags + [logger.name])
                logstash_handler.setLevel(loglevel)
                logger.addHandler(logstash_handler)
            if self.environment in ['DEVELOPMENT']:
                file_formatter = logging.Formatter(fmt='%(asctime)s.%(msecs)03d %(message)s',
                                                   datefmt='%Y-%m-%d %H:%M:%S')
                fh = logging.handlers.RotatingFileHandler(self.logfile_path + logger.name + '.log',
                                                          maxBytes=1024 * 1024 * 100, backupCount=10)
                fh.setFormatter(file_formatter)
                logger.addHandler(fh)

    @property
    def pubsub(self):
        """

        :return:
        """
        if not self._pubsub:
            from tornadis import PubSubClient
            from api_engine.lib.pubsub import PubSub

            pubsub_client = PubSubClient(host=self.redis.host, port=self.redis.port)
            self._pubsub = PubSub(self.kvs, pubsub_client)

        return self._pubsub

    @property
    def sms(self):
        """

        :return:
        """
        from api_engine.lib.msg import sms
        return sms.send

    @property
    def email(self):
        """

        :return:
        """
        from reelcam.scheduler import get_task
        return get_task('send_email')
