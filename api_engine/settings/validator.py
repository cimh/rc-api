# -*- coding: utf-8 -*-
from os.path import isfile
from validate import Validator as _Validator
from validate import ValidateError, VdtTypeError, VdtMissingValue
from validate import string_type, is_string_list, is_string


class VdtFileNotExists(ValidateError):
    """
    Vdt File Not Exists Error class.
    """
    def __init__(self, value):
        """

        :param value:
        :return:
        """
        ValidateError.__init__(self, 'the file is not exists at path "%s".' % (value,))


class VdtWrongHostValue(ValidateError):
    """
    A `host` type value validation error.
    """
    def __init__(self, value):
        """

        :param value:
        :return:
        """
        ValidateError.__init__(self, 'acceptable host type format is '
                                     'host< < addr< string > >:< port< integer > > >, but you pass: "%s".' % (value,))


def _pass(value):
    """

    :param value:
    :return:
    """
    return value


def is_filepath(value):
    """

    :param value:
    :return:
    """
    if not isinstance(value, string_type):
        raise VdtTypeError(value)
    if not isfile(value):
        raise VdtFileNotExists(value)
    return value


def is_host(value):
    """

    :param value:
    :return:
    """
    value = is_string(value)
    try:
        addr, port = value.split(':')
        port = int(port)
    except ValueError:
        raise VdtWrongHostValue(value)
    else:
        value = addr, port
    return value


def is_host_list(value, min=None, max=None):
    """

    :param value:
    :param min:
    :param max:
    :return:
    """
    return [is_host(mem) for mem in is_string_list(value, min=min, max=max)]


_val_functions = {
    'filepath': is_filepath,
    'host': is_host,
    'host_list': is_host_list,
}


class Validator(_Validator):
    """
    Validator class.
    """

    def __init__(self, functions=_val_functions):
        """

        :param functions:
        :return:
        """
        _Validator.__init__(self, functions)

    def check(self, check, value, missing=False):
        """
        A ``Validator.check`` modification that disallow assigning defaults for missing values.

        :param check:
        :param value:
        :param missing:
        :return:
        """
        if missing:
            raise VdtMissingValue()
        return super(Validator, self).check(check, value, missing)
