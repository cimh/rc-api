# -*- coding: utf-8 -*-
from configobj import ConfigObj, ConfigObjError, get_extra_values
from .validator import Validator


class ConfigValidationError(ConfigObjError):
    """
    Config Validation Error class.
    """
    pass


class RedundantMembersError(ConfigValidationError):
    """
    Redundant Members Error class.
    """
    pass


class Config(ConfigObj):
    """
    Config class.
    """
    def __init__(self, infile, configspec):
        """

        :param infile:
        :param configspec:
        :return:
        """
        ConfigObj.__init__(self, infile=infile, configspec=configspec, file_error=True)

    def _validate(self):
        """

        :return:
        """
        validator = Validator()
        result = self.validate(validator, preserve_errors=True)
        if result is not True:
            raise ConfigValidationError('Config validation failed on fields: %s' % str(result))
        extra = get_extra_values(self)
        if extra:
            raise RedundantMembersError('Config file contains redundant fields or sections: %s' % str(extra))
