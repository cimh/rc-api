# -*- coding: utf-8 -*-
from os.path import isfile, getsize
from configobj import ConfigObj
from validate import Validator
from .validator import _pass, _val_functions


_val_functions = {name: _pass for name in _val_functions}


class ConfigGen(ConfigObj):
    """
    Config Gen class.
    """

    def __init__(self, configspec):
        """

        :param configspec:
        :return:
        """
        ConfigObj.__init__(self, configspec=configspec)

        validator = Validator(functions=_val_functions)
        self.validate(validator, copy=True)

    def generate(self, outfile):
        """

        :param outfile:
        :return:
        """
        if isinstance(outfile, str):
            if isfile(outfile) and getsize(outfile):
                raise Warning('Exists config file is not empty! %s' % outfile)
            with open(outfile, 'wb') as fp:
                self.write(outfile=fp)
        elif hasattr(outfile, 'write'):
            self.write(outfile=outfile)
        else:
            raise TypeError('Illegal outfile type: %s' % type(outfile))
