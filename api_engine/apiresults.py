# -*- coding: utf-8 -*-
_200 = dict(code=200, msg='OK')
_206 = dict(code=206, msg='Partial Content')
_302 = dict(code=302, msg='Found')
_304 = dict(code=304, msg='Not modified')
_400 = dict(code=400, msg='Bad request')
_401 = dict(code=401, msg='Unauthorized')
_403 = dict(code=403, msg='Forbidden')
_404 = dict(code=404, msg='Not found')
_408 = dict(code=408, msg='Request Timeout')
_412 = dict(code=412, msg='Precondition Failed')
_423 = dict(code=423, msg='Locked')
_480 = dict(code=480, msg='Demo mode')
_500 = dict(code=500, msg='Internal Server Error')
_501 = dict(code=501, msg='Not implemented')
_503 = dict(code=503, msg='Service Unavailable')


def ok(**data):
    """
    API returned result, there were no errors.

    :param data:
    :return:
    """
    return dict(status=_200, data=data)


def partial(**data):
    """
    Usually meant an item is found but incomplete.

    :param data:
    :return:
    """
    return dict(status=_206, data=data)


def found(**data):
    """
    Usually meant an item is already exists.

    :param data:
    :return:
    """
    return dict(status=_302, data=data)


def not_modified(**data):
    """
    Usually means that no action was performed in backend.

    :param data:
    :return:
    """
    return dict(status=_304, data=data)


def bad_request(**data):
    """
    Missing required arguments.

    :param data:
    :return:
    """
    return dict(status=_400, data=data)


def unauthorized(**data):
    """
    Needs to be authorized to process request.

    :param data:
    :return:
    """
    return dict(status=_401, data=data)


def forbidden(**data):
    """
    User have no access to perform current request.

    :param data:
    :return:
    """
    return dict(status=_403, data=data)


def not_found(**data):
    """
    Usually, returns when an item doesn't exist in database.

    :param data:
    :return:
    """
    return dict(status=_404, data=data)


def request_timeout(**data):
    """
    Request timeout occured.

    :param data:
    :return:
    """
    return dict(status=_408, data=data)


def precondition_failed(**data):
    """

    :param data:
    :return:
    """
    return dict(status=_412, data=data)


def locked(**data):
    """
    Typically returned when the request can not be performed with certain parameters
    (mobile app build version for example).

    :param data:
    :return:
    """
    return dict(status=_423, data=data)


def demo_mode(**data):
    """
    Returned for read-only access for demo mode accounts

    :param data:
    :return:
    """
    return dict(status=_480, data=data)


def error(**data):
    """
    The error has been occurred during processing the request.

    :param data:
    :return:
    """
    return dict(status=_500, data=data)


def not_implemented(**data):
    """
    A method was not implemented so it won't return anything else.

    :param data:
    :return:
    """
    return dict(status=_501, data=data)


def service_unavailable(**data):
    """
    Typically returned when external service is unavailable or an error has occurred.

    :param data:
    :return:
    """
    return dict(status=_503, data=data)
