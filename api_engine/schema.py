# -*- coding: utf-8 -*-
import reelcam.api.handlers as handlers


class APISchema(object):
    """
    API Schema class.
    """

    def __init__(self):
        """

        :param api_name:
        :return:
        """
        self.handlers = {}
        self.schema = {}
        classname_len = len('APIHandler')

        for k in handlers.__dict__:
            if k[0].istitle():
                handler_name = k[:-classname_len].lower()
                self.handlers[handler_name] = handlers.__dict__[k]
                methods = [getattr(self.handlers[handler_name], name) for name in self.handlers[handler_name]._handles]
                for m in methods:
                    key = "%s:%s" % (handler_name, m.__name__)
                    self.schema[key] = m

    def get_method(self, name):
        """
        Get API method object by name.

        :param name: API method name.
        :return: APIMethod object.
        """
        import types
        _method = self.schema.get(name)
        _handler = self.handlers.get(name.split(':')[0])
        if not _method or not isinstance(_method, types.FunctionType):
            return None
        return APIMethod(_method.__name__, _method, _handler)


class APIMethod(object):
    """
    API method class.
    """

    def __init__(self, name, _method, _handler):
        """

        :param name: str
        :param _method: user-defined method
        :return:
        """
        self.name = name
        self._method = _method
        self._handler = _handler

    def get_handler(self, ctx):
        """
        Get handler for this method.

        :param ctx:
        :return:
        """
        return self._handler(ctx)
