.. _api-doc-chapter:

API
====
   .. autoclass:: reelcam.api.handlers.bookmarks
      :members:
   .. autoclass:: reelcam.api.handlers.chat
      :members:
   .. autoclass:: reelcam.api.handlers.feedback
      :members:
   .. autoclass:: reelcam.api.handlers.follow
      :members:
   .. autoclass:: reelcam.api.handlers.health
      :members:
   .. autoclass:: reelcam.api.handlers.history
      :members:
   .. autoclass:: reelcam.api.handlers.observe
      :members:
   .. autoclass:: reelcam.api.handlers.proxy
      :members:
   .. autoclass:: reelcam.api.handlers.search
      :members:
   .. autoclass:: reelcam.api.handlers.time
      :members:
   .. autoclass:: reelcam.api.handlers.upload
      :members:
   .. autoclass:: reelcam.api.handlers.user
      :members:
   .. autoclass:: reelcam.api.handlers.video
      :members:
   .. autoclass:: reelcam.api.handlers.wedcam.wedcam
      :members:
   .. autoclass:: reelcam.api.handlers.hbquest.quest
      :members:
   .. autoclass:: reelcam.api.handlers.hbquest.template
      :members:
   .. autoclass:: hellobot.api.handlers.subscription
      :members:

Index
=====

.. toctree ::
    :maxdepth: 2


* :ref:`modindex`
