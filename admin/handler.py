# -*- coding: utf-8 -*-
from pytz import timezone
from tornado import gen
from tornado.template import Loader
from tornado.web import RequestHandler
from api_engine import s
from utils.datetimemixin import dtm


class HBQuestAdminHTTPHandler(RequestHandler):
    """
    Admin HTTP Handler class.
    """
    # t = Template
    t_loader = Loader

    @gen.coroutine
    def get(self, *args, **kwargs):
        Quest = s().M.model('Quest')
        User = s().M.model('User')
        statuses = {0: 'Забронирован', 1: 'Игра', 2: 'Завершен'}
        quests = yield Quest.get(limit=1000, _sorts=['-start_ts'], _deleted=False)
        quests_data = [q.get_data() for q in quests]

        for q in quests_data:
            for k, v in list(q.items()):
                if k == 'start_ts':
                    moscow = timezone('Europe/Moscow')
                    q[k] = dtm.YmdHMS_from_dt(dtm.dt_from_ts(v).astimezone(moscow))

        user_ids = {q.members.bot.user_id for q in quests}
        user_ids.update({q.members.hunter.user_id for q in quests})
        members = yield User.get(_ids=list(user_ids))
        user_data = []
        for m in members:
            d = yield m.get_data(check_follow=False)
            user_data.append(d)

        user_data = members

        template = self.t_loader(self.get_template_path()).load('index.html')\
            .generate(quests=quests_data, users=user_data, statuses=statuses)
        self.write(template)

    def head(self, *args, **kwargs):
        pass

    def post(self, *args, **kwargs):
        pass

    @gen.coroutine
    def delete(self, *args, **kwargs):
        from api_engine.enums import VIDEO_PERMISSIONS
        qid = self.get_arguments('quest_id')[0]
        Quest = s().M.model('Quest')
        Video = s().M.model('Video')
        quest = yield Quest.get_one(_id=qid)
        video_ids = quest.get_videos()
        videos = yield Video.get(_ids=video_ids)
        for v in videos:
            v.permission = VIDEO_PERMISSIONS.list
            yield v.save()
        quest.is_deleted = True
        yield quest.save()

    def patch(self, *args, **kwargs):
        pass

    def put(self, *args, **kwargs):
        pass

    def options(self, *args, **kwargs):
        pass
