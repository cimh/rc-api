(function (global, undefined) {
    'use strict';

    var core;

    core = {
        config: null,
        socket: {
            /**
             * Dummy for backbone.iobind ioUnbind()
             *
             * @type Array
             */
            $events: [],
            connect: function (port) {
                var location = global.location,
                        url = location.protocol + '//' + location.hostname + ':' + port + '/ws',
                        cP = {secure: /s/.test(location.protocol)};

                this.io = new WebSocket(url);

                // send the command in url only if the connection is opened
                // command attribute is used in server-side.
                this.io.onopen = function(){
                    // in my convention, every message sent to the server must be:
                    // {"command":"action", "data":"data sent to the server"}
                    //ws.send(JSON.stringify({"command":model.url, data:model.attributes}));
                    core.debug('WS connected to ' + url, cP);
                };

                this.io.onmessage = function(message){
                    // message.data is a property sent by the server
                    // change it to suit your needs
                    var ret = JSON.parse(message.data);
                    // executes the success callback when receive a message from the server
                    options.success(ret);
                    core.debug('WS message:', message);
                };

                this.io.onclose = function() {
                    core.debug('WS disconnected');
                };
            },
            emit: function (method, data, callback, persistent) {
                core.debug('WS emit: ' + method, data);


                this.io.emit(method, data, function (response) {
                    core.debug('WS ack (' + method + '):', response);

                    // basic responce with auth required
                    if (response.code == 0) {
//                            core.Behaviors.userModal('/login', 'login');
                    }
                    else if (callback) {
                        // asume success if results is 1 or data is not null
                        if (response.result || response.data !== null) {
                            callback(response.data, response);
                        }
                    }
                });

                if (persistent) {
                    this.addConnectCommand(method, data, callback);
                }
            },
            ask: function (opt) {
                var method = opt.method,
                        data = opt.data,
                        success = opt.success,
                        error = opt.error,
                        persistent = opt.persistent;
                core.debug('WS emit: ' + method, data);
                this.io.emit(method, data, function (response) {
                    core.debug('WS ack (' + method + '):', response);

                    // basic responce with auth required
                    if (response.code == 0) {
                        core.Behaviors.userModal('/login', 'login');
                    }
                    else if (success && $.type(success) === 'function') {
                        // asume success if results is 1 or data is not null
                        if (response.result || response.data !== null) {
                            success(response.data, response);
                        } else if (error && $.type(error) === 'function') {
                            error(response);
                        }
                    }
                });
                if (persistent) {
                    this.addConnectCommand(method, data, callback);
                }
            },
            on: function (method, callback) {
                this.io && this.io.on(method, function (data) {
                    core.debug('WS event: ' + method, data);
                    callback(data);
                });
            },
            removeListener: function (method, callback) {
                this.io.removeListener(method, callback);
            },
            /**
             * Dummy for backbone.iobind ioUnbind()
             *
             * @param method
             */
            removeAllListeners: function (method) {
                this.io.removeAllListeners(method);
                delete this.io.$events[method];
            },
            disconnect: function () {
                this.io.disconnect();
            },
            addConnectCommand: function (method, data, callback) {
                this.connectCommands[method] = {
                    data: data,
                    callback: callback
                };
            },
            /**
             *
             * @param key
             * @param method
             * @param data
             * @param callback
             */
            add_connect_command: function (key, method, data, callback) {
                this.connect_commands[key] = {
                    method: method,
                    data: data,
                    callback: callback
                };
            },
            removeConnectCommand: function (method) {
                delete this.connectCommands[method];
            },
            remove_connect_command: function (key) {
                delete this.connect_commands[key];
            }
        },
        log: function(message, obj) {
            if (global.console) {
                message = (new Date()).toTimeString() + ' ' + message;
                if (obj !== undefined) {
                    global.console.log(message, obj);
                }
                else {
                    global.console.log(message);
                }
            }
        },
        debug: function(message, obj) {
            if (this.config && this.config.debug) {
                this.log(message, obj);
            }
        },
        init: function() {
            this.config = this.getDescriptor('hellobot-config');
            this.initWebSocket();
        },
        getDescriptor: function(source) {
            return this.getJSONFromHtml(source);
        },
        getJSONFromHtml: function(id) {
            var element, json;

            element = $('#' + id);
            if (!element.length) {
                return this.log('Elem #' + id + ' not found');
            }

            try {
                json = JSON.parse(element.html());
            }
            catch (e) {
                return this.log('Invalid JSON in #' + id);
            }

            return json;
        },
        initWebSocket: function() {
//            core.socket.connect(this.config.wsurl);
            core.socket.connect(8666);
            $(window).on('beforeunload', function () {
                core.socket.disconnect();
            });
        }
    };

    global.moderate = core;
}(this));