# -*- coding: utf-8 -*-
import logging
from collections import defaultdict

from tornado import escape
from tornado.websocket import WebSocketHandler, WebSocketClosedError, WebSocketProtocol13
from tornado.escape import json_decode
from tornado.ioloop import IOLoop

from api_engine.lib.event import EvtReceiver
from .handler import BaseRequestHandler
from utils.datetimemixin import dtm
from utils import DictDiffer, kvs
from api_engine import s
from api_engine import apiresults
from api_engine.auth.sessionmanager import sm
from reelcam.api import w

log = logging.getLogger(__name__)


class WebSocketConnection(BaseRequestHandler, WebSocketHandler, EvtReceiver):
    """
    WebSocket Connection class.
    """

    type = BaseRequestHandler.api.request_class.WS

    connections = dict()
    sessions = defaultdict(dict)
    prev_msg = None
    cooldown_timeout = 3  # seconds

    def check_origin(self, origin):
        """

        :param origin:
        :return:
        """
        return True

    def open(self):
        """

        :return:
        """
        if self.session:
            sm.add_connection(self.session, self.id)
            self.sessions[self.session][self.id] = self
            self.connections[self.id] = self
            # s().evt_bus.add_connection(self)
            if self.session != self.get_cookie('sessionid'):
                s().e('user:new_session', sender='SYS', connection_ids=[self.id], data=dict(session_id=self.session))
        else:
            self.close()
            return
        # log.debug('CONNECTION ID: %s opened', self.id)

    def on_message(self, message):
        """

        :param message:
        :return:
        """

        # log.debug('CLIENT MESSAGE was received on CONNECTION ID: %s', self.id)
        async def c(message):
            """

            :param message:
            :return:
            """
            if self.session:
                if self.request.ctx.request_user:
                    sm.expire(self.session)
                else:
                    sm.expire(self.session, _anon=True)
            else:
                self.on_close()
                await self.prepare()
                self.open()

            try:
                message = escape.json_decode(message)
                message['req_ts'] = dtm.now_YmdHMSs()
                if self.need_drop(message):
                    return

            except (TypeError, ValueError):
                log.warning('FAIL to decode CLIENT MESSAGE on CONNECTION ID: %s', self.id)
                return
                # else:
                # log.debug('CLIENT MESSAGE was decoded successfully on CONNECTION ID: %s', self.id)

            self.prev_msg = {k: v for k, v in list(message.items()) if k not in ['reqid', 'req_ts']}
            self.prev_msg['req_ts'] = dtm.now_YmdHMSs()
            request = self.api.request_class(self.request.ctx, message)
            if self.request.ctx.request_user_data and self.request.ctx.request_user_data['is_demo'] and \
               message['method'] in w:
                resp = apiresults.demo_mode()
            else:
                resp = await self.call_api(request)
            self.write_message(resp)
        IOLoop.current().spawn_callback(c, message)

    def on_close(self):
        """

        :return:
        """
        if self.session:
            sm.remove_connection(self.session, self.id)

        if self.id in self.connections:
            self.connections.pop(self.id, None)

        if self.session and self.session in self.sessions:
            self.sessions[self.session].pop(self.id, None)

        # log.debug('CONNECTION ID: %s closed', self.id)

    def write_message(self, message, binary=False):
        """
        Sends the given message to the client of this Web Socket.

        :param message:
        :param binary
        :return:
        """
        try:
            return super(WebSocketConnection, self).write_message(message, binary)
        except WebSocketClosedError:
            pass

    async def prepare(self):
        """

        :return:
        """
        await super(WebSocketConnection, self).prepare()
        if not self.session:
            if not self.request.headers.get('X-Health'):
                log.error('FAIL to set SESSION on CONNECTION ID: %s', self.id)
                self.close()
        # else:
        #     log.info('Set SESSION ID: %s on CONNECTION ID: %s', self.session, self.id)
        return

    @classmethod
    def signal_handler(cls, signum):
        """

        :param signum:
        :return:
        """
        log.warning('SIGNAL signum: %d was received', signum)
        for _id, c in list(cls.connections.items()):
            if c:
                c.close(1001, 'Server is going away.')

        # from utils.pubsub import EVENT
        # pubsub = s().pubsub
        # @TODO: how it does not work?
        # pubsub.unsubscribe(pubsub.get_key(EVENT))

    def need_drop(self, message):
        """

        :return:
        """
        if 'method' in message and message['method'] in w and self.prev_msg:
            time_delta = (dtm.dt_from_YmdHMSs(message['req_ts']) -
                          dtm.dt_from_YmdHMSs(self.prev_msg['req_ts'])).total_seconds()
            d = DictDiffer(message, self.prev_msg).changed()
            d.discard('req_ts')
            return self.prev_msg and not d and not time_delta >= self.cooldown_timeout
        else:
            return False

    def get_websocket_protocol(self):
        websocket_version = self.request.headers.get("Sec-WebSocket-Version")
        if websocket_version in ("7", "8", "13"):
            return MyWebSocketProtocol(
                self, compression_options=self.get_compression_options())


class MyWebSocketProtocol(WebSocketProtocol13):
    """
    Overriden to manually serve WS ping packets.
    """

    def _handle_message(self, opcode, data):
        """

        :param opcode:
        :param data:
        :return:
        """
        if opcode == 0x9:
            # ping, {"proxy_ping": "<proxy_ID>"}
            try:
                d = json_decode(data)
                proxy_id = d.get('proxy_ping')
                RedisProxyPingTracker.refresh(proxy_id)
            except Exception as e:
                pass
                # log.debug('Cannot decode WS ping: %s', repr(data))
        super(MyWebSocketProtocol, self)._handle_message(opcode, data)


class RedisProxyPingTracker(object):
    """
    Writes key to redis with last proxy ping timestamp.
    """

    PROXY_KEY_TTL = 3600

    @staticmethod
    def get_key(proxy_id, suffix=''):
        """

        :param proxy_id:
        :param suffix:
        :return:
        """
        return kvs.get_key('PROXY', proxy_id, suffix, s().get_scope())

    @classmethod
    def refresh(cls, proxy_id):
        """

        :return:
        """
        from utils.datetimemixin import dtm
        s().kvs.async_call("HSET", cls.get_key(proxy_id), 'TS', dtm.now_ts())
        s().kvs.async_call("HINCRBY", cls.get_key(proxy_id), 'PING', +1)
        s().kvs.async_call("EXPIRE", cls.get_key(proxy_id), cls.PROXY_KEY_TTL)
