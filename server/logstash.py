# -*- coding: utf-8 -*-
from .tcp import TCPStreamHandler


PORT = 8668


class LogstashHandler(TCPStreamHandler):
    """
    Logstash tcp handler.

    """

    max_connections = 2
    connections = set()

    @classmethod
    def accept_connection(cls, connection, address):
        """

        :param connection:
        :param address:
        """
        if len(cls.connections) >= cls.max_connections:
            connection.close()
            return
        new = cls(connection, address)
        cls.connections.add(new)

    def on_close(self):
        """

        :return:
        """
        self.connections.discard(self)
