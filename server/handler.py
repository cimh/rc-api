# -*- coding: utf-8 -*-
from tornado import gen
from tornado.web import RequestHandler
from tornado.gen import TimeoutError
from api_engine.api import API
from api_engine import apiresults
from datetime import timedelta

API_REQUEST_TIMEOUT = 60


class BaseRequestHandler(RequestHandler):
    """
    Base Request Handler class.
    """

    type = API.request_class.HTTP
    session = None
    api = API
    id = None
    """Connection unique ID."""

    def log_request(self):
        """

        :return:
        """
        pass

    def handle_request(self, request):
        """

        :return:
        """
        pass

    async def prepare(self):
        """

        :return:
        """
        if self.request.ctx:
            setattr(self.request, 'ctx', None)
        self.request.ctx = self.api.context_class(self.request)
        if not self.request.headers.get('X-Health'):
            sessionid = self.get_cookie('sessionid')
            platform = self.request.headers.get('platform', None) or \
                self.request.arguments.get('platform')[0] if self.request.arguments.get('platform') else None
            app_name = self.request.headers.get('app_name', None) or \
                self.request.arguments.get('app_name')[0] if self.request.arguments.get('app_name') else None
            await self.request.ctx.setup(sessionid, platform, app_name)
        return

    def get_message(self, method):
        """

        :return:
        """
        if self.request.type == self.api.request_class.HTTP:
            data = dict(files=self.request.files.copy())
            params = {k: v[0] for k, v in list(self.request.arguments.items())}
            params.pop('files', None)
            data.update(params)
            message = dict(method=method, data=data)
            return message

    def initialize(self):
        """

        :return:
        """
        from uuid import uuid1
        self.request.type = self.type
        self.request.connection_id = uuid1().hex
        self.request.ctx = None

    @property
    def session(self):
        """

        :return:
        """
        return self.request.ctx.session

    @property
    def id(self):
        """

        :return:
        """
        return self.request.connection_id

    async def call_api(self, api_request):
        """
        Call API with an exception capturing.

        :param api_request: APIRequest object.
        :return: dict API response.
        """
        try:
            return await self.api.call(api_request)
        except TimeoutError:
            return apiresults.request_timeout()
