# -*- coding: utf-8 -*-
import socket

from tornado.iostream import IOStream
from tornado.netutil import add_accept_handler


class TCPStreamHandler(object):
    """
    TCPStreamHandler class.

    """

    connection = None
    address = None
    stream = None

    def __init__(self, connection, address):
        """

        :param connection:
        :param address:
        """
        self.connection = connection
        self.address = address
        self.stream = IOStream(self.connection)
        self.stream.set_close_callback(self.on_close)

    @classmethod
    def accept_connection(cls, connection, address):
        """

        :param connection:
        :param address:
        :return:
        """
        raise NotImplementedError()

    def on_close(self):
        """

        :return:
        """
        raise NotImplementedError()


class TCPServer(object):
    """
    TCPServer class.

    """

    def __init__(self, connection_handler):
        """

        :return:
        """
        self.connection_handler = connection_handler
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.setblocking(0)

    def start(self, address="0.0.0.0", port=8999):
        """

        :param address:
        :param port:
        :return:
        """
        self.sock.bind((address, port))
        self.sock.listen(128)
        add_accept_handler(self.sock, self.connection_handler)
