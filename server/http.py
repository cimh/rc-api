# -*- coding: utf-8 -*- 
from tornado import gen
from api_engine.urlmatcher import urls
from .handler import BaseRequestHandler


class HTTPHandler(BaseRequestHandler):
    """
    HTTP Handler class.
    """

    type = BaseRequestHandler.api.request_class.HTTP

    @gen.coroutine
    def prepare(self):
        """

        :return:
        """
        yield super(HTTPHandler, self).prepare()
        if not self.session:
            if not self.request.headers.get('X-Health'):
                self.finish()
                return

        api_method = None
        if self.request.path in urls:
            api_method = urls[self.request.path]

        if not api_method:
            self.finish()
            return

        message = self.get_message(api_method)
        request = self.api.request_class(self.request.ctx, message)
        resp = yield self.call_api(request)
        self.finish(resp)
