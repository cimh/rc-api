# -*- coding: utf-8 -*-
import datetime
from tornado import web
from tornado.ioloop import IOLoop
from plop.collector import Collector


class ProfileHandler(web.RequestHandler):
    """
    Plop Profiling Handler
    """

    @web.asynchronous
    def get(self):
        """

        :return:
        """
        self.collector = Collector()
        self.collector.start()
        IOLoop.instance().add_timeout(
            datetime.timedelta(seconds=60),
            self.finish_profile)

    def finish_profile(self):
        """

        :return:
        """
        self.collector.stop()
        self.finish(repr(dict(self.collector.stack_counts)))
