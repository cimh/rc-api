# -*- coding: utf-8 -*-
from tornado import gen


class VideoLoader(object):
    """
    Video Loader class.

    """

    def __init__(self, app_name, reporter_user):
        """

        :param app_name: str Application name.
        :param reporter_user:
        :return:
        """
        self.app_name = app_name
        self.reporter_user = reporter_user

    @gen.coroutine
    def load_video(self, video_path, video_title, video_permission=None, video_location=None):
        """
        Create Video and upload video file.

        :param video_path: str Video file path.
        :param video_title: unicode Video title.
        :param video_permission: int Video permission.
        :param video_location: dict Video location {'lat': float lat, 'lon': float lon}.
        :return: Video object.
        """
        from os.path import abspath, exists, getsize
        from io import StringIO
        from tornado.util import ObjectDict
        from api_engine import s
        from reelcam.api.handlers import VideoAPIHandler
        from api_engine.enums import VIDEO_PERMISSIONS
        from reelcam.api.handlers import UploadApiHandler
        from reelcam.uploader import get_uploader

        Video = s().M.model('Video')

        video_path = abspath(video_path)
        if not exists(video_path):
            print(("Video", video_path, "not exists."))
            return

        if video_permission is None:
            video_permission = VIDEO_PERMISSIONS.all

        if not video_location:
            from reelcam.utils import default_location
            video_location = default_location()

        ctx = ObjectDict(request=ObjectDict(type="WS"), request_user=self.reporter_user, client_ip='127.0.0.1')
        video_handler = VideoAPIHandler(ctx)

        request = dict(
            title=video_title,
            lat=video_location['lat'],
            lon=video_location['lon'],
            app_name=self.app_name,
        )
        result = yield video_handler.handle("create_offline", request)
        del request

        video = None
        if result['status']['code'] is 200:
            video_id = result['data']['video_id']
            video = yield Video.get_one(_id=video_id)
        del result

        if not video:
            print(("Video create failed."))
            return
        else:
            if video.gps_incognito:
                video.gps_incognito = False
            video.permission = video_permission
            video = yield video.save()

        upload_handler = UploadApiHandler(ctx)
        result = upload_handler.mediastorage({})
        if result['status']['code'] is not 200:
            print("upload:mediastorage request failed.")
            return

        mediastorage = result['data']
        del result

        target = "ri"

        result = yield upload_handler.key(dict(video_id=video.id, target=target))
        if result['status']['code'] is not 200:
            print("upload:key request failed.")
            return

        video_key = result['data']['key']
        del result

        uploader = get_uploader()
        mpart = uploader.initiate_multipart(video_key)
        if not mpart:
            print("Can't create S3-multipart.")
            return

        video_file = open(video_path, 'rb')
        part_size = 20 * 1024 * 1024
        part_num = 0
        part = video_file.read(part_size)
        while part:
            part_num += 1
            mpart.upload_part(StringIO(part), part_num)
            part = video_file.read(part_size)
        mpart.complete_upload()
        video_file.close()

        request = dict(
            video_id=video.id,
            target="ri",
            token=video.reporter_token,
            part_num=part_num,
            size=getsize(video_path),
            mediastorage=mediastorage,
            mpart_id=mpart.id,
            key=str(video_key),
        )
        result = yield upload_handler.direct(request)
        if result['status']['code'] is not 200:
            print("upload:direct request failed.")
            return

        raise gen.Return(video)

    @staticmethod
    @gen.coroutine
    def reupload(video_id, target, original_key):
        """

        :param video_id:
        :param target:
        :param original_key:
        :return:
        """
        from api_engine import s
        from reelcam.scheduler import execute_task
        Video = s().M.model('Video')
        Upload = s().M.model('Upload')

        video = yield Video.get_one(_id=video_id)
        if not video:
            return

        if video.upload_ids[target]:
            upload = yield Upload.get_one(_id=video.upload_ids[target])
            upload.is_complete = True
            upload.task_complete = upload.is_reanimated = False
            upload.key = str(original_key)
            upload = yield upload.save()
        else:
            upload = Upload({'video_id': video.id, 'target': target, 'key': str(original_key)})
            upload.is_complete = True
            upload = yield upload.save()
            video.upload_ids[upload.target] = upload.id
            video.upload_ids = video.upload_ids.copy()
            yield video.save()

        execute_task('upload_complete', upload.id)
        raise gen.Return(upload.id)

    @staticmethod
    def get_original_key(video_id, reporter_id, target):
        """

        :param video_id:
        :param reporter_id:
        :param target:
        :return: Key object.
        """
        return '/'.join(['media', reporter_id, video_id, '.'.join([target, 'flv'])])

    @staticmethod
    def upload_s3(key, path):
        """

        :param key:
        :param path:
        :return:
        """
        from reelcam.uploader import get_uploader

        uploader = get_uploader()

        fp = open(path, 'rb')
        uploader.upload(key, fp)
        fp.close()
        uploader.set_acl(key, 'public-read')
