# -*- coding: utf-8 -*-
KS = ":"            # key separator
HS = "#"            # id separator


def get_prefix(prefix, scope=''):
    """

    :param prefix:
    :param scope:
    :return:
    """
    parts = []
    if scope:
        parts.append(scope)
    parts.append(prefix)
    return KS.join(parts)


def get_key(prefix, id, suffix='', scope=''):
    """

    :param prefix:
    :param id:
    :param suffix:
    :param scope:
    :return:
    """
    parts = [get_prefix(prefix, scope), id]
    if suffix:
        parts.append(suffix)
    return KS.join(parts)


def key_mask(scope=''):
    """

    :param scope:
    :return:
    """
    return get_prefix('*', scope)
