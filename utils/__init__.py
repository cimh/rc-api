# -*- coding: utf-8 -*- 


def all_subclasses(cls):
    """

    :param cls:
    :return:
    """
    return cls.__subclasses__() + [g for _s in cls.__subclasses__()
                                   for g in all_subclasses(_s)]


class DictDiffer(object):
    """
    Calculate the difference between two dictionaries as:
    (1) items added
    (2) items removed
    (3) keys same in both but changed values
    (4) keys same in both and unchanged values
    """

    def __init__(self, current_dict, past_dict):
        """

        :param current_dict:
        :param past_dict:
        """
        self.current_dict, self.past_dict = current_dict, past_dict
        self.set_current, self.set_past = set(current_dict.keys()), set(past_dict.keys())
        self.intersect = self.set_current.intersection(self.set_past)

    def added(self):
        """

        :return:
        """
        return self.set_current - self.intersect

    def removed(self):
        """

        :return:
        """
        return self.set_past - self.intersect

    def changed(self):
        """

        :return:
        """
        return set(o for o in self.intersect if self.past_dict[o] != self.current_dict[o])

    def unchanged(self):
        """

        :return:
        """
        return set(o for o in self.intersect if self.past_dict[o] == self.current_dict[o])


def check_latlon(lat, lon):
    """

    :param lat: float
    :param lon: float
    :return: bool
    """
    if not (-90 <= lat <= 90):
        return False
    elif not (-180 <= lon <= 180):
        return False
    else:
        return True
