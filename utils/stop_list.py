# -*- coding: utf-8 -*-
from os.path import dirname, join
import codecs


STOP_LIST_PARTIAL_MATCH = []


def get_stoplist_path(filename):
    """

    :param filename:
    :return:
    """
    return join(dirname(__file__), filename)


try:
    path = get_stoplist_path('stop_list_full_match.txt')
    with codecs.open(path, 'r', encoding='utf-8') as fp:
        STOP_LIST_FULL_MATCH = [line.strip() for line in fp]
except Exception:
    raise
