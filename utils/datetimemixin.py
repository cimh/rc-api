# -*- coding: utf-8 -*-
import pytz
from datetime import datetime, timedelta


class DatetimeMixin(object):
    """Mixin that provides different datetime conversions."""
    @classmethod
    def now(cls):
        """

        :return:
        """
        return datetime.now(pytz.utc)

    @classmethod
    def now_dt(cls):
        """

        :return:
        """
        return cls.now().replace(microsecond=0)

    @classmethod
    def now_ts(cls):
        """

        :return:
        """
        return cls.ts_from_datetime(cls.now_dt())

    @classmethod
    def today_dt(cls):
        """

        :return:
        """
        return cls.now_dt().replace(hour=0, minute=0, second=0)

    @classmethod
    def today_ts(cls):
        """

        :return:
        """
        return cls.ts_from_datetime(cls.today_dt())

    @classmethod
    def now_YmdHMS(cls):
        """

        :return:
        """
        return cls.YmdHMS_from_dt(cls.now_dt())

    @classmethod
    def now_YmdHMSs(cls):
        """

        :return:
        """
        return cls.YmdHMSs_from_dt(cls.now())

    @classmethod
    def now_Ymd(cls):
        """

        :return:
        """
        return cls.Ymd_from_dt(cls.now_dt())

    @classmethod
    def dt_from_ts(cls, ts):
        """

        :param ts:
        :return:
        """
        return datetime.utcfromtimestamp(float(ts)).replace(tzinfo=pytz.utc)

    @classmethod
    def ts_from_datetime(cls, dt):
        """

        :param dt:
        :return:
        """
        return int((dt.replace(tzinfo=pytz.utc) - datetime(1970, 1, 1, tzinfo=pytz.utc)).total_seconds()) \
            if dt is not None else None

    @classmethod
    def dt_from_YmdHMS(cls, date_string):
        """

        :param date_string:
        :return:
        """
        return datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)

    @classmethod
    def dt_from_YmdHMSs(cls, date_string):
        """

        :param date_string:
        :return:
        """
        return datetime.strptime(date_string.split('.')[0], '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)

    @classmethod
    def ts_from_YmdHMS(cls, date_string):
        """

        :param date_string:
        :return:
        """
        if not date_string:
            return None
        return cls.ts_from_datetime(cls.dt_from_YmdHMS(date_string))

    @classmethod
    def YmdHMS_from_ts(cls, ts):
        """

        :param ts:
        :return:
        """
        if ts is None:
            return None
        return cls.YmdHMS_from_dt(cls.dt_from_ts(ts))

    @classmethod
    def YmdHMS_from_dt(cls, dt):
        """

        :param dt:
        :return:
        """
        if dt is None:
            return None
        return dt.strftime('%Y-%m-%d %H:%M:%S')

    @classmethod
    def YmdHMSs_from_dt(cls, dt):
        """

        :param dt:
        :return:
        """
        if dt is None:
            return None
        return '.'.join([dt.strftime('%Y-%m-%d %H:%M:%S'), str(dt.microsecond / 1000).zfill(3)])

    @classmethod
    def YmdHM_from_dt(cls, dt):
        """

        :param dt:
        :return:
        """
        if dt is None:
            return None
        return dt.strftime('%Y-%m-%d %H:%M')

    @classmethod
    def YmdHM_from_ts(cls, ts):
        if ts is None:
            return None
        return cls.YmdHM_from_dt(cls.dt_from_ts(ts))

    @classmethod
    def YmdHMS_from_Ymd(cls, date_string):
        """

        :param date_string:
        :return:
        """
        return cls.YmdHMS_from_dt(cls.dt_from_Ymd(date_string))

    @classmethod
    def Ymd_from_dt(cls, dt):
        """

        :param dt:
        :return:
        """
        if not dt:
            return None
        return dt.strftime('%Y-%m-%d')

    @classmethod
    def dt_from_Ymd(cls, date_string):
        """

        :param date_string:
        :return:
        """
        return datetime.strptime(date_string, '%Y-%m-%d')

    @classmethod
    def ts_from_Ymd(cls, date_string):
        """

        :param date_string:
        :return:
        """
        return cls.ts_from_datetime(cls.dt_from_Ymd(date_string))

    @classmethod
    def dmY_from_ts(cls, ts):
        """

        :param ts:
        :return:
        """
        return cls.dt_from_ts(ts).strftime('%d-%m-%Y')

    @classmethod
    def HMS_from_ts(cls, ts):
        """

        :param ts:
        :return:
        """
        return cls.dt_from_ts(ts).strftime('%H:%M:%S')

dtm = DatetimeMixin
