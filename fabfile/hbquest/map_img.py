# -*- coding: utf-8 -*- 
from fabric.api import task
from tornado import gen
from tornado.ioloop import IOLoop


@task
def run():
    """

    :return:
    """
    IOLoop.current().run_sync(update_templates)


@gen.coroutine
def update_templates():
    """

    :return:
    """
    from api_engine import s
    Template = s().M.model('Template')
    templates, _ = yield Template.all(limit=10000)
    update_data = {
        'SPb': 'img/citydoom/MAP1.jpg',
        'Moscow': 'img/citydoom/MAP2.jpg',
        'Vladivostok': 'img/citydoom/MAP3.jpg',
    }
    for t in templates:
        for k, v in list(update_data.items()):
            if k in t.title:
                t.map_img = v
                yield t.save()
