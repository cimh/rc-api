# -*- coding: utf-8 -*-
from fabric.api import task
from tornado import gen


@task
def run():
    """
    Delete odd games.

    """
    from tornado.ioloop import IOLoop
    IOLoop.current().run_sync(main)


@gen.coroutine
def main():
    """

    :return:
    """
    from api_engine import s
    s()
    from elasticutils import get_es
    from elasticsearch.helpers import scan
    from tornado.util import ObjectDict

    if s().environment != "PRODUCTION":
        print("This task for PRODUCTION only!")
        return

    Quest = s().M.model('Quest')

    quest_videos = ['AVElF5sOlSZce_F8xphU',
                    'AVE_e2ZFZe1YXa-SlKkc',
                    'AVFJDMX04FK4afIqClIJ',
                    'AVFJ7c8p5pkn9o76H7up',
                    'AVHEZpDP5pkn9o76H8Cx']

    query = {
            "filtered": {
                "filter": {
                    "bool": {
                        "must": [
                            {"term": {"is_deleted": False}},
                        ],
                        "must_not": {
                            "terms": {
                                "members.bot.video_id": quest_videos
                            },
                        },
                    }
                }
            }
    }

    doc_type = Quest.mapper.get_mapping_type_name()
    index = Quest.mapper.get_index()
    es = get_es()
    scroll = scan(es, query=dict(query=query), doc_type=doc_type, index=index)
    for r in scroll:
        args = ObjectDict(r['_source'], es_meta=ObjectDict(id=r['_id']))
        quest = Quest(args)
        quest.is_deleted = True
        yield quest.save()
        print((quest, "was deleted"))
    return
