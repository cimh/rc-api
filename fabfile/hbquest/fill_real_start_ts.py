# -*- coding: utf-8 -*-
from fabric.api import task
from tornado import gen
from tornado.ioloop import IOLoop


@task
def run():
    """

    :return:
    """
    IOLoop.current().run_sync(main)


@gen.coroutine
def main():
    """

    :return:
    """
    from api_engine import s
    from api_engine.enums import QUEST_STATUSES
    Quest = s().M.model('Quest')
    count = yield Quest.mapper.get(status=QUEST_STATUSES.finished).count()
    quests = yield Quest.get(status=QUEST_STATUSES.finished, limit=count, _deleted=False)
    for q in quests:
        if not q.real_start_ts:
            q.real_start_ts = q.start_ts
            yield q.save()
            print(q)
