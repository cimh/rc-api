# -*- coding: utf-8 -*-
from fabric.api import task


@task
def run():
    """
    Correct videos parts ts type (from int to "YmdHMS").

    :return:
    """
    from copy import deepcopy
    from tornado.util import ObjectDict
    from elasticutils import get_es
    from elasticsearch.helpers import scan
    from api_engine import s
    s()
    from utils.datetimemixin import dtm

    Video = s().M.model('Video')

    doc_type = Video.mapper.get_mapping_type_name()
    index = Video.mapper.get_index()
    es = get_es()
    scroll = scan(es, scroll="1m", doc_type=doc_type, index=index)
    for r in scroll:
        args = ObjectDict(r['_source'], es_meta=ObjectDict(id=r['_id']))
        video = Video(args)
        if video.parts and isinstance(video.parts, list):
            parts = deepcopy(video.parts)
            for p in parts:
                for k in ("start_ts", "end_ts"):
                    if not p[k]:
                        continue
                    if isinstance(p[k], int):
                        try:
                            p[k] = dtm.YmdHMS_from_ts(p[k])
                        except Exception:
                            pass
            video.parts = parts
            video.save()
