# -*- coding: utf-8 -*-
from fabric.api import task
from tornado import gen


@task
def run():
    """

    :return:
    """
    from tornado.ioloop import IOLoop
    IOLoop.current().run_sync(main)


@gen.coroutine
def main():
    """

    :return:
    """
    from api_engine import s
    s()
    from elasticutils import get_es
    from elasticsearch.helpers import scan
    from tornado.util import ObjectDict
    Template = s().M.model('Template')

    doc_type = Template.mapper.get_mapping_type_name()
    index = Template.mapper.get_index()
    es = get_es()
    scroll = scan(es, doc_type=doc_type, index=index)
    for r in scroll:
        args = ObjectDict(r['_source'], es_meta=ObjectDict(id=r['_id']))
        t = Template(args)
        t.game_duration_seconds = s().hbquest.game_duration_seconds
        yield t.save()
    return
