# -*- coding: utf-8 -*- 
from fabric.api import task, runs_once
from api_engine import s
from tornado.ioloop import IOLoop
from tornado import gen


@runs_once
@task
def end():
    """

    :return:
    """
    quests = IOLoop.current().run_sync(end_main)

@gen.coroutine
def end_main():
    Quest = s().M.model('Quest')
    quests = yield Quest.pending(limit=10000)
    for q in quests:
        print((q.id))
        yield q.finish()
    raise gen.Return(quests)
