# -*- coding: utf-8 -*- 
from fabric.api import task
from tornado import gen
from tornado.ioloop import IOLoop


@task
def run():
    """

    :return:
    """
    IOLoop.current().run_sync(update_videos)

@gen.coroutine
def update_videos():
    """

    :return:
    """
    from api_engine import s
    from tornado.util import ObjectDict
    from api_engine.enums import QUEST_STATUSES
    User = s().M.model('User')
    sys_user = yield User.get_system()
    ctx = ObjectDict(request_user=sys_user)
    Quest = s().M.model('Quest', ctx)
    Video = s().M.model('Video', ctx)
    quests = yield Quest.get(limit=10000)
    for q in quests:
        if q.status == QUEST_STATUSES.finished:
            videos = yield Video.get(_ids=[q.members.bot.video_id])
            killed = bool(q.dead_list)
            for v in videos:
                comments, _ = yield v.comment_recent()
                if not comments:
                    comment = "победа: %s" % ("Беглец" if not killed else "Охотники")
                    r, _ = yield v.comment_add(sys_user, comment, q.end_ts)
