# -*- coding: utf-8 -*-
from fabric.api import task, runs_once
from tornado.ioloop import IOLoop
from tornado import gen
from api_engine import s

items = [{"code":"http://oxfordvision.ru/","picked":False,"item_id":"21f13bb7bf79fb50bd6183dba8ae152f","item_type":"ammo","description":"Невский 25","photo":"http://136.243.52.19:8080/stage/hbqtemplate/AVBMB-lqbouIfRdZYMAm/items/21f13bb7bf79fb50bd6183dba8ae152f/8b90adcc943e11e5.jpg","location":{"coordinates":[59.9354929,30.3233638],"type":"point"},"ts":1448543205},{"code":"www.money-honey.ru","picked":False,"item_id":"b1b47906a3912ce57d6047d1e8d68462","item_type":"ammo","description":"Переход Невский пр. Гос.Двор.","photo":"http://136.243.52.19:8080/stage/hbqtemplate/AVBMB-lqbouIfRdZYMAm/items/b1b47906a3912ce57d6047d1e8d68462/54f24130945d11e5.jpg","location":{"coordinates":[59.9342913,30.3340271],"type":"point"},"ts":1448556428},{"code":"www.estrada.spb.ru www.facebook.com/TeatrEstradyImARajkina http://vk.com/teatrestrady_spb","picked":False,"item_id":"fb738b3bf4fd3562228b976518c2f399","item_type":"ammo","description":"Между домами невский 36 и 38","photo":"http://136.243.52.19:8080/stage/hbqtemplate/AVBMB-lqbouIfRdZYMAm/items/fb738b3bf4fd3562228b976518c2f399/6038d2d494da11e5.jpg","location":{"coordinates":[59.9351004,30.3303833],"type":"point"},"ts":1448610134},{"code":"https://itunes.apple.com/ru/app/platius/id794999619?mt=8","picked":False,"item_id":"44a235b1486c4a42e0b1fd1d32c6d7eb","item_type":"ammo","description":"Невский 40-42","photo":"http://136.243.52.19:8080/stage/hbqtemplate/AVBMB-lqbouIfRdZYMAm/items/44a235b1486c4a42e0b1fd1d32c6d7eb/cf50a48094da11e5.jpg","location":{"coordinates":[59.9347446,30.3324858],"type":"point"},"ts":1448610320},{"code":"http://www.rusmuseum.ru/home/","picked":False,"item_id":"b8241d4816038fbaae2ce885e36bcba9","item_type":"ammo","description":"Невский 17","photo":"http://136.243.52.19:8080/stage/hbqtemplate/AVBMB-lqbouIfRdZYMAm/items/b8241d4816038fbaae2ce885e36bcba9/1609f9ea94df11e5.jpg","location":{"coordinates":[59.9358809,30.3206352],"type":"point"},"ts":1448612157},{"code":"http://www.znakomstva-spb.ru/","picked":False,"item_id":"f0817f0285b9973321dfc99adfa0b0ea","item_type":"ammo","description":"Невский 21","photo":"http://136.243.52.19:8080/stage/hbqtemplate/AVBMB-lqbouIfRdZYMAm/items/f0817f0285b9973321dfc99adfa0b0ea/cd4b1d5a94df11e5.jpg","location":{"coordinates":[59.9358628,30.3220343],"type":"point"},"ts":1448612464},{"code":"https://www.sberbank.ru/common/img/uploaded/secure/QR/","picked":False,"item_id":"df7774f2a6cf05186b2fa116aec053b6","item_type":"ammo","description":"невский 16","photo":"http://136.243.52.19:8080/stage/hbqtemplate/AVBMB-lqbouIfRdZYMAm/items/df7774f2a6cf05186b2fa116aec053b6/821972d494fb11e5.jpg","location":{"coordinates":[59.9366621,30.3179774],"type":"point"},"ts":1448624364},{"code":"http://rasp.orgp.spb.ru/1669","picked":False,"item_id":"1db3c925ea60e9bc79c2a8490482dc0b","item_type":"ammo","description":"малая морская 7","photo":"http://136.243.52.19:8080/stage/hbqtemplate/AVBMB-lqbouIfRdZYMAm/items/1db3c925ea60e9bc79c2a8490482dc0b/a89f7cf694fa11e5.jpg","location":{"coordinates":[59.9359335,30.3138229],"type":"point"},"ts":1448623999},{"code":"http://www.knigaplus.ru/","picked":False,"item_id":"0c610769aecdd75ac104c21f131c51f9","item_type":"ammo","description":"Большая морская 17","photo":"http://136.243.52.19:8080/stage/hbqtemplate/AVBMB-lqbouIfRdZYMAm/items/0c610769aecdd75ac104c21f131c51f9/d13f40be94ec11e5.jpg","location":{"coordinates":[59.9346244,30.3150715],"type":"point"},"ts":1448618054}]

@runs_once
@task
def update():
    """

    :return:
    """
    quests = IOLoop.current().run_sync(update_template)


@gen.coroutine
def update_template():
    """

    :return:
    """
    Template = s().M.model('Template')
    t = yield Template.get_one(_id="AVAopKvSV0RP390Uthb_")
    t.items = items
    yield t.save()
