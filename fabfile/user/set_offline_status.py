# -*- coding: utf-8 -*-
from fabric.api import task
from tornado import gen


@task
def run():
    """
    Set User.is_online to False and User.status to USER_STATUS.idle for all users.

    :return:
    """
    from tornado.ioloop import IOLoop
    IOLoop.current().run_sync(main)


@gen.coroutine
def main():
    """

    :return:
    """
    from api_engine import s
    from elasticutils import get_es
    from elasticsearch.helpers import scan
    from tornado.util import ObjectDict
    from api_engine.enums import USER_STATUS
    User = s().M.model('User')

    doc_type = User.mapper.get_mapping_type_name()
    index = User.mapper.get_index()
    es = get_es()
    scroll = scan(es, doc_type=doc_type, index=index)
    for r in scroll:
        args = ObjectDict(r['_source'], es_meta=ObjectDict(id=r['_id']))
        user = User(args)
        user.status = USER_STATUS.idle
        user.is_online = False
        yield user.save()
