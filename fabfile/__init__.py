# -*- coding: utf-8 -*-
from api_engine import s
try:
    s()
except Exception:
    pass
from . import scripts
try:
    from . import elastic
except Exception:
    pass
from . import _server
from . import hbquest
from . import wedcam
from . import video
from . import user
