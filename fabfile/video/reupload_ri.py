# -*- coding: utf-8 -*-
from fabric.api import task
from tornado import gen


@task
def run(path):
    """
    Reupload ri videos. Accept path param that contains files named like '<video_id>.flv'.

    :param path:
    :return:
    """
    from functools import partial
    from tornado.ioloop import IOLoop
    IOLoop.current().run_sync(partial(main, path))


@gen.coroutine
def main(path):
    """

    :param path:
    :return:
    """
    from os import listdir
    from os.path import join
    from api_engine import s
    from utils.video_loader import VideoLoader as loader
    Video = s().M.model('Video')

    ext = '.flv'
    target = 'ri'

    videos = [p for p in listdir(path) if p.endswith(ext)]
    for name in videos:
        video_id = name[:-len(ext)]
        video = yield Video.get_one(_id=video_id)
        if not video:
            print(('video_id', video_id, 'NOT FOUND'))
            continue
        print(('Starting reupload of video_id', video_id, 'for target', target))
        original_key = loader.get_original_key(video_id, video.reporter_id, target)
        loader.upload_s3(original_key, join(path, name))
        print(('Complete upload to s3 by key', original_key))
        upload_id = yield loader.reupload(video_id, target, original_key)
        if upload_id:
            print(('Started celery task for upload_id', upload_id))
