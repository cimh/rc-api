# -*- coding: utf-8 -*-
from fabric.api import task
from tornado import gen


@task
def run():
    """

    :return:
    """
    from tornado.ioloop import IOLoop
    IOLoop.current().run_sync(main)


@gen.coroutine
def main():
    """

    :return:
    """
    from api_engine import s
    from api_engine.enums import OFFICE_LOCATION, SAN_FRANCISCO_LOCATION
    from reelcam.utils import distance

    Video = s().M.model('Video')
    User = s().M.model('User')
    users = yield User.search(dict(location=OFFICE_LOCATION, radius=500, limit=5000))
    print((len(users)))
    for u in users:
        if distance(u.gps_location, OFFICE_LOCATION) <= 500:
            u.gps_location = SAN_FRANCISCO_LOCATION
            yield u.save()

    videos = yield Video.search(dict(location=OFFICE_LOCATION, radius=500, limit=5000))  # @TODO: Video.search
    print((len(videos)))
    for v in videos:
        if distance(v.gps_location, OFFICE_LOCATION) <= 500:
            v.gps_location = SAN_FRANCISCO_LOCATION
            yield v.save()
