# -*- coding: utf-8 -*-
from fabric.api import task
from tornado import gen
from tornado.ioloop import IOLoop


@task
def run(reporter_id, title, video_path, app_name="reelcam"):
    """
    Load Video (reporter_id, title, video_path, app_name='reelcam').

    :param reporter_id: str Reporter User ID.
    :param title: unicode Video title.
    :param video_path: str Video file path.
    :param app_name: str Application name.
    """
    from functools import partial
    func = partial(main, reporter_id, title, video_path, app_name)
    IOLoop.current().run_sync(func)


@gen.coroutine
def main(reporter_id, title, video_path, app_name="reelcam"):
    """

    :param reporter_id: str Reporter User ID.
    :param title: unicode Video title.
    :param video_path: str Video file path.
    :param app_name: str Application name.
    """
    from api_engine import s
    from utils.video_loader import VideoLoader
    User = s().M.model('User')

    reporter_user = yield User.get_one(_id=reporter_id)
    loader = VideoLoader(app_name, reporter_user)
    video = yield loader.load_video(video_path, title)
    if video:
        print(("Video", video.id, "was successfully uploaded."))
    else:
        print("Video upload failed.")

    return
