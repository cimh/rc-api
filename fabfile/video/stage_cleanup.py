# -*- coding: utf-8 -*-
import functools
import fabric.api
import tornado.gen


def delete_from_ceph():
    """

    :return:
    """
    print("Delete from Ceph:")
    from reelcam.uploader import get_uploader
    uploader = get_uploader()
    video_key_prefix = 'media/'
    for key in uploader.bucket.list():
        if not key.key.startswith(video_key_prefix):
            continue
        print(("{name}\t{size}\t{modified}".format(
            name=key.name,
            size=key.size,
            modified=key.last_modified,
        )))
        uploader.delete(key.key)
    print("Complete.")


@tornado.gen.coroutine
def delete_from_index():
    """

    :param timeout: float in seconds.
    """
    from elasticsearch.helpers import scan
    from elasticsearch.exceptions import NotFoundError
    from elasticutils import get_es
    from api_engine import s

    print("Delete from ES-index:")
    test_videos = ["AVQUxzqrNY9EUyXl5v2D", "AVPHHbps3jfMSmSS24_u", "AVOjgCKSRXSPHBmD1lLD", "AVO9CGFIRXSPHBmD1lOE"]
    Video = s().M.model('Video')
    doc_type = Video.mapper.get_mapping_type_name()
    index = Video.mapper.get_index()
    es = get_es()
    scroll = scan(es, doc_type=doc_type, index=index)
    count = 0
    for r in scroll:
        if r['_id'] in test_videos:
            continue
        try:
            result = es.delete(index=Video.mapper.get_index(),
                               doc_type=Video.mapper.get_mapping_type_name(),
                               id=r['_id'])
        except NotFoundError:
            pass
        else:
            if result:
                count += 1
    print(("deleted %d videos" % count))
    print("Complete.")


@tornado.gen.coroutine
def reset_user_counters():
    """

    :return:
    """
    print("Reset User counters:")
    from elasticutils import get_es
    from elasticsearch.helpers import scan
    from tornado.util import ObjectDict
    from api_engine import s
    User = s().M.model('User')
    doc_type = User.mapper.get_mapping_type_name()
    index = User.mapper.get_index()
    es = get_es()
    scroll = scan(es, doc_type=doc_type, index=index)
    for r in scroll:
        args = ObjectDict(r['_source'], es_meta=ObjectDict(id=r['_id']))
        user = User(args)
        user.total_video_duration = 0
        yield user.save()
    print("Complete.")


@tornado.gen.coroutine
def main(from_ceph=False, from_es=False, dry_run=True):
    """
    Delete all Video data via the Elasticsearch and the Ceph APIs.

    :param from_ceph: bool delete from Ceph.
    :param from_es: bool delete from Es-index.
    :param dry_run: bool The never real changes would here.
    """
    from api_engine import s
    if s().environment != "STAGE":
        print(("This task is only for STAGE environment, but your environment:", s().environment))
        return
    if dry_run:
        print("Dry RUN!")
        return
    if from_ceph:
        delete_from_ceph()
    if from_es:
        yield delete_from_index()
        yield reset_user_counters()
    return


@fabric.api.task
def run(from_ceph='false', from_es='false', dry_run='true'):
    """
    Delete all Video data via the Elasticsearch and the Ceph APIs.

    :param from_ceph: bool delete from Ceph.
    :param from_es: bool delete from Es-index.
    :param dry_run: bool The never real changes would here.
    """
    def to_boolean(s):
        if s == 'false':
            return False
        elif s == 'true':
            return True
        else:
            raise TypeError("Expected a 'true' or 'false'.")
    dry_run = to_boolean(dry_run)
    from_es = to_boolean(from_es)
    from_ceph = to_boolean(from_ceph)
    from tornado.ioloop import IOLoop
    result = IOLoop.current().run_sync(functools.partial(main, from_ceph, from_es, dry_run))
    return result
