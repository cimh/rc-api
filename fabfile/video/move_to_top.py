# -*- coding: utf-8 -*-
from fabric.api import task
from tornado import gen


@task
def run():
    """
    Set Video.ggg to 20.

    :return:
    """
    from tornado.ioloop import IOLoop
    IOLoop.current().run_sync(main)


@gen.coroutine
def main():
    """

    :return:
    """
    from api_engine import s
    Video = s().M.model('Video')

    if s().environment != "PRODUCTION":
        return

    ids = ['AVUCn2JhkZn4AuKbycub', 'AVUCoN2ukZn4AuKbycud', 'AVUCowDpkZn4AuKbycuf', 'AVUCpMqzkZn4AuKbycuh']

    videos = yield Video.get(_ids=ids)
    for video in videos:
        video.ggg = 60
        yield video.save()
