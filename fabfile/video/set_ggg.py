# -*- coding: utf-8 -*-
from fabric.api import task
from tornado import gen


@task
def run():
    """
    Set Video.count_comments.

    :return:
    """
    from tornado.ioloop import IOLoop
    IOLoop.current().run_sync(main)


@gen.coroutine
def main():
    """

    :return:
    """
    from api_engine import s
    from elasticutils import get_es
    from elasticsearch.helpers import scan
    from tornado.util import ObjectDict
    Video = s().M.model('Video')

    doc_type = Video.mapper.get_mapping_type_name()
    index = Video.mapper.get_index()
    es = get_es()
    scroll = scan(es, doc_type=doc_type, index=index)
    for r in scroll:
        args = ObjectDict(r['_source'], es_meta=ObjectDict(id=r['_id']))
        video = Video(args)
        video.ggg = 10
        yield video.save()

    ids = ["AVL_3F7nb579nh91rOb2",
           "AVL_wLBjb579nh91rObx",
           "AVL_pdvD5pkn9o76H9Z_",
           "AVL_pLsk5pkn9o76H9Z-",
           "AVL_oiGMb579nh91rObp",
           "AVL_f0cS5pkn9o76H9Z3",
           "AVL_fnZ6b579nh91rObl",
           "AVL_fA0eb579nh91rObk",
           "AVL_YAN0b579nh91rObh",
           "AVL_Xgdeb579nh91rObg",
           "AVL_Wshqb579nh91rObf",
           "AVL_T3LIb579nh91rObe",
           "AVL_JzvZb579nh91rObZ",
           "AVL_JvpJZe1YXa-SlMV8",
           "AVL_Id7Nb579nh91rObW",
           "AVL_HgcNb579nh91rObP",
           "AVL_JzvZb579nh91rObZ",
           "AVL_JvpJZe1YXa-SlMV8",
           "AVL_Id7Nb579nh91rObW",
           "AVL_HgcNb579nh91rObP",
           "AVL_EVBWb579nh91rObL",
           "AVL-9nzpZe1YXa-SlMV5",
           "AVL-7xif5pkn9o76H9Za"]

    for video_id in ids:
        video = yield Video.get_one(_id=video_id)
        if video:
            video.ggg = 1
            yield video.save()
