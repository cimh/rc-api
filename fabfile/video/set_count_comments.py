# -*- coding: utf-8 -*-
from fabric.api import task
from tornado import gen


@task
def run():
    """
    Set Video.count_comments.

    :return:
    """
    from tornado.ioloop import IOLoop
    IOLoop.current().run_sync(main)


@gen.coroutine
def main():
    """

    :return:
    """
    from api_engine import s
    s()
    from elasticutils import get_es
    from elasticsearch.helpers import scan
    from tornado.util import ObjectDict
    Comment = s().M.model('Comment')
    Video = s().M.model('Video')

    doc_type = Video.mapper.get_mapping_type_name()
    index = Video.mapper.get_index()
    es = get_es()
    scroll = scan(es, doc_type=doc_type, index=index)
    for r in scroll:
        args = ObjectDict(r['_source'], es_meta=ObjectDict(id=r['_id']))
        video = Video(args)
        count_comments = yield Comment.mapper.get(video_id=video.id, is_deleted=False).count()
        video.count_comments = count_comments
        yield video.save()
