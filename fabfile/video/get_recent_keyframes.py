# -*- coding: utf-8 -*-
from fabric.api import task
from tornado import gen


@task
def run(workdir):
    """
    Download recent wedcam RI videos and get theirs keyframes via ffmpeg.

    :param workdir: str working directory.
    """
    from tornado.ioloop import IOLoop

    def done(result): IOLoop.current().stop()
    IOLoop.current().add_future(main(workdir), done)
    IOLoop.current().start()


@gen.coroutine
def main(workdir):
    """

    :param workdir: str working directory.
    """
    import os
    import shlex
    from os.path import abspath
    from subprocess import Popen, PIPE
    from api_engine import s
    from reelcam.uploader import get_uploader
    from reelcam.models.upload import RI
    Video = s().M.model('Video', scope=s().get_scope("wedcam"))
    Wedding = s().M.model('Wedding')

    max_videos = 30

    trend_videos, _ = yield Wedding.trend_videos(limit=max_videos)
    best_videos, _ = yield Video.get_wedcam(recent=True, limit=max_videos)
    videos = []
    for tv, bv in zip(trend_videos, best_videos):
        if tv and tv.media_urls.get(RI):
            videos.append(tv)
        if bv and bv.media_urls.get(RI):
            videos.append(bv)
        if len(videos) >= max_videos:
            break
    uploader = get_uploader()
    workdir = abspath(workdir)
    os.chdir(workdir)
    for v in videos:
        os.makedirs(v.id)
        os.chdir(v.id)
        name = 'ri.flv'
        fp = open(name, 'wb')
        key = v.media_urls.get(RI)
        print(('Downloading ..\nkey:', key, '\npath:', os.getcwd()))
        uploader.download(key, fp)
        fp.close()
        print('.. COMPLETE')
        ffmpeg = 'ffmpeg -loglevel error -i %s -vf "select=eq(pict_type\,I)" ' \
                 '-vsync vfr -qscale:v 2 thumbnails-%%05d.jpeg' % name
        p = Popen(shlex.split(ffmpeg), stdout=PIPE, stderr=PIPE)
        print(('Starting ..\n', ffmpeg))
        p.communicate()
        print('.. COMPLETE')
        os.remove(name)
        os.chdir(workdir)
