# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch
from fabric.api import task, runs_once
from tornado import gen
from api_engine import s

from pprint import pprint
try:
    from esmanager import ESManager
except Exception as e:
    pprint(e)


@task
def setup():
    """
    Setup all necessary indices and templates in Elasticsearch.

    :return:
    """
    ESManager().setup(verbose=True)
    clear_redis()


@task
def setup_dry():
    """
    Dry run, not applying any changes to Elasticsearch database.

    :return:
    """
    ESManager(dry=True).setup(verbose=True)

@task
@runs_once
def reindex(old_name, new_name=None):
    """
    Reindex index <old_name> to <new_name>.

    :param old_name:
    :param new_name:
    :return:
    """
    import random
    from string import lowercase
    from elasticsearch.helpers import reindex as r
    es = Elasticsearch(hosts=[s().elastic.host], port=s().elastic.host.split(':')[1], timeout=300, max_retries=10)
    if new_name:
        n = new_name + "-v_" + ''.join(random.choice(lowercase) for _ in range(4))
    else:
        n = old_name + "-v_" + ''.join(random.choice(lowercase) for _ in range(4))
    r(es, old_name, n, chunk_size=10000)
    args = dict(actions=[
        dict(add=dict(alias=old_name, index=n))
    ])
    es.indices.delete(old_name)
    es.indices.update_aliases(args)


@task
@runs_once
def rename_type(index_name, old_name, new_name):
    """
    Renames mapping type name in <index_name> from <old_name> to <new_name>.

    :param index_name:
    :param old_name:
    :param new_name:
    :return:
    """
    es = Elasticsearch(hosts=[s().elastic.host], port=s().elastic.host.split(':')[1], timeout=300, max_retries=10)
    res = es.search(index=index_name, body={"query": {"match_all": {}}}, size=100000)
    for hit in res['hits']['hits']:
        es.delete(index=index_name, doc_type=hit['_type'], id=hit['_id'])
        es.index(index=index_name, doc_type=new_name, id=hit['_id'], body=hit['_source'])
    es.indices.refresh(index=index_name)


@task
@runs_once
def clear_redis():
    """

    :return:
    """
    from tornado.ioloop import IOLoop
    IOLoop.current().run_sync(__clear_redis)


@gen.coroutine
def __clear_redis():
    """

    :return:
    """
    keys = yield s().kvs.call("KEYS", "*:SESSION:*")
    for key in keys:
        conn_cnt = yield s().kvs.call("HGET", key, 'CONN_CNT')
        if isinstance(conn_cnt, str) and int(conn_cnt) <= 0:
            s().kvs.async_call("DEL", key)
