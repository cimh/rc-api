# -*- coding: utf-8 -*-
from .generate_config_file import generate_config_file
from . import uploader
from . import clear_excess_screenshots
