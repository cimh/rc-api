# -*- coding: utf-8 -*-
from fabric.api import task


@task
def photo_and_screens_migration():
    """

    :return:
    """
    from tornado.escape import json_decode
    from elasticutils import S, get_es
    from reelcam.mappers.video.video import VideoEntryMappingType as VM
    from reelcam.mappers.user import UserEntryMappingType as UM
    es = get_es()

    count = S(UM).count()
    result = S(UM).all()[: count]
    for r in result:
        try:
            photo_keys = json_decode(r.photo)
        except (TypeError, ValueError):
            continue
        else:
            es.update(UM.get_index(), UM.get_mapping_type_name(), r._id, body={'doc': dict(photo_keys=photo_keys)})

    count = S(VM).count()
    result = S(VM).all()[: count]
    for r in result:
        try:
            screenshot_keys = json.loads(r.shot_urls)
        except (TypeError, ValueError):
            continue
        else:
            es.update(VM.get_index(), VM.get_mapping_type_name(), r._id, body={'doc': dict(screenshot_keys=screenshot_keys)})