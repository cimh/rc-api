# -*- coding: utf-8 -*-
from fabric.api import task


@task
def generate_config_file():
    """
    Generates config file via configspec.

    :return:
    """
    from api_engine.settings import Settings
    print(('configspec:', Settings.configspec_path))

    Settings.generate_config_file()

    print(('config:', Settings.config_path))
