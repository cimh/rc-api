# -*- coding: utf-8 -*-
from fabric.api import task, runs_once
from copy import deepcopy
from elasticsearch.helpers import scan, bulk


@task
@runs_once
def run():
    from api_engine import s
    search = scan(s().get_es(), index='video-index', size=1000)
    actions = []
    for r in search:
        if r['_source']['screenshot_keys']:
            screenshot_keys = deepcopy(r['_source']['screenshot_keys'])
            for k in r['_source']['screenshot_keys']:
                k = int(k.replace('screen', ''))
                if k > 3:
                    screenshot_keys.pop('screen'+str(k))
            r['_source']['screenshot_keys'] = screenshot_keys
            actions.append(r)
    if actions:
        bulk(s().get_es(), index='video-index', doc_type='video-entry', actions=actions)
