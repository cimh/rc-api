# -*- coding: utf-8 -*-
from fabric.api import task


@task
def create_bucket():
    """
    Creates S3 bucket.
    :return:
    """
    from api_engine import s
    from reelcam.uploader.s3 import S3Uploader
    conn = S3Uploader.connect_s3(s().ceph.host, s().ceph.port, s().ceph.access_key, s().ceph.secret_key)
    bucket = S3Uploader.create_bucket(S3Uploader.get_bucket_name(), conn)
    if bucket:
        print(('OK:', bucket.name))
    else:
        print('FAIL')
