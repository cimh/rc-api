# -*- coding: utf-8 -*-
from fabric.api import task


@task
def run(upload_id):
    """
    Run upload_complete celery task (upload_id,).
    :param upload_id: str Upload ID.
    """
    from reelcam.scheduler import get_task
    task = get_task('upload_complete')
    result = task.delay(upload_id)
    print((result.id))
