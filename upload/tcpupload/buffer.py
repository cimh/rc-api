# -*- coding: utf-8 -*-
from collections import deque
from tornado import gen


class ChunkBuffer(object):
    """
    ChunkBuffer class.
    Left-push right-pop buffer working on Redis.

    """

    chunks = deque()

    def __init__(self, redis, key):
        """

        :param redis: tornadis.Client object.
        :param key: str Redis key.
        """
        self.redis = redis
        self.key = key

    @gen.coroutine
    def load(self):
        """
        Load Buffer state from Redis.

        :return:
        """
        llen = yield self.redis.call("LLEN", self.key)
        if llen:
            chunks = yield self.redis.call("LRANGE", self.key, -llen, -1)
            self.chunks.extend([len(c) for c in chunks])

    @gen.coroutine
    def push(self, data):
        """
        Push DATA to Buffer.

        :param data: str DATA.
        """
        self.chunks.appendleft(len(data))
        yield self.redis.call("LPUSH", self.key, data)

    @gen.coroutine
    def pop(self):
        """
        Pop DATA from Buffer.

        :return: str DATA.
        """
        self.chunks.pop()
        data = yield self.redis.call("RPOP", self.key)
        raise gen.Return(data)
