# -*- coding: utf-8 -*-
from struct import unpack
from tornado import gen
from tornado.ioloop import PeriodicCallback
from server.tcp import TCPStreamHandler
from api_engine import s
from .buffer import ChunkBuffer


EOT = '\x04'


class TCPUpload(TCPStreamHandler):
    """
    TCPUpload class.

    """

    started = set()  # TCPUpload objects set.

    key_prefix = 'UPLOAD'

    header_len = 2+2+32+64

    video_id = None
    target = None
    token = None

    buffer = None
    buffered_data_size = 0  # In bytes.
    flushed_data_size = 0  # In bytes.
    min_chunk_size = 5*1024*1024  # In bytes.

    flusher_time = 300  # In milliseconds.
    _flushing = False  # True if flush now.

    _finished = False
    _closed = False

    # Tornadis Client object.
    redis = None

    def __init__(self, connection, address, redis):
        """

        :param connection:
        :param address:
        :param redis:
        :return:
        """
        super(TCPUpload, self).__init__(connection, address)
        self.redis = redis
        self.fp = open('stream.upload', 'wb')
        self._read_stream()

    @classmethod
    def accept_connection(cls, connection, address):
        """

        :param connection:
        :param address:
        :return:
        """
        cls(connection, address, s().kvs)

    def on_close(self):
        """

        :return:
        """
        print('stream closed')
        self._closed = True

    @gen.coroutine
    def _read_stream(self):
        """

        :return:
        """
        header = yield self.stream.read_bytes(self.header_len)
        try:
            data_size, target, token, video_id = self.read_header(header)
            print((data_size, target, token, video_id))
        except TypeError:
            self.stream.close()
            return

        if not self.buffer:
            # video = yield self.check_video(token, video_id)
            # if not video:
            #     self.stream.close()
            #     return
            yield self.start_upload(target, video_id)

        if data_size <= 0:
            return

        data = yield self.stream.read_bytes(data_size)
        if data_size == 1 and data == EOT:
            self._finished = True
            self.stream.close()
            return

        yield self.new_chunk(data)
        return

    @classmethod
    def read_header(cls, header):
        """

        :param header: str chunk header.
        :return: tuple (data_size, target, token, video_id).
        """
        targets = {
            0x02: 'r',
            0x03: 'o'
        }
        video_id_len = 20  # Bytes.
        token_len = 32  # Bytes.

        _length, _target, _video_id, _token = unpack('!HH32s64s', header)
        if _target not in targets:
            raise TypeError
        target = targets[_target]

        video_id = _video_id[-video_id_len:]
        token = _token[-token_len:]

        data_size = _length - cls.header_len + 2
        return data_size, target, token, video_id

    @gen.coroutine
    def check_video(self, token, video_id):
        """

        :return:
        """
        Video = s().M.model('Video')

        video = yield Video.get_one(_id=video_id)
        if not video:
            return
        if token and token in [video.reporter_token, video.observer_token]:
            return
        raise gen.Return(video)

    @gen.coroutine
    def start_upload(self, target, video_id):
        """

        :param target:
        :param video_id:
        :return:
        """
        self.target = target
        self.video_id = video_id
        yield self.setup_buffer()
        if not self.started:
            self.run_flusher()
        self.started.add(self)
        return

    @gen.coroutine
    def setup_buffer(self):
        """

        :param key:
        :return:
        """
        key = self.get_key(self.key_prefix, self.video_id, self.target)
        buf = ChunkBuffer(self.redis, key)
        yield buf.load()
        self.buffer = buf
        self.buffered_data_size = sum(buf.chunks)
        return

    @gen.coroutine
    def new_chunk(self, data):
        """

        :param data:
        :return:
        """
        yield self.buffer.push(data)
        self.buffered_data_size += len(data)

    @classmethod
    def run_flusher(cls):
        """

        :return:
        """
        @gen.coroutine
        def flusher():
            print(('started:', cls.started))
            for u in list(cls.started):
                yield u.periodical_flush()

        cb = PeriodicCallback(flusher, cls.flusher_time)
        cb.start()

    @gen.coroutine
    def periodical_flush(self):
        """

        :return:
        """
        print('periodical_flush')
        if not self._flushing:
            if self._finished or self._closed:
                self.started.discard(self)

                if self.flushed_data_size < self.buffered_data_size:
                    chunk = yield self.flush_all()
                    yield self.write_chunk(chunk)

                if self._finished:
                    yield self.finish()

                if self._closed:
                    self.fp.close()
                return

            if self.need_flush():
                self._flushing = True
                chunk = yield self.flush_chunk()
                self._flushing = False
                if chunk:
                    yield self.write_chunk(chunk)

    @gen.coroutine
    def flush_all(self):
        """

        :return:
        """
        chunk = ''
        if self._flushing:
            raise gen.Return(chunk)
        self._flushing = True
        for i in range(0, len(self.buffer.chunks)):
            data = yield self.buffer.pop()
            chunk += data
            self.flushed_data_size += len(data)
        raise gen.Return(chunk)

    def need_flush(self):
        """

        :return:
        """
        return (self.buffered_data_size - self.flushed_data_size) >= self.min_chunk_size

    @gen.coroutine
    def flush_chunk(self):
        """

        :return:
        """
        chunk = ''
        while len(chunk) < self.min_chunk_size:
            data = yield self.buffer.pop()
            chunk += data
            self.flushed_data_size += len(data)
        raise gen.Return(chunk)

    @gen.coroutine
    def write_chunk(self, data):
        """

        :param data:
        :return:
        """
        self.fp.write(data)

    @gen.coroutine
    def finish(self):
        """

        :return:
        """
        print('finish')
        if not self.stream.closed():
            self.stream.close()
        yield self.redis.call("DEL", self.buffer.key)
        self.fp.close()
