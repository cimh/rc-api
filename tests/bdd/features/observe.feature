Feature: Observe API Handler


  Background:


  @skip
  @observe
  @observe.get_requests
  Scenario: Get all observing requests
    Given a not implemented test


  @skip
  @observe
  @observe.request
  Scenario: Request user for observing video
    Given a not implemented test


  @skip
  @observe
  @observe.accept
  Scenario: Accept observing request
    Given a not implemented test


  @skip
  @observe
  @observe.reject
  Scenario: Reject observing request
    Given a not implemented test


  @skip
  @observe
  @observe.reject_all
  Scenario: Reject all observing requests
    Given a not implemented test


  @skip
  @observe
  @observe.begin
  Scenario: Begin observing
    Given a not implemented test


  @skip
  @observe
  @observe.end
  Scenario: End observing
    Given a not implemented test

