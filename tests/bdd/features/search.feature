Feature: Search API Handler


  Background:


  @search
  @search.find
  Scenario: Find users and videos
    When I request for "search:find"
      Then I should see "bad_request" response
    When I request for 'search:find' with data '{"query": "greg"}'
      Then "users" should be present in "successful" response
      And "videos" should be present in "successful" response

