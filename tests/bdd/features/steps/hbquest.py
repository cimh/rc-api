# -*- coding: utf-8 -*- 
from tornado.testing import gen_test
from hamcrest import assert_that, equal_to, is_not
from behave import *
from tornado import escape


@when("I request for '{method}' with created HBQuest")
@gen_test
def step_impl(context, method):
    """

    :type context behave.runner.Context
    :param method:
    :param data:
    :return:
    """
    d = dict(method=method, data=dict(hbquest_id=context.feature.data.hbquest_id))
    context.request = context.api.request_class(context.request.ctx, d)
    context.response = yield context.api.call(context.request)


@when("I request for '{method}' with '{hbquest_member}' HBQuest member video with data '{data}'")
@gen_test
def step_impl(context, method, hbquest_member, data):
    """

    :type context behave.runner.Context
    :param method:
    :param hbquest_meber:
    :param data:
    :return:
    """
    d = dict(method=method, data=escape.json_decode(data))
    d['data'].update(video_id=context.feature.data.hbquest_members[hbquest_member]['video_id'])
    context.request = context.api.request_class(context.request.ctx, d)
    context.response = yield context.api.call(context.request)


@then('I should see "{hbquest_id}" HBQuest ID in request.ctx')
def step_impl(context, hbquest_id):
    """

    :type context behave.runner.Context
    :param status:
    :return:
    """
    assert_that(context.request.ctx.hbquest_id, equal_to(hbquest_id))


@then('I should NOT see "{hbquest_id}" HBQuest ID in request.ctx')
def step_impl(context, hbquest_id):
    """

    :type context behave.runner.Context
    :param status:
    :return:
    """
    assert_that(context.request.ctx.hbquest_id, is_not(equal_to(hbquest_id)))


@then('I should see created HBQuest ID in request.ctx')
def step_impl(context):
    """

    :type context behave.runner.Context
    :param status:
    :return:
    """
    assert_that(context.request.ctx.hbquest_id, equal_to(context.feature.data.hbquest_id))


@then('I should NOT see created HBQuest ID in request.ctx')
def step_impl(context):
    """

    :type context behave.runner.Context
    :param status:
    :return:
    """
    assert_that(context.request.ctx.hbquest_id, is_not(equal_to(context.feature.data.hbquest_id)))
