# -*- coding: utf-8 -*-
import sys
sys.path.append("../../../")
from behave import given
from tornado import gen
from tornado.testing import gen_test


# @TODO: make plain
LOGIN_GREG = {"method": "user:login", "data": {"login": "79673406857", "pass": "321"}}
LOGIN_MARY = {"method": "user:login", "data": {"login": "832080382", "pass": "321"}}
LOGIN_MILES = {"method": "user:login", "data": {"login": "872633390", "pass": "321"}}
LOGIN_SARAH = {"method": "user:login", "data": {"login": "453976469", "pass": "321"}}
LOGIN_MORIN = {"method": "user:login", "data": {"login": "152599164", "pass": "321"}}


@given("I am Greg")
@gen_test
def step_impl(context):
    """

    :type context behave.runner.Context
    :return:
    """
    if context.request.ctx.request_user:
        context.request.ctx.request_user = None
    context.request = context.api.request_class(context.request.ctx, LOGIN_GREG)
    context.response = yield context.api.call(context.request)


@given("I am Mary")
@gen_test
def step_impl(context):
    """

    :type context behave.runner.Context
    :return:
    """
    if context.request.ctx.request_user:
        context.request.ctx.request_user = None
    context.request = context.api.request_class(context.request.ctx, LOGIN_MARY)
    context.response = yield context.api.call(context.request)


@given("I am Miles")
@gen_test
def step_impl(context):
    """

    :type context behave.runner.Context
    :return:
    """
    if context.request.ctx.request_user:
        context.request.ctx.request_user = None
    context.request = context.api.request_class(context.request.ctx, LOGIN_MILES)
    context.response = yield context.api.call(context.request)


@given("I am Sarah")
@gen_test
def step_impl(context):
    """

    :type context behave.runner.Context
    :return:
    """
    if context.request.ctx.request_user:
        context.request.ctx.request_user = None
    context.request = context.api.request_class(context.request.ctx, LOGIN_SARAH)
    context.response = yield context.api.call(context.request)


@given("I am Morin")
@gen_test
def step_impl(context):
    """

    :type context behave.runner.Context
    :return:
    """
    if context.request.ctx.request_user:
        context.request.ctx.request_user = None
    context.request = context.api.request_class(context.request.ctx, LOGIN_MORIN)
    context.response = yield context.api.call(context.request)


@given("There are recorded videos in system")
def step_impl(context):
    """

    :type context behave.runner.Context
    :return:
    """
    return True


@given("There is a user profile in system")
def step_impl(context):
    """

    :type context behave.runner.Context
    :return:
    """
    return True


@given("a not implemented test")
def step_impl(context):
    """

    :type context behave.runner.Context
    :return:
    """
    raise NotImplementedError


@given("There are created HBQuest templates in system")
def step_impl(context):
    """

    :type context behave.runner.Context
    :return:
    """
    return True


@given("we have created HBQuest")
def step_impl(context):
    """

    :type context behave.runner.Context
    :return:
    """
    assert context.feature.data.hbquest_id is not None, "HBQuest is not created"
