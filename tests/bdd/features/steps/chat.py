# -*- coding: utf-8 -*-
from tornado.testing import gen_test
from tornado import escape
from behave import *
from hamcrest import assert_that, equal_to
from features.steps import save_method_name


@when("I request for '{method}' with created chat and message_id")
def step_impl(context, method):
    """
    :type context: behave.runner.Context
    :param method:
    :return:
    """
    d = dict(method=method, data=dict(chat_id=context.feature.data.chat_id, message_id=context.feature.data.message_id))
    save_method_name(context, method)
    context.request = context.api.request_class(context.request.ctx, d)
    context.response = yield context.api.call(context.request)


@when("I request for '{method}' with created chat and data '{data}'")
@gen_test
def step_impl(context, method, data):
    """

    :type context behave.runner.Context
    :param method:
    :param data:
    :return:
    """
    d = dict(method=method, data=escape.json_decode(data))
    d['data'].update({'chat_id': context.feature.data.chat_id})
    save_method_name(context, method)
    context.request = context.api.request_class(context.request.ctx, d)
    context.response = yield context.api.call(context.request)


@when("I request for '{method}' with message_id saved in ctx.feature")
@gen_test
def step_impl(context, method):
    """

    :type context behave.runner.Context
    :param method:
    :return:
    """
    d = dict(method=method, data=dict(message_id=context.feature.data.message_id))
    save_method_name(context, method)
    context.request = context.api.request_class(context.request.ctx, d)
    context.response = yield context.api.call(context.request)


@then("'{name}' value in data should be equal '{value}'")
def step_impl(context, name, value):
    """

    :type context behave.runner.Context
    :param name:
    :param value:
    :return:
    """
    assert_that(str(context.response['data'][name]), equal_to(value))
