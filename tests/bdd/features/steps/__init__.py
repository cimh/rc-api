# -*- coding: utf-8 -*-
from behave import *
import sys
sys.path.append("../../")
import os
from api_engine.apiresults import _200, _302, _304, _400, _401, _403
from hamcrest import assert_that, equal_to, has_key, has_entries, has_length, is_not
from tornado.httputil import HTTPServerRequest
from tornado import escape, gen
from uuid import uuid1
os.environ['ASYNC_TEST_TIMEOUT'] = '30.0'
from tornado.testing import gen_test
from utils.datetimemixin import dtm


def save_method_name(context, name=None):
    """

    :type context behave.runner.Context
    :param name:
    :return:
    """
    if name:
        setattr(context.feature.data, 'method', name)


@gen_test
async def mock_request(context):
    """

    :type context behave.runner.Context
    :param user:
    :return:
    """
    from api_engine.api import API
    request = HTTPServerRequest(uri='/')
    request.connection_id = uuid1().hex
    context.api = API
    request.type = context.api.request_class.WS
    request.ctx = context.api.context_class(request)
    await request.ctx.setup()
    context.request = context.api.request_class(request.ctx, {})


@when('I request for "{method}"')
@gen_test
def step_impl(context, method):
    """

    :type context behave.runner.Context
    :param method:
    :return:
    """
    data = dict(data={})
    data['method'] = method
    save_method_name(context, method)
    context.request = context.api.request_class(context.request.ctx, data)
    context.response = yield context.api.call(context.request)


@when("I request for '{method}' with data '{data}'")
@gen_test
def step_impl(context, method, data):
    """

    :type context behave.runner.Context
    :param method:
    :param data:
    :return:
    """
    d = {}
    d['method'] = method
    d['data'] = escape.json_decode(data)
    if method == "hbquest:create":
        d['data']['start_ts'] = dtm.now_ts() + 300
    save_method_name(context, method)
    context.request = context.api.request_class(context.request.ctx, d)
    context.response = yield context.api.call(context.request)


@when("I request for '{method}' with saved in context.feature '{fields}' and '{req_data}'")
@gen_test
def step_impl(context, method, fields, req_data):
    """

    :type context behave.runner.Context
    :param method:
    :param fields: str json array
    :param req_data:
    :return:
    """
    data = dict()
    fields = escape.json_decode(fields)
    for attr in fields:
        data[attr] = getattr(context.feature.data, attr, None)
    data.update(escape.json_decode(req_data))
    d = dict(method=method, data=data)
    save_method_name(context, method)
    context.request = context.api.request_class(context.request.ctx, d)
    context.response = yield context.api.call(context.request)


@when("I request for '{method}' with saved in context.feature '{fields}'")
@gen_test
def step_impl(context, method, fields):
    """

    :type context behave.runner.Context
    :param method:
    :param fields: str json array
    :return:
    """
    data = dict()
    fields = escape.json_decode(fields)
    for attr in fields:
        data[attr] = getattr(context.feature.data, attr, None)
    d = dict(method=method, data=data)
    save_method_name(context, method)
    context.request = context.api.request_class(context.request.ctx, d)
    context.response = yield context.api.call(context.request)


@then('"{num}" items should be present in "{status}" response')
def step_impl(context, num, status):
    """

    :type context behave.runner.Context
    :param num:
    :param status:
    :return:
    """
    if status == "successful":
        _status = _200
    assert_that(context.response, has_entries(status=_status))
    assert_that(context.response['data']['items'], has_length(int(num)))


@then('"{key}" should be present in "{status}" response')
def step_impl(context, key, status):
    """

    :type context behave.runner.Context
    :param key:
    :param status:
    :return:
    """
    if status == "successful":
        _eq = _200
    assert_that(context.response['status'], equal_to(_eq))
    assert_that(context.response['data'], has_key(key))


@then('I should see "{status}" response')
def step_impl(context, status):
    """

    :type context behave.runner.Context
    :param status:
    :return:
    """
    if status == "successful":
        _status = _200
    if status == "found":
        _status = _302
    if status == "not_modified":
        _status = _304
    if status == "bad_request":
        _status = _400
    if status == "unauthorized":
        _status = _401
    if status == "forbidden":
        _status = _403
    assert_that(context.response, has_entries(status=_status))


@then('I should see \'{_data}\' in "{status}" response data')
def step_impl(context, _data, status):
    """

    :type context behave.runner.Context
    :param _data:
    :param status:
    :return:
    """
    if status == "successful":
        _status = _200
    _data = escape.json_decode(_data)
    assert_that(context.response, has_entries(status=_status))
    assert_that(context.response, has_entries(data=_data))


@then("'{attr}' value should be saved in context.feature as '{f_attr}'")
def step_impl(context, attr, f_attr):
    """

    :type context behave.runner.Context
    :param attr:
    :param f_attr:
    :return:
    """
    setattr(context.feature.data, f_attr, context.response['data'].get(attr))


@then("event with same method name should be emitted")
def step_impl(context):
    """

    :type context behave.runner.Context
    :return:
    """
    event = context.evt_bus.q_events.pop()
    check_event_name(event)
    assert_that(event.data['method'], equal_to(context.feature.data.method))


@then("event with name '{name}' should be emitted")
def step_impl(context, name):
    """

    :type context behave.runner.Context
    :param name:
    :return:
    """
    event = context.evt_bus.q_events.pop()
    check_event_name(event)
    assert_that(event.data['method'], equal_to(str(name)))


@then("I should see '{keys}' keys in event data")
def step_impl(context, keys):
    """

    :type context behave.runner.Context
    :param keys:
    :return:
    """
    event = context.evt_bus.q_events.pop()
    check_event_name(event)
    keys = escape.json_decode(keys)
    for k in range(len(keys)):
        assert_that(event.data['data'], has_key(keys[k]))


@then("I should see '{keys}' keys in response data")
def step_impl(context, keys):
    """

    :type context behave.runner.Context
    :param keys:
    :return:
    """
    keys = escape.json_decode(keys)
    for k in range(len(keys)):
        assert_that(context.response.get('data'), has_key(keys[k]))


@then("I should see '{keys}' keys in '{nested}' in response data")
def step_impl(context, keys, nested):
    """

    :type context behave.runner.Context
    :param keys:
    :param nested:
    :return:
    """
    keys = escape.json_decode(keys)
    for k in range(len(keys)):
        assert_that(context.response.get('data').get(nested), has_key(keys[k]))


@then("I should see '{keys}' keys in '{nested}' in first item in response data with keys '{inner_keys}'")
def step_impl(context, keys, nested, inner_keys):
    """

    :type context behave.runner.Context
    :param keys:
    :param nested:
    :param inner_keys:
    :return:
    """
    keys = escape.json_decode(keys)
    inner_keys = escape.json_decode(inner_keys)
    for k in range(len(keys)):
        item = context.response.get('data').get(nested)[0]
        assert_that(item, has_key(keys[k]))
        for j in range(len(inner_keys)):
            assert_that(item[keys[k]], has_key(inner_keys[j]))


@then("i wait for '{seconds}' seconds")
@gen_test
def step_impl(context, seconds):
    """

    :type context behave.runner.Context
    :param seconds:
    :return:
    """
    yield gen.sleep(int(seconds))


def check_event_name(event):
    """

    :param event:
    :return:
    """
    assert_that(event.data['data'], has_key('event_user_id'))