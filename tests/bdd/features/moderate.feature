Feature: Moderate API Handler


  Background:


  @skip
  @moderate
  @moderate.block_video
  Scenario: Block video
    Given a not implemented test


  @skip
  @moderate
  @moderate.unblock_video
  Scenario: Unblock video
    Given a not implemented test


  @skip
  @moderate
  @moderate.block_reporter
  Scenario: Block reporter
    Given a not implemented test


  @skip
  @moderate
  @moderate.unblock_reporter
  Scenario: Unblock reporter
    Given a not implemented test


  @skip
  @moderate
  @moderate.block_user
  Scenario: Block user
    Given a not implemented test


  @skip
  @moderate
  @moderate.unblock_user
  Scenario: Unblock user
    Given a not implemented test


  @skip
  @moderate
  @moderate.video_list
  Scenario: Get list of online videos
    Given a not implemented test
