Feature: Proxy API Handler


  Background:


  @skip
  @proxy
  @proxy.login
  Scenario: Login for video proxy
    Given a not implemented test


  @proxy
  @proxy.online
  Scenario: Mark proxy as online
    Given I am Greg
    When I request for 'proxy:online' with data '{"proxy_port_video": 11091, "proxy_port_rtmp": 5990, "proxy_port_web": 11090}'
    Then I should see "successful" response

