Feature: HBQuest API Handler
  Background:


  @hbquest
  @hbquest.create
  Scenario: Create HBQuest
    Given I am Mary
    When I request for 'hbquest:create' with data '{"template_id":"AVAZ4x2rWpv5XSOE9zz0","start_ts":0,"hunter_id":"AUuloJIFhjGEJyVYv0KQJ","hunter_email":"hunter@hbquest.com","bot_id":"AUXTGE6KfE65BDkBBAk6e","bot_email":"bot@hbquest.com"}'
    Then I should see "successful" response
    And 'quest_id' value should be saved in context.feature as 'hbquest_id'
    And i wait for '1' seconds


  @skip
  @hbquest
  @hbquest.delete
  Scenario: Delete HBQuest
    Given a not implemented test


  @skip
  @hbquest
  @hbquest.edit
  Scenario: Edit HBQuest
    Given a not implemented test


  @hbquest
  @hbquest.get_list
  Scenario: Get list of HBQuests
    When I request for 'hbquest:get_list' with data '{"items": {"AVAZ9ANTWpv5XSOE9z0A": null}}'
    Then "1" items should be present in "successful" response


  @hbquest
  @hbquest.get
  Scenario: Get HBQuest
    Given I am Mary
    When I request for "hbquest:get"
    Then I should see "successful" response
    And 'members' value should be saved in context.feature as 'hbquest_members'


  @hbquest
  @hbquest.start
  Scenario: Start HBQuest
    Given I am Mary
    When I request for 'hbquest:start' with created HBQuest
    Then I should see "successful" response
    When I request for 'hbquest:start' with created HBQuest
    Then I should see "found" response

    Given I am Miles
    When I request for 'hbquest:start' with created HBQuest
    Then I should see "successful" response
    When I request for 'hbquest:start' with created HBQuest
    Then I should see "found" response

    Given I am Mary
    When I request for 'hbquest:start' with created HBQuest
    Then I should see "found" response

    Given I am Miles
    When I request for 'hbquest:start' with created HBQuest
    Then I should see "found" response


  @hbquest
  @hbquest.stop
  Scenario: Stop HBQuest
    Given I am Mary
    When I request for 'hbquest:stop' with created HBQuest
    Then I should see "successful" response
    And i wait for '1' seconds


  @hbquest
  @hbquest.live
  Scenario: Get live HBQuests
    Given I am Greg
    When I request for "hbquest:live"
    Then "1" items should be present in "successful" response


  @hbquest
  @hbquest.pickup
  Scenario: Pickup item
    Given I am Mary
    When I request for "hbquest:pickup"
    Then I should see "successful" response

    Given I am Miles
    # Bad lat lon, 71 meters from HBQuest Item.
    When I request for 'video:set_coords' with 'bot' HBQuest member video with data '{"lat": 59.936131238491264, "lon": 30.323363800000003, "time":0, "type":0, "seconds":0}'
    Then I should see "successful" response
#
#    When I request for 'hbquest:code' with data '{"type":"item", "code":"xyz"}'
#    Then I should see "forbidden" response

    # Good lat lon, 62 meters from HBQuest Item.
    When I request for 'video:set_coords' with 'bot' HBQuest member video with data '{"lat": 59.93605032234448, "lon": 30.323363800000003, "time":0, "type":0, "seconds":0}'
    Then I should see "successful" response
    # Bad lat lon, 100 meters from HBQuest Item.
    When I request for 'video:set_coords' with 'bot' HBQuest member video with data '{"lat": 59.93639196829755, "lon": 30.323363800000003, "time":0, "type":0, "seconds":0}'
    Then I should see "successful" response

    When I request for 'hbquest:code' with data '{"type":"item", "code":"xyz"}'
    Then I should see "successful" response


  @hbquest
  @hbquest.shoot
  Scenario: Shoot at runaway
    Given I am Mary
    When I request for "hbquest:shoot"
    Then I should see "successful" response

    Given I am Miles
    When I request for 'hbquest:code' with data '{"type":"member", "code":"greg"}'
    Then I should see "successful" response

    When I request for 'hbquest:code' with data '{"type":"member", "code":"morin"}'
    Then I should see "successful" response

    When I request for 'hbquest:code' with data '{"type":"member", "code":"greg"}'
    Then I should see "successful" response

    When I request for 'hbquest:code' with data '{"type":"member", "code":"greg"}'
    Then I should see "forbidden" response

    When I request for 'hbquest:code' with data '{"type":"member", "code":"morin"}'
    Then I should see "forbidden" response

    When I request for "hbquest:get"
    Then I should see "successful" response


  @hbquest
  @hbquest.recent
  Scenario: Get recent HBQuests
    Given I am Mary
    When I request for "hbquest:recent"
    Then I should see "successful" response
