Feature: Feedback API Handler


  Background:


  @skip
  @feedback
  @feedback.submit
  Scenario: Submits feedback data
    Given a not implemented test


  @feedback
  @feedback.request
  Scenario: Feedback request method
    When I request for 'feedback:request' with data '{"user_id":"AU7jyXiDPDf16PhX9l9f"}'
    Then I should see "successful" response


  @skip
  @feedback
  @feedback.contact_us
  Scenario: Feedback contact us method
    Given a not implemented test

    