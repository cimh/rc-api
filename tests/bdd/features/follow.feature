Feature: Follow API Handler


  Background:


  @follow
  @follow.count
  Scenario: Get count of follow requests
    Given I am Greg
    When I request for "follow:count"
    Then I should see '{"count": 0}' in "successful" response data


  @emit_event
  @follow
  @follow.follow
  Scenario: Follow user
    Given I am Greg
    When I request for 'follow:follow' with data '{"user_id": "AUuloJIFhjGEJyVYv0KQJ"}'
    Then I should see "successful" response
    And event with same method name should be emitted
    And i wait for '1' seconds
    
    
  @follow
  @follow.requests
  Scenario: Get follow requests
    Given I am Mary
    When I request for "follow:requests"
    Then "1" items should be present in "successful" response


  @emit_event
  @follow
  @follow.accept
  Scenario: Accept follow request
    Given I am Mary
    When I request for 'follow:accept' with data '{"follow_id": "AU7jyXiDPDf16PhX9l9f_AUuloJIFhjGEJyVYv0KQJ"}'
    Then I should see "successful" response
    And event with same method name should be emitted


  @emit_event
  @follow
  @follow.follow
  Scenario: Follow user
    Given I am Mary
    When I request for 'follow:follow' with data '{"user_id": "AU7jyXiDPDf16PhX9l9f"}'
    Then I should see "successful" response
    And event with same method name should be emitted


  @emit_event
  @follow
  @follow.discard
  Scenario: Discard follow request
    Given I am Greg
    When I request for 'follow:discard' with data '{"follow_id": "AUuloJIFhjGEJyVYv0KQJ_AU7jyXiDPDf16PhX9l9f"}'
    Then I should see "successful" response
    And event with same method name should be emitted
    
    
  @follow
  @follow.followers_count
  Scenario: Get count of followers
    Given I am Greg
    When I request for 'follow:followers_count' with data '{"user_id": "AUuloJIFhjGEJyVYv0KQJ"}'
    Then I should see '{"count": 1}' in "successful" response data


  @skip
  @emit_event
  @follow
  @follow.disfollow
  Scenario: Disfollow user
    Given I am Greg
    When I request for 'follow:disfollow' with data '{"user_id": "AUuloJIFhjGEJyVYv0KQJ"}'
    Then I should see "successful" response
    And event with same method name should be emitted


  @skip
  @follow
  @follow.is_followed
  Scenario: Is another user followed by user
    Given a not implemented test


  @skip
  @follow
  @follow.is_follower
  Scenario: Is another user following user
    Given a not implemented test


  @skip
  @emit_event
  @follow
  @follow.undisfollow
  Scenario: Undisfollow user
    Given I am Greg
    When I request for 'follow:undisfollow' with data '{"user_id": "AUuloJIFhjGEJyVYv0KQJ"}'
    Then I should see "successful" response
    And event with same method name should be emitted


  @follow
  @follow.unfollow
  Scenario: Unfollow user
    Given I am Greg
    When I request for 'follow:unfollow' with data '{"user_id": "AUuloJIFhjGEJyVYv0KQJ"}'
    Then I should see "successful" response
    And event with same method name should be emitted

