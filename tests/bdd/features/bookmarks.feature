Feature: Bookmarks API Handler


#  Background:
#    Given There are registered users in system:
#    | id                   | fullname |
#    | AU7jyXiDPDf16PhX9l9f | Greg     |
#    And There are recorded videos in system:
#    | id                   | owner_id             |
#    | AU7jyXiDPDj16PhX3l3b | AU8b8gQnWSdcks5oJlRQ |
#    | AU7jyXiDPDj16PhX3l2c | AU7jyXiDPDf16PhX9l9f |


  @bookmarks
  @bookmarks.mine
  Scenario: User checks his bookmarks
    Given I am Greg
    When I request for "bookmarks:mine"
    Then "1" items should be present in "successful" response
    And I should see '["has_more", "items"]' keys in response data


  @bookmarks
  @bookmarks.get
  Scenario: Get bookmark
    Given I am Greg
    When I request for 'bookmarks:get' with data '{"bookmark_id": "AU7jyXiDPDf16PhX9l9f_AU7jyXiDPDj16PhX3l2c"}'
    Then I should see "successful" response
    And I should see '["video_id", "user_id", "id", "ts"]' keys in response data


  @bookmarks
  @bookmarks.get_list
  Scenario: Get list of bookmarks
    Given I am Greg
    When I request for 'bookmarks:get_list' with data '{"items": {"AU7jyXiDPDf16PhX9l9f_AU7jyXiDPDj16PhX3l2c": 123}}'
    Then "1" items should be present in "successful" response
    And I should see '["AU7jyXiDPDf16PhX9l9f_AU7jyXiDPDj16PhX3l2c"]' keys in 'items' in first item in response data with keys '["video_id", "user_id", "id", "ts"]'


  @emit_event
  @bookmarks
  @bookmarks.delete
  Scenario: Remove video from bookmarks.
    Given I am Greg
    When I request for 'bookmarks:delete' with data '{"video_id": "AU7jyXiDPDj16PhX3l2c"}'
    Then I should see "successful" response
    And event with same method name should be emitted
    And i wait for '1' seconds


  @emit_event
  @bookmarks
  @bookmarks.add
  Scenario: Add video to bookmarks.
    Given I am Greg
    When I request for 'bookmarks:add' with data '{"video_id": "AU7jyXiDPDj16PhX3l3b"}'
    Then I should see "successful" response
    And event with same method name should be emitted
