Feature: Test unauth empty requests

  @empty
  @bookmarks
  @bookmarks.get
  Scenario: Get bookmark
    When I request for "bookmarks:get"
    Then I should see "bad_request" response


  @empty
  @bookmarks
  @bookmarks.get_list
  Scenario: Get list of bookmarks
    When I request for "bookmarks:get_list"
    Then I should see "bad_request" response


  @empty
  @bookmarks
  @bookmarks.add
  Scenario: Add video to bookmarks
    When I request for "bookmarks:add"
    Then I should see "bad_request" response


  @empty
  @bookmarks
  @bookmarks.delete
  Scenario: Delete video from bookmarks
    When I request for "bookmarks:delete"
    Then I should see "bad_request" response


  @empty
  @bookmarks
  @bookmarks.mine
  Scenario: Show my bookmarks
    When I request for "bookmarks:mine"
    Then I should see "unauthorized" response


  @empty
  @chat
  @chat.get
  Scenario: Get or create chat with user
    When I request for "chat:get"
    Then I should see "bad_request" response


  @empty
  @chat
  @chat.get_list
  Scenario: Get list of chats
    When I request for "chat:get_list"
    Then I should see "bad_request" response


  @empty
  @chat
  @chat.get_one
  Scenario: Get one chat
    When I request for "chat:get_one"
    Then I should see "bad_request" response


  @empty
  @chat
  @chat.get_recent
  Scenario: Get recent chats
    When I request for "chat:get_recent"
    Then I should see "unauthorized" response


  @empty
  @chat
  @chat.message_new
  Scenario: New message in chat
    When I request for "chat:message_new"
    Then I should see "bad_request" response


  @empty
  @chat
  @chat.message_read
  Scenario: Read a message in chat
    When I request for "chat:message_read"
    Then I should see "bad_request" response


  @empty
  @chat
  @chat.message_delete
  Scenario: Delete message from chat
    When I request for "chat:message_delete"
    Then I should see "bad_request" response


  @empty
  @chat
  @chat.message_read_all
  Scenario: Read all messages in chat
    When I request for "chat:message_read_all"
    Then I should see "unauthorized" response


  @empty
  @chat
  @chat.message_unread_count
  Scenario: Get count of unread messages
    When I request for "chat:message_unread_count"
    Then I should see "unauthorized" response


  @empty
  @feedback
  @feedback.submit
  Scenario: Submits feedback data
    When I request for "feedback:submit"
    Then I should see "bad_request" response


  @empty
  @feedback
  @feedback.contact_us
  Scenario: Feedback contact us method
    When I request for "feedback:contact_us"
    Then I should see "bad_request" response


  @empty
  @follow
  @follow.accept
  Scenario: Accept follow request
    When I request for "follow:accept"
    Then I should see "bad_request" response


  @empty
  @follow
  @follow.discard
  Scenario: Discard follow request
    When I request for "follow:discard"
    Then I should see "bad_request" response


  @empty
  @follow
  @follow.count
  Scenario: Get count of follow requests
    When I request for "follow:count"
    Then I should see "unauthorized" response


  @empty
  @follow
  @follow.disfollow
  Scenario: Disfollow user
    When I request for "follow:disfollow"
    Then I should see "bad_request" response


  @empty
  @follow
  @follow.follow
  Scenario: Follow user
    When I request for "follow:follow"
    Then I should see "bad_request" response


  @empty
  @follow
  @follow.followers_count
  Scenario: Get count of followers
    When I request for "follow:followers_count"
    Then I should see "unauthorized" response


  @empty
  @follow
  @follow.is_followed
  Scenario: Is another user followed by user
    When I request for "follow:is_followed"
    Then I should see "bad_request" response


  @empty
  @follow
  @follow.is_follower
  Scenario: Is another user following user
    When I request for "follow:is_follower"
    Then I should see "bad_request" response


  @empty
  @follow
  @follow.requests
  Scenario: Get follow requests
    When I request for "follow:requests"
    Then I should see "unauthorized" response


  @empty
  @follow
  @follow.undisfollow
  Scenario: Undisfollow user
    When I request for "follow:undisfollow"
    Then I should see "bad_request" response


  @empty
  @follow
  @follow.unfollow
  Scenario: Unfollow user
    When I request for "follow:unfollow"
    Then I should see "bad_request" response


  @empty
  @history
  @history.counters
  Scenario: Get history counters
    When I request for "history:counters"
    Then I should see "unauthorized" response

  @empty
  @history
  @history.get
  Scenario: Get history record
    When I request for "history:get"
    Then I should see "bad_request" response


  @empty
  @history
  @history.get_list
  Scenario: Get list of history records
    When I request for "history:get_list"
    Then I should see "bad_request" response


  @empty
  @history
  @history.read
  Scenario: Read history record
    When I request for "history:read"
    Then I should see "bad_request" response


  @empty
  @history
  @history.read_list
  Scenario: Read list of history records
    When I request for "history:read_list"
    Then I should see "bad_request" response


  @empty
  @history
  @history.read_all
  Scenario: Read all history records
    When I request for "history:read_all"
    Then I should see "bad_request" response


  @empty
  @observe
  @observe.get_requests
  Scenario: Get all observing requests
    When I request for "observe:get_requests"
    Then I should see "bad_request" response


  @empty
  @observe
  @observe.request
  Scenario: Request user for observing video
    When I request for "observe:request"
    Then I should see "bad_request" response


  @empty
  @observe
  @observe.accept
  Scenario: Accept observing request
    When I request for "observe:accept"
    Then I should see "bad_request" response


  @empty
  @observe
  @observe.reject
  Scenario: Reject observing request
    When I request for "observe:reject"
    Then I should see "bad_request" response


  @empty
  @observe
  @observe.reject_all
  Scenario: Reject all observing requests
    When I request for "observe:reject_all"
    Then I should see "bad_request" response


  @empty
  @observe
  @observe.begin
  Scenario: Begin observing
    When I request for "observe:begin"
    Then I should see "bad_request" response


  @empty
  @observe
  @observe.end
  Scenario: End observing
    When I request for "observe:end"
    Then I should see "bad_request" response


  @skip
  @empty
  @paypal
  @paypal.paypal_ipn_listener
  Scenario: PayPal IPN listener method
    When I request for "paypal:paypal_ipn_listener"
    Then I should see "bad_request" response


  @empty
  @proxy
  @proxy.login
  Scenario: Login for video proxy
    When I request for "proxy:login"
    Then I should see "bad_request" response


  @empty
  @proxy
  @proxy.online
  Scenario: Mark proxy as online
    When I request for "proxy:online"
    Then I should see "bad_request" response


  @empty
  @search
  @search.find
  Scenario: Find users and videos
    When I request for "search:find"
    Then I should see "bad_request" response


  @empty
  @user
  @user.get_online_users
  Scenario: Get online users
    Given I am Greg
    When I request for "user:get_online_users"
    Then I should see "successful" response
    And I should see '["items", "has_more"]' keys in response data


  ##@skip
  @empty
  @user
  @user.edit
  Scenario: Edit user profile
    When I request for "user:edit"
    Then I should see "unauthorized" response


  ##@skip
  @empty
  @user
  @user.followers
  Scenario: Get a list of followers
    When I request for "user:followers"
    Then I should see "unauthorized" response


  #@skip
  @empty
  @user
  @user.followed
  Scenario: Get a list of followed users
    When I request for "user:followed"
    Then I should see "unauthorized" response


  @empty
  @user
  @user.get
  Scenario: Get user profile
    When I request for "user:get"
    Then I should see "bad_request" response


  @empty
  @user
  @user.get_list
  Scenario: Get multiple user profiles
    When I request for "user:get_list"
    Then I should see "bad_request" response


  @empty
  @user
  @user.login_social
  Scenario: Login from social network
    When I request for "user:login_social"
    Then I should see "bad_request" response


  @empty
  @user
  @user.register
  Scenario: Register
    When I request for "user:register"
    Then I should see "bad_request" response


  @empty
  @user
  @user.restore_pw
  Scenario: Restore password
    When I request for "user:restore_pw"
    Then I should see "bad_request" response


  @empty
  @user
  @user.set_push_token
  Scenario: Set push token
    When I request for "user:set_push_token"
    Then I should see "bad_request" response


  @empty
  @user
  @user.setgps
  Scenario: Set GPS coords for user
    When I request for "user:setgps"
    Then I should see "bad_request" response


  @empty
  @user
  @user.setpw
  Scenario: Set password
    When I request for "user:setpw"
    Then I should see "bad_request" response


  @empty
  @user
  @user.settings
  Scenario: Get user settings
    When I request for "user:settings"
    Then I should see "successful" response
    And I should see '["gps_incognito_max_radius", "uploadhost", "system_user_id"]' keys in response data


  @empty
  @user
  @user.sms_get_code
  Scenario: Get SMS code
    When I request for "user:sms_get_code"
    Then I should see "bad_request" response


  @empty
  @user
  @user.sms_get_code_w_resp
  Scenario: Get SMS code in response for validation request
    When I request for "user:sms_get_code_w_resp"
    Then I should see "bad_request" response


  @empty
  @user
  @user.sms_validation
  Scenario: Validate user with SMS
    When I request for "user:sms_validation"
    Then I should see "bad_request" response


  @empty
  @user
  @user.social_bind
  Scenario: Bind social network
    When I request for "user:social_bind"
    Then I should see "bad_request" response


  @skip
  @empty
  @user
  @user.social_stats_24h
  Scenario: Get system stats
    Given I am Greg
    When I request for "user:social_stats_24h"
    Then I should see "unauthorized" response


  @empty
  @user
  @user.social_unbind
  Scenario: Unbind social network
    When I request for "user:social_unbind"
    Then I should see "bad_request" response


  @empty
  @user
  @user.swiper
  Scenario: Get Swiper
    When I request for "user:swiper"
    Then I should see "successful" response
    And I should see '["has_more", "items"]' keys in response data


  @empty
  @user
  @user.system
  Scenario: Get system user
    Given I am Greg
    When I request for "user:system"
    Then I should see "successful" response


  @empty
  @user
  @user.track
  Scenario: Track user
    When I request for "user:track"
    Then I should see "bad_request" response


  @empty
  @user
  @user.track_list
  Scenario: Track list of users
    When I request for "user:track_list"
    Then I should see "bad_request" response


  @empty
  @user
  @user.untrack
  Scenario: Untrack user
    When I request for "user:untrack"
    Then I should see "bad_request" response


  @empty
  @user
  @user.untrack_list
  Scenario: Untrack list of users
    When I request for "user:untrack_list"
    Then I should see "bad_request" response


  @empty
  @user
  @user.login
  Scenario: Login
    When I request for "user:login"
    Then I should see "bad_request" response


  @empty
  @user
  @user.delete
  Scenario: Delete user profile
    When I request for "user:delete"
    Then I should see "bad_request" response


  @empty
  @video
  @video.comment_add
  Scenario: Add comment
    When I request for "video:comment_add"
    Then I should see "bad_request" response


  @empty
  @video
  @video.comment_delete
  Scenario: Delete comment
    When I request for "video:comment_delete"
    Then I should see "bad_request" response


  @empty
  @video
  @video.comment_get_list
  Scenario: Get list of comments
    When I request for "video:comment_get_list"
    Then I should see "bad_request" response


  @empty
  @video
  @video.comment_recent
  Scenario: Get recent comments
    When I request for "video:comment_recent"
    Then I should see "bad_request" response


  @empty
  @video
  @video.count
  Scenario: Get count
    When I request for "video:count"
    Then I should see "bad_request" response


  @empty
  @video
  @video.create_offline
  Scenario: Create offline video
    When I request for "video:create_offline"
    Then I should see "bad_request" response


  @empty
  @video
  @video.delete
  Scenario: Delete video
    When I request for "video:delete"
    Then I should see "bad_request" response


  @empty
  @video
  @video.edit
  Scenario: Edit video
    When I request for "video:edit"
    Then I should see "bad_request" response


  @empty
  @video
  @video.find
  Scenario: Find video
    When I request for "video:find"
    Then I should see "bad_request" response


  @empty
  @video
  @video.finish
  Scenario: Finish video
    When I request for "video:finish"
    Then I should see "bad_request" response


  @empty
  @video
  @video.get
  Scenario: Get video
    When I request for "video:get"
    Then I should see "bad_request" response


  @empty
  @video
  @video.get_list
  Scenario: Get video list
    When I request for "video:get_list"
    Then I should see "bad_request" response


  @empty
  @video
  @video.get_permission
  Scenario: Get permissions for video
    When I request for "video:get_permission"
    Then I should see "bad_request" response


  @empty
  @video
  @video.get_place_name
  Scenario: Get nearby place name
    When I request for "video:get_place_name"
    Then I should see "unauthorized" response


  @empty
  @video
  @video.live
  Scenario: Video Live tab
    When I request for "video:live"
    Then I should see "successful" response
    And I should see '["has_more", "items"]' keys in response data
    
    
  @empty
  @video
  @video.live_msg
  Scenario: Video live message
    When I request for "video:live_msg"
    Then I should see "bad_request" response


  @empty
  @video
  @video.mine
  Scenario: Show my videos
    When I request for "video:mine"
    Then I should see "unauthorized" response


  @empty
  @video
  @video.near_me
  Scenario: Video Near me tab
    When I request for "video:near_me"
    Then I should see "successful" response
    And I should see '["has_more", "items"]' keys in response data
    
    
  @empty
  @video
  @video.new_view
  Scenario: +1 video view
    When I request for "video:new_view"
    Then I should see "bad_request" response


  @empty
  @video
  @video.pause
  Scenario: Pause video
    When I request for "video:pause"
    Then I should see "bad_request" response


  @empty
  @video
  @video.popular
  Scenario: Video Popular tab
    When I request for "video:popular"
    Then I should see "successful" response
    And I should see '["has_more", "items"]' keys in response data


  @empty
  @video
  @video.reanimate
  Scenario: Reanimate video
    When I request for "video:reanimate"
    Then I should see "bad_request" response


  @empty
  @video
  @video.recent
  Scenario: Video Recent tab
    When I request for "video:recent"
    Then I should see "successful" response
    And I should see '["has_more", "items"]' keys in response data


  @empty
  @video
  @video.x_recent
  Scenario: Video X-Recent full tab
    When I request for "video:x_recent"
    Then I should see "successful" response
    And I should see '["has_more", "items"]' keys in response data


  @empty
  @video
  @video.x_near_me
  Scenario: Video X-Near Me full tab
    When I request for "video:x_near_me"
    Then I should see "successful" response
    And I should see '["has_more", "items"]' keys in response data


  @empty
  @video
  @video.x_live
  Scenario: Video X-Live full tab
    When I request for "video:x_live"
    Then I should see "successful" response
    And I should see '["has_more", "items"]' keys in response data


  @empty
  @video
  @video.x_popular
  Scenario: Video X-Popular full tab
    When I request for "video:x_popular"
    Then I should see "successful" response
    And I should see '["has_more", "items"]' keys in response data


  @empty
  @video
  @video.record_uploaded
  Scenario: Video record uploaded
    When I request for "video:record_uploaded"
    Then I should see "bad_request" response


  @empty
  @video
  @video.set_ban
  Scenario: +1 ban
    When I request for "video:set_ban"
    Then I should see "bad_request" response


  @empty
  @video
  @video.set_coords
  Scenario: Video set coords
    When I request for "video:set_coords"
    Then I should see "bad_request" response


  @empty
  @video
  @video.set_dislike
  Scenario: +1 dislike
    When I request for "video:set_dislike"
    Then I should see "bad_request" response


  @empty
  @video
  @video.set_gadget_info
  Scenario: Video set gadget info
    When I request for "video:set_gadget_info"
    Then I should see "bad_request" response


  @empty
  @video
  @video.set_like
  Scenario: +1 like
    When I request for "video:set_like"
    Then I should see "bad_request" response


  @empty
  @video
  @video.set_offline_coords
  Scenario: Video set offline coords
    When I request for "video:set_offline_coords"
    Then I should see "bad_request" response


  @empty
  @video
  @video.set_permission_for_all
  Scenario: Sets permissions for all videos
    When I request for "video:set_permission_for_all"
    Then I should see "unauthorized" response


  @empty
  @video
  @video.social_post
  Scenario: Video social posting
    When I request for "video:social_post"
    Then I should see "bad_request" response


  @empty
  @video
  @video.create
  Scenario: Create video
    When I request for "video:create"
    Then I should see "bad_request" response


  @empty
  @video
  @video.start
  Scenario: Start video
    When I request for "video:start"
    Then I should see "bad_request" response


  @empty
  @video
  @video.start_view
  Scenario: Video view start
    When I request for "video:start_view"
    Then I should see "bad_request" response


  @empty
  @video
  @video.startcheck
  Scenario: Video start check
    When I request for "video:startcheck"
    Then I should see "bad_request" response


  @empty
  @video
  @video.stop_view
  Scenario: Video view stop
    When I request for "video:stop_view"
    Then I should see "bad_request" response


  @empty
  @video
  @video.track
  Scenario: Track video
    When I request for "video:track"
    Then I should see "bad_request" response


  @empty
  @video
  @video.track_list
  Scenario: Track list of videos
    When I request for "video:track_list"
    Then I should see "bad_request" response


  @empty
  @video
  @video.unset_ban
  Scenario: -1 ban
    When I request for "video:unset_ban"
    Then I should see "bad_request" response


  @empty
  @video
  @video.unset_dislike
  Scenario: -1 dislike
    When I request for "video:unset_dislike"
    Then I should see "bad_request" response


  @empty
  @video
  @video.unset_like
  Scenario: -1 like
    When I request for "video:unset_like"
    Then I should see "bad_request" response


  @empty
  @video
  @video.untrack
  Scenario: Untrack video
    When I request for "video:untrack"
    Then I should see "bad_request" response


  @empty
  @video
  @video.untrack_list
  Scenario: Untrack list of videos
    When I request for "video:untrack_list"
    Then I should see "bad_request" response

