Feature: Video API Handler


  Background:


  @video
  @video.comment_add
  Scenario: Add comment
    Given I am Greg
    When I request for 'video:comment_add' with data '{"video_id": "AU7jyXiDPDj16PhX3l3b", "body": "A comment for video."}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'comment_id'
    And i wait for '1' seconds


  @video
  @video.comment_get_list
  Scenario: Get list of comments
    Given I am Greg
    When I request for 'video:comment_get_list' with data '{"items": {"AU7jyXiDPDj16PhX3l3b": null}}'
    Then "1" items should be present in "successful" response


  @video
  @video.comment_recent
  Scenario: Get recent comments
    Given I am Greg
    When I request for 'video:comment_recent' with data '{"video_id": "AU7jyXiDPDj16PhX3l3b"}'
    Then "1" items should be present in "successful" response


  @video
  @video.comment_delete
  Scenario: Delete comment
    Given I am Greg
    When I request for 'video:comment_delete' with saved in context.feature '["comment_id"]' and '{"video_id": "AU7jyXiDPDj16PhX3l3b"}'
    Then I should see "successful" response


  @skip
  @video
  @video.count
  Scenario: Get count
    Given a not implemented test


  @skip
  @video
  @video.create_offline
  Scenario: Create offline video
    Given a not implemented test


  @skip
  @video
  @video.delete
  Scenario: Delete video
    Given a not implemented test


  @skip
  @video
  @video.edit
  Scenario: Edit video
    Given a not implemented test


  @skip
  @video
  @video.find
  Scenario: Find video
    Given I am Greg
    When I request for 'video:find' with data '{"subject": "excepteur"}'
    Then I should see "successful" response


  @skip
  @video
  @video.finish
  Scenario: Finish video
    Given I am Greg
    When I request for 'video:finish' with saved in context.feature '["video_id", "reporter_token"]'
    Then I should see "successful" response


  @video
  @video.get
  Scenario: Get video
    Given I am Greg
    When I request for 'video:get' with data '{"video_id": "AU7jyXiDPDj16PhX3l3b"}'
    Then I should see "successful" response
    And I should see '["id", "subject", "live", "count_bans", "count_comments", "count_dislikes", "count_likes", "count_views", "reporter_start_ts", "is_bookmarked", "is_banned", "is_disliked", "is_liked", "ts", "lat", "lon", "audio_url", "video_url", "tags", "duration", "permission", "record_uploaded", "shot_urls"]' keys in 'video' in response data


  @video
  @video.get_place_name
  Scenario: Get nearby place name
    Given I am Greg
    When I request for "video:get_place_name"
    Then I should see "successful" response
    And I should see '["title"]' keys in response data


  @video
  @video.get_list
  Scenario: Get video list
    Given I am Greg
    When I request for 'video:get_list' with data '{"items": {"AU7jyXiDPDj16PhX3l3b": 0, "AU7jyXiDPDj16PhX3l2c": 0}}'
    Then "2" items should be present in "successful" response


  @skip
  @video
  @video.get_permission
  Scenario: Get permissions for video
    Given a not implemented test


  @skip
  @video
  @video.get_place_name
  Scenario: Get nearby place name
    Given a not implemented test


  @video
  @video.i_follow
  Scenario: Video I follow tab
    Given I am Greg
    When I request for "video:i_follow"
    Then I should see "successful" response
    And I should see '["has_more", "items"]' keys in response data


  @skip
  @video
  @video.live_msg
  Scenario: Video live message
    Given a not implemented test


  @skip
  @video
  @video.new_view
  Scenario: +1 video view
    Given a not implemented test


  @skip
  @video
  @video.pause
  Scenario: Pause video
    Given a not implemented test


  @skip
  @video
  @video.reanimate
  Scenario: Reanimate video
    Given a not implemented test


  @skip
  @video
  @video.record_uploaded
  Scenario: Video record uploaded
    Given a not implemented test


  @skip
  @video
  @video.set_ban
  Scenario: +1 ban
    Given a not implemented test


  @skip
  @video
  @video.set_coords
  Scenario: Video set coords
    Given a not implemented test


  @emit_event
  @video
  @video.set_dislike
  Scenario: +1 dislike
    Given I am Sarah
    When I request for 'video:set_dislike' with data '{"video_id": "AU7jyXiDPDj16PhX3l3b"}'
    Then I should see "successful" response
    And event with name 'video:like' should be emitted


  @skip
  @video
  @video.set_gadget_info
  Scenario: Video set gadget info
    Given a not implemented test


  @emit_event
  @video
  @video.set_like
  Scenario: +1 like
    Given I am Greg
    When I request for 'video:set_like' with data '{"video_id": "AU7jyXiDPDj16PhX3l3b"}'
    Then I should see "successful" response
    And event with name 'video:like' should be emitted


  @skip
  @video
  @video.set_offline_coords
  Scenario: Video set offline coords
    Given a not implemented test


  @video
  @video.set_permission_for_all
  Scenario: Sets permissions for all videos
    Given I am Greg
    When I request for "video:set_permission_for_all"
    Then I should see "successful" response


  @skip
  @video
  @video.social_post
  Scenario: Video social posting
    Given a not implemented test
    

  @video
  @video.popular
  Scenario: Popular videos
    Given I am Greg
    When I request for 'video:popular' with data '{"offset": 20, "limit": 10}'
    Then I should see "successful" response
    And I should see '["has_more", "items"]' keys in response data


  @video
  @video.create
  Scenario: Create video
    Given I am Greg
    When I request for 'video:create' with data '{"title": "my test video"}'
    Then I should see "successful" response
    And 'video_id' value should be saved in context.feature as 'video_id'
    And 'reporter_token' value should be saved in context.feature as 'reporter_token'


  @video
  @video.start
  Scenario: Start video
    Given I am Greg
    When I request for 'video:start' with saved in context.feature '["video_id", "reporter_token"]'
    Then I should see "successful" response


  @video
  @video.start_view
  @video.stop_view
  Scenario: Video view start and stop
    Given I am Greg
    When I request for 'video:start_view' with saved in context.feature '["video_id"]'
    Then I should see "successful" response
    And i wait for '1' seconds

    When I request for 'video:stop_view' with saved in context.feature '["video_id"]'
    Then I should see "successful" response


  @video
  @video.finish
  Scenario: Finish video
    Given I am Greg
    When I request for 'video:finish' with saved in context.feature '["video_id", "reporter_token"]'
    Then I should see "successful" response


  @video
  @video.start
  Scenario: Start finished video
    Given I am Greg
    When I request for 'video:start' with saved in context.feature '["video_id", "reporter_token"]'
    Then I should see "forbidden" response


  @skip
  @video
  @video.startcheck
  Scenario: Video start check
    Given a not implemented test


  @skip
  @video
  @video.track
  Scenario: Track video
    Given a not implemented test


  @skip
  @video
  @video.track_list
  Scenario: Track list of videos
    Given a not implemented test


  @skip
  @video
  @video.unset_ban
  Scenario: -1 ban
    Given a not implemented test


  @emit_event
  @video
  @video.unset_like
  Scenario: -1 like
    Given I am Greg
    When I request for 'video:unset_like' with data '{"video_id": "AU7jyXiDPDj16PhX3l3b"}'
    Then I should see "successful" response
    And event with name 'video:unlike' should be emitted


  @video
  @video.untrack
  Scenario: Untrack video
    Given I am Greg
    When I request for 'video:untrack' with data '{"video_id": "AU7jyXiDPDj16PhX3l3b"}'
    Then I should see "successful" response


  @video
  @video.untrack_list
  Scenario: Untrack list of videos
    Given I am Greg
    When I request for 'video:untrack_list' with data '{"items": {"AU7jyXiDPDj16PhX3l3b": null}}'
    Then I should see "successful" response


  @skip
  @emit_event
  @video
  @video.unset_dislike
  Scenario: -1 dislike
    Given I am Sarah
    When I request for 'video:unset_dislike' with data '{"video_id": "AU7jyXiDPDj16PhX3l3b"}'
    Then I should see "successful" response
    And event with name 'video:unlike' should be emitted
