Feature: User API Handler


  Background:


  @emit_event
  @user
  @user.edit
  Scenario: Edit user profile
    Given I am Greg
    When I request for 'user:edit' with data '{"gender": "male", "gps_incognito": true}'
    Then I should see "successful" response
    And event with name 'user:gps_incognito' should be emitted


  @user
  @user.followers
  Scenario: Get a list of followers
    Given I am Greg
    When I request for 'user:followers' with data '{"user_id": "AU7jyXiDPDf16PhX9l9f"}'
    Then I should see "successful" response


  @user
  @user.followed
  Scenario: Get a list of followed users
    Given I am Greg
    When I request for 'user:followed' with data '{"user_id": "AU7jyXiDPDf16PhX9l9f"}'
    Then I should see "successful" response


  @user
  @user.get
  Scenario: Get user profile
    Given I am Greg
    When I request for 'user:get' with data '{"user_id": "AU7jyXiDPDf16PhX9l9f"}'
    Then I should see "successful" response
    And I should see '["id", "ts", "fullname", "last_name", "first_name", "rating", "count_bans", "count_likes", "count_dislikes", "lon", "lat", "phone", "country", "language", "language2", "gender", "birthday", "gps_incognito", "is_deleted", "follow_approve", "region", "user_type", "status", "is_online", "default_video_permission"]' keys in response data


  @user
  @user.get_list
  Scenario: Get multiple user profiles
    Given I am Greg
    When I request for 'user:get_list' with data '{"items": {"AU7jyXiDPDf16PhX9l9f": null}}'
    Then I should see "successful" response
    And I should see '["AU7jyXiDPDf16PhX9l9f"]' keys in 'items' in first item in response data with keys '["id", "ts", "fullname", "first_name", "last_name", "rating", "app_name", "count_bans", "count_likes", "count_dislikes", "lat", "lon", "phone", "country", "language", "language2", "gender", "birthday", "gps_incognito", "is_deleted", "follow_approve", "region", "user_type", "status", "is_online"]'


  @skip
  @user
  @user.login_social
  Scenario: Login from social network
    Given There is a user profile in system
    When I request for 'user:login_social' with data '{"provider": "facebook", "token": "token"}'
    Then I should see "successful" response


  @user
  @user.myprofile
  Scenario: Get my profile
    Given I am Greg
    When I request for "user:myprofile"
    Then I should see "successful" response
    And I should see '["id", "ts", "fullname", "first_name", "last_name", "rating", "app_name", "count_bans", "count_likes", "count_dislikes", "lat", "lon", "phone", "country", "language", "language2", "gender", "birthday", "gps_incognito", "is_deleted", "follow_approve", "region", "user_type", "status", "is_online", "default_video_permission", "phone", "last_login", "joined_ts"]' keys in response data


  @skip
  @user
  @user.register
  Scenario: Register
    Given a not implemented test


  @skip
  @user
  @user.restore_pw
  Scenario: Restore password
    Given I am Greg
    When I request for 'user:restore_pw' with data '<string>'
    Then I should get an sms
    And it should contain new password


  @skip
  @user
  @user.set_botmode
  Scenario: Set bot mode
    Given a not implemented test


  @skip
  @user
  @user.set_push_token
  Scenario: Set push token
    Given a not implemented test


  @skip
  @user
  @user.setgps
  Scenario: Set GPS coords for user
    Given a not implemented test


  @skip
  @user
  @user.setpw
  Scenario: Set password
    Given I am Greg
    When I request for 'user:setpw' with data '{"password": "456"}'
    Then I should see "successful" response

    
  @user
  @user.settings
  Scenario: Get user settings
    Given I am Greg
    When I request for 'user:settings' with data '{}'
    Then I should see "successful" response
    And I should see '["gps_incognito_max_radius", "help_email", "site_url", "uploadhost", "company_tel", "company_name", "system_user_id", "video_alias"]' keys in response data


  @skip
  @user
  @user.sms_get_code
  Scenario: Get SMS code
    Given a not implemented test


  @skip
  @user
  @user.sms_get_code_w_resp
  Scenario: Get SMS code in response for validation request
    Given a not implemented test


  @skip
  @user
  @user.sms_validation
  Scenario: Validate user with SMS
    Given a not implemented test


  @skip
  @user
  @user.social_bind
  Scenario: Bind social network
    Given a not implemented test


  @skip
  @user
  @user.social_unbind
  Scenario: Unbind social network
    Given a not implemented test


  @user
  @user.track
  Scenario: Track user
    Given I am Greg
    When I request for 'user:track' with data '{"user_id": "AUuloJIFhjGEJyVYv0KQJ"}'
    Then I should see "successful" response


  @user
  @user.track_list
  Scenario: Track list of users
    Given I am Greg
    When I request for 'user:track_list' with data '{"items": {"AUuloJIFhjGEJyVYv0KQJ": null}}'

    
  @user
  @user.untrack
  Scenario: Untrack user
    Given I am Greg
    When I request for 'user:untrack' with data '{"user_id": "AUuloJIFhjGEJyVYv0KQJ"}'
    Then I should see "successful" response


  @user
  @user.untrack_list
  Scenario: Untrack list of users
    Given I am Greg
    When I request for 'user:untrack_list' with data '{"items": {"AUuloJIFhjGEJyVYv0KQJ": null}}'


  @skip
  @user
  @user.logout
  Scenario: Logout
    Given I am Greg
    When I request for "user:logout"
    Then I should see "successful" response


  @skip
  @user
  @user.login
  Scenario: Login
    Given There is a user profile in system
    When I request for 'user:login' with data '{"login": "79673406857", "pass": "123"}'
    Then I should see "successful" response


  @skip
  @user
  @user.delete
  Scenario: Delete user profile
    Given I am Greg
    When I request for 'user:delete' with data '{"password": "123"}'
    Then I should see "successful" response

