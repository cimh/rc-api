Feature: Paypal API Handler


  Background:

  @skip
  @paypal
  @paypal.paypal_ipn_listener
  Scenario: PayPal IPN listener method
    When I request for 'paypal:paypal_ipn_listener' with data '{"payment_status": "Completed", "receiver_email":"anymail@example.com", "custom":"AU7jyXiDPDf16PhX9l9f", "item_name":"basic", "txn_id": "transaction_id"}'
    Then I should see "forbidden" response

