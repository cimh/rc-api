Feature: History API Handler


  Background:


  @skip
  @history
  @history.get
  Scenario: Get history record
    Given a not implemented test


  @skip
  @history
  @history.get_list
  Scenario: Get list of history records
    Given a not implemented test


  @skip
  @history
  @history.read
  Scenario: Read history record
    Given a not implemented test


  @skip
  @history
  @history.read_list
  Scenario: Read list of history records
    Given a not implemented test


  @skip
  @history
  @history.read_all
  Scenario: Read all history records
    Given a not implemented test


  @history
  @history.bookmarks
  Scenario: Get list of bookmarks history records
    Given I am Greg
    When I request for "history:bookmarks"
    Then I should see "successful" response


  @history
  @history.likes
  Scenario: Get list of video likes history records
    Given I am Greg
    When I request for "history:likes"
    Then I should see "successful" response


  @skip
  @history
  @history.echo
  Scenario: Get list of video likes and comments
    Given I am Greg
    When I request for "history:echo"
    Then I should see "successful" response


  @history
  @history.videos
  Scenario: Get list of videos history records
    Given I am Greg
    When I request for "history:videos"
    Then I should see "successful" response


  @history
  @history.followers
  Scenario: Get list of followers history records
    Given I am Greg
    When I request for "history:followers"
    Then I should see "successful" response


  @history
  @history.wedcam
  Scenario: Get list of wedcam history records
    Given I am Greg
    When I request for "history:wedcam"
    Then I should see "successful" response


  @history
  @history.counters
  Scenario: Get counters for history tabs
    Given I am Greg
    When I request for "history:counters"
    Then I should see "successful" response
    And I should see '["comments", "echo", "followers", "likes", "bookmarks", "wedcam"]' keys in response data
