Feature: Chat API Handler


  Background:


  @chat
  @chat.get_one
  Scenario: Get or create chat with user
    Given I am Greg
    When I request for 'chat:get_one' with data '{"user_id": "AUuloJIFhjGEJyVYv0KQJ"}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'chat_id'


  @skip
  @chat
  @chat.get_list
  Scenario: Get list of chats
    Given a not implemented test


  @chat
  @chat.get_recent
  Scenario: Get recent chats
    Given I am Greg
    When I request for "chat:get_recent"
    Then I should see "successful" response


  @emit_event
  @chat
  @chat.message_new
  Scenario: New message in chat
    Given I am Greg
    When I request for 'chat:message_new' with created chat and data '{"body": "msgmsg"}'
    Then I should see "successful" response
    And i wait for '1' seconds
    And 'id' value should be saved in context.feature as 'message_id'
    And event with same method name should be emitted


  @chat
  @chat.message_unread_count
  Scenario: Get count of unread messages
    Given I am Mary
    When I request for "chat:message_unread_count"
    Then I should see "successful" response
    And 'count' value in data should be equal '1'


  @chat
  @chat.message_read
  Scenario: Read a message in chat
    Given I am Greg
    When I request for 'chat:message_read' with message_id saved in ctx.feature
    Then I should see "forbidden" response

    Given I am Mary
    When I request for 'chat:message_read' with message_id saved in ctx.feature
    Then I should see "successful" response


  @chat
  @chat.message_read_all
  Scenario: Read all messages in chat
    Given I am Greg
    When I request for 'chat:message_read_all' with data '{}'
    Then I should see "successful" response


  @chat
  @chat.message_delete
  Scenario: Delete message from chat
    Given I am Greg
    When I request for 'chat:message_delete' with created chat and message_id
    Then I should see "successful" response

