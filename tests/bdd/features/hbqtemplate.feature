Feature: HBQTemplate API Handler
  Background:
    Given There are created HBQuest templates in system


  @hbqtemplate
  @hbqtemplate.create
  Scenario: Create HBQTemplate
    Given I am Greg
    When I request for 'hbqtemplate:create' with data '{"game_duration_seconds": 600, "spawn_point": {"bot": {"type":"point", "coordinates": [1.0, 2.0]}}, "title": "aabbcc", "items": [{"item_type":"ammo", "code": "xyz", "location": {"coordinates": [1.0, 2.0], "type": "point"}}], "runaways":[{"user_id":"AU7jyXiDPDf16PhX9l9f","qr_code":"greg","description":"Runaway #1"},{"user_id":"AUHE3O0y4jz7pGwtDyZRl","qr_code":"morin","description":"Runaway #2"}]}'
    Then I should see "successful" response
    And i wait for '1' seconds


  @hbqtemplate
  @hbqtemplate.all
  Scenario: Get all HBQTemplates
    Given I am Greg
    When I request for "hbqtemplate:all"
    Then "2" items should be present in "successful" response
    And i wait for '1' seconds


  @hbqtemplate
  @hbqtemplate.get
  Scenario: Get HBQTemplate by ID
    Given I am Greg
    When I request for 'hbqtemplate:get' with data '{"template_id":"AVAZ4x2rWpv5XSOE9zz0"}'
    Then I should see "successful" response


  @hbqtemplate
  @hbqtemplate.get_list
  Scenario: Get list of HBQTemplates
    Given I am Greg
    When I request for 'hbqtemplate:get_list' with data '{"items": {"AVAZ4x2rWpv5XSOE9zz0": null}}'
    Then "1" items should be present in "successful" response
