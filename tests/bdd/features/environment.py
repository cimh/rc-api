# -*- coding: utf-8 -*-
import sys

import requests

sys.path.append("../../")
from api_engine import s
from tornado.ioloop import IOLoop
from tornado import gen


def setup(context):
    """
    Create ES indices and load test data.

    :type context behave.runner.Context
    :return:
    """
    s().set_environment('TESTING')
    from esmanager import ESManager
    ESManager().setup(verbose=True)
    load(
        'data/bookmarks.json',
        'data/users.json',
        'data/proxy.json',
        'data/videos.json',
        'data/hbqtemplates.json',
        'data/hbquests.json'
    )


def before_all(context):
    """
    :type context behave.runner.Context
    :return:
    """
    setup(context)
    context.config.setup_logging()
    from features.steps import mock_request
    context.io_loop = IOLoop.current()
    # context.io_loop.set_blocking_log_threshold(0.05)
    context.evt_bus = s().evt_bus
    mock_request(context)


def before_feature(context, feature):
    """

    :param context:
    :param feature:
    :return:
    """
    from tornado.util import ObjectDict
    context.feature.data = ObjectDict()


def after_feature(context, feature):
    """

    :param context:
    :param feature:
    :return:
    """
    del context.feature.data


def before_scenario(context, scenario):
    """

    :type context behave.runner.Context
    :return:
    """
    pass


def after_scenario(context, scenario):
    """

    :type context behave.runner.Context
    :return:
    """
    pass


def before_tag(context, tag):
    """
    :type context behave.runner.Context
    :param tag:
    :return:
    """
    if tag == 'empty':
        context.api_user = context.request.ctx.request_user
        context.request.ctx.request_user = None


def after_tag(context, tag):
    """
    :type context behave.runner.Context
    :param tag:
    :return:
    """
    if tag =='empty':
        if getattr(context, 'api_user', None):
            context.request.ctx.set_user(context.api_user)

def before_step(context, step):
    """

    :type context behave.runner.Context
    :return:
    """
    pass


def after_all(context):
    """
    :type context behave.runner.Context
    :return:
    """
    reset_es()
    IOLoop.current().run_sync(reset_redis)


def reset_es():
    """

    :return:
    """
    from esmanager import ESManager
    ESManager().delete('*-testing-*')


@gen.coroutine
def reset_redis():
    """

    :return:
    """
    # Clean Redis testing keys.
    from utils import kvs
    mask = kvs.key_mask(s().get_scope())
    keys = yield s().kvs.call("KEYS", mask)
    s().kvs.async_call("DEL", *keys)


def load(*args):
    """
    Load test data from JSON files in ES.

    :param args: tuple File paths.
    :return:
    """
    import json
    es = s().get_es()
    for path in args:
        json_data = open(path)
        data = json.load(json_data)
        es.bulk(data, refresh=True)
        json_data.close()
        url = "%s/_refresh" % (s().elastic.host)
        requests.post(url)
