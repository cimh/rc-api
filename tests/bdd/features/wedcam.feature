Feature: WEDCam API Handler

  @wedcam
  @wedcam.marry
  Scenario: User Marry
    Given I am Sarah
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'user_id'

    Given I am Greg
    When I request for 'wedcam:marry' with saved in context.feature '["user_id"]'
    Then I should see "successful" response
    When I request for 'wedcam:marry' with saved in context.feature '["user_id"]'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'wedding_id'
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'user_id'

    Given I am Sarah
    When I request for 'wedcam:marry' with saved in context.feature '["user_id"]'
    Then I should see "successful" response

    Given I am Miles
    When I request for 'wedcam:marry' with saved in context.feature '["user_id"]'
    Then I should see "forbidden" response
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'user_id'

    Given I am Sarah
    When I request for 'wedcam:marry' with saved in context.feature '["user_id"]'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'wedding_id'


  @wedcam
  @wedcam.get
  Scenario: Get Wedding
    When I request for 'wedcam:get' with saved in context.feature '["wedding_id"]'
    Then I should see "successful" response


  @wedcam
  @wedcam.invite
  Scenario: User Invite
    Given I am Greg
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'user_id'

    Given I am Miles
    When I request for 'wedcam:invite' with saved in context.feature '["wedding_id", "user_id"]'
    Then I should see "successful" response

    Given I am Sarah
    When I request for 'wedcam:invite' with saved in context.feature '["wedding_id", "user_id"]'
    Then I should see "forbidden" response


  @wedcam
  @wedcam.accept
  Scenario: User Accept Invite
    Given I am Greg
    When I request for 'wedcam:accept' with saved in context.feature '["wedding_id"]'
    Then I should see "successful" response

    Given I am Mary
    When I request for 'wedcam:accept' with saved in context.feature '["wedding_id"]'
    Then I should see "forbidden" response


  @wedcam
  @wedcam.decline
  Scenario: User Decline Invite
    Given I am Greg
    When I request for 'wedcam:decline' with saved in context.feature '["wedding_id"]'
    Then I should see "successful" response

    When I request for 'wedcam:decline' with saved in context.feature '["wedding_id"]'
    Then I should see "successful" response


  @wedcam
  @wedcam.divorce
  Scenario: User Divorce
    Given I am Sarah
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'user_id'
    When I request for 'wedcam:divorce' with saved in context.feature '["user_id"]'
    Then I should see "forbidden" response

    Given I am Greg
    When I request for 'wedcam:divorce' with saved in context.feature '["user_id"]'
    Then I should see "forbidden" response

    Given I am Miles
    When I request for 'wedcam:divorce' with saved in context.feature '["user_id"]'
    Then I should see "successful" response
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'user_id'

    Given I am Sarah
    When I request for 'wedcam:divorce' with saved in context.feature '["user_id"]'
    Then I should see "forbidden" response


  @wedcam
  @wedcam.weddings
    Scenario: Get User Weddings
    Given I am Miles
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And i wait for '1' seconds
    And 'id' value should be saved in context.feature as 'user_id'
    When I request for 'wedcam:weddings' with saved in context.feature '["user_id"]'
    Then "0" items should be present in "successful" response
    And i wait for '1' seconds

    Given I am Sarah
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'user_id'
    When I request for 'wedcam:weddings' with saved in context.feature '["user_id"]'
    Then "0" items should be present in "successful" response

    Given I am Greg
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'user_id'
    When I request for 'wedcam:weddings' with saved in context.feature '["user_id"]'
    Then "0" items should be present in "successful" response


  @wedcam
  @wedcam.invites
  Scenario: Get User Invites
    Given I am Morin
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'user_id'
    When I request for 'wedcam:invites' with saved in context.feature '["user_id"]'
    Then "0" items should be present in "successful" response

    Given I am Greg
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'user_id'
    When I request for 'wedcam:invites' with saved in context.feature '["user_id"]'
    Then "0" items should be present in "successful" response

    Given I am Mary
    When I request for 'user:myprofile' with data '{}'
    Then I should see "successful" response
    And 'id' value should be saved in context.feature as 'user_id'
    When I request for 'wedcam:invites' with saved in context.feature '["user_id"]'
    Then "0" items should be present in "successful" response


  @wedcam
  @wedcam.best_videos
  Scenario: Get Wedding Videos
    When I request for 'wedcam:best_videos' with saved in context.feature '["wedding_id"]'
    Then "0" items should be present in "successful" response
