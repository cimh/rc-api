# -*- coding: utf-8 -*-
import sys
from api_engine import s
from invoke import task
from server.http import HTTPHandler
from server.websocket import WebSocketConnection
from tornado import web
from tornado.ioloop import IOLoop, PeriodicCallback
from tornado.httpserver import HTTPServer


@task
def admin():
    """
    Run admin panel.
    """
    port = '8999'
    app = admin_app()
    start_server(app, port)
    sys.stdout.write("Started Admin HTTP [:%s].\n" % port)
    io_loop()


@task
def http():
    """
    Run HTTP server.
    """
    port = s().tornado.upload_port
    app = http_app()
    start_server(app, port)
    sys.stdout.write("Started HTTP [:%s].\n" % port)
    io_loop()


@task
def ws():
    """
    Run WebSocket server.
    """
    port = s().tornado.port
    app = ws_app()
    start_server(app, port)
    sys.stdout.write("Started WS [:%s].\n" % port)
    io_loop()


def tcp(handler, port=8999):
    """

    :param handler:
    :param port:
    :return:
    """
    from server.tcp import TCPServer
    server = TCPServer(connection_handler=handler.accept_connection)
    server.start(port=port)


def admin_app():
    """

    :return:
    """
    from admin.handler import HBQuestAdminHTTPHandler
    app = web.Application([
        # (r'/static/(.*)', web.StaticFileHandler, {'path': s().cwd+'/admin/static'}),
        (r'/hbquest/*', HBQuestAdminHTTPHandler),
    ], template_path=s().cwd+'/admin/static',
       debug=s().environment == 'DEVELOPMENT')
    return app


def http_app():
    """

    :return:
    """
    app = web.Application([
        (r'.*', HTTPHandler),
    ], debug=s().environment == 'DEVELOPMENT')
    if s().sentry.on:
        app.sentry_client = s().sentry_client
    return app


def ws_app():
    """

    :return:
    """
    # subscribing for Redis events
    from server.profile import ProfileHandler
    WebSocketConnection.subscribe()
    app = web.Application([
        (r'/static/(.*)', web.StaticFileHandler, {'path': s().cwd+'/static'}),
        (r'/ws', WebSocketConnection),
        ('/_profile', ProfileHandler),
    ], debug=s().environment == 'DEVELOPMENT')
    if s().sentry.on:
        app.sentry_client = s().sentry_client
    return app

@task
def both():
    """
    Run both servers.
    """
    # if s().environment != "PRODUCTION":
    #     import guppy
    #     from guppy.heapy import Remote
    #     Remote.on()
    from server import logstash
    from upload.tcpupload.tcpupload import TCPUpload
    http_port = s().tornado.upload_port
    ws_port = s().tornado.port
    logstash_port = logstash.PORT
    tcp_upload_port = s().tornado.tcp_upload_port
    start_server(ws_app(), ws_port)
    start_server(http_app(), http_port)
    tcp(logstash.LogstashHandler, logstash_port)
    tcp(TCPUpload, tcp_upload_port)
    sys.stdout.write("Started WS [:%s] and HTTP [:%s] and TCP [:%s, :%s].\n" % (ws_port, http_port,
                                                                                logstash_port, tcp_upload_port))
    io_loop()


def start_server(app, port):
    """
    `~tornado.tcpserver.TCPServer.listen`: simple single-process::

    :param app: `tornado.web.Application`
    :param port: Port to run on
    :return:
    """
    server = HTTPServer(app, xheaders=True)
    server.listen(port)


def dot():
    """

    :return:
    """
    def print_dot():
        sys.stdout.write(".")
        sys.stdout.flush()
    cb = PeriodicCallback(print_dot, 20)
    cb.start()


def io_loop():
    """
    IOLoop handling after server start

    :return:
    """
    signals()
    blocking_threshold_sec = 5
    if s().environment not in ["PRODUCTION", "TESTING"]:
        blocking_threshold_sec = 0.5
    IOLoop.current().set_blocking_log_threshold(blocking_threshold_sec)
    IOLoop.current().start()


def signals():
    """

    :return:
    """
    import signal
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler)


def signal_handler(signum, frame):
    """

    :param signum:
    :param frame:
    :return:
    """
    sys.stdout.write("Caught signal %s..\n" % signum)
    IOLoop.current().add_callback_from_signal(WebSocketConnection.signal_handler, signum)
    IOLoop.current().stop()
