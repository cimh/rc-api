# -*- coding: utf-8 -*-
from api_engine import s
from tornado import gen
from invoke import task


try:
    from pprint import pprint
    s()
    from esmanager import ESManager
except Exception as e:
    pprint(e)


@task
def setup():
    """
    ESManager.setup()

    :return:
    """
    ESManager().setup(verbose=True)
    clear_redis()

@task
def setup_dry():
    """
    Dry run, not applying any changes to Elasticsearch database.

    :return:
    """
    ESManager(dry=True).setup(verbose=True)

@task
def clear_redis():
    """
    Clears *:SESSION:* keys in Redis.

    :return:
    """
    @gen.coroutine
    def __clear_redis():
        """

        :return:
        """
        keys = yield s().kvs.call("KEYS", "*:SESSION:*")
        for key in keys:
            conn_cnt = yield s().kvs.call("HGET", key, 'CONN_CNT')
            if isinstance(conn_cnt, str) and int(conn_cnt) <= 0:
                s().kvs.async_call("DEL", key)
    from tornado.ioloop import IOLoop
    IOLoop.current().run_sync(__clear_redis)
