[![build status](http://gitlab.dev.sv/ci/projects/1/status.png?ref=master)](http://gitlab.dev.sv/ci/projects/1?ref=master)
[![Documentation Status](http://rtd.dev.sv/projects/es-backend/badge/?version=latest)](http://es-backend.rtd.dev.sv/en/latest/?badge=latest)


# Skipping builds
There is one more way to skip all builds, if your commit message contains tag [ci skip]. In this case, commit will be created but builds will be skipped