# -*- coding: utf-8 -*-


class MediaStorage(object):
    """
    A MediaStorage class.

    """
    def __init__(self, uploader):
        """

        :param uploader:
        """
        self.uploader = uploader
        self.type = uploader.mediastorage_type
        self.extra = uploader.get_mediastorage_extra()

    def get_info(self, remote_ip=None):
        """

        :param remote_ip:
        :return:
        """
        host = self.uploader.get_nearest_host(remote_ip) if remote_ip else self.uploader.get_host()
        return dict(type=self.type, host=host, extra=self.extra)
