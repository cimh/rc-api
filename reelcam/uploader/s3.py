# -*- coding: utf-8 -*-
import logging
from .base import BaseUploader, BaseMultiPartUpload
import boto
import boto.s3.connection
from boto.s3.multipart import MultiPartUpload
from boto.exception import S3ResponseError


log = logging.getLogger(__name__)


# class S3Connection(boto.s3.connection.S3Connection):
#     """
#     Patch the original S3Connection to provide a control of s3 connection num_retries.
#
#     """
#     def make_request(self, method, bucket='', key='', headers=None, data='',
#                      query_args=None, sender=None, override_num_retries=None):
#         return super(S3Connection, self).make_request(method, bucket, key, headers, data,
#                                                       query_args, sender, override_num_retries=1)
#
# boto.s3.connection.S3Connection = S3Connection


class S3MultiPartUpload(BaseMultiPartUpload):
    """
    S3 Multipart Upload class.
    """

    def _upload_part(self, fp, part_num):
        """
        Upload (append) chunk to ``key``.
        Require Uploader.complete_multipart call when last chunk uploaded.
        :param fp:
        :param part_num:
        :return:
        """
        try:
            self.mp.upload_part_from_file(fp, part_num)
        except Exception as e:
            log.exception('upload_part(): Exception occured( "%s" ). key="%s", bucket="%s"', e.message, self.mp.key_name, self.mp.bucket)
            raise e
        else:
            log.debug('upload_part(): OK. key="%s", upload_id="%s", part_num="%d"', self.mp.key_name, self.mp.id, part_num)
            return part_num

    def _complete_upload(self):
        """
        Call this when last chunk has been uploaded.

        :return:
        """
        try:
            self.mp.complete_upload()
        except Exception as e:
            log.exception('complete_upload(): Exception occured( "%s" ). key="%s", upload_id="%s"', e.message, self.mp.key_name, self.mp.id)
            raise e
        else:
            log.debug('complete_upload(): OK. key="%s", upload_id="%s"', self.mp.key_name, self.mp.id)

    def _cancel_upload(self):
        """

        :return:
        """
        try:
            self.mp.cancel_upload()
        except Exception as e:
            log.exception('cancel_upload(): Exception occured( "%s" ). key="%s", upload_id="%s"', e.message, self.mp.key_name, self.mp.id)
            raise e
        else:
            log.debug('cancel_upload(): OK. key="%s", upload_id="%s"', self.mp.key_name, self.mp.id)


class S3Uploader(BaseUploader):
    """
    Ceph S3 Uploader class.
    """

    mediastorage_type = 's3'
    ACCESS_KEY = None

    @classmethod
    def get_size(cls, key):
        """

        :param key:
        :return:
        """
        pass

    @classmethod
    def bulk_delete(cls, keys):
        """

        :param keys:
        :return:
        """
        pass

    @classmethod
    def ping(cls, host):
        """

        :param host:
        :return:
        """
        pass

    @classmethod
    def get_hosts(cls):
        """

        :return:
        """
        return [':'.join([cls.HOST, str(cls.PORT)])]

    def __init__(self, host, port, access_key, secret_key, multipart_class=S3MultiPartUpload):
        """

        :param host:
        :param port:
        :param access_key:
        :param secret_key:
        :param multipart_class:
        """
        super(S3Uploader, self).__init__(multipart_class)
        self.conn = self.connect_s3(host, port, access_key, secret_key)
        self.__class__.HOST = host
        self.__class__.PORT = port
        try:
            self.bucket = self.conn.get_bucket(self.get_bucket_name(), validate=True)
        except S3ResponseError:
            log.error('Bucket not found: %s', self.get_bucket_name())
            self.bucket = None

    @classmethod
    def connect_s3(cls, host, port, access_key, secret_key):
        """

        :param host:
        :param port:
        :param access_key:
        :param secret_key:
        :return:
        """
        conn = boto.connect_s3(
            aws_access_key_id=access_key,
            aws_secret_access_key=secret_key,
            host=host,
            port=port,
            is_secure=False,
            calling_format=boto.s3.connection.OrdinaryCallingFormat(),
        )
        return conn

    @classmethod
    def create_bucket(cls, name, connection):
        """

        :param name:
        :param connection:
        :return:
        """
        return connection.create_bucket(name)

    def _get_multipart(self, key, upload_id=None):
        """

        :param key:
        :param upload_id:
        :return:
        """
        if upload_id is not None:
            mp = MultiPartUpload(self.bucket)
            mp.key_name = key
            mp.bucket_name = self.bucket.name
            mp.id = upload_id
        else:
            mp = self.bucket.initiate_multipart_upload(key)
        return mp

    def _upload(self, key, fp):
        """

        :param key:
        :param fp:
        :return:
        """
        try:
            k = self.bucket.new_key(key)
            k.set_contents_from_file(fp)
        except Exception as e:
            log.exception('upload(): Exception occured( "%s" ). key="%s", bucket="%s"', e.message, key, self.bucket)
            raise e
        else:
            log.debug('upload(): OK. key="%s", bucket="%s"', key, self.bucket)
            return key

    def download(self, key, fp, cb=None, err=None):
        """

        :param key:
        :param fp:
        :param cb: callback
        :param err: error callback (if needed)
        :return:
        """
        try:
            k = self.bucket.get_key(str(key))
            if not k:
                log.warn('download(): Key not found. key="%s", bucket="%s"', key, self.bucket)
                if err:
                    err()
                return None
            k.get_contents_to_file(fp)
        except Exception as e:
            log.exception('download(): Exception occured( "%s" ). key="%s", bucket="%s"', e.message, key, self.bucket)
            if err:
                err()
            return None
        else:
            log.debug('download(): OK. key="%s", bucket="%s"', key, self.bucket)
            if cb:
                cb(key)
            return key

    def _get(self, key):
        """

        :param key:
        :return:
        """
        try:
            k = self.bucket.get_key(key)
            if not k:
                log.warn('get(): Key not found. key="%s", bucket="%s"', key, self.bucket)
                return
            contents = k.get_contents_as_string()
        except Exception as e:
            log.exception('get(): Exception occured( "%s" ). key="%s", bucket="%s"', e.message, key, self.bucket)
            raise e
        else:
            log.debug('get(): OK. key="%s", bucket="%s"', key, self.bucket)
            return contents

    def _delete(self, key):
        """

        :param key:
        :return:
        """
        try:
            k = self.bucket.get_key(key)
            if not k:
                log.warn('delete(): Key not found. key="%s", bucket="%s"', key, self.bucket)
                return
            k.delete()
        except Exception as e:
            log.exception('delete(): Exception occured( "%s" ). key="%s", bucket="%s"', e.message, key, self.bucket)
            raise e
        else:
            log.debug('delete(): OK. key="%s", bucket="%s"', key, self.bucket)
            return key

    def _initiate_multipart(self, key):
        """

        :param key:
        :return:
        """
        try:
            mp = self._get_multipart(key)
        except Exception as e:
            log.exception('initiate_multipart(): Exception occured( "%s" ). key="%s", bucket="%s"', e.message, key, self.bucket)
            raise e
        else:
            log.debug('initiate_multipart(): OK. key="%s", upload_id="%s", bucket="%s"', key, mp.id, self.bucket)
            return mp, key

    def _resume_multipart(self, key, mpart_id):
        """

        :param key:
        :param mpart_id:
        :return:
        """
        try:
            mp = self._get_multipart(key, mpart_id)
        except Exception as e:
            log.exception('resume_multipart(): Exception occured( "%s" ). key="%s", bucket="%s"', e.message, key, self.bucket)
            raise e
        else:
            log.debug('resume_multipart(): OK. key="%s", upload_id="%s", bucket="%s"', key, mp.id, self.bucket)
            return mp, key

    def _set_acl(self, key, acl):
        """

        :param key:
        :param acl:
        :return:
        """
        try:
            k = self.bucket.get_key(key)
            if not k:
                log.warn('set_acl(): Key not found. key="%s", acl="%s", bucket="%s"', key, acl, self.bucket)
                return
            k.set_canned_acl('public-read')
        except Exception as e:
            log.exception('set_acl(): Exception occured( "%s" ). key="%s", acl="%s", bucket="%s"', e.message, key, acl, self.bucket)
            raise e
        else:
            log.debug('set_acl(): OK. key="%s", bucket="%s"', key, self.bucket)

    def get_url(self, key, host=None):
        """

        :param key:
        :param host: 127.0.0.1:80
        :return:
        """
        pattern = 'http://%%s/%s/%%s' % self.get_bucket_name()
        if not host:
            host = self.get_host()
        return pattern % (host, str(key))

    def get_mediastorage_extra(self):
        """

        :return:
        """
        return dict(bucket=self.get_bucket_name())
