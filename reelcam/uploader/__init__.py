# -*- coding: utf-8 -*-
from api_engine import s
from .s3 import S3Uploader


def get_uploader():
    """
    Returns an upload instance type of settings dependently.
    If take an uploader instance successfully save it into _uploader at module global namespace.

    :return:
    """
    global _uploader
    try:
        if _uploader:
            return _uploader
    except NameError:
        pass
    if s().mediastorage == 'CEPH':
        _uploader = S3Uploader(s().ceph.host, s().ceph.port, s().ceph.access_key, s().ceph.secret_key)
    else:
        raise TypeError('Unknown MEDIASTORAGE type: %r' % s().mediastorage)
    return _uploader
