# -*- coding: utf-8 -*-
from api_engine import s
from reelcam.utils import location_from_ip, distance
from api_engine.lib.uploader import Uploader, MultiPartUpload
from .mediastorage import MediaStorage
from utils import kvs


class MultipartUploadInterface(object):
    """
    MultiPart Upload Interface class.
    """

    def _handle(self, name, **kwargs):
        """

        :param name:
        :param kwargs:
        :return:
        """
        cb = kwargs.pop('cb')
        cb_err = kwargs.pop('cb_err')
        if 'key' in kwargs:
            kwargs['key'] = str(kwargs['key'])
        try:
            result = self._implemented(name)(**kwargs)
        except Exception as e:
            handler = self._error_handler(name)
            return handler(e, cb=cb_err, **kwargs)
        else:
            handler = self._ok_handler(name)
            return handler(result, cb=cb)

    def _error_handler(self, name):
        """

        :param name:
        :return:
        """
        return getattr(self, '_'.join(['handle', name, 'error']))

    def _ok_handler(self, name):
        """

        :param name:
        :return:
        """
        return getattr(self, '_'.join(['handle', name, 'ok']))

    def _implemented(self, name):
        """

        :param name:
        :return:
        """
        return getattr(self, ''.join(['_', name]))


class BaseUploader(MultipartUploadInterface, Uploader):
    """
    Uploader class.
    """

    KS = kvs.KS

    mediastorage_type = '<unknown>'
    NODES_COORDS = dict()  # gps locations cache

    @classmethod
    def cache_key_prefix(cls):
        """

        :return:
        """
        return cls.KS.join([s().app, s().mediastorage])

    @classmethod
    def get_bucket_name(cls):
        """
        
        :return:
        """
        return s().app.lower()

    @classmethod
    def create_bucket(cls, name, connection):
        """
        Creates a bucket.
        :param name:
        :param connection:
        :return:
        """
        raise NotImplementedError

    def __init__(self, multipart_class):
        """

        :param multipart_class:
        :return:
        """
        self.multipart_class = multipart_class

    @classmethod
    def get_host_dead_flag_cache_key(cls, host):
        """

        :param host:
        :return:
        """
        return cls.KS.join([cls.KS.join([cls.cache_key_prefix(), 'DEAD_FLAG']), host])

    @classmethod
    def set_host_dead_flag(cls, host):
        """

        :param host:
        :return:
        """
        cache_key = cls.get_host_dead_flag_cache_key(host)
        s().kvs.async_call("SET", str(cache_key), str(), 60)

    @classmethod
    def get_host_dead_flag(cls, host):
        """

        :param host:
        :return:
        """
        cache_key = cls.get_host_dead_flag_cache_key(host)
        res = None

        def cb(result):
            """

            :param result:
            :return:
            """
            res = result

        s().kvs.async_call("GET", str(cache_key), callback=cb)
        return False if res is None else True

    @classmethod
    def get_nearest_host(cls, ip):
        """

        :param ip:
        :return:
        """
        current_location = location_from_ip(ip)
        nodes_coords = cls.get_nodes_coords()
        distances = {host: distance(current_location, nodes_coords[host]) for host in nodes_coords}
        nearest_host = sorted(distances, key=distances.get)[0]
        return nearest_host

    @classmethod
    def get_nodes_coords(cls):
        """

        :return:
        """
        nodes_coords = cls.get_cached_nodes_coords()
        if not nodes_coords:
            nodes_coords = cls.make_nodes_coords()
            cls.cache_nodes_coords(nodes_coords)
        return nodes_coords

    @classmethod
    def make_nodes_coords(cls):
        """

        :return:
        """
        nodes_coords = {host: location_from_ip(host.split(':')[0]) for host in cls.get_hosts()}
        return nodes_coords

    @classmethod
    def cache_nodes_coords(cls, nodes_coords):
        """

        :param nodes_coords:
        :return:
        """
        cls.NODES_COORDS.update(nodes_coords)

    @classmethod
    def get_cached_nodes_coords(cls):
        """

        :return:
        """
        return cls.NODES_COORDS

    @classmethod
    def get_hosts(cls):
        """

        :return:
        """
        raise NotImplementedError
    
    def handle_upload_error(self, exc, cb=None, **kwargs):
        """

        :param exc:
        :param cb:
        :param kwargs:
        :return:
        """
        if cb:
            cb(kwargs['key'])
        return

    def handle_get_error(self, exc, cb=None, **kwargs):
        """

        :param exc:
        :param cb:
        :param kwargs:
        :return:
        """
        if cb:
            cb()
        
    def handle_delete_error(self, exc, cb=None, **kwargs):
        """

        :param exc:
        :param cb:
        :param kwargs:
        :return:
        """
        if cb:
            cb(kwargs['key'])
        return
        
    def handle_initiate_multipart_error(self, exc, cb=None, **kwargs):
        """

        :param exc:
        :param cb:
        :param kwargs:
        :return:
        """
        if cb:
            cb()
        return

    def handle_resume_multipart_error(self, exc, cb=None, **kwargs):
        """

        :param exc:
        :param cb:
        :param kwargs:
        :return:
        """
        return self.handle_initiate_multipart_error(exc, cb)

    def handle_set_acl_err(self, exc, cb=None, **kwargs):
        """

        :param exc:
        :param cb:
        :param kwargs:
        :return:
        """
        if cb:
            cb()
        
    def handle_upload_ok(self, result, cb=None):
        """

        :param result:
        :param cb:
        :return:
        """
        if cb:
            cb(result)
        return result
        
    def handle_get_ok(self, result, cb=None):
        """

        :param result:
        :param cb:
        :return:
        """
        raise NotImplementedError
        
    def handle_delete_ok(self, result, cb=None):
        """

        :param result:
        :param cb:
        :return:
        """
        if cb:
            cb(result)
        return result
        
    def handle_initiate_multipart_ok(self, result, cb=None):
        """

        :param result:
        :param cb:
        :return:
        """
        result = self.multipart_class(result[0], result[1])
        if cb:
            cb(result)
        return result
        
    def handle_resume_multipart_ok(self, result, cb=None):
        """

        :param result:
        :param cb:
        :return:
        """
        return self.handle_initiate_multipart_ok(result, cb)

    def handle_set_acl_ok(self, result, cb=None):
        """

        :param result:
        :param cb:
        :return:
        """
        if cb:
            cb()

    def upload(self, key, fp, cb=None, cb_err=None):
        """

        :param key:
        :param fp:
        :param cb:
        :param cb_err:
        :return:
        """
        return self._handle('upload', key=key, fp=fp, cb=cb, cb_err=cb_err)

    def download(self, key, fp, cb=None, err=None):
        """

        :param key:
        :param fp:
        :param cb:
        :param err:
        :return:
        """
        raise NotImplementedError
            
    def get(self, key, cb=None, cb_err=None):
        """

        :param key:
        :param cb:
        :param cb_err:
        :return:
        """
        return self._handle('get', key=key, cb=cb, cb_err=cb_err)
            
    def delete(self, key, cb=None, cb_err=None):
        """

        :param key:
        :param cb:
        :param cb_err:
        :return:
        """
        return self._handle('delete', key=key, cb=cb, cb_err=cb_err)
            
    def initiate_multipart(self, key, cb=None, cb_err=None):
        """

        :param key:
        :param cb:
        :param cb_err:
        :return:
        """
        return self._handle('initiate_multipart', key=key, cb=cb, cb_err=cb_err)
    
    def resume_multipart(self, key, mpart_id, cb=None, cb_err=None):
        """

        :param key:
        :param mpart_id:
        :param cb:
        :param cb_err:
        :return:
        """
        return self._handle('resume_multipart', key=key, mpart_id=mpart_id, cb=cb, cb_err=cb_err)

    def set_acl(self, key, acl, cb=None, cb_err=None):
        """

        :param key:
        :param acl:
        :param cb:
        :param cb_err:
        :return:
        """
        return self._handle('set_acl', key=key, acl=acl, cb=cb, cb_err=cb_err)

    def get_url(self, key):
        """

        :param key:
        :return:
        """
        raise NotImplementedError

    @classmethod
    def get_host(cls):
        """

        :return:
        """
        return '%s:%d' % (cls.HOST, cls.PORT)

    def get_mediastorage_extra(self):
        """

        :return:
        """
        raise NotImplementedError

    def get_mediastorage(self):
        """

        :return:
        """
        return MediaStorage(self)


class BaseMultiPartUpload(MultipartUploadInterface, MultiPartUpload):
    """
    Base Multipart Upload class.
    """

    def __init__(self, multipart, key):
        """

        :param multipart:
        :param key:
        """
        self.mp = multipart
        self.key = key
        self.id = multipart.id

    def handle_upload_part_error(self, exc, cb=None, **kwargs):
        """

        :param exc:
        :param cb:
        :param kwargs:
        :return:
        """
        if cb:
            cb(self, kwargs['part_num'])
        return

    def handle_complete_upload_error(self, exc, cb=None, **kwargs):
        """

        :param exc:
        :param cb:
        :param kwargs:
        :return:
        """
        if cb:
            cb(self)
        return

    def handle_cancel_upload_error(self, exc, cb=None, **kwargs):
        """

        :param exc:
        :param cb:
        :param kwargs:
        :return:
        """
        raise NotImplementedError
        
    def handle_upload_part_ok(self, result, cb=None):
        """

        :param result:
        :param cb:
        :return:
        """
        if cb:
            cb(result)
        return result

    def handle_complete_upload_ok(self, result, cb=None):
        """

        :param result:
        :param cb:
        :return:
        """
        if cb:
            cb(self)
        return self

    def handle_cancel_upload_ok(self, result, cb=None):
        """

        :param result:
        :param cb:
        :return:
        """
        raise NotImplementedError
        
    def upload_part(self, fp, part_num, cb=None, cb_err=None):
        """

        :param fp:
        :param part_num:
        :param cb:
        :param cb_err:
        :return:
        """
        return self._handle('upload_part', fp=fp, part_num=part_num, cb=cb, cb_err=cb_err)
        
    def complete_upload(self, cb=None, cb_err=None):
        """

        :param cb:
        :param cb_err:
        :return:
        """
        return self._handle('complete_upload', cb=cb, cb_err=cb_err)
        
    def cancel_upload(self, cb=None, cb_err=None):
        """

        :param cb:
        :param cb_err:
        :return:
        """
        return self._handle('cancel_upload', cb=cb, cb_err=cb_err)
