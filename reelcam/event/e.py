# -*- coding: utf-8 -*-
from api_engine.lib import eventdriven


class E(object):
    """
    E class.
    """

    def __new__(cls):  # a Singleton
        """

        :param cls:
        :return:
        """
        return cls

    @classmethod
    def register(cls, event_name, data, recipients, obj):
        """

        :param event_name:
        :param data:
        :param recipients:
        :param obj:
        :return:
        """
        event = dict(name=event_name, data=data)
        for r in recipients:
            if isinstance(r, eventdriven.EventDriven):
                r.notify(obj, event)
        obj.emit_event(event)

    @classmethod
    def subscribe(cls, subscribers, instance):
        """

        :param subscribers:
        :param instance:
        :return:
        """
        list([s.subscribe(instance) for s in subscribers])

    @classmethod
    def unsubscribe(cls, subscribers, instance):
        """

        :param subscribers:
        :param instance:
        :return:
        """
        list([s.unsubscribe(instance) for s in subscribers])
