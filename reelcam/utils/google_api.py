# -*- coding: utf-8 -*-
from urllib.parse import urlencode
from tornado import gen
from tornado.escape import json_decode
from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from api_engine.enums import VIDEO_SUBJECT_MIN_LENGTH


def is_ascii(s):
    """

    :param s:
    :return:
    """
    try:
        s.decode('ascii')
        return True
    except:
        return False


class GoogleAPI(object):
    """
    Google places API.
    """

    URL_STREET = "https://maps.googleapis.com/maps/api/geocode/json"
    URL_PLACE = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
    RADIUS = 1000  # in meters
    API_KEY = 'AIzaSyCq6F1v6_i9M9BYUjnoBG1oXvS3Cb84_yU'

    # https://developers.google.com/places/supported_types
    TYPES = [
        "airport",
        "amusement_park",
        "aquarium",
        "art_gallery",
        "bakery",
        "bar",
        "bus_station",
        "cafe",
        "car_dealer",
        "car_wash",
        "casino",
        "church",
        "gym"
    ]

    LIMIT_PLACES = 9

    @gen.coroutine
    def get_street(self, lat, lon, language='en'):
        """
        Get street name via google geocode API.

        :param lat: latitude
        :param lon: longitude
        :return: list with one street or empty list
        """
        params = {
            'latlng': "%s,%s" % (lat, lon),
            'key': self.API_KEY,
            'language': language
        }
        req = HTTPRequest(
            self.URL_STREET + "?" + urlencode(params),
        )
        try:
            resp = yield AsyncHTTPClient().fetch(req)
            data = json_decode(resp.body)
            street = [data['results'][0]["formatted_address"].split(",")[0]]
        except:
            street = []

        raise gen.Return(street)

    @gen.coroutine
    def get_places(self, lat, lon, language='en'):
        """
        Get places via google places API limited LIMIT_PLACES var.

        :param lat: latitude
        :param lon: longitude
        :return:  list with places or empty list
        """
        params = {
            'location': "%s,%s" % (lat, lon),
            'radius': self.RADIUS,
            'key': self.API_KEY,
            'language': language,
        }

        req = HTTPRequest(
            self.URL_PLACE + "?" + urlencode(params),
        )
        try:
            resp = yield AsyncHTTPClient().fetch(req)
            data = json_decode(resp.body)
            places_names = [
                i["name"] for i in data['results'] if (len(i["name"]) >= VIDEO_SUBJECT_MIN_LENGTH)
            ]
        except:
            places_names = []

        raise gen.Return(places_names[:self.LIMIT_PLACES])

    @gen.coroutine
    def get_street_and_places(self, lat, lon, language):
        """
        Get nearest street name and nearest places.

        :param lat:
        :param lon:
        :return: list of street name and places
        """
        streets, places = yield [self.get_street(lat, lon, language),
                                 self.get_places(lat, lon, language)]
        raise gen.Return(streets + places)
