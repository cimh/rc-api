# -*- coding: utf-8 -*-
import re
import random
from base64 import b64decode
from io import StringIO
from math import pow, sqrt
from geopy.distance import vincenty
from geoip2.errors import AddressNotFoundError
from api_engine import s


def enum(*sequential, **named):
    """

    :param sequential:
    :param named:
    :return:
    """
    enums = dict(list(zip(sequential, list(range(len(sequential))))), **named)
    return type('Enum', (), enums)


def stringio_from_base64(string):
    """

    :param string:
    :return:
    """
    try:
        buf = b64decode(string)
    except TypeError:
        return None
    return StringIO(buf)


def location_from_ip(ip):
    """
    get location by ip
    :param ip:
    :return: coords from db if found or default geo coords
    """
    import geoip2.database
    global geoip_db_reader
    try:
        geoip_db_reader
    except NameError:
        geoip_db_reader = geoip2.database.Reader(s().geoip_path)

    try:
        response = geoip_db_reader.city(ip)
        return dict(
            lat=response.location.latitude,
            lon=response.location.longitude)
    except (IOError, ValueError, AddressNotFoundError):
        return default_location()


def get_country_code(phone=None, ip=None):
    """
    Get country code - a 2-letter uppercase ISO 3166-1 country code (e.g. "GB")
    if phone passed -> get country code by phone
    else -> get country code by location

    :return (str): country code or empty string(if something wrong)
    """
    import geoip2.database
    global geoip_db_reader
    from phonenumbers import parse, geocoder
    try:
        geoip_db_reader
    except NameError:
        geoip_db_reader = geoip2.database.Reader(s().geoip_path)

    if phone:
        try:
            country_code = geocoder.region_code_for_number(parse("+"+phone))
            if country_code is None:
                country_code = ""
        except:
            country_code = ""
    elif ip:
        try:
            country_code = geoip_db_reader.city(ip).country.iso_code
        except AddressNotFoundError:
            country_code = ""
    else:
        country_code = ""
    return country_code


def default_location():
    """
    make default location in radius 6 km
    from center of Easter Island

    :return: dict(lat,lon)
    """
    from math import pi, cos, sin
    # center point of Easter Island
    lat0 = s().default_gps_point['lat']
    lon0 = s().default_gps_point['lon']
    # convert the radius r into degrees
    r0 = 1/111.300
    # 7 km radius
    r = 6*r0
    # make random values in [0, 1)
    u = random.random()
    v = random.random()

    w = r * sqrt(u)
    t = 2 * pi * v
    x = w * cos(t)
    y = w * sin(t)

    x1 = x / cos(lon0)

    lat = lat0 + x1
    lon = lon0 + y

    return {
        'lat': lat,
        'lon': lon
    }


def distort_location(gps_location, max_radius):
    """

    :param gps_location:
    :param max_radius: kilometers
    :return:
    """
    from geopy.distance import distance
    point = distance().destination(
        (gps_location['lat'], gps_location['lon']),
        random.randint(0, 180),
        distance=random.uniform(0.01, max_radius))
    distorted_gps_location = dict(lat=point.latitude, lon=point.longitude)
    return distorted_gps_location


def distance(location1, location2):
    """
    Get distance between lat lon pairs in meters.

    :param location1: dict(lat=float, lon=float) latitude.
    :param location2: dict(lat=float, lon=float) longitude.
    :return:
    """
    p1 = float(location1['lat']), float(location1['lon'])
    p2 = float(location2['lat']), float(location2['lon'])
    distance = vincenty(p1, p2)
    return distance.meters


def break_to_name_and_index(name):
    """
    break name into base_name and possible postfix number
    example:
    Adam(1) - have to break into Adam, 1
    Silvester-Stallone(3) - to Silvester-Stallone,1
    Eva.Stallone - to Eva.Stallone,0

    :param name:
    :return:
    """
    # regexp to search such values ("Adam(1)", "Silvester-Stalone(3)")
    regex = re.compile('(.+)\((\d+)\)$', re.I | re.U)
    # res - list (Adam, 1) or
    res = regex.findall(name)
    return res[0] if len(res) else (name, 0)
