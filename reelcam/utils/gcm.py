# -*- coding: utf-8 -*-
import logging
from tornado.escape import json_encode
from tornado.httpclient import AsyncHTTPClient
from tornado.httpclient import HTTPRequest
from api_engine.enums import ANDROID, IOS, UNKNOWN

log = logging.getLogger(__name__)


class GCM(object):
    """
    Google Cloud Messaging class.
    """

    GCM_URL = 'https://android.googleapis.com/gcm/send'

    def __init__(self, api_key):
        """
        api_key : google api key
        """
        self.api_key = api_key

        self.headers = {
            'Authorization': 'key=%s' % self.api_key,
            'Content-Type': 'application/json',
        }

    @staticmethod
    def construct_payload(registration_ids, data=None, notification=None, collapse_key=None,
                          delay_while_idle=False, time_to_live=None, is_json=True, dry_run=False, priority=None):
        """
        Construct the dictionary mapping of parameters.
        Encodes the dictionary into JSON if for json requests.
        Helps appending 'data.' prefix to the plaintext data: 'hello' => 'data.hello'

        :return constructed dict or JSON payload
        """
        payload = {}
        if is_json:
            payload['registration_ids'] = registration_ids
            if data:
                payload['data'] = data
            if notification:
                payload['notification'] = notification
        else:
            payload['registration_id'] = registration_ids
            if data:
                for key, value in list(data.items()):
                    payload['data.%s' % key] = value

        if delay_while_idle:
            payload['delay_while_idle'] = delay_while_idle

        if time_to_live:
            payload['time_to_live'] = time_to_live

        if collapse_key:
            payload['collapse_key'] = collapse_key

        if dry_run:
            payload['dry_run'] = True

        if priority:
            payload['priority'] = priority

        if is_json:
            payload = json_encode(payload)

        return payload

    def json_request(self, registration_ids, data=None, notification=None, collapse_key=None,
                     delay_while_idle=False, time_to_live=None, dry_run=False, priority="high"):
        """
        Makes a JSON request to GCM servers

        :param registration_ids: list of the registration ids
        :param data: dict mapping of key-value pairs of messages
        :param notification:
        :param collapse_key:
        :param delay_while_idle:
        :param time_to_live:
        :param dry_run:
        :param priority:
        :return dict of response body from Google including multicast_id, success, failure, canonical_ids, etc
        """
        payload = self.construct_payload(
            registration_ids,
            data,
            notification,
            collapse_key,
            delay_while_idle,
            time_to_live,
            True,
            dry_run,
            priority,
        )

        request = HTTPRequest(
            self.GCM_URL,
            method="POST",
            headers=self.headers,
            body=payload,
        )

        def handle_request(response):
            """

            :param response:
            :return:
            """
            if response.error:
                log.error("Error: %s", response.error)
            else:
                log.debug(": %s\n".join(["GCM response", "request", "headers", ""]), response.body,
                         response.request.body, repr(response.request.headers._dict))
        AsyncHTTPClient().fetch(request, callback=handle_request)

    def send_push(self, e_name=None, e_data=None, e_notification=None, e_user_img=None, users=None,
                  body_loc_key=None, body_loc_args=None, app_name=None):
        """

        :param e_name:
        :param e_data:
        :param e_notification:
        :param e_user_img:
        :param users:
        :param body_loc_key:
        :param body_loc_args:
        :param app_name:
        :return:
        """
        if e_name in ["hbquest:user_start", "hbquest:next_runaway"]:
            return

        e_data['event'] = e_name
        e_data['img'] = e_user_img
        notification = dict(
            sound='default',
            badge=1,
            method=e_name,
            data=e_data,
            body_loc_key=body_loc_key,
            body_loc_args=body_loc_args,
        )
        if e_notification:
            notification.update(e_notification)

        android_tokens = set()
        other_tokens = set()
        ios_tokens = set()

        for u in users:
            android_tokens = u.get_tokens(ANDROID, app_name)
            if android_tokens:
                android_tokens.update(android_tokens)

            ios_tokens = u.get_tokens(IOS, app_name)
            if ios_tokens:
                other_tokens.update(ios_tokens)

            unknown_tokens = u.get_tokens(UNKNOWN, app_name)
            if unknown_tokens:
                other_tokens.update(unknown_tokens)

        data = dict(message=json_encode(e_data), body_loc_key=body_loc_key, body_loc_args=body_loc_args)
        if android_tokens or ios_tokens:
            if android_tokens:
                self.json_request(registration_ids=list(android_tokens), data=data,
                                  time_to_live=300)
            elif ios_tokens:
                self.json_request(registration_ids=list(ios_tokens),
                                  time_to_live=300, notification=notification)

        if other_tokens:
            self.json_request(registration_ids=list(other_tokens), data=data, time_to_live=300)
