# -*- coding: utf-8 -*-
from datetime import timedelta
from celery.schedules import crontab


class CeleryConf(object):
    """
    Celery app config class.
    """

    BROKER_TRANSPORT_OPTIONS = {
        # Setting to 1 hour to keep celery working when no tasks are present.
        'socket_timeout': 3600,
    }

    CELERYBEAT_SCHEDULE = {
        'kill_lost': {
            'task': 'reelcam.scheduler.periodic.kill_lost',
            'schedule': timedelta(seconds=60),
        },
        'clean_users': {
            'task': 'reelcam.scheduler.periodic.clean_users',
            'schedule': timedelta(days=1),
        },
        'clear_redis_events': {
            'task': 'reelcam.scheduler.periodic.clear_redis_events',
            'schedule': timedelta(days=1),
        },
        'remove_dead_proxy': {
            'task': 'reelcam.scheduler.periodic.remove_dead_proxy',
            'schedule': timedelta(seconds=60),
        },
        'wedcam_export_buys': {
            'task': 'reelcam.scheduler.periodic.wedcam_export_buys',
            'schedule': crontab(hour=21, minute=0)
        }
    }
