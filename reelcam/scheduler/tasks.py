# -*- coding: utf-8 -*-
from .app import app


@app.task(time_limit=600)
def upload_complete(_id):
    """
    An upload complete task.

    :param _id:
    :return:
    """
    from functools import partial
    from tornado.ioloop import IOLoop
    from api_engine import s
    Upload = s().M.model('Upload')
    IOLoop.current().run_sync(partial(Upload.complete_task, _id))


@app.task
def hbquest_finish_timeout(hbquest_id):
    """

    :param hbquest_id:
    :return:
    """
    from api_engine import s
    Quest = s().M.model('Quest')
    Quest.finish_timeout(hbquest_id)


@app.task
def hbquest_delete_unpaid(hbquest_id):
    """

    :param hbquest_id:
    :return:
    """
    from api_engine import s
    Quest = s().M.model('Quest')
    Quest.delete_unpaid(hbquest_id)


@app.task
def send_email(*args, **kwargs):
    """

    :param args:
    :param kwargs:
    :return:
    """
    from api_engine.lib.msg import email
    email.send(*args, **kwargs)
