# -*- coding: utf-8 -*-


def get_task(name):
    """

    :return:
    """
    from . import tasks
    _task = getattr(tasks, name, None)
    if not _task:
        raise NameError('No task \'%s\'' % name)
    else:
        return _task


def execute_task(name, *args, **kwargs):
    """

    :param name:
    :param args:
    :param kwargs:
    :return:
    """
    task = get_task(name)
    return task.delay(*args, **kwargs)
