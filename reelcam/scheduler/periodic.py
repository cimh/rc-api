# -*- coding: utf-8 -*-
from .app import app


USER_TTL = 7*24*60*60


@app.task
def kill_lost():
    """

    :return:
    """
    from tornado import gen
    from tornado.ioloop import IOLoop

    @gen.coroutine
    def main():
        """

        :return:
        """
        from tornado.util import ObjectDict
        from api_engine import s

        User = s().M.model('User')
        request_user = yield User.get_system()
        ctx = ObjectDict(client_ip="127.0.0.1", request_user=request_user)
        Video = s().M.model('Video', ctx)
        yield Video.kill_lost()

    IOLoop.current().run_sync(main)


@app.task
def clean_users():
    """

    :return:
    """
    from functools import partial
    from tornado.ioloop import IOLoop
    from api_engine import s
    from utils.datetimemixin import dtm
    from api_engine.enums import USER_TYPE
    User = s().M.model('User')

    query = {
        "query": {
            "filtered": {
                "query": {"match_all": {}},
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "term": {"is_sms_validated": False}
                            },
                            {
                                "missing": {"field": "social_auth"}
                            },
                            {
                                "or": [
                                    {
                                        "term": {"user_type": USER_TYPE.user}
                                    },
                                    {
                                        "term": {"user_type": USER_TYPE.bot}
                                    },
                                    {
                                        "term": {"user_type": USER_TYPE.invite}
                                    }
                                ]
                            },
                            {
                                "range": {
                                    "crete_ts": {
                                        "to": dtm.YmdHMS_from_ts(dtm.now_ts()-USER_TTL)
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
    }
    es = User.mapper.get_es()
    main = partial(es.delete_by_query, index=User.mapper.get_index(), doc_type=User.mapper.get_mapping_type_name(), body=query)
    IOLoop.current().run_sync(main)


@app.task
def clear_redis_events():
    """
    clear expired events from redis
    used in cron

    :return:
    """
    from tornado import gen
    from tornado.ioloop import IOLoop
    from utils.datetimemixin import dtm
    from datetime import timedelta
    from reelcam.models.mixins.event.subscriber import Subscriber
    from api_engine import s
    # in days
    REDIS_EVENTS_TTL_DAYS = int(s().ttl.redis_events / 86400)

    #########################################
    # 1. get expire instances ids (ins_ids) from expire_date sets (expire_{date})

    # 2. search events ids (e_ids) by instances ids and del instances keys (ins_ids)

    # 3. go through e_ids and remove ins_ids from sets

    # ################# START 1 #####################
    # get not expired keys and union instances ids which are not expired
    @gen.coroutine
    def main():
        """

        :return:
        """
        not_expired_keys = [Subscriber().expire_key(dtm.Ymd_from_dt(dtm.now() - timedelta(days=i)))
                            for i in range(REDIS_EVENTS_TTL_DAYS)]
        not_expired_instances_ids = yield s().kvs.call("SUNION", *not_expired_keys)

        # get expired keys and union instances ids which are seems expired
        expired_keys = [Subscriber().expire_key(dtm.Ymd_from_dt(dtm.now() - timedelta(days=i)))
                        for i in range(REDIS_EVENTS_TTL_DAYS, REDIS_EVENTS_TTL_DAYS+7)]
        seems_expired_instances_ids = yield s().kvs.call("SUNION", *expired_keys)
        # delete expired_keys
        s().kvs.async_call("DEL", *expired_keys)

        # get exactly expired ids
        expired_instances_ids = list(set(seems_expired_instances_ids) - set(not_expired_instances_ids))
        # ################## END 1 ######################

        # temporary
        # expired_instances_ids = not_expired_instances_ids
        # expired_instances_ids = {"test"}

        if expired_instances_ids:
            # ################# START 2 #####################
            # get events ids
            events_ids = yield s().kvs.call("SUNION", *expired_instances_ids)
            # delete expired_instances_ids
            s().kvs.async_call("DEL", *expired_instances_ids)
            # ################## END 2 ######################

            # ################# START 3 #####################
            for event in events_ids:
                s().kvs.async_call("SREM", event, *expired_instances_ids)
            # ################## END 3 ######################
            # print events_ids

    IOLoop.current().run_sync(main)


@app.task(time_limit=5)
def remove_dead_proxy():
    """

    :return:
    """
    from server.websocket import RedisProxyPingTracker
    from tornado import gen
    from tornado.ioloop import IOLoop
    from utils.datetimemixin import dtm
    from api_engine import s
    from elasticutils import get_es
    from elasticsearch.helpers import scan

    TIMEOUT_SECONDS = 10
    Proxy = s().M.model('Proxy')
    doc_type = Proxy.mapper.get_mapping_type_name()
    index = Proxy.mapper.get_index()

    @gen.coroutine
    def main():
        es = get_es()
        scroll = scan(es, doc_type=doc_type, index=index)
        for p in scroll:
            proxy_key = RedisProxyPingTracker.get_key(p['_id'])
            last_ping_ts = yield s().kvs.call("HGET", proxy_key, 'TS')
            if last_ping_ts and dtm.now_ts() - int(last_ping_ts) >= TIMEOUT_SECONDS:
                yield Proxy.delete(p['_id'])
                yield s().kvs.call("DEL", proxy_key)

    IOLoop.current().run_sync(main)


@app.task
def wedcam_export_buys():
    """

    :return:
    """
    from tornado.ioloop import IOLoop
    from tornado import gen

    @gen.coroutine
    def main():
        """

        :return:
        """
        from utils.datetimemixin import dtm
        from api_engine import s
        Wedding = s().M.model('Wedding')
        User = s().M.model('User')
        weddings = yield Wedding.gte_create_ts(dtm.ts_from_datetime(dtm.now()))
        if weddings:
            text = yield Wedding.email_text_buy(weddings)
            emails = ['slava.alennikov@seventel.ru',
                      'vladimir.khorev@gmail.com',
                      'yaroslav.mitrofanov@seventel.ru',
                      'darya.khoreva@seventel.ru']
            for email in emails:
                u = User(dict(email=email))
                u.send_email('wedcam', text, 'WedCam buy attempts ' + dtm.now_Ymd(), html=False)

    IOLoop.current().run_sync(main)
