# -*- coding: utf-8 -*-
import logstash
import sys
import socket
import traceback
from utils.datetimemixin import dtm
from api_engine import s


class CustomLogstashHandler(logstash.UDPLogstashHandler):
    """
    Custom LogStash Handler class.
    """

    def __init__(self, host, port=5959, message_type='logstash', tags=None, fqdn=False, version=0):
        """

        :param host:
        :param port:
        :param message_type:
        :param tags:
        :param fqdn:
        :param version:
        :return:
        """
        self.default_tags = [s().app.lower(), s().environment.lower()]
        if tags is not None:
            tags.extend(self.default_tags)
        else:
            tags = self.default_tags
        super(CustomLogstashHandler, self).__init__(host, port, message_type, tags, fqdn, version)

    def makePickle(self, record):
        """

        :param record:
        :return:
        """
        msg, args = record.msg, record.args
        record.msg, record.args = '', []
        result = super(CustomLogstashHandler, self).makePickle(record)
        record.msg, record.args = msg, args
        return result

    def send(self, s):
        """

        :param s:
        :return:
        """
        try:
            super(CustomLogstashHandler, self).send(s)
        except socket.error:
            traceback.print_exception(*sys.exc_info(), file=sys.stderr)
            sys.stderr.write('@@@@@> CustomLogstashHandler.send(), ts: %s, '
                             'message length: %d, '
                             'message head: %s, '
                             'message tail: %s\n' % (dtm.now_YmdHMSs(), len(s), s[:300], s[-150:]))
