# -*- coding: utf-8 -*- 
from ..base import BaseMapper


class MsgEntryMappingType(BaseMapper):
    """
    Instant Messaging Msg mapper class.
    """
