# -*- coding: utf-8 -*- 
from ..base import BaseMapper


class ChatEntryMappingType(BaseMapper):
    """
    Instant Messaging Chat mapper class.
    """
