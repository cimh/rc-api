# -*- coding: utf-8 -*-
from asyncelasticutils import S, F
from tornado import gen
import api_engine.enums as enums
from .base import BaseMapper


class ProxyEntryMappingType(BaseMapper):
    """
    Proxy Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'host': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'port_video': {
                            'type': 'integer',
                        },
                        'port_rtmp': {
                            'type': 'integer',
                        },
                        'port_web': {
                            'type': 'integer',
                        },
                        'status': {
                            'type': 'integer',
                        },
                        'connections': {
                            'type': 'integer',
                        },
                        'active': {
                            'type': 'boolean',
                        },
                        'gps_location': {
                            'type': 'geo_point',
                            'lat_lon': True,
                            'fielddata': {
                                'format': 'compressed',
                                'precision': '1cm',
                            },
                        },
                    }
                }
            }
        }

    @classmethod
    @gen.coroutine
    def get_by_host_and_ports(cls, data):
        """

        :param data:
        :return:
        """
        f = F()
        for key in data:
            f &= F(**{key: data[key]})
        result = yield S(cls).filter(f).execute()
        raise gen.Return(result.objects[0] if len(result) else None)

    @classmethod
    def get_closest(cls, location=None):
        """

        :param location:
        :return:
        """
        orderings = ['connections']
        if location is not None:
            orderings.insert(0, cls.o__geo_distance_asc(location))
        qs = S(cls).filter(status=enums.PROXY_STATUS.online).order_by(*orderings)[:1]
        return qs
