# -*- coding: utf-8 -*- 
from reelcam.mappers.base import BaseMapper


class HEntryMappingType(BaseMapper):
    """
    History mapper class.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'method': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'e_user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'e_object_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'seen': {
                            'type': 'boolean',
                        },
                        'is_visible': {
                            'type': 'boolean',
                        },
                        'data': {
                            'type': 'object',
                            'properties': {},
                        },
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                    }
                }
            }
        }