# -*- coding: utf-8 -*-
from ..base import BaseMapper
from asyncelasticutils import S, F, get_es
from tornado import gen


class VideoLikeEntryMappingType(BaseMapper):
    """
    Video Like Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'positive': {
                            'type': 'boolean',
                        }, # like when True, and dislike, when False
                    }
                }
            }
        }

    @classmethod
    @gen.coroutine
    def like_count_date(cls, date_start, date_end):
        """

        :param date_start:
        :param date_end:
        :return:
        """
        es = get_es()
        res = yield es.search(index=cls.get_index(), body={
            "filter": {
                "bool": {
                    "must": [
                        {
                            "range": {
                                cls.get_mapping_type_name() + ".modify_ts": {
                                    "gte": date_start,
                                    "lte": "now"
                                }
                            }
                        },
                        {
                            "term": {
                                "positive": True
                            }
                        }
                    ]
                }
            }
        })
        raise gen.Return(res['hits']['total'])

    @classmethod
    @gen.coroutine
    def dislike_count_date(cls, date_start, date_end):
        """

        :param date_start:
        :param date_end:
        :return:
        """
        es = get_es()
        res = yield es.search(index=cls.get_index(), body={
            "filter": {
                "bool": {
                    "must": [
                        {
                            "range": {
                                cls.get_mapping_type_name() + ".modify_ts": {
                                    "gte": date_start,
                                    "lte": "now"
                                }
                            }
                        },
                        {
                            "term": {
                                "positive": False
                            }
                        }
                    ]
                }
            }
        })
        raise gen.Return(res['hits']['total'])
