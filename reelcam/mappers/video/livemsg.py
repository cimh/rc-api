# -*- coding: utf-8 -*- 
from ..base import BaseMapper


class LiveMsgEntryMappingType(BaseMapper):
    """
    Video Live Message mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'message': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'seconds': {
                            'type': 'integer',
                        },
                    }
                }
            }
        }
