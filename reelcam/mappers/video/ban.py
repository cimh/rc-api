# -*- coding: utf-8 -*-
import pytz
from ..base import BaseMapper
from asyncelasticutils import S, F, Q, get_es
from datetime import datetime


class VideoBanEntryMappingType(BaseMapper):
    """
    Video Ban Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                    }
                }
            }
        }

    @classmethod
    def add(cls, _video_id, _user_id):
        """

        :param _video_id:
        :param _user_id:
        :return:
        """
        data = dict(video_id=_video_id, user_id=_user_id, ts=datetime.now(pytz.utc).strftime('%Y-%m-%d %H:%M:%S'))
        result = cls.index(document=data)
        return result

    @classmethod
    def remove(cls, _video_id, _user_id):
        """

        :param _video_id:
        :param _user_id:
        :return:
        """
        es = get_es()
        result = es.delete_by_query(index=cls.get_index(), doc_type=cls.get_mapping_type_name(), body=Q(user_id__match=_user_id, must=True) + Q(video_id__match=_video_id, must=True))
        return result

    @classmethod
    def check_exists(cls, _video_id, _user_id):
        """

        :param _video_id:
        :param _user_id:
        :return:
        """
        result = S(cls).filter(F(video_id=_video_id) & F(user_id=_user_id))
        if len(result):
            return result[0]
