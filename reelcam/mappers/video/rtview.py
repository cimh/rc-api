# -*- coding: utf-8 -*-
from ..base import BaseMapper
from asyncelasticutils import S, F, get_es
from tornado import gen


class RTViewEntryMappingType(BaseMapper):
    """
    Realtime View Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                    }
                }
            }
        }

    @classmethod
    @gen.coroutine
    def count(cls, video_id):
        """

        :param video_id:
        :return:
        """
        r = yield S(cls).filter(F(video_id=video_id)).count()
        raise gen.Return(r)

    @classmethod
    @gen.coroutine
    def _delete_all_by_video_id(cls, video_id):
        q = {
            "query": {
                "bool": {
                    "must": {
                        "term": {
                            "video_id": video_id
                        }
                    }
                }
            }
        }
        yield get_es().delete_by_query(index=cls.get_index(), doc_type=cls.get_mapping_type_name(), body=q)
