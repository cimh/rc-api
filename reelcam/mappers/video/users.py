# -*- coding: utf-8 -*-
from ..base import BaseMapper
from asyncelasticutils import S, Q, F, get_es
from tornado import gen

class VideoUsersEntryMappingType(BaseMapper):
    """
    Video User List Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                    }
                }
            }
        }

    @classmethod
    def add(cls, user_id, video_id):
        """

        :param user_id:
        :param video_id:
        :return:
        """
        es = get_es()
        cls.index(document=dict(user_id=user_id, video_id=video_id))

    @classmethod
    def remove(cls, user_id, video_id):
        """

        :param user_id:
        :param video_id:
        :return:
        """
        es = get_es()
        es.delete_by_query(
            index=cls.get_index(),
            doc_type=cls.get_mapping_type_name(),
            body=Q(user_id__match=user_id, must=True) + Q(video_id__match=video_id, must=True)
        )

    @classmethod
    @gen.coroutine
    def _get_by_user_id(cls, user_id, limit=10, offset=0):
        """

        :param user_id:
        :return:
        """
        result = yield S(cls).filter(F(user_id=user_id))[offset: limit].execute()
        raise gen.Return([r.video_id for r in result])
