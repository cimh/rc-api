# -*- coding: utf-8 -*-
from asyncelasticutils import S, F, get_es
from tornado import gen
from datetime import timedelta
import api_engine.enums as enums
from api_engine import s
from api_engine.enums import VIDEO_PERMISSIONS
from ..base import BaseMapper


class VideoEntryMappingType(BaseMapper):
    """
    Video mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'settings': {
                'analysis': {
                    'analyzer': {
                        'video_subject': {
                            'tokenizer': 'video_subject_count',
                            'char_filter': 'html_strip',
                            'filter': ['asciifolding'],
                        },
                        'video_tags': {
                            'type': 'custom',
                            'tokenizer': 'comma',
                            'filter': ['trim', 'lowercase'],
                        },
                        'nGram_analyzer': {
                            'type': 'custom',
                            'tokenizer': 'whitespace',
                            'filter': ['lowercase', 'asciifolding', 'nGram_filter'],
                        },
                        'subject_analyzer': {
                            'type': 'custom',
                            'tokenizer': 'standard',
                            'filter': ['lowercase', 'asciifolding']
                        },
                        'tags_analyzer': {
                            'type': 'custom',
                            'tokenizer': 'tags_ngram',
                            'filter': ['lowercase', 'asciifolding'],
                        },
                    },
                    'tokenizer': {
                        'comma': {
                            'type': 'pattern',
                            'pattern': ',',
                        },
                        'tags_ngram': {
                            'type': 'edgeNGram',
                            'min_gram': '1',
                            'max_gram': '255',
                            'token_chars': ['letter', 'digit'],
                        },
                        'video_subject_count': {
                            'type': 'pattern',
                            # 'pattern': '^[a-zA-Z\s]+',
                            'pattern': '([\\(]+([0-9]+)[\\)]+)',
                            'group': 2,
                        }
                    },
                    'filter': {
                        'nGram_filter': {
                            'type': 'nGram',
                            'min_gram': 3,
                            'max_gram': 50,
                        }
                    }
                },
            },
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'access_observer': {
                            'type': 'boolean',
                        },
                        'category_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'count_bans': {
                            'type': 'integer',
                        },
                        'count_comments': {
                            'type': 'integer',
                        },
                        'count_dislikes': {
                            'type': 'integer',
                        },
                        'count_likes': {
                            'type': 'integer',
                        },
                        'count_views': {
                            'type': 'integer',
                        },
                        'count_watchers': {
                            'type': 'integer',
                        },
                        'is_deleted': {
                            'type': 'boolean',
                        },
                        'delete_status': {
                            'type': 'integer',
                        },
                        'duration': {
                            'type': 'float',
                        },
                        'filesize': {
                            'type': 'integer',
                        },
                        'gps_incognito': {
                            'type': 'boolean',
                        },
                        'gps_location': {
                            'type': 'geo_point',
                            'lat_lon': True,
                            'fielddata': {
                                'format': 'compressed',
                                'precision': '1cm',
                            },
                        },
                        'last_pause_time': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'master_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'observer_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'observer_start_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'observer_close_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'observer_token': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'observe_request_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'owner_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'permission': {
                            'type': 'integer',
                        },
                        'proxy_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'record_uploaded': {
                            'type': 'boolean'
                        },
                        'reporter_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'reporter_start_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'reporter_close_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'reporter_token': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'status': {
                            'type': 'integer'
                        },
                        'subject': {
                            'type': 'string',
                            'index': 'not_analyzed',
                            'fields': {
                                'analyzed': {
                                    'type': 'string',
                                    'index_analyzer': 'nGram_analyzer',
                                    'search_analyzer': 'subject_analyzer'
                                },
                            },
                        },
                        'tags': {
                            'type': 'string',
                            'index': 'not_analyzed',
                            'fields': {
                                'analyzed': {
                                    'type': 'string',
                                    'index_analyzer': 'nGram_analyzer',
                                    'search_analyzer': 'tags_analyzer',
                                },
                            },
                        },
                        'visible': {
                            'type': 'boolean',
                        },
                        'media_urls': {
                            'properties': {
                                'o': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'r': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'ri': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                            },
                        },
                        'shot_urls': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'screenshot_keys': {
                            'properties': {
                                'screen1': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'screen2': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'screen3': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                            }
                        },
                        'is_reanimated': {
                            'type': 'boolean',
                        },
                        'coords': {
                            'properties': {
                                'gps_location': {
                                    'type': 'geo_point',
                                    'lat_lon': True,
                                    'fielddata': {
                                        'format': 'compressed',
                                        'precision': '1cm'
                                    }
                                },
                                'gps_type': {
                                    'type': 'integer'
                                },
                                'time': {
                                    'type': 'integer'
                                },
                                'seconds': {
                                    'type': 'integer'
                                },
                                'modify_ts': {
                                    'type': 'date',
                                    'format': 'yyyy-MM-dd HH:mm:ss',
                                },
                            }
                        },
                        'is_special': {
                            'type': 'boolean',
                        },
                        'country_code': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'upload_ids': {
                            'properties': {
                                'o': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'r': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'ri': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                            },
                        },
                        'parts': {
                            'properties': {
                                'duration': {
                                    'type': 'float',
                                },
                                'start_ts': {
                                    'type': 'date',
                                    'format': 'yyyy-MM-dd HH:mm:ss',
                                },
                                'end_ts': {
                                    'type': 'date',
                                    'format': 'yyyy-MM-dd HH:mm:ss',
                                },
                            }
                        },
                        'scope': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'wedding_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'is_wedcam_trend': {
                            'type': 'boolean',
                        },
                        'is_visible': {
                            'type': 'boolean',
                        },
                        'ggg': {
                            'type': 'integer',
                        },
                        'is_photo': {
                            'type': 'boolean',
                        },
                        'is_external': {
                            'type': 'boolean',
                        },
                    }
                }
            }
        }

    @classmethod
    def get_warmers(cls):
        return {
            "warmers": {
                "w_actual_videos": {
                    "aggs": {
                        "real_users": {
                            "filter": {
                                "bool": {
                                    "must": [
                                        {
                                            "term": {
                                                "is_deleted": False
                                            }
                                        },
                                        {
                                            "term": {
                                                "delete_status": 1
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        }

    @classmethod
    def get_i_follow(cls, user_id=None, user_ids=None, wedding_ids=None, scope=None, platform=None):
        """

        :param user_id:
        :param user_ids:
        :param wedding_ids:
        :param scope:
        :return:
        """
        is_wedcam_trend = False
        if scope and scope == s().get_scope(enums.WEDCAM):
            is_wedcam_trend = True
        f = cls.f_not_live(platform, is_wedcam_trend=is_wedcam_trend) & ~cls.f_owner_id(user_id)

        if user_ids and wedding_ids:
            f &= F(reporter_id__in=user_ids) | F(wedding_id__in=wedding_ids)
        elif user_ids:
            f &= F(reporter_id__in=user_ids)
        elif wedding_ids:
            f &= F(wedding_id__in=wedding_ids)
        else:
            return None

        results = S(cls).order_by('-reporter_start_ts')
        results = results.filter(f)
        if scope:
            results = results.filter(scope=scope)
        return results

    @classmethod
    def get_live(cls, user_id=None, scope=None):
        """

        :param user_id:
        :param scope:
        :return:
        """
        results = S(cls).order_by('-reporter_start_ts')
        f = cls.f_tab_live()
        if user_id:
            f &= cls.f_owner_id(user_id)
        results = results.filter(f)
        if scope:
            results = results.filter(scope=scope)
        return results

    @classmethod
    def get_near_me(cls, location, user_id=None, scope=None, platform=None):
        """

        :param location:
        :param user_id:
        :param scope:
        :return:
        """
        results = S(cls)
        f = cls.f_not_live(platform)
        if user_id:
            f &= cls.f_owner_id(user_id)
        results = results.filter(f)
        if scope:
            results = results.filter(scope=scope)
        if location:
            geo_filter = {
                "geo_distance": {
                    "distance": "1km",
                    "gps_location": {
                        "lat": location['lat'],
                        "lon": location['lon']
                    }
                }
            }
            results = results.filter_raw(geo_filter)
        results = results.order_by(cls.o__geo_distance_asc(location))
        return results

    @classmethod
    def get_popular(cls, user_id=None, scope=None, platform=None):
        """

        :param user_id:
        :param scope:
        :return:
        """
        results = S(cls).order_by('-count_views')
        f = cls.f_not_live(platform)

        if user_id:
            f &= cls.f_owner_id(user_id)
        if scope:
            f &= F(scope=scope)
        results = results.filter(f)
        return results

    @classmethod
    def get_wedcam(cls, user_id=None, wedding_id=None, platform=None):
        """

        :param user_id:
        :param wedding_id:
        :return:
        """
        f = cls.f_not_live(platform)

        if user_id:
            f &= cls.f_owner_id(user_id)

        if wedding_id:
            f &= F(wedding_id=wedding_id)

        f &= F(scope=s().get_scope("WEDCAM"))

        f &= ~F(wedding_id=None)
        f &= ~F(wedding_id="")
        results = S(cls).filter(f)
        return results

    @classmethod
    def get_popular_week(cls, user_id=None, scope=None):
        """
        Temporary method

        :param user_id:
        :param scope:
        :return:
        """
        results = cls.get_popular(user_id, scope)
        f = F(reporter_start_ts__gte=cls.YmdHMS_from_dt(cls.now() - timedelta(days=7)))
        results = results.filter(f)
        return results

    @classmethod
    def exclude_filter(cls, results, exclude):
        """
        Temporary method for week popular

        :param results:
        :param exclude:
        :return:
        """
        f = F()
        for i in exclude:
            f |= F(_id=i._id)
        results = results.filter(~f)
        return results

    @classmethod
    def get_recent(cls, user_id=None, scope=None, platform=None):
        """

        :param user_id:
        :param scope:
        :return:
        """
        f = cls.f_not_live(platform)
        # temporary filter - hide bg video (category_id only exist in karaoke video)
        f &= F(category_id=None)
        ############################
        results = S(cls).order_by('-reporter_start_ts')
        if user_id:
            f &= cls.f_owner_id(user_id)
        results = results.filter(f)
        if scope:
            f &= F(scope=scope)
        results = results.filter(f)
        return results

    @classmethod
    def reported(cls, user_id):
        """

        :param user_id:
        :return:
        """
        return F(reporter_id=user_id)

    @classmethod
    @gen.coroutine
    def set_permission_for_all_owned(cls, user_id, permission):
        """

        :param user_id:
        :param permission:
        :return:
        """
        f = cls.f_owner_id(user_id) & cls.f_not_deleted()
        query = S(cls).filter(f)
        count = yield query.count()
        videos = yield query[:count].execute()
        for v in videos:
            v.es_meta.source.update(_id=v._id)
            v.es_meta.source.update(permission=permission)
        cls.bulk_index([v.es_meta.source for v in videos], id_field='_id')

    @classmethod
    def f_finished(cls):
        """

        :return:
        """
        return F(status=enums.VIDEO_STATUS.finished)

    @classmethod
    def f_lost_finished(cls):
        """

        :return:
        """
        return F(status=enums.VIDEO_STATUS.lost_finished)

    @classmethod
    def f_owner_id(cls, user_id):
        """

        :param user_id:
        :return:
        """
        return F(owner_id=user_id)

    @classmethod
    def f_privacy(cls, current_user_id, video_ids=None, user_ids=None):
        """

        :param current_user_id:
        :param video_ids:
        :param user_ids:
        :return:
        """
        f = F()

        # all videos with public visibility
        f |= F(permission=enums.VIDEO_PERMISSIONS.all)

        if current_user_id is None:
            return f

        # all videos which are owned by followed users
        if user_ids:
            f |= F(owner_id__in=user_ids) & \
                F(permission__in=[enums.VIDEO_PERMISSIONS.followers, enums.VIDEO_PERMISSIONS.followers_and_list])

        # all videos which are in video-users index and user_id is allowed to view
        if video_ids:
            f |= F(_id__in=video_ids) & \
                F(permission__in=[enums.VIDEO_PERMISSIONS.list, enums.VIDEO_PERMISSIONS.followers_and_list])

        # all owned videos
        f |= F(owner_id=current_user_id)

        return f

    @classmethod
    def wedcam_privacy(cls, current_user_id, wedding_id=None, user_ids=None, wedding_ids=None):
        """

        :param current_user_id:
        :param wedding_id:
        :param user_ids:
        :param wedding_ids:
        :return:
        """
        f = F(permission=VIDEO_PERMISSIONS.all)
        f |= F(reporter_id=current_user_id)
        if wedding_id:
            f |= F(wedding_id=wedding_id)
        if user_ids:
            f |= F(permission=VIDEO_PERMISSIONS.followers) & F(reporter_id__in=user_ids) & F(wedding_id=None)
        if wedding_ids:
            f |= F(permission=VIDEO_PERMISSIONS.followers) & F(wedding_id__in=wedding_ids)
        return f

    @classmethod
    def f_country_code(cls, raw):
        """
        Add filtering by country_code to raw query.
        Filter by country_code 'RU' and not 'RU' users
        if 'RU' -> show all users
        else -> show except 'RU' users

        :param raw:
        :return: raw
        """
        from reelcam.utils import get_country_code
        if not s().country_code_filter:
            return raw
        country_code = cls.ctx.request_user.country_code \
            if cls.ctx.request_user else get_country_code(ip=cls.ctx.client_ip)
        if country_code == 'RU':
            return raw
        else:
            return raw.filter(~F(country_code='RU'))

    @classmethod
    def f_has_screenshot(cls):
        """

        :return:
        """
        return ~F(screenshot_keys=None)

    @classmethod
    def f_tabs_base(cls):
        """

        :return:
        """
        f = F()
        f &= cls.f_not_deleted()
        f &= cls.f_has_screenshot()
        return f

    @classmethod
    def f_tabs(cls, is_photo=False):
        """

        :return:
        """
        f = cls.f_tabs_base()
        recorded = F(filesize__gte=s().video.min_filesize) & F(duration__gte=s().video.min_duration)
        if is_photo:
            f &= F(is_photo=True) | recorded
        else:
            f &= (F(is_photo=False) | F(is_photo=None)) & recorded
        return f

    @classmethod
    def f_not_live(cls, platform=None, is_wedcam_trend=False):
        """

        :return:
        """
        f = cls.f_finished()
        f &= cls.f_tabs_base()
        f &= ~F(is_special=True)
        if not is_wedcam_trend:
            f &= F(is_wedcam_trend=False)
        if platform in [enums.ANDROID, enums.IOS]:
            f &= cls.f_tabs(is_photo=True)
        return f

    @classmethod
    def f_tab_live(cls):
        """

        :return:
        """
        return ~cls.f_finished() & ~cls.f_lost_finished() & cls.f_tabs_base()

    @classmethod
    def f_reported_by(cls, user_id):
        """

        :param user_id:
        :return:
        """
        return F(reporter_id=user_id)

    @classmethod
    def f_reported_lives(cls, user_id):
        """

        :param user_id:
        :return:
        """
        f = cls.f_reported_by(user_id)
        f &= ~cls.f_finished()
        return f

    @classmethod
    def search(cls, start_ts=None, end_ts=None, scope=None, **kwargs):
        """

        :param query:
        :param start_ts:
        :param end_ts:
        :param location:
        :param video_scope:
        :param radius:
        :return:
        """
        # by default search all (live or finished)
        f = (cls.f_tabs() & cls.f_finished()) | cls.f_tab_live()

        if 'type_stream' in kwargs:
            # live
            if kwargs['type_stream'] == 2:
                f = cls.f_tab_live()
            # or finished
            elif kwargs['type_stream'] == 1:
                f = cls.f_tabs() & cls.f_finished()

        if 'query' in kwargs:
            for q in kwargs['query']:
                f = f & F(**{"subject.analyzed": q})
            for q in kwargs['tags']:
                f = f & F(**{"tags.analyzed": q})
                # f = f & F(tags__prefix=q)

        if start_ts and end_ts:
            f = f & F(
                reporter_start_ts__gte=cls.YmdHMS_from_Ymd(start_ts),
                reporter_start_ts__lte=cls.YmdHMS_from_dt(cls.dt_from_Ymd(end_ts)+timedelta(days=1))
            )

        if 'location' in kwargs:
            f = f & F(gps_location__distance=("%dm" % kwargs['radius'],
                                              kwargs['location']['lat'],
                                              kwargs['location']['lon']))

        if scope:
            f &= F(scope=scope)
        results = S(cls).filter(f).order_by('-reporter_start_ts')
        return results

    @classmethod
    def get_lost_paused(cls):
        """

        :return:
        """
        f = (F(status=enums.VIDEO_STATUS.paused) | cls.f_lost_finished()) & \
            F(last_pause_time__lt=cls.YmdHMS_from_dt(cls.now_dt() - timedelta(seconds=s().video.finish_timeout)))
        qs = S(cls).filter(f)
        return qs

    @classmethod
    @gen.coroutine
    def get_reported_lives_count(cls, user_id):
        """

        :param user_id:
        :return:
        """
        f = ~F(status=enums.VIDEO_STATUS.finished) & F(reporter_id=user_id)
        q = S(cls).filter(f)
        count = yield q.count()
        raise gen.Return(count)

    @classmethod
    def set_screenshot_keys(cls, _id, screenshot_keys):
        """
        Set screenshot_keys for video by id.
        Use this method directly instead of '.save()' for screenshot_keys field update.
        :param _id:
        :param screenshot_keys:
        :return:
        """
        cls.update(_id, body=dict(doc=dict(screenshot_keys=screenshot_keys)))

    @classmethod
    @gen.coroutine
    def get_total_duration(cls, reporter_id):
        """
        Get total duration of User videos.

        :param reporter_id: str User ID.
        :return:
        """
        q = {
            "query": {
                "filtered": {
                    "filter": {
                        "and": [
                            {
                                "term": {
                                    "reporter_id": reporter_id
                                }
                            },
                            {
                                "term": {
                                    "status": enums.VIDEO_STATUS.finished
                                }
                            },
                            {
                                "term": {
                                    "is_deleted": False
                                }
                            }
                        ]
                    }
                }
            },
            "aggs": {
                "total_duration": {
                    "sum": {
                        "field": "duration"
                    }
                }
            }
        }
        res = yield get_es().search(index=cls.get_index(), doc_type=cls.get_mapping_type_name(),
                                    body=q, params=dict(search_type="count"))

        raise gen.Return(int(res["aggregations"]["total_duration"]["value"]))

    @classmethod
    @gen.coroutine
    def _delete_user(cls, user_id):
        """
        Delete all user video when user deletes his profile

        :return:
        """
        f = cls.f_owner_id(user_id) & cls.f_not_deleted()
        query = S(cls).filter(f)
        count = yield query.count()
        videos = yield query[:count].execute()
        for v in videos:
            v.es_meta.source.update(_id=v._id)
            v.es_meta.source.update(is_deleted=True, delete_status=enums.VIDEO_DELETE_STATUS.user_deleted)

        cls.bulk_index([v.es_meta.source for v in videos], id_field='_id')

    @classmethod
    def get_not_finished(cls, user_id):
        """
        Get not finished video by user

        :param user_id: user id
        :return:
        """

        f = cls.f_owner_id(user_id) & cls.f_not_deleted() & ~cls.f_finished()
        return S(cls).filter(f)

    @classmethod
    def tab_filter(cls, scope=None):
        """

        :param scope:
        :return:
        """
        from api_engine.enums import VIDEO_STATUS
        filter = F(is_deleted=False)
        filter &= F(filesize__gte=s().video.min_filesize)
        filter &= F(duration__gte=s().video.min_duration)
        filter &= F(status=VIDEO_STATUS.finished)
        filter &= ~F(is_special=True)
        filter &= F(permission=enums.VIDEO_PERMISSIONS.all)
        if scope:
            filter &= F(scope=scope)
        return filter

    @classmethod
    def f_is_wedcam_trend(cls):
        """

        :return:
        """
        return F(is_wedcam_trend=True)

    @classmethod
    def f_is_visible(cls):
        """

        :return:
        """
        return F(is_visible=True)

    @classmethod
    def f_reporter_id(self, reporter_id):
        """

        :param reporter_id:
        :return:
        """
        return F(reporter_id=reporter_id)
