# -*- coding: utf-8 -*-
from ..base import BaseMapper
from elasticsearch.helpers import bulk_index
from asyncelasticutils import S, F
from tornado import gen


class VideoGpsEntryMappingType(BaseMapper):
    """
    Video Gps Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'gps_location': {
                            'type': 'geo_point',
                            'lat_lon': True,
                            'fielddata': {
                                'format': 'compressed',
                                'precision': '1cm',
                            },
                        },
                        'second': {
                            'type': 'integer',
                        },
                    }
                }
            }
        }

    @classmethod
    def add(cls, video_id, gps_location, second):
        """

        :param video_id:
        :param gps_location:
        :param second:
        :return:
        """
        document = dict(video_id=video_id, gps_location=gps_location, second=second, modify_ts=now_YmdHMS())
        cls.index(document=document)

    @classmethod
    def add_bulk(cls, video_id, coords):
        """

        :param video_id:
        :param coords:
        :return:
        """
        for coord in coords:
            coord['video_id'] = video_id
            coord['modify_ts'] = cls.now_YmdHMS()

        bulk_index(cls.get_es(),
                   coords,
                   index=cls.get_index(),
                   doc_type=cls.get_mapping_type_name())

    @classmethod
    @gen.coroutine
    def get_by_video_id(cls, video_id):
        """

        :param video_id:
        :return:
        """
        f = F(video_id=video_id)
        count = yield S(cls).filter(f).count()
        result = S(cls).order_by('second').filter(f)[: count]
        result = yield result.execute()
        raise gen.Return([r.get_data() for r in result])

    def get_data(self):
        """

        :return:
        """
        return {
            'lat': self.gps_location.get('lat'),
            'lon': self.gps_location.get('lon'),
            'second': self.second,
            'ts': self.ts_from_YmdHMS(self.modify_ts),
        }
