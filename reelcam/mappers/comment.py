# -*- coding: utf-8 -*-
from asyncelasticutils import S, F
from .base import BaseMapper


class CommentEntryMappingType(BaseMapper):
    """
    Comment Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'body': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'is_deleted': {
                            'type': 'boolean',
                        },
                    }
                }
            }
        }

    @classmethod
    def get_recent(cls, video_id):
        """

        :param video_id:
        :return:
        """
        results = S(cls).filter(video_id=video_id)\
                        .filter(cls.f_not_deleted())\
                        .order_by('-create_ts')
        return results

    @classmethod
    def get_multiple(cls, ids):
        """

        :param ids:
        :return:
        """
        f = F()
        for i in ids:
            f |= F(video_id=i)
        f &= cls.f_not_deleted()
        results = S(cls).filter(f)\
                        .order_by('-create_ts')
        return results
