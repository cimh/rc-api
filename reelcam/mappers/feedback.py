# -*- coding: utf-8 -*-
from .base import BaseMapper


class FeedbackEntryMappingType(BaseMapper):
    """
    A Feedback mapper
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'name': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'subject': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'text': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'email': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                    }
                }
            }
        }
