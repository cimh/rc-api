# -*- coding: utf-8 -*-
from .base import BaseMapper
from asyncelasticutils import S, F
from tornado import gen


class FollowEntryMappingType(BaseMapper):
    """
    Follow Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        '_from': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'message': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        '_to': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'status': {
                            'type': 'integer',
                        },
                        'first_request_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'last_request_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        '_from_is_hidden': {
                            'type': 'boolean',
                        },
                        '_to_is_hidden': {
                            'type': 'boolean',
                        },
                    }
                }
            }
        }

    @classmethod
    @gen.coroutine
    def _delete_user(cls, user_id):
        """
        Set _from_is_hidden or _to_is_hidden flag to deleted user

        :param _user_id(str): user id
        :return: None
        """

        q = S(cls).filter(F(_from=user_id) | F(_to=user_id))
        count = yield q.count()
        results = yield q[:count].execute()
        for r in results:
            yield cls.delete(_id=r._id)