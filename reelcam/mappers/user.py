# -*- coding: utf-8 -*-
from asyncelasticutils import S, F
from tornado import gen
import api_engine.enums as enums
from .base import BaseMapper


class UserEntryMappingType(BaseMapper):
    """
    User Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'settings': {
                'analysis': {
                    'analyzer': {
                        'not_analyzed_lowercase': {
                            'tokenizer': 'keyword',
                            'filter': 'lowercase',
                        },
                        'nGram_analyzer': {
                            'type': 'custom',
                            'tokenizer': 'whitespace',
                            'filter': ['lowercase', 'asciifolding', 'nGram_filter'],
                        },
                        'fullname_base': {
                            'type': 'pattern',
                            'pattern': "[\\^\\(\\d+\\)]+$",
                        },
                        'fullname_analyzer': {
                            'type': 'custom',
                            'tokenizer': 'whitespace',
                            'filter': [
                                'lowercase',
                                'asciifolding'
                            ]
                        },
                    },
                    'filter': {
                        'nGram_filter': {
                            'type': 'nGram',
                            'min_gram': 3,
                            'max_gram': 50
                        }
                    }
                },
            },
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'app_name': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'birthday': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd'
                        },
                        'count_bans': {
                            'type': 'integer',
                        },
                        'count_dislikes': {
                            'type': 'integer',
                        },
                        'count_likes': {
                            'type': 'integer',
                        },
                        'country': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'date_joined': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd',
                        },
                        'joined_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'default_video_permission': {
                            'type': 'integer',
                        },
                        'email': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'first_name': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'phone': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'followed': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'followers': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'follow_approve': {
                            'type': 'boolean',
                        },
                        'fullname': {
                            'type': 'string',
                            'analyzer': 'not_analyzed_lowercase',
                            'fields': {
                                'analyzed': {
                                    'type': 'string',
                                    'index_analyzer': 'nGram_analyzer',
                                    'search_analyzer': 'fullname_analyzer',
                                },
                                'base': {
                                    'type': 'string',
                                    'analyzer': 'fullname_base',
                                },
                            },
                        },
                        'gender': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'gift_wedding_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'gps_incognito': {
                            'type': 'boolean',
                        },
                        'gps_location': {
                            'type': 'geo_point',
                            'lat_lon': True,
                            'fielddata': {
                                'format': 'compressed',
                                'precision': '1cm',
                            },
                        },
                        'gps_type': {
                            'type': 'integer',
                        },
                        'gps_ts': {
                            'type': 'integer',
                        },
                        'is_active': {
                            'type': 'boolean',
                        },
                        'is_deleted': {
                            'type': 'boolean',
                        },
                        'is_demo': {
                            'type': 'boolean',
                        },
                        'delete_status': {
                            'type': 'integer',
                        },
                        'language': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'language2': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'last_login': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'last_name': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'password': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'photo': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'photo_keys': {
                            'properties': {
                                'origin': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'big': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'medium': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'small': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'profile': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                            }
                        },
                        'rating': {
                            'type': 'integer',
                        },
                        'region': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        # for auth via socials
                        'social_auth': {
                            'properties': {
                                'facebook': {
                                    'properties': {
                                        'uid': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'token': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'expires': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                    },
                                },
                                'twitter': {
                                    'properties': {
                                        'uid': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'token': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'token_secret': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                    },
                                },
                                'google': {
                                    'properties': {
                                        'uid': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'token': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'expires': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                    },
                                },
                            },
                        },
                        # for posting in socials
                        'social': {
                            'properties': {
                                'facebook': {
                                    'properties': {
                                        'uid': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'token': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'expires': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                    },
                                },
                                'twitter': {
                                    'properties': {
                                        'uid': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'token': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'token_secret': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                    },
                                },
                                'google': {
                                    'properties': {
                                        'uid': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'token': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                        'expires': {
                                            'type': 'string',
                                            'index': 'not_analyzed',
                                        },
                                    },
                                },
                            },
                        },
                        'is_online': {
                            'type': 'boolean',
                        },
                        'gadget': {
                            'type': 'boolean',
                        },
                        'user_type': {
                            'type': 'integer',
                        },
                        'status': {
                            'type': 'integer',
                        },
                        'sms_codes': {
                            'properties': {
                                'value': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'send_ts': {
                                    'type': 'date',
                                    'format': 'yyyy-MM-dd HH:mm:ss',
                                },
                            }
                        },
                        'is_sms_validated': {
                            'type': 'boolean',
                        },
                        'subscription_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'subscription_expire': {
                            'type': 'integer',
                        },
                        'total_video_duration': {
                            'type': 'integer',
                        },
                        'is_staff': {
                            'type': 'boolean',
                        },
                        'country_code': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'citydoom_rating': {
                            'type': 'integer',
                        },
                        'citydoom_rank': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'wedding_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'joined_wedding_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'gcm_tokens': {
                            'type': 'nested',
                            'properties': {
                                'platform': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'app_name': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                                'token': {
                                    'type': 'string',
                                    'index': 'not_analyzed',
                                },
                            },
                        },
                    }
                }
            }
        }

    @classmethod
    def get_warmers(cls):
        """

        :return:
        """
        return {
            "warmers": {
                "w_actual_users": {
                    "aggs": {
                        "real_users": {
                            "filter": {
                                "bool": {
                                    "must": [
                                        {
                                            "term": {
                                                "is_deleted": False
                                            }
                                        },
                                        {
                                            "term": {
                                                "delete_status": 1
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        }

    @classmethod
    def get_swiper(cls):
        """

        :param offset:
        :param limit:
        :return:
        """
        return S(cls).filter(cls.f_is_active() & cls.f_user_type()).order_by('-rating')

    @classmethod
    def f_user_type(cls, user_type=enums.USER_TYPE.user):
        """

        :param user_type:
        :return:
        """
        return F(user_type=user_type)

    @classmethod
    def f_is_active(cls):
        """
        :return: filter not deleted and sms validated users
        """
        return cls.f_not_deleted() & (F(is_sms_validated=True) | ~F(social_auth=None))

    @classmethod
    def f_country_code(cls, raw):
        """
        Add filtering by country_code to raw query.
        Filter by country_code 'RU' and not 'RU' users
        if 'RU' -> show all users
        else -> show except 'RU' users

        :param raw:
        :return: raw
        """
        from reelcam.utils import get_country_code
        from api_engine import s
        if not s().country_code_filter:
            return raw

        country_code = cls.ctx.request_user.country_code \
            if cls.ctx.request_user else get_country_code(ip=cls.ctx.client_ip)

        if country_code == 'RU':
            return raw
        else:
            return raw.filter(~F(country_code=country_code))

    @classmethod
    def search(cls, **kwargs):
        """

        :param query:
        :param location:
        :param user_scope:
        :param radius:
        :return:
        """
        f = cls.f_is_active()

        if 'type_user' in kwargs:
            # bot
            if kwargs['type_user'] == 2:
                f = f & F(user_type=enums.USER_TYPE.bot)
            # or reporter (standard user)
            elif kwargs['type_user'] == 1:
                f = f & F(user_type=enums.USER_TYPE.user)

        if 'query' in kwargs:
            for q in kwargs['query']:
                f = f & F(**{"fullname.analyzed": q})

        if 'location' in kwargs:
            f = f & F(gps_location__distance=("%dm" % kwargs['radius'],
                                              kwargs['location']['lat'],
                                              kwargs['location']['lon']))

        results = S(cls).filter(f)
        return results

    @classmethod
    def all(cls):
        """

        :return:
        """
        return S(cls).filter(cls.f_is_active() & cls.f_user_type()).order_by('-joined_ts')

    @classmethod
    def get_online_users(cls, data):
        """
        Get online users by location and user type

        :param data (dict):
            location (dict): lat lon location.
            radius (int): Radius of search in meters.
            type_user (int): User type. [3-all, 2-bot, 1-user]
        """

        raw = cls.search(**data)
        raw = raw.filter(F(is_online=True) & ~F(is_staff=True)).order_by('-rating')
        return raw
