# -*- coding: utf-8 -*-
from asyncelasticutils import S, F
from .base import BaseMapper


class ChatEntryMappingType(BaseMapper):
    """
    Chat Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'user1_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user2_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user1_unread_count': {
                            'type': 'integer',
                        },
                        'user2_unread_count': {
                            'type': 'integer',
                        },
                        'user1_last_msg_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'user2_last_msg_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'user1_last_msg_body': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user2_last_msg_body': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'last_msg_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                    }
                }
            }
        }

    @classmethod
    def f_actual(cls, user_id):
        """

        :return:
        """
        f_user = F(user1_id=user_id) | F(user2_id=user_id)
        f_mess_exists = (~F(user1_last_msg_body=None) | ~F(user2_last_msg_body=None))
        return f_user & f_mess_exists

    @classmethod
    def get_by_user_id(cls, _id, offset=0, limit=10):
        """

        :param _id:
        :param limit:
        :param offset:
        :return:
        """
        results = S(cls).order_by('-last_msg_ts').filter(cls.f_actual(_id))
        return results[offset:offset+limit]

    @classmethod
    def get_all(cls, user_id):
        """

        :param user_id:
        :return:
        """
        f = F(is_deleted=False)
        f &= F(user1_id=user_id) | F(user2_id=user_id)
        qs = S(cls)
        qs = qs.filter(f)
        return qs
