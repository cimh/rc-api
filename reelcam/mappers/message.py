# -*- coding: utf-8 -*-
from .base import BaseMapper
from asyncelasticutils import S, F
from tornado import gen


class MessageEntryMappingType(BaseMapper):
    """
    Message Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'chat_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        '_from': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        '_to': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'body': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'created_at': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'deleted_at': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'is_read': {
                            'type': 'boolean',
                        },
                        'delete_by_from': {
                            'type': 'boolean',
                        },
                        'delete_by_to': {
                            'type': 'boolean',
                        },
                    }
                }
            }
        }

    @classmethod
    def get_delete_filter(cls, user_id):
        """

        :param user_id:
        :return:
        """
        f = F(_from=user_id) & F(delete_by_from=False)
        f |= F(_to=user_id) & F(delete_by_to=False)
        return f

    @classmethod
    def get_recent(cls, chat_id, current_user_id=None):
        """

        :param chat_id:
        :param current_user_id:
        :return:
        """
        f = cls.get_delete_filter(current_user_id) if current_user_id is not None else F()
        return S(cls).order_by('-created_at').filter(f).filter(chat_id=chat_id)

    @classmethod
    @gen.coroutine
    def get_unread_count_by_user_id(cls, user_id):
        """

        :param user_id:
        :return:
        """
        f = cls.get_delete_filter(user_id) & F(_to=user_id) & F(is_read=False)
        count = yield S(cls).filter(f).count()
        raise gen.Return(count)
