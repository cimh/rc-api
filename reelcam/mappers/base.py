# -*- coding: utf-8 -*-
from asyncelasticutils import MappingType, Indexable, get_es, S, F, PythonMixin
from utils.datetimemixin import DatetimeMixin
from api_engine import s
from tornado import gen
from elasticutils import get_es as get_sync_es


class BaseMapper(DatetimeMixin, MappingType, Indexable):
    """
    Base Mapper.
    """

    # Max count of rows to return from Elasticsearch by default
    MAX_SIZE = 100

    # Specify how many times should the update operation be retried when a conflict occurs
    RETRY_ON_CONFLICT = 5

    @classmethod
    def get_index(cls):
        """
        Returns index name.

        :return:
        """
        name = cls.__name__.split('EntryMappingType')[0].lower()
        if s().environment == 'TESTING':
            name += '-testing'
        name += '-index'
        return name

    @classmethod
    def get_mapping_type_name(cls):
        """
        Returns mapping type name.

        :return:
        """
        return cls.__name__.split('EntryMappingType')[0].lower() + '-entry'

    @classmethod
    def get_warmers(cls):
        """

        :return:
        """
        return {}

    @classmethod
    def get(cls, _sorts=None, ts=0, _deleted=None, _ids=None, **kwargs):
        """

        :param _sorts: (list|tuple)
        :param ts: (int)
        :param _deleted: (bool)
        :param _ids: (list|tuple) If tuple then it is a pair of list of document IDs and
                                  a field name for which will be filtered.
                                  Also it may be a simple list of document IDs that would be converted to
                                  tuple with an '_id' field name.
        :param kwargs:
        :return:
        """
        if isinstance(_ids, list):
            _ids = (_ids, '_id')

        qs = S(cls)

        f = kwargs.get('_filter', F())

        if _ids and isinstance(_ids, tuple):
            try:
                ids, field = _ids  # ([id1, id2, ..], 'field')
            except (TypeError, ValueError):
                ids, field = (None,)*2
            if ids and field and isinstance(ids, (list, tuple)) \
                    and isinstance(field, str):
                f &= F(**{field+'__in': ids})

        if _deleted is True:
            f &= cls.f_deleted()
        elif _deleted is False:
            f &= cls.f_not_deleted()

        if _sorts:
            qs = qs.order_by(*_sorts)

        if ts:
            f &= F(modify_ts__gte=ts)

        for key, value in list(kwargs.items()):
            if key != '_filter':
                f &= F(**{key: value})
                kwargs.pop(key)

        qs = qs.filter(f)

        return qs

    @classmethod
    @gen.coroutine
    def save(cls, data):
        """

        :param data:
        :param _id:
        :return:
        """
        from elasticsearch.exceptions import ConflictError
        if data.get('_id'):
            doc_id = data.pop('_id')
            body = {'doc': data}
            es = cls.get_es()
            try:
                result = yield es.update(cls.get_index(), cls.get_mapping_type_name(),
                                         doc_id, body=body, retry_on_conflict=cls.RETRY_ON_CONFLICT)
            except ConflictError as e:
                raise e
            raise gen.Return(result['_id'])

        else:
            if '_id' in data:
                data.pop('_id', None)
                _id = data.pop('_custom_id', None)
            es = get_es()
            result = yield es.index(index=cls.get_index(), doc_type=cls.get_mapping_type_name(),
                                    body=data, id=_id, op_type='create')
            if result['created']:
                raise gen.Return(result['_id'])

            return

    @classmethod
    @gen.coroutine
    def update(cls, _id, body):
        """

        :param _id:
        :param body:
        :return:
        """
        es = cls.get_es()
        result = yield es.update(cls.get_index(),
                                 cls.get_mapping_type_name(),
                                 _id,
                                 body=body,
                                 retry_on_conflict=cls.RETRY_ON_CONFLICT)
        raise gen.Return(result['_id'])

    @classmethod
    @gen.coroutine
    def delete(cls, _id):
        """

        :param _id:
        :return:
        """
        es = cls.get_es()
        result = yield es.delete(index=cls.get_index(), doc_type=cls.get_mapping_type_name(), id=_id)
        raise gen.Return(result)

    # ordering
    @classmethod
    def o__geo_distance_asc(cls, gps_location):
        """

        :param gps_location:
        :return:
        """
        ordering = {'_geo_distance': {'gps_location': gps_location,
                                      'order': 'asc',
                                      'unit': 'm'}}
        return ordering

    @classmethod
    @gen.coroutine
    def count_date(cls, date_start, date_end):
        """

        :param date_start:
        :param date_end:
        :return:
        """
        es = get_es()
        res = yield es.search(index=cls.get_index(), body={
            "filter": {
                "bool": {
                    "must": {
                        "range": {
                            cls.get_mapping_type_name() + ".modify_ts": {
                                "gte": date_start,
                                "lte": "now"
                            }
                        }
                    }
                }
            }
        })
        raise gen.Return(res['hits']['total'])

    @classmethod
    def get_mapping_properties(cls):
        """

        :return:
        """
        mapping = cls.get_mapping()
        if not mapping:
            return {}
        return cls.get_mapping()['mappings'][cls.get_mapping_type_name()]['properties']

    @classmethod
    def has_field(cls, field_name):
        """

        :param field_name:
        :return:
        """
        return field_name in cls.get_mapping_properties()

    @classmethod
    def f_not_deleted(cls):
        """

        :return:
        """
        return F(is_deleted=False)

    @classmethod
    def f_deleted(cls):
        """

        :return:
        """
        return F(is_deleted=True)

    @classmethod
    def is_video_associated(cls):
        """

        :return:
        """
        return cls.has_field('video_id')

    @classmethod
    @gen.coroutine
    def _mget(cls, ids):
        """

        :param ids:
        :return:
        """
        es = cls.get_es()
        query_params = {'_source': True}
        result = {'docs': []}

        if ids and isinstance(ids, (list, tuple)):
            r = yield es.mget({'ids': ids},
                              index=cls.get_index(),
                              doc_type=cls.get_mapping_type_name(),
                              **query_params)
            if isinstance(r, dict):
                result.update(r)
            else:
                raise TypeError

        assert isinstance(result['docs'], list)

        result = {doc['_id']: MGetResult(**doc) for doc in result['docs']}

        result = [result.get(_id) for _id in ids]

        raise gen.Return(result)

    @classmethod
    def get_sync_es(cls):
        """
        Returns an Elasticsearch object

        Override this if you need special functionality.

        :returns: a elasticsearch `elasticutils.Elasticsearch` instance
        """
        return get_sync_es()

    @classmethod
    def bulk_index(cls, documents, id_field='id', es=None, index=None):
        """
        Adds or updates a batch of documents.

        :param documents:
        :param id_field:
        :param es:
        :param index:
        :return:
        """
        if es is None:
            es = cls.get_sync_es()
        super(BaseMapper, cls).bulk_index(documents, id_field=id_field, es=es, index=index)


class MGetResult(PythonMixin):
    """
    MGet result class.
    """

    def __init__(self, found, **kwargs):
        """

        :param found:
        :param kwargs:
        :return:
        """
        es_meta = self.to_python(kwargs)
        es_meta = self.EsMeta(**es_meta)
        found = self.to_python(found)
        if found:
            assert isinstance(es_meta.source, dict)
        self.found = found
        self.es_meta = es_meta

    def __bool__(self):
        """

        :return:
        """
        return bool(self.found)

    def __getitem__(self, item):
        """

        :param item:
        :return:
        """
        if not self.found:
            self._not_found()
        return self.es_meta.source[item]

    def __iter__(self):
        """

        :return:
        """
        if not self.found:
            self._not_found()
        return self.es_meta.source.__iter__()

    def _not_found(self):
        """

        :return:
        """
        if not self.found:
            raise ValueError('Document id=%s not found.' % self.es_meta.id)

    class EsMeta(object):
        """
        ES Meta class.
        """

        def __init__(self, _index, _type, _id, _source=None, _version=None, **kwargs):
            """

            :param _index:
            :param _type:
            :param _id:
            :param _source:
            :param _version:
            :param kwargs:
            :return:
            """
            self.index = _index
            self.type = _type
            self.id = _id
            self.source = _source
            self.version = _version
