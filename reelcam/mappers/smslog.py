# -*- coding: utf-8 -*-
from .base import BaseMapper


class SmsLogEntryMappingType(BaseMapper):
    """
    SmsLog Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'sms_gate': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'phone': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'sender': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'text': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'response': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'remote_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'time_send': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss'
                        },
                        'time_status_changed': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss'
                        },
                        'status': {
                            'type': 'integer',
                        },
                        'delivery_time': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        }
                    }
                }
            }
        }
