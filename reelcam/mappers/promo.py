# -*- coding: utf-8 -*-
from .base import BaseMapper


class PromoEntryMappingType(BaseMapper):
    """
    Promo entry mapping type.

    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'code': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'code_type': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'start_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'end_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'ratio': {
                            'type': 'float',
                        }
                    }
                }
            }
        }
