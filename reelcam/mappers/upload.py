# -*- coding: utf-8 -*-
from .base import BaseMapper


class UploadEntryMappingType(BaseMapper):
    """
    Upload session mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'target': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'part_num': {
                            'type': 'integer',
                        },
                        'mpart_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'size': {
                            'type': 'integer',
                        },
                        'key': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'is_complete': {
                            'type': 'boolean',
                        },
                        'task_complete': {
                            'type': 'boolean',
                        },
                        'is_reanimated': {
                            'type': 'boolean',
                        },
                    }
                }
            }
        }
