# -*- coding: utf-8 -*-
from .base import BaseMapper


class BookmarkEntryMappingType(BaseMapper):
    """
    Bookmark Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                    }
                }
            }
        }
