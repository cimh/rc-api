# -*- coding: utf-8 -*-
from .base import BaseHistoryMapper


class ObserveHistoryEntryMappingType(BaseHistoryMapper):
    """
    Observe History Mapper.
    """

    @classmethod
    def add(cls, data):
        """

        :param data:
        :return:
        """
        pass

    @classmethod
    def get_event_data(cls, data):
        """

        :param data:
        :return:
        """
        data.pop('result')
        return data
