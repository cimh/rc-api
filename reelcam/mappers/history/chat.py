# -*- coding: utf-8 -*-
from .base import BaseHistoryMapper


class ChatHistoryEntryMappingType(BaseHistoryMapper):
    """
    Chat History Mapper.
    """

    @classmethod
    def get_mapping_type_name(cls):
        """

        :return:
        """
        return 'h_chat'

    @classmethod
    def get_mapping(cls):
        """

        :param cls:
        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'event': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'event_user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user1_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user2_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user1_last_msg_body': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user2_last_msg_body': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user1_last_msg_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'user2_last_msg_ts': {
                            'type': 'date',
                            'format': 'yyyy-MM-dd HH:mm:ss',
                        },
                        'user1_unread_count': {
                            'type': 'integer',
                        },
                        'user2_unread_count': {
                            'type': 'integer',
                        },
                        'seen': {
                            'type': 'boolean',
                        },
                        'is_visible': {
                            'type': 'boolean',
                        },
                        'chat_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'message_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        }
                    }
                }
            }
        }

    @classmethod
    def get_event_data(cls, data):
        """

        :param data:
        :return:
        """
        return dict(
            event=data['event'],
            event_user_id=data['event_user_id'],
            user1_id=data['user1_id'],
            user2_id=data['user2_id'],
            user1_last_msg_body=data['result']['body'] if data['result']['user_id'] == data['user1_id'] else data['user1_last_msg_body'],
            user2_last_msg_body=data['result']['body'] if data['result']['user_id'] == data['user2_id'] else data['user2_last_msg_body'],
            user1_last_msg_ts=data['result']['ts'] if data['result']['user_id'] == data['user1_id'] else data['user1_last_msg_ts'],
            user2_last_msg_ts=data['result']['ts'] if data['result']['user_id'] == data['user2_id'] else data['user2_last_msg_ts'],
            user1_unread_count=data['user1_unread_count']+1 if data['result']['user_id'] == data['user2_id'] else data['user1_unread_count'],
            user2_unread_count=data['user2_unread_count']+1 if data['result']['user_id'] == data['user1_id'] else data['user2_unread_count'],
            message_id=data['message_id'],
            chat_id=data['id'],
            modify_ts=cls.now_YmdHMS(),
            seen=cls.DEFAULT_SEEN,
        )
