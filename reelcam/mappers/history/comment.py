# -*- coding: utf-8 -*-
from .base import BaseHistoryMapper
from api_engine import s
from tornado import gen


class CommentHistoryEntryMappingType(BaseHistoryMapper):
    """
    Comment History Mapper.
    """

    @classmethod
    def get_mapping_type_name(cls):
        """

        :return:
        """
        return 'h_comment'

    @classmethod
    def get_mapping(cls):
        """

        :param cls:
        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'event': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'event_user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'comment_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'body': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'seen': {
                            'type': 'boolean',
                        },
                        'is_visible': {
                            'type': 'boolean',
                        },
                    }
                }
            }
        }

    @classmethod
    def get_event_data(cls, data):
        """

        :param data:
        :return:
        """
        return dict(
            event=data['event'],
            event_user_id=data['event_user_id'],
            comment_id=data['id'],
            user_id=data['user_id'],
            video_id=data['video_id'],
            body=data['body'],
            modify_ts=data['ts'],
            seen=cls.DEFAULT_SEEN
        )

    @classmethod
    @gen.coroutine
    def add(cls, data):
        """
        Save comment in history

        :param data:
        :return:
        """
        Video = s().M.model('Video')
        document = cls.get_event_data(data)
        video = yield Video.get_one(_id=document['video_id'])
        if not video:
            return
        document['is_visible'] = True
        document['user_id'] = video.owner_id
        document['modify_ts'] = cls.now_YmdHMS()
        result = cls.index(document=document)
        raise gen.Return(result)
