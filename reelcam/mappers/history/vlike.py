# -*- coding: utf-8 -*-
from asyncelasticutils import F
from .base import BaseHistoryMapper


class VideoLikeHistoryEntryMappingType(BaseHistoryMapper):
    """
    Video Like History Mapper.
    """

    @classmethod
    def get_mapping_type_name(cls):
        """

        :return:
        """
        return 'h_vlike'

    @classmethod
    def get_mapping(cls):
        """

        :param cls:
        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'event': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'event_user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'positive': {
                            'type': 'boolean',
                        },  # like when True, and dislike, when False
                        'seen': {
                            'type': 'boolean',
                        },
                        'is_visible': {
                            'type': 'boolean',
                        },
                    }
                }
            }
        }

    @classmethod
    def get_event_data(cls, data):
        """

        :param data:
        :return:
        """
        return dict(
            event=data['event'],
            event_user_id=data['result']['user_id'],
            video_id=data['id'],
            user_id=data['owner_id'],
            positive=data['result']['positive'],
            seen=cls.DEFAULT_SEEN,
            modify_ts=cls.now_YmdHMS()
        )

    @classmethod
    def add(cls, data):
        """

        :param data:
        :return:
        """
        event_data = cls.get_event_data(data)
        if data['event'] == 'video:like':
            super(VideoLikeHistoryEntryMappingType, cls).add(data)

    @classmethod
    def f_main(cls, user_id):
        """

        :param user_id:
        :return:
        """
        f = super(VideoLikeHistoryEntryMappingType, cls).f_main(user_id)
        f &= F(event='video:like')
        return f
