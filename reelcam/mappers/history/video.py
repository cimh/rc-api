# -*- coding: utf-8 -*-
from copy import deepcopy
from elasticsearch.helpers import bulk_index
from asyncelasticutils import F
from .base import BaseHistoryMapper
from api_engine import s


class VideoHistoryEntryMappingType(BaseHistoryMapper):
    """
    Video History Mapper.
    """

    @classmethod
    def get_mapping_type_name(cls):
        """

        :return:
        """
        return 'h_video'

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'event': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'event_user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'status': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'seen': {
                            'type': 'boolean',
                        },
                        'is_visible': {
                            'type': 'boolean',
                        },
                    }
                }
            }
        }

    @classmethod
    def get_event_data(cls, data):
        """

        :param data:
        :return:
        """
        return dict(
            event=data['event'],
            event_user_id=data['event_user_id'],
            user_id=data['owner_id'],
            video_id=data['id'],
            status=data['status'],
            modify_ts=cls.now_YmdHMS(),
            seen=cls.DEFAULT_SEEN,
        )

    @classmethod
    def add(cls, data):
        """

        :param data:
        :return:
        """

        documents = list()

        document = cls.get_event_data(data)
        document['is_visible'] = True

        documents.append(document)

        Video = s().M.model('Video', cls.ctx)

        try:
            video = Video.get(_id=document['video_id'])[0]
        except IndexError:
            return
        users = video.get_allowed_users()
        for user in users:
            doc = deepcopy(document)
            doc['user_id'] = user.id
            documents.append(doc)

        bulk_index(cls.get_es(),
                   documents,
                   index=cls.get_index(),
                   doc_type=cls.get_mapping_type_name())

    @classmethod
    def f_main(cls, user_id):
        """

        :param user_id:
        :return:
        """
        f = super(VideoHistoryEntryMappingType, cls).f_main(user_id)
        f &= F(event='video:create')
        return f
