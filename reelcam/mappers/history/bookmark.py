# -*- coding: utf-8 -*-
from .base import BaseHistoryMapper


class BookmarkHistoryEntryMappingType(BaseHistoryMapper):
    """
    Bookmark History Mapper.
    """

    @classmethod
    def get_mapping_type_name(cls):
        """

        :return:
        """
        return 'h_bookmark'

    @classmethod
    def get_mapping(cls):
        """

        :param cls:
        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'event': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'event_user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'video_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'seen': {
                            'type': 'boolean',
                        },
                        'is_visible': {
                            'type': 'boolean',
                        },
                    }
                }
            }
        }

    @classmethod
    def get_event_data(cls, data):
        """

        :param data:
        :return:
        """
        return dict(
            event=data['event'],
            event_user_id=data['event_user_id'],
            user_id=data['video_owner_id'],
            video_id=data['video_id'],
            modify_ts=cls.now_YmdHMS(),
            seen=cls.DEFAULT_SEEN
        )
