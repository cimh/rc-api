# -*- coding: utf-8 -*-
from ..base import BaseMapper
from asyncelasticutils import S, F, get_es
from tornado import gen


class BaseHistoryMapper(BaseMapper):
    """
    Base History Mapper.
    """

    DEFAULT_SEEN = False

    @classmethod
    def get_index(cls):
        """

        :return:
        """
        from api_engine import s
        name = 'history'
        if s().environment == 'TESTING':
            name += '-testing'
        name += '-index'
        return name

    @classmethod
    def add(cls, data):
        """

        :param data:
        :return:
        """
        document = cls.get_event_data(data)
        document['is_visible'] = True
        result = cls.index(document=document)

        return result

    @classmethod
    def get_by_user_id(cls, user_id):
        """

        :param user_id:
        :return:
        """
        result = S(cls).filter(cls.f_main(user_id)).order_by('-modify_ts')

        return result

    @classmethod
    @gen.coroutine
    def _get_count(cls, user_id):
        """

        :param user_id:
        :return:
        """
        q = S(cls).filter(cls.f_counter(user_id))
        c = yield q.count()
        raise gen.Return(c)

    @classmethod
    @gen.coroutine
    def read(cls, _id):
        """

        :param _id:
        :return:
        """
        result = S(cls).filter(F(_id=_id))
        result = yield result.execute()
        if not len(result):
            return
        for r in result:
            yield cls.update(r._id, body=dict(doc=dict(seen=True)))

    @classmethod
    @gen.coroutine
    def read_all(cls, user_id):
        """

        :param user_id:
        :return:
        """
        q = S(cls).filter(F(user_id=user_id) & F(seen=False))
        count = yield q.count()
        result = yield q[:count].execute()
        for r in result:
            r.es_meta.source.update(_id=r._id)
            r.es_meta.source.update(seen=True)
        cls.bulk_index([r.es_meta.source for r in result], id_field='_id')

    @classmethod
    @gen.coroutine
    def get_by_video_id(cls, video_id, offset=0, limit=10):
        """

        :param video_id:
        :param offset:
        :param limit:
        :return:
        """
        if not cls.is_video_associated():
            raise gen.Return([])
        raise gen.Return(cls.get(video_id=video_id, offset=offset, limit=limit))

    @classmethod
    @gen.coroutine
    def get_count_by_video_id(cls, video_id):
        """

        :param video_id:
        :return:
        """
        if not cls.is_video_associated():
            raise gen.Return(0)
        c = yield cls._get_count(_filter=F(video_id=video_id))
        raise gen.Return(c)

    @classmethod
    @gen.coroutine
    def get_all_by_video_id(cls, video_id):
        """

        :param video_id:
        :return:
        """
        if not cls.is_video_associated():
            raise gen.Return([])
        count = yield cls.get_count_by_video_id(video_id)
        r = yield cls.get(video_id=video_id, limit=count).execute()
        raise gen.Return(r)

    @classmethod
    def get_echo(cls, user_id):
        """
        Get likes and comments

        :param user_id:
        :return:
        """
        f = cls.f_main(user_id)
        f &= (F(event='video:like') | F(event='video:comment_add'))
        result = S(cls).doctypes('h_vlike,h_comment').filter(f).order_by('-modify_ts')

        return result

    @classmethod
    @gen.coroutine
    def hide_by_video_id(cls, video_id):
        """

        :param video_id:
        :return:
        """
        if not cls.is_video_associated():
            return
        historys = yield cls.get_all_by_video_id(video_id)
        for h in historys:
            yield cls.update(h.id, dict(doc=dict(is_visible=False)))
        return

    @classmethod
    @gen.coroutine
    def unhide_by_video_id(cls, video_id):
        """

        :param video_id:
        :return:
        """
        if not cls.is_video_associated():
            return
        historys = yield cls.get_all_by_video_id(video_id)
        for h in historys:
            yield cls.update(h.id, dict(doc=dict(is_visible=True)))
        return

    @classmethod
    def f_base(cls, user_id):
        """

        :param user_id:
        :return:
        """
        return cls.f_user(user_id) & F(is_visible=True)

    @classmethod
    def f_user(cls, user_id):
        """

        :param user_id:
        :return:
        """
        return F(user_id=user_id) & ~F(event_user_id=user_id)

    @classmethod
    def f_main(cls, user_id):
        """

        :return:
        """
        return cls.f_base(user_id)

    @classmethod
    def f_counter(cls, user_id):
        """

        :return:
        """
        return cls.f_main(user_id) & F(seen=False)
