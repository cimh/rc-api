# -*- coding: utf-8 -*-
from asyncelasticutils import S, F
from tornado import gen
import api_engine.enums as enums
from .base import BaseHistoryMapper


class FollowHistoryEntryMappingType(BaseHistoryMapper):
    """
    Follow History Mapper.
    """

    @classmethod
    def get_mapping_type_name(cls):
        """

        :return:
        """
        return 'h_follow'

    @classmethod
    def get_mapping(cls):
        """

        :param cls:
        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'event': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'event_user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'follow_request_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        '_from_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'user_id': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'status': {
                            'type': 'integer',
                        },
                        'message': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'seen': {
                            'type': 'boolean',
                        },
                        'is_visible': {
                            'type': 'boolean',
                        },
                    }
                }
            }
        }

    @classmethod
    def get_event_data(cls, data):
        """

        :param data:
        :return:
        """
        return dict(
            event=data['event'],
            event_user_id=data['event_user_id'],
            _from_id=data['from'],
            user_id=data['to'],
            status=data['status'],
            modify_ts=cls.now_YmdHMS(),
            message=data['message'],
            seen=cls.DEFAULT_SEEN,
            follow_request_id=data['follow_request_id'],
        )

    @classmethod
    def add(cls, data):
        """

        :param data:
        :return:
        """
        if cls.is_follow_request(data) \
                or cls.is_follow_discard(data):
            yield cls.request(data)

        elif cls.is_follow_accept(data):
            yield cls.accept(data)

        super(FollowHistoryEntryMappingType, cls).add(data)

    @classmethod
    def is_follow_request(cls, data):
        """

        :param data:
        :return:
        """
        return data['event'] == 'follow:follow' and data['status'] == enums.FOLLOW_STATUS.request

    @classmethod
    def is_follow_discard(cls, data):
        """

        :param data:
        :return:
        """
        return data['event'] == 'follow:discard'

    @classmethod
    def is_follow_accept(cls, data):
        """

        :param data:
        :return:
        """
        return data['event'] == 'follow:accept'

    @classmethod
    def f_main(cls, user_id):
        """

        :return:
        """
        return cls.f_requests(user_id)

    @classmethod
    def f_requests_from(cls, from_id, to_id):
        """

        :param from_id:
        :param to_id:
        :return:
        """
        f = cls.f_requests(to_id) & F(_from_id=from_id)
        return f

    @classmethod
    def f_requests(cls, to_id):
        """

        :param to_id:
        :return:
        """
        f = super(FollowHistoryEntryMappingType, cls).f_main(to_id)
        f &= F(event='follow:follow') & F(status=enums.FOLLOW_STATUS.request)
        return f

    @classmethod
    @gen.coroutine
    def request(cls, data):
        """

        :param data:
        :return:
        """
        requests = yield cls.get_requests(data['from'], data['to'])
        list(map(cls.delete, (req.id for req in requests)))

    @classmethod
    @gen.coroutine
    def accept(cls, data):
        """

        :param data:
        :return:
        """
        requests = yield cls.get_requests(data['from'], data['to'])
        list([cls.update(_id, body=dict(doc=dict(seen=True))) for _id in (r.id for r in requests)])

    @classmethod
    @gen.coroutine
    def get_requests(cls, from_id, to_id):
        """

        :param from_id:
        :param to_id:
        :return:
        """
        f = cls.f_requests_from(from_id, to_id)
        count = yield cls._get_count(_filter=f)
        raw = yield S(cls).order_by('-modify_ts').filter(f)[:count].execute()
        raise gen.Return(raw)
