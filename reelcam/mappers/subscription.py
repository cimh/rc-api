# -*- coding: utf-8 -*-
from .base import BaseMapper


class SubscriptionEntryMappingType(BaseMapper):
    """
    SmsLog Mapper.
    """

    @classmethod
    def get_mapping(cls):
        """

        :return:
        """
        return {
            'mappings': {
                cls.get_mapping_type_name(): {
                    'properties': {
                        'name': {
                            'type': 'string',
                            'index': 'not_analyzed',
                        },
                        'limit': {
                            'type': 'integer',
                        }
                    }
                }
            }
        }
