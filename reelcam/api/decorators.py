# -*- coding: utf-8 -*-
from functools import wraps
from api_engine import apiresults
from api_engine.enums import USER_TYPE
from tornado.concurrent import Future

DEFAULT_ALLOWED_TYPES = [USER_TYPE.user, USER_TYPE.bot, USER_TYPE.proxy, USER_TYPE.hbq_operator,
                         USER_TYPE.wedcam_operator, USER_TYPE.wedcam_trend, USER_TYPE.system]


def req_auth(allowed_types=[]):
    """
    :param allowed_types: - allowed user types
    :return:
    """

    if not isinstance(allowed_types, (list, tuple)):
        allowed_types = [allowed_types]
    allowed_types = DEFAULT_ALLOWED_TYPES + allowed_types

    # @wraps
    def decorator(func):
        """

        :param func:
        :return:
        """
        @wraps(func)
        def wrap(self, *args, **kw):
            """

            :param args:
            :param kw:
            :return:
            """
            user = self.ctx.request_user
            if user is None:
                f = Future()
                f.set_result(apiresults.unauthorized())
                return f
            if user.user_type not in allowed_types:
                f = Future()
                f.set_result(apiresults.forbidden())
                return f
            return func(self, *args, **kw)
        return wrap
    return decorator


def req_unauth(func):
    """

    :param func:
    :return:
    """
    @wraps(func)
    def wrapped(self, data):
        """

        :param data:
        :return:
        """
        if self.ctx.request_user is not None:
            f = Future()
            f.set_result(apiresults.forbidden())
            return f
        return func(self=self, data=data)

    return wrapped
