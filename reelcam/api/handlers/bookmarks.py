# -*- coding: utf-8 -*-
from api_engine import apiresults, s
from ..decorators import req_auth
from .base import BaseAPIHandler
from tornado import gen
from api_engine.enums import REELCAM, CITYDOOM, WEDCAM


class BookmarksAPIHandler(BaseAPIHandler):
    """
    Bookmarks API Handler class.
    """

    _handles = {
        'add': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'delete': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'get': {
            'bookmark_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'ts': dict(type='integer'),
        },
        'get_list': {
            'items': dict(type='dict', required=True, keyschema=dict(type='integer', nullable=True),
                          maxlength=BaseAPIHandler.ES_ID_MAX_LENGTH),
        },
        'mine': {
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
            'scope': dict(type='string', allowed=[REELCAM, CITYDOOM, WEDCAM], minlength=6, maxlength=8,
                          nullable=True),
            'ts': dict(type='integer'),
        },
    }

    @req_auth()
    async def add(self, data):
        """
        Adds video to user bookmarks by video ID.

        :param: ``str`` **video_id**: Video ID.

        Emits event **bookmarks:add**

        .. code:: javascript

            {
                "data": {
                    "event_user_id": "<user ID>",
                    "id": "<bookmark ID>",
                    "ts": "<timestamp>",
                    "user_id": "<bookmark user ID>",
                    "video_id": "<bookmarked video ID>"
                },
                "event_user_id": "<user ID>",
                "method": "bookmarks:add"
            }

        Returns

        **404 Not Found**
            if no video with `video_id` found

        **400 Bad Request**
            for bad data in args

        **302 Found**
            with `id` and `modify_ts` in data if bookmark already exists

        **200 OK** 
            with `id` and `ts` in `data` in success

        """
        Video = s().M.model('Video', self.ctx)
        Bookmark = s().M.model('Bookmark', self.ctx)
        video_id = data['video_id']
        user_id = self.ctx.request_user.id
        video = await Video.get_one(_id=video_id, _deleted=False)
        if not video:
            return apiresults.not_found()
        b = Bookmark.new(user_id=user_id, video_id=video_id)
        b = await Bookmark.get_one(_id=b.custom_id)
        if not b:
            bookmark = Bookmark.new(user_id=user_id, video_id=video_id, scope=video.scope)
            if not bookmark.validate():
                return apiresults.bad_request()
            bookmark = await bookmark.add(video)
            return apiresults.ok(**bookmark.get_idts())
        else:
            return apiresults.found(**b.get_idts())

    @req_auth()
    async def delete(self, data):
        """
        Removes bookmark from user bookmarks by video ID.

        :param: ``str`` **video_id**: Video ID.

        Emits event **bookmarks:delete**

        .. code:: javascript

            {
                "data": {
                    "event_user_id": "<user ID>",
                    "id": "<bookmark ID>",
                    "ts": "<timestamp>",
                    "user_id": "<bookmark owner ID>",
                    "video_id": "<bookmarked Video ID>"
                },
                "event_user_id": "<User ID>",
                "method": "bookmarks:delete"
            }

        Returns

        **404 Not Found**
            if no bookmark with `bookmark_id` found

        **200 OK**
            in success

        """
        Bookmark = s().M.model('Bookmark', self.ctx)
        Video = s().M.model('Video', self.ctx)
        user_id = self.ctx.request_user.id
        video_id = data['video_id']
        video = await Video.get_one(_id=video_id)
        b = Bookmark.new(user_id=user_id, video_id=video_id)
        bookmark = await Bookmark.get_one(_id=b.custom_id)
        if bookmark:
            await bookmark.delete(video)
        else:
            return apiresults.not_found()
        return apiresults.ok()

    @req_auth()
    async def get(self, data):
        """
        Get bookmark data by bookmark ID.

        :param: ``str`` **bookmark_id**: Bookmark ID.

        Returns

        **404 Not Found**
            if no bookmark with `bookmark_id` found

        **403 Forbidden**
            if authorized user is not in bookmark

        **200 OK**
            in success

        """
        Bookmark = s().M.model('Bookmark', self.ctx)
        bookmark = await Bookmark.get_one(_id=data['bookmark_id'])
        if not bookmark:
            return apiresults.not_found()
        if bookmark.user_id != self.ctx.request_user.id:
            return apiresults.forbidden()
        return apiresults.ok(**bookmark.get_data())

    @req_auth()
    async def get_list(self, data):
        """
        Get bookmarks data in list by bookmark IDs.

        :param: ``dict`` **items**: { ``str`` **id1**: ``int`` ts1, **id2**: ts2, ...}

        Returns

        **200 OK**
            with `items` in `data` in success

        """
        Bookmark = s().M.model('Bookmark', self.ctx)
        ids = list(data.get('items', []))
        bookmarks = await Bookmark.get(_ids=ids, limit=len(ids))
        items = [{bookmark.id: bookmark.get_data()} for bookmark in bookmarks
                 if bookmark.user_id == self.ctx.request_user.id]
        return apiresults.ok(items=items)

    @req_auth()
    async def mine(self, data):
        """
        Returns list of bookmarks in form of ID-timestamp pairs.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``int`` **ts**: Timestamp.

        Returns

        **200 OK**
            with `items` and `has_more` in `data`

        """
        scope = s().get_scope(data.get('scope'))
        Bookmark = s().M.model('Bookmark', self.ctx, scope=scope)
        current_user_id = self.ctx.request_user.id
        bookmarks, has_more = await Bookmark.get_mine(current_user_id, data.get('limit', 10), data.get('offset', 0))
        return apiresults.ok(items=[bookmark.get_idts() for bookmark in bookmarks], has_more=has_more)
