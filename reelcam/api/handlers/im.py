# -*- coding: utf-8 -*- 
from .base import BaseAPIHandler
from tornado import gen
from api_engine import apiresults


class IMAPIHandler(BaseAPIHandler):
    """
    Instant Messaging API Handler class.
    """

    _handles = {
        'delete': {
            'im_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'msg_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'get': {
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'im_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'read': {
            'msg_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'track': {
            'im_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'untrack': {
            'im_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'write': {
            'im_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'body': dict(type='string', minlength=1),
        },
    }

    @gen.coroutine
    def delete(self, data):
        """

        :param data:
        :return:
        """
        raise gen.Return(apiresults.not_implemented())

    @gen.coroutine
    def get(self, data):
        """

        :param data:
        :return:
        """
        raise gen.Return(apiresults.not_implemented())

    @gen.coroutine
    def read(self, data):
        """

        :param data:
        :return:
        """
        raise gen.Return(apiresults.not_implemented())

    @gen.coroutine
    def track(self, data):
        """

        :param data:
        :return:
        """
        raise gen.Return(apiresults.not_implemented())

    @gen.coroutine
    def untrack(self, data):
        """

        :param data:
        :return:
        """
        raise gen.Return(apiresults.not_implemented())

    @gen.coroutine
    def write(self, data):
        """

        :param data:
        :return:
        """
        raise gen.Return(apiresults.not_implemented())
