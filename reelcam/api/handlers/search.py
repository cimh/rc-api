# -*- coding: utf-8 -*-
from api_engine import apiresults
from .base import BaseAPIHandler
from api_engine import s
from tornado import gen
from api_engine.enums import CITYDOOM, REELCAM, WEDCAM


class SearchAPIHandler(BaseAPIHandler):
    """
    Search API Handler.
    """

    _handles = {
        'find': {
            'scope': dict(type='string', allowed=[REELCAM, CITYDOOM, WEDCAM],
                          minlength=6, maxlength=8, nullable=True),
            'query': dict(type='string', empty_or_minlength=3, maxlength=256),
            'location': dict(type='dict', schema={'lat': {'type': 'float'}, 'lon': {'type': 'float'}}),
            'start_ts': dict(type='string'),
            'end_ts': dict(type='string'),
            'type_stream': dict(type='integer', allowed=[3, 2, 1, 0]),
            'type_user': dict(type='integer', allowed=[3, 2, 1, 0]),
            'radius': dict(type='integer'),
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
    }

    async def find(self, data):
        """
        Searches app content by username or video title.

        :param: ``str`` **scope**: data scope.
        :param: ``str`` **query**: Query to search in User.fullname and video titles.
        :param: ``str`` **location**: Search Location.
        :param: ``date`` **start_ts**: Video.reporter_ts date.
        :param: ``date`` **end_ts**: Video.reporter_ts end date.
        :param: ``int`` **type_stream**: Video type. [3-all, 2-live, 1-finished, 0-don't search videos]. Default is 3.
        :param: ``int`` **type_user**: User type. [3-all, 2-bot, 1-user, 0-don't search users]. Default is 3.
        :param: ``int`` **radius**: Radius of search in meters.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **400 Bad Request**
            if none of arguments received.

        **200 OK**
            with `users` and `videos` in `data` in success

        """
        scope = s().get_scope(data.pop('scope', None))
        Video = s().M.model('Video', self.ctx, scope=scope)
        User = s().M.model('User', self.ctx)

        if data.get('location'):
            if data['location'].get('lng'):
                data['location']['lon'] = data['location'].pop('lng')

        # remove left right spaces and make lowercase
        if 'query' in data:
            data['query'] = data['query'].strip().lower()

        # if neither query nor location in data
        if not (('query' in data and len(data['query']) != 0) or 'location' in data):
            return apiresults.bad_request()

        # parse string to query (username or video subject) and tags (video tags)
        if 'query' in data:
            arr_query = data['query'].split()
            data['tags'] = [q[1:] for q in arr_query if q[0] == "#"]
            data['query'] = [q for q in arr_query if q[0] != "#" and len(q) > 2]

        # do not search videos if "type_stream" is 0
        if 'type_stream' not in data or data['type_stream']:
            videos = await Video.search(data)
        else:
            videos = []

        # do not search users by tag or if "type_user" is 0
        if not ('tags' in data and data['tags']) and ('type_user' not in data or data['type_user']):
            users = await User.search(data)
        else:
            users = []
        items = dict(users=[], videos=[])
        for u in users:
            d = await u.get_data()
            items['users'].append(d)

        for v in videos:
            d = await v.get_data()
            items['videos'].append(d)

        if scope == s().get_scope("wedcam"):
            if data.get('query'):
                Wedding = s().M.model('Wedding')
                weddings = await Wedding.search(data)
                items['weddings'] = [wed.get_data() for wed in weddings]

        return apiresults.ok(**items)
