# -*- coding: utf-8 -*-
from api_engine import apiresults
from .base import BaseAPIHandler
from ..decorators import req_auth
from ...models.subscription import Subscription
from utils.datetimemixin import dtm
import base64
import binascii


class SubscriptionApiHandler(BaseAPIHandler):
    """
    Subscription API Handler.
    """

    _handles = {
        'in_app_purchase_android': {
            'product_id': dict(required=True, type='string', minlength=1),
            'purchase_token': dict(required=True, type='string', minlength=1),
        },
        'in_app_purchase_ios': {
            'transaction': dict(required=True, type='string', minlength=1),
            'receipt': dict(required=True, type='string', minlength=1),
        },
    }

    @req_auth()
    def in_app_purchase_android(self, data):
        """
        In app purchase for android platform.

        :param: ``str`` product_id: The purchased subscription id.
        :param: ``str`` purchase_token: Token for purchase verification.
        """
        subscription = Subscription.in_app_purchase_android(data["product_id"], data["purchase_token"])

        if subscription and subscription["expire_ts"] > dtm.now_ts():
            # Proceed transaction if verified
            self.ctx.request_user.proceed_purchase(
                data["product_id"],
                subscription["expire_ts"],
                'playmarket',
                subscription["autoRenewing"],
                data["purchase_token"])
            return apiresults.ok()
        else:
            return apiresults.error(error="in_app_purchase_failed")

    @req_auth()
    def in_app_purchase_ios(self, data):
        """
        In app purchase for ios platform.

        :param: ``str`` receipt: Base64 string for purchase verification.
        """
        receipt = data['receipt']

        # Check receipt for base64 validation
        try:
            base64.b64decode(receipt)
        except binascii.Error:
            return apiresults.bad_request()

        Subscription.in_app_purchase_ios(receipt)
        return apiresults.ok()
