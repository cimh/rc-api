# -*- coding: utf-8 -*-
from api_engine import apiresults
from .base import BaseAPIHandler
from ..decorators import req_auth
from api_engine import s
from tornado import gen


class ProxyAPIHandler(BaseAPIHandler):
    """
    Proxy API Handler.
    """

    _handles = {
        'login': {
            'login': dict(required=True, type='string', regex='[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+', maxlength=128),
            'pass': dict(required=True, type='string'),
            'device': dict(type='string'),
        },
        'online': {
            'proxy_port_video': dict(required=True, type='integer'),
            'proxy_port_rtmp': dict(required=True, type='integer'),
            'proxy_port_web': dict(required=True, type='integer'),
        },
    }

    async def login(self, data):
        """
        Custom login method to avoid using user:login when proxy is logging in.

        :param: ``str`` **login**: Login.
        :param: ``str`` **pass**: Password.
        :param: ``str`` **device**: For compatibility?

        Returns

        **404 Not Found**
              if no user with `login` found

        **403 Forbidden**
            if login was failed

        **200 OK**
            with `email`, `id`, `sessionid` and `auth` in `data` in success

        """
        User = s().M.model('User', self.ctx)

        if self.ctx.request_user:
            return apiresults.not_modified()

        user = await User.get_one(email=data['login'].lower())
        if not user:
            return apiresults.not_found()

        if user.is_deleted:
            return apiresults.forbidden(detete_status=user.delete_status)

        device = data.get('device', None)
        gadget = (device == 'gadget')

        r = await user.login(data['pass'], gadget=gadget)
        if not r:
            return apiresults.forbidden()

        return apiresults.ok(email=user.email,
                             id=user.id,
                             sessionid=self.ctx.session,
                             auth='normal')

    @req_auth()
    async def online(self, data):
        """
        Get ID of proxy that is currently online.

        :param: ``int`` **proxy_port_video**: Proxy video port.
        :param: ``int`` **proxy_port_rtmp**: Proxy RTMP port.
        :param: ``int`` **proxy_port_web**: Proxy Web port.

        Returns

        **200 OK**
            with proxy ID in `id` in success

        """
        Proxy = s().M.model('Proxy', self.ctx)

        _data = {key.split('proxy_')[1]: data[key]
                 for key in [key for key in data if key.startswith('proxy_')]}
        _data.update(host=self.ctx.client_ip)

        proxy = await Proxy.get_by_host_and_ports(_data)
        if proxy is not None:
            await proxy.set_online()

        else:
            proxy = Proxy.new(**_data)
            proxy = await proxy.save()
            await proxy.set_online()

        return apiresults.ok(id=proxy.id)
