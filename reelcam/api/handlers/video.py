# -*- coding: utf-8 -*-
from uuid import uuid4
from tornado import gen
from tornado.ioloop import IOLoop
from api_engine import apiresults
from api_engine import s
from api_engine.enums import APP_NAMES, GPS_TYPES, VIDEO_SUBJECT_MIN_LENGTH, VIDEO_PERMISSIONS, USER_TYPE, VIDEO_STATUS
from reelcam.utils import location_from_ip
from utils.datetimemixin import dtm
from .base import BaseAPIHandler
from ..decorators import req_auth
from ...event import E
from ...models.social import Social
from api_engine.enums import REELCAM, CITYDOOM, WEDCAM


class VideoAPIHandler(BaseAPIHandler):
    """
    Video API Handler class.
    """

    _handles = {
        'can_comment': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'comment_add': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'body': dict(required=True, type='string', minlength=1, maxlength=1024),
        },
        'comment_delete': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'comment_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'comment_get_list': {
            'items': dict(type='dict', required=True, keyschema={'type': 'integer', 'nullable': True}),
        },
        'comment_recent': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'count': {
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'create': {
            'title': dict(required=True, type='string', minlength=VIDEO_SUBJECT_MIN_LENGTH, maxlength=128),
            'lat': dict(type='float'),
            'lon': dict(type='float'),
            'seconds': dict(type='integer'),
            'type': dict(type='integer', allowed=[GPS_TYPES.network, GPS_TYPES.gps]),
            'start_ts': dict(type='string', minlength=1),
            'app_name': dict(type='string', allowed=APP_NAMES),
            'is_external': dict(type='boolean'),
        },
        'create_offline': {
            'title': dict(required=True, type='string', minlength=3, maxlength=128),
            'lat': dict(type='float'),
            'lon': dict(type='float'),
            'seconds': dict(type='integer'),
            'type': dict(type='integer', allowed=[GPS_TYPES.network, GPS_TYPES.gps]),
            'start_ts': dict(type='string', minlength=1),
            'close_ts': dict(type='string', minlength=1),
            'app_name': dict(type='string', allowed=APP_NAMES),
        },
        'delete': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'force': dict(type='boolean')
        },
        'edit': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'subject': dict(type='string', minlength=3, maxlength=128),
            'gps_incognito': dict(type='boolean'),
            'permission': dict(type='integer', allowed=[0, 1, 2, 3]),
            'tags': dict(type='string', minlength=3, maxlength=128),
            'wedding_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'favorites': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'limit': dict(type='integer'),
            'offset': dict(type='integer'),
        },
        'find': {
            'subject': dict(required=True, type='string', minlength=3),
        },
        'finish': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'get': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'get_coords': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH)
        },
        'get_list': {
            'items': dict(type='dict', required=True, keyschema={'type': 'integer', 'nullable': True},
                          maxlength=100),
        },
        'get_permission': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'get_place_name': {
            'lat': dict(type='float'),
            'lon': dict(type='float'),
            'language': dict(type='string', minlength=2, maxlength=2),
        },
        'i_follow': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'live': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'live_msg': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'message': dict(required=True, type='string', minlength=1),
        },
        'live_msg_timeline': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'mine': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'limit': dict(type='integer'),
            'offset': dict(type='integer'),
        },
        'near_me': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
            'lat': dict(type='float', min=-90, max=90),
            'lon': dict(type='float', min=-180, max=180),
        },
        'new_view': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'pause': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'reporter_token': dict(type='string', minlength=32),
        },
        'popular': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'reanimate': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'recent': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'record_uploaded': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'reporter_token': dict(type='string', minlength=32),
        },
        'set_ban': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'set_coords': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'reporter_token': dict(type='string', minlength=32),
            'lat': dict(required=True, type='float', min=-90, max=90),
            'lon': dict(required=True, type='float', min=-180, max=180),
            'time': dict(required=True, type='integer', min=0),
            'seconds': dict(required=True, type='integer'),
            'type': dict(required=True, type='integer', allowed=[GPS_TYPES.network, GPS_TYPES.gps])
        },
        'set_dislike': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'set_gadget_info': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'i1': dict(type='integer'),
            'i2': dict(type='integer'),
            'i3': dict(type='integer'),
            'i4': dict(type='integer'),
            'i5': dict(type='integer'),
            'i6': dict(type='integer'),
            'i7': dict(type='integer'),
            'i8': dict(type='integer'),
            's1': dict(type='string'),
            's2': dict(type='string'),
        },
        'set_like': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'set_offline_coords': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'coords': {
                'required': True,
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'lat': dict(required=True, type='float', min=-90, max=90),
                        'lon': dict(required=True, type='float', min=-180, max=180),
                        'time': dict(required=True, type='integer', min=0),
                        'seconds': dict(required=True, type='integer'),
                        'type': dict(required=True, type='integer', allowed=[GPS_TYPES.network, GPS_TYPES.gps])
                    }
                }
            },
        },
        'set_permission_for_all': {

        },
        'social_post': {
            'providers': dict(type='list', required=True, schema=dict(type='string')),
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'start': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'reporter_token': dict(type='string', minlength=32),
        },
        'start_view': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'startcheck': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'reporter_token': dict(type='string', minlength=32),
        },
        'stop_view': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'track': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'track_list': {
            'items': dict(type='dict', required=True, keyschema={'type': 'integer', 'nullable': True},
                          maxlength=100),
        },
        'unset_ban': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'unset_dislike': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'unset_like': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'untrack': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'untrack_list': {
            'items': dict(type='dict', required=True, keyschema={'type': 'integer', 'nullable': True},
                          maxlength=100),
        },
        'x_live': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'x_mine': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'limit': dict(type='integer'),
            'offset': dict(type='integer'),
        },
        'x_near_me': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
            'lat': dict(type='float', min=-90, max=90),
            'lon': dict(type='float', min=-180, max=180),
        },
        'x_popular': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'x_recent': {
            'scope': dict(type='string', allowed=APP_NAMES, nullable=True),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
    }

    @req_auth()
    async def can_comment(self, data):
        """
        Check that user can comment video.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if user cannot comment video

        **200 OK**
            in success

        """
        Video = s().M.model('Video')

        video = await Video.get_one(_id=data['video_id'])
        if not video:
            return apiresults.not_found()

        can_comment = await video.can_comment(self.ctx.request_user.id)
        if can_comment:
            return apiresults.ok()

        return apiresults.forbidden()

    @req_auth()
    async def comment_add(self, data):
        """
        Add comment to video.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **body**: Comment body.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if user cannot comment video

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not video:
            return apiresults.not_found()

        comment = await video.comment_add(self.ctx.request_user, data['body'])
        if comment:
            return apiresults.ok(id=comment.id)
        return apiresults.forbidden()

    @req_auth()
    async def comment_delete(self, data):
        """
        Delete video comment by video_id and comment_id.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **comment_id**: Comment ID.

        Returns

        **404 Not Found**
            if no video or comment found

        **403 Forbidden**
            if user cannot delete this comment

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        Comment = s().M.model('Comment', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not video:
            return apiresults.not_found()

        comment = await Comment.get_one(_id=data['comment_id'], video_id=data['video_id'], _deleted=False)
        if not comment:
            return apiresults.not_found()

        if not comment.is_author(self.ctx.request_user):
            return apiresults.forbidden()

        await video.comment_delete(data['comment_id'])

        return apiresults.ok()

    async def comment_get_list(self, data):
        """
        Get recent comments for video by ID multiple.

        :param: ``list`` **items**: {``str`` **video1_id**: ``int`` ts1, **video2_id**: ts2, ...}

        Returns

        **200 OK**
            with comments list in `items`

        """
        Video = s().M.model('Video', self.ctx)
        ids = list(data.get('items', []))
        videos = await Video.get(_ids=ids, limit=len(ids))
        comments = await Video.comment_multiple(videos=videos, limit=100)

        return apiresults.ok(items=comments)

    async def comment_recent(self, data):
        """
        Get recent comments for video by ID.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **200 OK**
            with comments list in `items`

        """
        Video = s().M.model('Video', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not video:
            return apiresults.not_found()

        comments, has_more = await video.comment_recent(offset=data.get('offset', 0),
                                                        limit=data.get('limit', 10))

        await Video._join_users_to_comments(comments)

        items = [{comment.id: comment.get_data()} for comment in comments]

        return apiresults.ok(items=items, has_more=has_more)

    async def count(self, data):
        """
        Returns count of videos for user ID or current user if none.

        :param: ``str`` **user_id**: User ID.

        Returns

        **400 Bad Request**
            if not authorized and no `user_id` in `data`

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        user_id = data.get('user_id', None)

        if not user_id:
            if self.ctx.request_user:
                user_id = self.ctx.request_user.id
            else:
                return apiresults.bad_request()

        count = await Video.get_owned_by_count(user_id)

        return apiresults.ok(count=count)

    @req_auth()
    async def create(self, data):
        """
        Creates new video from current user.

        :param: ``str`` **title**: Video subject.
        :param: ``float`` **lat**: Latitude of GPS point.
        :param: ``float`` **lon**: Longitude of GPS point.
        :param: ``str`` **start_ts**: Timestamp of video start point.
        :param: ``str`` **app_name**: Application name.

        Returns

        **403 Forbidden**
            if no proxy available or video could not create

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        Wedding = s().M.model('Wedding')

        location = {}
        if data.get('lat') is not None:
            if data.get('lon') is not None:
                try:
                    location['lat'] = float(data['lat'])
                    location['lon'] = float(data['lon'])
                except (TypeError, ValueError):
                    pass

        if not location:
            location = location_from_ip(self.ctx.client_ip)

        Proxy = s().M.model('Proxy', self.ctx)
        proxy = await Proxy.get_closest(self.ctx.client_ip)
        if not proxy:
            return apiresults.forbidden()

        current_user = await User.get_one(_id=self.ctx.request_user.id)

        data.update(subject=data.pop('title').strip())
        scope = s().get_scope(data.get('app_name'))
        Video = s().M.model('Video', self.ctx, scope=scope)
        new_subject, exist_video = await Video.check_exists(data['subject'], current_user)

        permission = current_user.default_video_permission
        is_wedcam_trend = False
        if data.get('app_name') == "wedcam":
            if current_user.joined_wedding_id:
                if current_user.wedding_id != current_user.joined_wedding_id:
                    permission = VIDEO_PERMISSIONS.all
        if current_user.user_type is USER_TYPE.wedcam_trend:
            is_wedcam_trend = True

        photo = None
        if data.get('photo'):
            from base64 import b64decode
            from reelcam.models.video.video import check_photo
            try:
                photo = b64decode(data.pop('photo'))
                if not check_photo(photo):
                    raise TypeError
            except TypeError:
                return apiresults.bad_request()

        video = Video(dict(reporter_token=uuid4().hex,
                           observer_token=uuid4().hex,
                           subject=new_subject if exist_video else data['subject'],
                           permission=permission,
                           gps_incognito=current_user.gps_incognito,
                           owner_id=current_user.id,
                           reporter_id=current_user.id,
                           proxy_id=proxy.id,
                           country_code=current_user.country_code,
                           wedding_id=getattr(current_user, 'joined_wedding_id', None),
                           is_wedcam_trend=is_wedcam_trend,
                           status=VIDEO_STATUS.paused,
                           last_pause_time=dtm.now_YmdHMS(),
                           is_photo=bool(photo),
                           reporter_start_ts=dtm.now_YmdHMS(),
                           is_external=data.get('is_external', False)))
        if video.is_photo:
            video.reporter_start_ts = dtm.now_YmdHMS()
            video.status = VIDEO_STATUS.finished
        video = await video.save()
        if video.is_photo:
            video = await video.upload_photo(photo)

        IOLoop.current().add_callback(video.set_gps, location, data.get('time', 0),
                                      data.get('seconds'), data.get('type'))
        IOLoop.current().add_callback(Video.finish_previous, current_user.id, video.id)

        if video.is_wedcam_trend:
            trend = await Wedding.get_trend(current_user.id)
            await trend.add_video(video.id)

        is_reached_limit = await current_user.is_reached_subscription_limit()

        return apiresults.ok(title=video.subject,
                                       reporter_token=video.reporter_token,
                                       video_id=video.id,
                                       proxy_host=proxy.host,
                                       proxy_port_video=proxy.port_video,
                                       is_reached_limit=is_reached_limit)

    @req_auth()
    async def create_offline(self, data):
        """
        Creates new video from current user.

        :param: ``str`` **title**: Video subject.
        :param: ``float`` **lat**: Latitude of GPS point.
        :param: ``float`` **lon**: Longitude of GPS point.
        :param: ``str`` **start_ts**: Timestamp of video start point.
        :param: ``str`` **close_ts**: Timestamp of video stop point.
        :param: ``str`` **app_name**: Application name.

        Returns

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        scope = s().get_scope(data.get('app_name'))
        Video = s().M.model('Video', self.ctx, scope=scope)
        user = await User.get_one(_id=self.ctx.request_user.id)

        location = {}
        if data.get('lat') is not None:
            if data.get('lon') is not None:
                try:
                    location['lat'] = float(data['lat'])
                    location['lon'] = float(data['lon'])
                except (TypeError, ValueError):
                    pass

        if not location:
            location = location_from_ip(self.ctx.client_ip)

        start_ts = data.get('start_ts', None)
        close_ts = data.get('close_ts', None)

        if None not in (start_ts, close_ts):

            start_ts = dtm.dt_from_YmdHMS(start_ts)
            close_ts = dtm.dt_from_YmdHMS(close_ts)
            _now = dtm.now_dt()

            if start_ts >= _now:

                if close_ts < start_ts:

                    delta = close_ts - start_ts
                    start_ts = _now
                    close_ts = start_ts + delta

                else:
                    start_ts = _now
                    close_ts = None

            elif close_ts > start_ts:

                close_ts = None

        else:
            start_ts = dtm.now_dt()
            close_ts = None

        data.update(subject=data.pop('title').strip())
        new_subject, exist_video = await Video.check_exists(data['subject'], user)

        permission = user.default_video_permission
        if data.get('app_name') == WEDCAM:
            if user.joined_wedding_id:
                if user.wedding_id != user.joined_wedding_id:
                    permission = VIDEO_PERMISSIONS.all

        video = Video(dict(reporter_token=uuid4().hex,
                           observer_token=uuid4().hex,
                           subject=new_subject if exist_video else data['subject'],
                           permission=permission,
                           gps_incognito=user.gps_incognito,
                           last_pause_time=dtm.now_YmdHMS(),
                           owner_id=user.id,
                           reporter_id=user.id,
                           reporter_start_ts=dtm.YmdHMS_from_dt(start_ts),
                           reporter_close_ts=dtm.YmdHMS_from_dt(close_ts),
                           country_code=user.country_code,
                           wedding_id=getattr(user, 'joined_wedding_id', None),
                           status=VIDEO_STATUS.finished))

        video = await video.save()

        await video.set_gps(location, data.get('time', 0), data.get('seconds'), data.get('type'))

        return apiresults.ok(
            title=video.subject,
            reporter_token=video.reporter_token,
            video_id=video.id,
        )

    @req_auth()
    async def delete(self, data):
        """
        Deletes a video by ID.

        :param: ``str`` **video_id**: ID of video.
        :param: ``bool`` **force**: Forced delete flag.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if could not delete video

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)

        video = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not video:
            return apiresults.not_found()

        video = await Video.delete_video(data['video_id'], self.ctx.request_user.id, data.get('force', False))
        if video:
            return apiresults.ok()
        return apiresults.forbidden()

    @req_auth()
    async def edit(self, data):
        """
        Edit video data.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **subject**: Video title.
        :param: ``bool`` **gps_incognito**: GPS Incognito parameter.
        :param: ``int`` **permission**: Video permissions.
        :param: ``str`` **tags**: Video tags.

        Returns

        **403 Forbidden**
            if could not edit video

        **304 Not Modified**
            if video with same `subject` already exists

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not video:
            return apiresults.not_found()

        if 'subject' in data:
            data['subject'] = data['subject'].strip()
            if video.subject != data['subject']:
                new_subject, exist_video = await Video.check_exists(data['subject'], self.ctx.request_user)
                if exist_video:
                    return apiresults.found(subject=new_subject)

        video = await Video.edit(data['video_id'], data, self.ctx.request_user.id)
        if video:
            return apiresults.ok()
        return apiresults.forbidden()

    @req_auth()
    async def favorites(self, data):
        """
        Get Favorite Videos.

        :param: ``str`` **scope**: Scope.
        :param: ``int`` **offset**: results Offset.
        :param: ``int`` **limit**: results Limit.

        Returns

        **200 OK**
            in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)
        limit = data.get('limit', 10)
        offset = data.get('offset', 0)
        favorites = await Video.get_favorites(self.ctx.request_user.id, limit=limit, offset=offset)
        return apiresults.ok(**favorites)

    @req_auth()
    async def find(self, data):
        """
        Searches for video by video subject.

        :param: ``str`` **subject**: Video subject.

        Returns

        **404 Not Found**
            if no video found

        **200 OK**
            in success

        """
        scope = s().get_scope(data.get('app_name'))
        Video = s().M.model('Video', self.ctx, scope=scope)

        data['subject'] = data['subject'].strip()
        new_subject, video = await Video.check_exists(data['subject'], self.ctx.request_user)
        if not video:
            return apiresults.not_found()

        return apiresults.ok(video_id=video.id,
                                       duration=video.duration,
                                       new_subject=new_subject,
                                       start_ts=dtm.ts_from_YmdHMS(video.reporter_start_ts))

    @req_auth()
    async def finish(self, data):
        """
        Finish video by ID if reporter token match server token.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if user is not reporter or proxy user

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not video:
            return apiresults.not_found()

        if not (video.reporter_id == self.ctx.request_user.id or self.ctx.request_user.user_type is USER_TYPE.proxy) \
                or video.is_external:
            return apiresults.forbidden()

        if video.is_finished():
            return apiresults.ok(status=video.status)

        else:
            await video.finish()
            return apiresults.ok()

    async def get(self, data):
        """
        Returns video data by video ID.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if video is not opened to public access

        **206 Partial Content**
            if no screenshots uploaded or video did not finished

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        _id = data.get('video_id', None)

        video = await Video.get_one(_id=_id, _deleted=False)

        if not video:
            return apiresults.not_found()

        has_access = await video.has_access(self.ctx.request_user)
        if not has_access:
            return apiresults.forbidden()

        d = await video.get_data()

        if video.is_finished() and not video.screenshots_uploaded():
            return apiresults.partial(video=d)

        return apiresults.ok(video=d)

    async def get_coords(self, data):
        """
        Get a list of coords for video

        :param: ``str`` **video_id**: Video ID

        Returns

        **403 Forbidden**
            if no access to video

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not video:
            return apiresults.not_found()

        has_access = await video.has_access()
        if not has_access:
            return apiresults.forbidden()

        return apiresults.ok(coords=(await video.get_coords()))

    async def get_list(self, data):
        """
        Returns list of video data by list of video IDs.

        :param: ``list`` **items**: {``str`` **id1**: ``int`` ts1, **id2**: ts2, ...}

        Returns

        **200 OK**
            with `items` in success

        """
        Video = s().M.model('Video', self.ctx)
        ids = list(data.get('items', []))
        videos = await Video.get(_ids=ids, limit=len(ids))
        items = []
        for video in videos:
            d = await video.get_data()
            items.append({video.id: d})
        return apiresults.ok(items=items)

    @req_auth()
    async def get_permission(self, data):
        """
        Returns video permission and video users.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if user is not video owner

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)

        video = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not video:
            return apiresults.not_found()

        if not video.is_owner(self.ctx.request_user):
            return apiresults.forbidden()

        users = await video.users_get()
        users = [{user.id: user.get_idts()} for user in users]

        return apiresults.ok(permission=video.permission, users=users)

    @req_auth()
    async def get_place_name(self, data):
        """
        Get place names using Google API.

        :param: ``float`` **lat**: Point latitude.
        :param: ``float`` **lon**: Point longitude.
        :param: ``str`` **language**: Language in ISO 639-1

        Returns

        **200 OK**
            in success

        """
        user = self.ctx.request_user

        lat = data.get('lat', None)
        lon = data.get('lon', None)
        if None in (lat, lon):
            ip = self.ctx.client_ip
            location = location_from_ip(ip)
        else:
            location = dict(lat=float(lat), lon=float(lon))

        location['language'] = data.get('language')
        title = await user.get_video_title(**location)
        if title:
            return apiresults.ok(title=title)
        else:
            return apiresults.not_found()

    @req_auth()
    async def i_follow(self, data):
        """
        Returns list of videos of users who are followed by current user.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``str`` **scope**: Scope.

        Returns

        **200 OK**
            with `items` in `data` in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)

        videos, has_more = await Video.get_i_follow(self.ctx.request_user, offset=data.get('offset', 0),
                                                    limit=data.get('limit', 10))

        return apiresults.ok(items=videos, has_more=has_more)

    async def live(self, data):
        """
        Returns live tab videos. Uses current user scope if no user_id present in request.

        :param: ``str`` **user_id**: User ID.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``str`` **scope**: Scope.

        Returns

        **200 OK**
            with `items` in `data` in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)

        videos, has_more = await Video.get_live(self.ctx.request_user, data.get('user_id', None),
                                                offset=data.get('offset', 0), limit=data.get('limit', 10))

        return apiresults.ok(items=videos, has_more=has_more)

    async def x_live(self, data):
        """
        Returns live tab videos. Uses current user scope if no user_id present in request.

        :param: ``str`` **user_id**: User ID.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``str`` **scope**: Scope.

        Returns

        **200 OK**
            with `items` in `data` in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)

        videos, has_more = await Video.x_get_live(self.ctx.request_user, data.get('user_id', None),
                                                  offset=data.get('offset', 0), limit=data.get('limit', 10))

        return apiresults.ok(items=videos, has_more=has_more)

    @req_auth()
    async def live_msg(self, data):
        """
        Posts live message for video by video ID.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **message**: Message body.

        Emits event **video:live_msg**

        .. code:: javascript

            {
                "event_user_id": "<user ID>",
                "data": {
                    "seconds": <integer seconds>,
                    "message": "<message string>",
                    "video_id": "<video ID>",
                    "event_user_id": "<user ID>",
                    "user": {
                        "id": "<user ID>",
                        "ts": "<timestamp>"
                    }
                },
                "method": "video:live_msg"
            }

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if video is not active

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        v = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not v:
            return apiresults.not_found()
        else:
            if v.is_active():
                await v.live_msg(data['message'])
                return apiresults.ok()
            else:
                return apiresults.forbidden()

    async def live_msg_timeline(self, data):
        """
        Returns list of live messages for video by video ID.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **200 OK**
            with `items` in `data` in success

        """
        LiveMsg = s().M.model('LiveMsg', self.ctx)
        limit = await LiveMsg.mapper.get(video_id=data['video_id']).count()
        messages = await LiveMsg.get(video_id=data['video_id'], _sorts=['seconds'], limit=limit)
        return apiresults.ok(items=[msg.get_data() for msg in messages])

    @req_auth()
    async def mine(self, data):
        """
        Get mine videos tab.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``str`` **scope**: Scope.

        Returns

        **200 OK**
            with `items` in `data` in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)
        mine = await Video.get_mine(self.ctx.request_user.id,
                                    limit=data.get('limit', 10), offset=data.get('offset', 0))
        return apiresults.ok(**mine)

    @req_auth()
    async def x_mine(self, data):
        """
        Get full mine videos tab.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``str`` **scope**: Scope.

        Returns

        **200 OK**
            with `items` in `data` in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)
        videos, has_more = await Video.x_get_mine(self.ctx.request_user.id,
                                                  limit=data.get('limit', 10), offset=data.get('offset', 0))
        return apiresults.ok(items=videos, has_more=has_more)

    async def near_me(self, data):
        """
        Returns near me tab videos.

        :param: ``str`` **user_id**: User ID.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``str`` **scope**: Scope.
        :param: ``float`` **lat**: Latitude of GPS point.
        :param: ``float`` **lon**: Longitude of GPS point.

        Returns

        **200 OK**
            with `items` and `has_more` in `data` in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)

        lat = data.get('lat', None)
        lon = data.get('lon', None)

        if None in (lat, lon):
            ip = self.ctx.client_ip
            location = location_from_ip(ip)

        else:
            location = dict(lat=float(lat), lon=float(lon))

        near_me, has_more = await Video.get_near_me(location, self.ctx.request_user, data.get('user_id', None),
                                                    offset=data.get('offset', 0), limit=data.get('limit', 10))
        videos = [dict(id=video.id, ts=dtm.ts_from_YmdHMS(video.modify_ts)) for video in near_me]
        return apiresults.ok(items=videos, has_more=has_more)

    async def x_near_me(self, data):
        """
        Returns full near me tab videos.

        :param: ``str`` **user_id**: User ID.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``str`` **scope**: Scope.
        :param: ``float`` **lat**: Latitude of GPS point.
        :param: ``float`` **lon**: Longitude of GPS point.

        Returns

        **200 OK**
            with `items` and `has_more` in `data` in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)

        lat = data.get('lat', None)
        lon = data.get('lon', None)

        if None in (lat, lon):
            ip = self.ctx.client_ip
            location = location_from_ip(ip)

        else:
            location = dict(lat=float(lat), lon=float(lon))

        videos, has_more = await Video.x_get_near_me(location, self.ctx.request_user, data.get('user_id', None),
                                                     offset=data.get('offset', 0), limit=data.get('limit', 10))

        return apiresults.ok(items=videos, has_more=has_more)

    @req_auth()
    async def record_uploaded(self, data):
        """
        Called by client when video did uploaded completely.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **reporter_token**: Reporter token.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if reporter_token is not valid

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not video:
            return apiresults.not_found()

        if not (video.reporter_id == self.ctx.request_user.id or self.ctx.request_user.user_type is USER_TYPE.proxy):
            return apiresults.forbidden()
        else:
            # video.ri_uploaded()
            return apiresults.ok()

    async def new_view(self, data):
        """
        Adds realtime view instance to video.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **404 Not Found**
            if no video found

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)

        v = await Video.get(_id=data['video_id'])
        if not v:
            return apiresults.not_found()
        else:
            v = v.pop()
        user_id = self.ctx.request_user.id if self.ctx.request_user else None
        r = await v.new_view(user_id)
        return apiresults.ok()

    @req_auth()
    async def pause(self, data):
        """
        Pause video by ID if reporter token match server token.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **reporter_token**: Reporter token.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if reporter_token is not valid or video is not active

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not video:
            return apiresults.not_found()

        elif not (video.reporter_id == self.ctx.request_user.id or self.ctx.request_user.user_type is USER_TYPE.proxy) \
                or video.is_external:
            return apiresults.forbidden()

        elif video.is_paused():
            return apiresults.ok(status=video.status)

        elif video.is_active():
            await video.pause()
            return apiresults.ok()
        else:
            return apiresults.forbidden(status=video.status)

    async def popular(self, data):
        """
        Returns popular tab videos.

        :param: ``str`` **user_id**: User ID.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``str`` **scope**: Scope.

        Returns

        **200 OK**
            with `items` and `has_more` in `data` in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)

        videos, has_more = await Video.get_popular(self.ctx.request_user, data.get('user_id', None),
                                                   offset=data.get('offset', 0), limit=data.get('limit', 10))

        return apiresults.ok(items=videos, has_more=has_more)

    async def x_popular(self, data):
        """
        Returns full popular video tab.

        :param: ``str`` **user_id**: User ID.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``str`` **scope**: Scope.

        Returns

        **200 OK**
            with `items` and `has_more` in `data` in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)

        videos, has_more = await Video.x_get_popular(self.ctx.request_user, data.get('user_id', None),
                                                     offset=data.get('offset', 0), limit=data.get('limit', 10))

        return apiresults.ok(items=videos, has_more=has_more)

    @req_auth()
    async def reanimate(self, data):
        """
        Reanimates video (changing video status from `finished` to `active`).

        :param: ``str`` **video_id**: Video ID.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if user is not video owner or no proxy available

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not video:
            return apiresults.not_found()

        if not video.is_owner(self.ctx.request_user) or video.is_external:
            return apiresults.forbidden()

        Proxy = s().M.model('Proxy', self.ctx)
        if not video.is_finished():
            p = await Proxy.get_one(_id=video.proxy_id)
            if not p:
                return apiresults.forbidden()
            return apiresults.ok(title=video.subject,
                                           reporter_token=video.reporter_token,
                                           video_id=video.id,
                                           proxy_host=p.host,
                                           proxy_port_video=p.port_video,
                                           duration=video.duration)

        proxy = await Proxy.get_closest(self.ctx.client_ip)
        if not proxy:
            return apiresults.forbidden()

        await video.reanimate(proxy.id)

        return apiresults.ok(title=video.subject,
                                       reporter_token=video.reporter_token,
                                       video_id=video.id,
                                       proxy_host=proxy.host,
                                       proxy_port_video=proxy.port_video,
                                       duration=video.duration)

    async def recent(self, data):
        """
        Returns recent tab videos.

        :param: ``str`` **user_id**: User ID.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``str`` **scope**: Scope

        Returns

        **200 OK**
            in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)

        videos, has_more = await Video.get_recent(self.ctx.request_user, data.get('user_id', None),
                                                  offset=data.get('offset', 0), limit=data.get('limit', 10))

        return apiresults.ok(items=videos, has_more=has_more)

    async def x_recent(self, data):
        """
        Returns full recent tab videos.

        :param: ``str`` **user_id**: User ID.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.
        :param: ``str`` **scope**: Scope

        Returns

        **200 OK**
            in success

        """
        scope = s().get_scope(data.get('scope'))
        Video = s().M.model('Video', self.ctx, scope=scope)

        videos, has_more = await Video.x_get_recent(self.ctx.request_user, data.get('user_id', None),
                                                    offset=data.get('offset', 0), limit=data.get('limit', 10))

        return apiresults.ok(items=videos, has_more=has_more)

    @req_auth()
    async def set_ban(self, data):
        """
        Adds a ban from user for video by ID.

        :param: ``str`` **video_id**: Video ID.

        Emits event **video:ban**

        .. code:: javascript

            {
                "data": {
                    "proxy_id": "<proxy ID>",
                    "wedding_id": "<wedding ID>",
                    "video_url": "<video URL>",
                    "lon": "<float>",
                    "is_liked": "<bool>",
                    "count_likes": "<count likes>",
                    "observer_start_ts": "<timestamp>",
                    "gps_incognito": "<bool>",
                    "ts": "<timestamp>",
                    "duration": "<integer seconds>",
                    "reporter_id": "<user ID>",
                    "event_user_id": "<user ID>",
                    "count_comments": "<count comments>",
                    "id": "<video ID>",
                    "subject": "<string>",
                    "last_pause_time": "<timestamp>",
                    "record_uploaded": "<bool>",
                    "permission": "<integer>",
                    "access_observer": "<bool>",
                    "is_bookmarked": "<bool>",
                    "live": "<bool>",
                    "parts": [],
                    "count_bans": "<count bans>",
                    "master_id": "<user ID>",
                    "audio_url": "<audio URL>",
                    "owner_id": "<user ID>",
                    "status": "<integer>",
                    "shot_urls": {
                        "screen1": "<screenshot URL>",
                        "screen2": "<screenshot URL>",
                        "screenN": "<screenshot URL>"
                    },
                    "tags": "<#tags>",
                    "is_disliked": "<bool>",
                    "reporter_start_ts": "<timestamp>",
                    "observer_id": "<user ID>",
                    "lat": "<float>",
                    "is_banned": "<bool>",
                    "delete_status": "<integer>",
                    "visible": "<bool>",
                    "video_id": "<video ID>",
                    "count_views": "<count views>",
                    "count_dislikes": "<count dislikes>"
                },
                "event_user_id": "<user ID>",
                "method": "video:ban"
            }

        Returns

        **404 Not Found**
            if no video found

        **304 Not Modified**
            if user already banned video

        **200 OK**
            with `ban_id` in `data` in success

        """
        Video = s().M.model('Video', self.ctx)

        data['user_id'] = self.ctx.request_user.id

        video = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not video:
            return apiresults.not_found()

        has_access = await video.has_access()
        can_ban = False
        import pytz
        from datetime import timedelta
        date_joined = self.ctx.request_user.date_joined.replace(tzinfo=pytz.utc)
        if (dtm.now_dt() - date_joined) > timedelta(days=7):
            can_ban = True
        if self.ctx.request_user.user_type is USER_TYPE.moderator:
            can_ban = True
        if not (has_access and can_ban):
            return apiresults.forbidden()

        v = await video.ban(data)

        if v:
            return apiresults.ok(ban_id=v.id)
        else:
            return apiresults.not_modified()

    @req_auth()
    async def set_coords(self, data):
        """
        Set coords for video.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **reporter_token**: Video reporter_token.
        :param: ``float`` **lat**: Video latitude.
        :param: ``float`` **lon**: Video longitude.
        :param: ``int`` **time**: a second of video.
        :param: ``int`` **seconds**: current Video duration.
        :param: ``int`` **type**: GPS source type (network or satellite, see ``~.enums.GPS_TYPES``).

        Returns
        **404 Not Found**
            if no video found

        **403 Not Modified**
            if user cannot set coords

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        v = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not v:
            return apiresults.not_found()
        else:
            v = await v.set_coords(data['video_id'], data['lat'], data['lon'],
                                   data['time'], data['seconds'], data['type'],
                                   self.ctx.request_user.id, token=data.get('reporter_token'))
            if v:
                return apiresults.ok()
            else:
                return apiresults.forbidden()

    @req_auth()
    async def set_dislike(self, data):
        """
        Removes like and adds dislike from current user for a video by ID.

        :param: ``str`` **video_id**: Video ID.

        Emits event **video:like**

        .. code:: javascript

            {
                "event_user_id": "<user ID>",
                "data": {
                    "is_disliked": "<bool>",
                    "is_liked": "<bool>",
                    "event_user_id": "<user ID>",
                    "video_id": "<video ID>",
                    "count_bans": "<count bans>",
                    "count_dislikes": "<count dislikes>",
                    "count_likes": "<count likes>"
                },
                "method": "video:like"
            }

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if no access

        **304 Not Modified**
            if user already disliked video

        **200 OK**
            with `like_id` in `data` in success

        """
        Video = s().M.model('Video', self.ctx)

        data['user_id'] = self.ctx.request_user.id

        video = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not video:
            return apiresults.not_found()

        has_access = await video.has_access()
        if not has_access:
            return apiresults.forbidden()

        v = await video.like(data, False)

        if v:
            return apiresults.ok(like_id=v.id)
        else:
            return apiresults.not_modified()

    @req_auth()
    async def set_gadget_info(self, data):
        """
        Saves gadget info (@Anatoliy)

        :param: ``str`` **video_id**: Video ID.
        :param: ``int`` **i1**: ?
        :param: ``int`` **i2**: ?
        :param: ``int`` **i3**: ?
        :param: ``int`` **i4**: ?
        :param: ``int`` **i5**: ?
        :param: ``int`` **i6**: ?
        :param: ``int`` **i7**: ?
        :param: ``int`` **i8**: ?
        :param: ``str`` **s1**: ?
        :param: ``str`` **s2**: ?

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if user is not reporter for video

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        GPS = s().M.model('GPS', self.ctx)

        video = await Video.get_one(_id=data.get('video_id', None), _deleted=False)

        if not video:
            return apiresults.not_found()

        if not video.is_reporter(self.ctx.request_user):
            return apiresults.forbidden()

        gps_data = {}

        gps_data['i1'] = data.get('i1', 0)
        gps_data['i2'] = data.get('i2', 0)
        gps_data['i3'] = data.get('i3', 0)
        gps_data['i4'] = data.get('i4', 0)
        gps_data['i5'] = data.get('i5', 0)
        gps_data['i6'] = data.get('i6', 0)
        gps_data['i7'] = data.get('i7', 0)
        gps_data['i8'] = data.get('i8', 0)
        gps_data['s1'] = data.get('s1', '')
        gps_data['s2'] = data.get('s2', '')

        gps = GPS(gps_data)
        gps.save()

        return apiresults.ok()

    @req_auth()
    async def set_like(self, data):
        """
        Adds like from current user for a video by ID.

        :param: ``str`` **video_id**: Video ID.

        Emits event **video:like**

        .. code:: javascript

            {
                "event_user_id": "<user ID>",
                "data": {
                    "is_disliked": "<bool>",
                    "is_liked": "<bool>",
                    "event_user_id": "<user ID>",
                    "video_id": "<video ID>",
                    "count_bans": "<count bans>",
                    "count_dislikes": "<count dislikes>",
                    "count_likes": "<count likes>"
                },
                "method": "video:like"
            }

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if no access

        **304 Not Modified**
            if user already liked video

        **200 OK**
            with `like_id` in `data` in success

        """
        Video = s().M.model('Video', self.ctx)

        data['user_id'] = self.ctx.request_user.id

        video = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not video:
            return apiresults.not_found()

        has_access = await video.has_access()
        if not has_access:
            return apiresults.forbidden()

        v = await video.like(data, True)

        if v:
            return apiresults.ok(like_id=v.id)
        else:
            return apiresults.not_modified()

    async def set_offline_coords(self, data):
        """
        Set offline coords for video.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **reporter_token**: Video reporter_token.
        :param: ``list`` **coords**: List of dicts with coords data (lat, lon, time).

        Returns

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)

        video = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not video:
            # return apiresults.not_found()
            return apiresults.ok()

        await video.set_gps_bulk(data['coords'])

        return apiresults.ok()

    @req_auth()
    async def set_permission_for_all(self, data):
        """
        Sets permissions for all videos for current user.

        Returns

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        user = self.ctx.request_user
        await Video.set_permission_for_all_owned(user)
        return apiresults.ok()

    @req_auth()
    async def social_post(self, data):
        """
        Post message to bound socials networks.

        :param: ``list`` **providers**: Social network name ("facebook", "twitter").
        :param: ``str`` **video_id**: Video ID.

        Returns

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)
        errors = ""
        for provider in data['providers']:
            _, error = await Social.post_video(self.ctx.request_user, provider, video)
            errors += error if error else ""

        return apiresults.ok(error=errors)

    @req_auth()
    async def start(self, data):
        """
        Starts video by ID if reporter token match server token.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **reporter_token**: Reporter token.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if `reporter_token` is not valid or video is not active

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not video:
            return apiresults.not_found()

        if not (video.reporter_id == self.ctx.request_user.id or self.ctx.request_user.user_type is USER_TYPE.proxy) \
                or video.is_external:
            return apiresults.forbidden()

        if video.is_active():
            return apiresults.ok(video_observer_token=video.observer_token)

        elif video.is_paused():
            await video.start()
            return apiresults.ok(video_observer_token=video.observer_token)

        else:
            return apiresults.forbidden(status=video.status)

    @req_auth()
    async def start_view(self, data):
        """
        Starts live view.

        :param: ``str`` **video_id**: Video ID.

        Emits event **video:start_view**

        .. code:: javascript

            {
                "data": {
                    "video_id": "<video ID>",
                    "event_user_id": "<user ID>",
                    "id": "<video ID>",
                    "count_watchers": "<count watchers>"
                },
                "event_user_id": "<user ID>",
                "method": "video:start_view"
            }

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if video is not active

        **304 Not Modified**
            if user is already viewing video

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)

        v = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not v:
            return apiresults.not_found()

        if not v.is_active():
            return apiresults.forbidden()

        r = await v.start_view(self.ctx.request_user.id)
        return apiresults.ok()

    @req_auth()
    async def startcheck(self, data):
        """
        Checks that live video translation can be resumed.
        For instance video can be paused when gadget camera switching and this do video:startcheck before resuming.
        Returns 200 if video model has `paused` or `active` state else returns 403.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **reporter_token**: Reporter token.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if `reporter_token` is not valid or startcheck failed

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)

        video = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not video:
            return apiresults.not_found()

        if not (video.reporter_id == self.ctx.request_user.id or self.ctx.request_user.user_type is USER_TYPE.proxy):
            return apiresults.forbidden()

        if video.startcheck():
            return apiresults.ok(video_observer_token=video.observer_token)
        else:
            return apiresults.forbidden(status=video.status)

    @req_auth()
    async def stop_view(self, data):
        """
        Stops live view.

        :param: ``str`` **video_id**: Video ID.

        Emits event **video:stop_view**

        .. code:: javascript

            {
                "data": {
                    "video_id": "<video ID>",
                    "event_user_id": "<user ID>",
                    "id": "<video ID>",
                    "count_watchers": "<count watchers>"
                },
                "event_user_id": "<user ID>",
                "method": "video:stop_view"
            }

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if video is not active

        **304 Not Modified**
            if user already stopped viewing video

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        RTView = s().M.model('RTView', self.ctx)

        v = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not v:
            return apiresults.not_found()

        if not v.is_active():
            return apiresults.forbidden()

        view = RTView(dict(video_id=v.id, user_id=self.ctx.request_user.id))
        r = await RTView.get_one(_id=view.custom_id)
        if r:
            await v.stop_view(self.ctx.request_user.id)
        return apiresults.ok()

    async def track(self, data):
        """
        Subscribes current connection to requested video events.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if user have no access to video

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)

        video = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not video:
            return apiresults.not_found()

        has_access = await video.has_access(self.ctx.request_user)
        if not has_access:
            return apiresults.forbidden()

        subscriber = self.ctx.request_user if self.ctx.request_user else self.ctx.wsconnection
        E.subscribe([subscriber], video)

        return apiresults.ok()

    async def track_list(self, data):
        """
        Subscribes current connection to requested list of videos events.

        :param: ``list`` **items**: {``str`` **id1**: ``int`` ts1, **id2**: ts2, ...}

        Returns

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        ids = list(data.get('items', []))
        videos = await Video.get(_ids=ids, limit=len(ids))

        subscriber = self.ctx.request_user if self.ctx.request_user else self.ctx.wsconnection

        for v in videos:
            E.subscribe([subscriber], v)

        return apiresults.ok()

    @req_auth()
    async def unset_ban(self, data):
        """
        Removes a ban from video by ID.

        :param: ``str`` **video_id**: Video ID.

        Emits event **video:unban**

        .. code:: javascript

            {
                "data": {
                    "proxy_id": "<proxy ID>",
                    "wedding_id": "<wedding ID>",
                    "video_url": "<video URL>",
                    "lon": "<float>",
                    "is_liked": "<bool>",
                    "count_likes": "<count likes>",
                    "observer_start_ts": "<timestamp>",
                    "gps_incognito": "<bool>",
                    "ts": "<timestamp>",
                    "duration": "<integer seconds>",
                    "reporter_id": "<user ID>",
                    "event_user_id": "<user ID>",
                    "count_comments": "<count comments>",
                    "id": "<video ID>",
                    "subject": "<string>",
                    "last_pause_time": "<timestamp>",
                    "record_uploaded": "<bool>",
                    "permission": "<integer>",
                    "access_observer": "<bool>",
                    "is_bookmarked": "<bool>",
                    "live": "<bool>",
                    "parts": [],
                    "count_bans": "<count bans>",
                    "master_id": "<user ID>",
                    "audio_url": "<audio URL>",
                    "owner_id": "<user ID>",
                    "status": "<integer>",
                    "shot_urls": {
                        "screen1": "<screenshot URL>",
                        "screen2": "<screenshot URL>",
                        "screenN": "<screenshot URL>"
                    },
                    "tags": "<#tags>",
                    "is_disliked": "<bool>",
                    "reporter_start_ts": "<timestamp>",
                    "observer_id": "<user ID>",
                    "lat": "<float>",
                    "is_banned": "<bool>",
                    "delete_status": "<integer>",
                    "visible": "<bool>",
                    "video_id": "<video ID>",
                    "count_views": "<count views>",
                    "count_dislikes": "<count dislikes>"
                },
                "event_user_id": "<user ID>",
                "method": "video:unban"
            }


        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if user have no access to video

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        VideoBan = s().M.model('VideoBan', self.ctx)

        user_id = self.ctx.request_user.id
        video_id = data['video_id']

        video = await Video.get_one(_id=video_id, _deleted=False)

        if not video:
            return apiresults.not_found()

        has_access = await video.has_access()
        if not has_access:
            return apiresults.forbidden()

        vban = VideoBan.new(user_id=user_id, video_id=video_id)
        vban_id = vban.custom_id

        exists = await VideoBan.check_exists(_id=vban_id)
        if exists:
            await video.unban(vban_id)
        else:
            return apiresults.not_found()

        return apiresults.ok()

    @req_auth()
    async def unset_dislike(self, data):
        """
        Removes dislike from current user for a video by ID.

        :param: ``str`` **video_id**: Video ID.

        Emits event **video:unlike**

        .. code:: javascript

            {
                "event_user_id": "<user ID>",
                "data": {
                    "is_disliked": "<bool>",
                    "is_liked": "<bool>",
                    "event_user_id": "<user ID>",
                    "video_id": "<video ID>",
                    "count_bans": "<count bans>",
                    "count_dislikes": "<count dislikes>",
                    "count_likes": "<count likes>"
                },
                "method": "video:unlike"
            }

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if user have no access to video

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        VideoLike = s().M.model('VideoLike', self.ctx)

        data['user_id'] = self.ctx.request_user.id

        video = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not video:
            return apiresults.not_found()

        has_access = await video.has_access()
        if not has_access:
            return apiresults.forbidden()

        vlike = await VideoLike.check_exists(data['video_id'], data['user_id'])

        if vlike:
            await video.unlike(vlike)
        else:
            return apiresults.not_found()

        return apiresults.ok()

    @req_auth()
    async def unset_like(self, data):
        """
        Removes like from current user for a video by ID.

        :param: ``str`` **video_id**: Video ID.

        Emits event **video:unlike**

        .. code:: javascript

            {
                "event_user_id": "<user ID>",
                "data": {
                    "is_disliked": "<bool>",
                    "is_liked": "<bool>",
                    "event_user_id": "<user ID>",
                    "video_id": "<video ID>",
                    "count_bans": "<count bans>",
                    "count_dislikes": "<count dislikes>",
                    "count_likes": "<count likes>"
                },
                "method": "video:unlike"
            }


        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if user have no access to video

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        VideoLike = s().M.model('VideoLike', self.ctx)

        data['user_id'] = self.ctx.request_user.id

        video = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not video:
            return apiresults.not_found()

        has_access = await video.has_access()
        if not has_access:
            return apiresults.forbidden()

        vlike = await VideoLike.check_exists(data['video_id'], data['user_id'])

        if vlike:
            await video.unlike(vlike)
        else:
            return apiresults.not_found()

        return apiresults.ok()

    async def untrack(self, data):
        """
        Unsubscribe current connection from requested video events.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **404 Not Found**
            if no video found
        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        video = await Video.get_one(_id=data['video_id'], _deleted=False)

        if not video:
            return apiresults.not_found()

        subscriber = self.ctx.request_user if self.ctx.request_user else self.ctx.wsconnection
        E.unsubscribe([subscriber], video)

        return apiresults.ok()

    async def untrack_list(self, data):
        """
        Unsubscribes current connection from requested list of videos events.

        :param: ``list`` **items**: {``str`` **id1**: ``int`` ts1, **id2**: ts2, ...}

        Returns

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        ids = list(data.get('items', []))
        videos = await Video.get(_ids=ids, limit=len(ids))

        subscriber = self.ctx.request_user if self.ctx.request_user else self.ctx.wsconnection

        for v in videos:
            E.unsubscribe([subscriber], v)

        return apiresults.ok()
