# -*- coding: utf-8 -*-
from tornado.concurrent import Future
from api_engine import apiresults
from api_engine.request import APIRequest
from api_engine.validator import CustomValidator
from ...models.base import BaseModel


class BaseAPIHandler(object):
    """
    Base API Handler class.
    """

    validator = CustomValidator(allow_unknown=True)

    _ctx = None

    _handles = {}

    """Maximum limit value in Elasticsearch query."""
    MAX_LIMIT = 100

    """Maximum offset value in Elasticsearch query."""
    MAX_OFFSET = 10000

    """Elasticsearch ID string minimum length."""
    ES_ID_MIN_LENGTH = 20

    """Elasticsearch ID string maximum length."""
    ES_ID_MAX_LENGTH = 100

    """Base validation schema."""
    base_schema = {'offset': dict(type='integer', min=0, max=MAX_OFFSET),
                   'limit': dict(type='integer', min=0, max=MAX_LIMIT)}

    def __init__(self, ctx):
        """

        :param ctx:
        :return:
        """
        self._ctx = ctx
        BaseModel.ctx = self._ctx

    @property
    def ctx(self):
        """

        :return:
        """
        return self._ctx

    async def handle(self, method, data):
        """
        Run method handler.

        :param method: str method name.
        :param data: dict input params.
        :return: Future object with dict result.
        """
        results_future = Future()
        results_future.set_result(apiresults.bad_request())
        if not self.validate(method, data):
            return results_future
        handle = getattr(self, method, None)
        if not handle:
            return results_future
        api_results = handle(data)
        return api_results

    def validate(self, method, data):
        """
        Check method name and input params.

        :param method: str method name.
        :param data: dict input params.
        :return: bool.
        """
        if method not in self._handles:
            return False

        schema = self._handles[method]
        if self.ctx.request.type == APIRequest.WS:
            schema.update(self.base_schema)
        elif self.ctx.request.type == APIRequest.HTTP:
            schema.update(files={})  # TODO:

        if 'interview_id' in data:
            data['video_id'] = data['interview_id']
        if not self.validator.validate(data, schema, normalize=False):
            return False
        return True
