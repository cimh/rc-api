# -*- coding: utf-8 -*-
import logging
from json import dumps
from urllib.parse import urlencode
from .base import BaseAPIHandler
from api_engine import apiresults, s
from tornado import gen
from tornado.escape import json_decode
from tornado.ioloop import IOLoop
from tornado.httpclient import AsyncHTTPClient
from api_engine.request import APIRequest


log = logging.getLogger(__name__)


class RFIAPIHandler(BaseAPIHandler):
    """
    RFI Bank API handler class.
    """

    # RFI Refund URL.
    refund_url = "https://partner.rficb.ru/alba/refund"

    citydoom_key = "7a4988259a264ce4c05c7121fc584f0d"
    wedcam_key = "d01f9d586369238f32433de51c15e8ca"

    _handles = {
        'post_payment': {
            'tid': dict(required=True),
            'name': dict(required=True),
            'comment': dict(required=True),
            'partner_id': dict(required=True),
            'service_id': dict(required=True),
            'order_id': dict(required=True),
            'type': dict(required=True),
            'partner_income': dict(required=True),
            'system_income': dict(required=True),
            'check': dict(required=True),
            'test': dict()
        }
    }

    @gen.coroutine
    def post_payment(self, data):
        """
        Called when payment succeeded by RFI.

        Returns

        **200 OK**
            everytime

        """
        from hashlib import md5
        from api_engine.enums import CITYDOOM, WEDCAM

        if self.ctx.request.type == APIRequest.HTTP \
                and self.ctx.request.method != 'POST':
            raise gen.Return('Only POST allowed')
        log.info('On Request: %s', dumps(data, indent=3, sort_keys=True))

        app_name = CITYDOOM
        _id = data['comment']
        if ':' in data['comment']:
            splitted = data['comment'].split(':')
            app_name = splitted[0]
            _id = splitted[1]

        if app_name == CITYDOOM:
            api_key = self.citydoom_key
        elif app_name == WEDCAM:
            api_key = self.wedcam_key
        else:
            raise gen.Return(apiresults.bad_request())

        check_data = [data['tid'],
                      data['name'],
                      data['comment'],
                      data['partner_id'],
                      data['service_id'],
                      data['order_id'],
                      data['type'],
                      data['partner_income'],
                      data['system_income'],
                      data['test'] if 'test' in data else '',
                      api_key]
        check = md5()
        check.update("".join(check_data))

        if check.hexdigest() != data['check']:
            log.error('Check failed, check="%s", %s', check.hexdigest(), dumps(data, indent=3, sort_keys=True))
            # raise gen.Return(apiresults.bad_request())

        if float(data['system_income']) != float(s().hbquest.game_cost_rub):
            log.error('Money mismatch, game_cost_rub="%s", %s',
                      s().hbquest.game_cost_rub, dumps(data, indent=3, sort_keys=True))
            # raise gen.Return(apiresults.bad_request())

        @gen.coroutine
        def callback(app_name, _id, tid):

            if app_name == CITYDOOM:
                Quest = s().M.model('Quest')
                quest = yield Quest.get_one(_id=_id)
                if quest:
                    yield quest.pay(tid)
                    return

            elif app_name == WEDCAM:
                Wedding = s().M.model('Wedding')
                wedding = yield Wedding.get_one(_id=_id)
                if wedding:
                    yield wedding.pay(tid)

        IOLoop.current().spawn_callback(callback, app_name, _id, data['tid'])

        log.info('Success: %s', dumps(data, indent=3, sort_keys=True))

        raise gen.Return(apiresults.ok())

    @gen.coroutine
    def refund(self, data):
        """
        Payment refund operation.

        :param: ``str`` **tid**: Transaction ID.
        :param: ``float`` **amount**: Refund amount (optional), must be a greater or equal of amount of initial Transaction.

        Returns

        **503 Service Unavailable**
            on bad JSON from RFI

        **403 Forbidden**
            on error from RFI

        **200 OK**
            on success.

        """
        params = dict()
        params['tid'] = str(data['tid'])
        if 'amount' in data:
            params['amount'] = str(data.get('amount', ''))
        method = "POST"
        params['api_key'] = s().rfi.api_key
        # check = self._sign(method, self.refund_url, params, s().rfi.api_key)
        # params['check'] = check  # TODO: trouble with RFI API 2.0
        headers = {
            'Content-Type': "application/x-www-form-urlencoded"
        }
        body = urlencode(params)
        resp = yield AsyncHTTPClient().fetch(self.refund_url, method=method, headers=headers, body=body)
        r = resp.body.decode('unicode_escape')
        try:
            r = json_decode(r)
        except(TypeError, ValueError):
            log.error('Refund error (service unavailable): tid="%s", amount="%s"', params['tid'], params.get('amount'))
            raise gen.Return(apiresults.service_unavailable())
        else:
            if r.get('status', '') == 'success':
                payback_id = int(r.get('payback_id', 0))
                log.info('Refund success: payback_id="%s", tid="%s", amount="%s"', payback_id, params['tid'], params.get('amount'))
                raise gen.Return(apiresults.ok(payback_id=payback_id))
            else:
                error = r.get('message', '')
                log.error('Refund error: tid="%s", amount="%s"', params['tid'], params.get('amount'))
                raise gen.Return(apiresults.forbidden(error=error))

    @classmethod
    def _sign(cls, method, url, params, secret_key):
        """
        Get RFI API request sign.

        :param: ``str`` **method**: HTTP method.
        :param: ``str`` **url**: api URL.
        :param: ``dict`` **params**: request body params.
        :param: ``str`` **secret_key**: RFI API key.

        """
        import urllib.request, urllib.parse, urllib.error
        import hmac
        import hashlib
        import base64
        exclude = ['check', 'mac']
        url_parsed = urllib.parse.urlparse(url)
        keys = [param for param in params if param not in exclude]
        keys.sort()

        result = []
        for key in keys:
            value = urllib.parse.quote(
                str(params.get(key) or '').encode('utf-8'),
                safe='~'
            )
            result.append('{}={}'.format(key, value))

        data = "\n".join([
            method,
            url_parsed.hostname,
            url_parsed.path,
            "&".join(result)
        ])

        digest = hmac.new(
            secret_key,
            data,
            hashlib.sha256
        ).digest()
        signature = base64.b64encode(digest)
        return signature
