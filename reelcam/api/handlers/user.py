# -*- coding: utf-8 -*-
from datetime import date
from tornado import gen
from api_engine import apiresults
from api_engine import s
from api_engine.auth import Auth
from api_engine.enums import GPS_TYPES, USER_TYPE, USER_STATUS
from reelcam.utils import get_country_code, stringio_from_base64
from utils.datetimemixin import dtm
from .base import BaseAPIHandler
from ..decorators import req_auth, req_unauth
from ...models.follow import Follow
from api_engine.enums import CITYDOOM, WEDCAM, REELCAM


class UserAPIHandler(BaseAPIHandler):
    """
    User API Handler class.
    """

    ANDROID_PLATFORM = "Android"
    IOS_PLATFORM = "ios"

    _handles = {
        'delete': {
            'password': dict(required=True, type='string', minlength=1)
        },
        'edit': {
            'follow_approve': dict(type='boolean'),
            'default_video_permission': dict(type='integer', allowed=[0, 1, 2, 3]),
            'gps_incognito': dict(type='boolean'),
            'country': dict(type='string', minlength=2, maxlength=2),
            'region': dict(type='string', minlength=2, maxlength=32),
            'language': dict(type='string', minlength=2, maxlength=2),
            'language2': dict(type='string', minlength=2, maxlength=2),
            'gender': dict(type='string', allowed=['male', 'female']),
            'birthday': dict(type='string', regex='\d{1,2}/\d{1,2}/\d{4}', minlength=1),
            'photo': dict(type='string', minlength=1),
            'fullname': dict(type='string', minlength=1, maxlength=64),
            'password': dict(type='string', minlength=1),
            'first_name': dict(type='string', minlength=1, maxlength=64),
            'last_name': dict(type='string', minlength=1, maxlength=64),
        },
        'followed': {
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer', min=0),
            'limit': dict(type='integer', min=0),
        },
        'followers': {
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'offset': dict(type='integer', min=0),
            'limit': dict(type='integer', min=0),
        },
        'get': {
            'user_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'get_by_phone': {
            'phone': dict(required=True, type='string', minlength=3),
        },
        'get_list': {
            'items': dict(type='dict', required=True,
                          keyschema={'type': 'integer', 'nullable': True},
                          maxlength=100),
        },
        'get_online_users': {
            'location': dict(type='dict', schema=dict(lat=dict(type='float'), lon=dict(type='float'))),
            'radius': dict(type='integer'),
            'type_user': dict(type='integer'),
            'offset': dict(type='integer', min=0),
            'limit': dict(type='integer', min=0),
        },
        'login': {
            'login': dict(required=True, type='string', minlength=1, maxlength=50),
            'pass': dict(required=True, type='string', minlength=1),
            'device': dict(type='string', allowed=['gadget', 'desktop']),
            'build': dict(type='string'),
            'platform': dict(type='string'),
            'app_name': dict(type='string', allowed=[REELCAM, CITYDOOM, WEDCAM], minlength=6, maxlength=8),
        },
        'login_social': {
            'provider': dict(type='string', required=True),
            'token': dict(type='string', required=True),
            'token_secret': dict(type='string'),
            'device': dict(type='string', allowed=['gadget', 'desktop']),
        },
        'logout': {
            'platform': dict(type='string', allowed=[ANDROID_PLATFORM, IOS_PLATFORM]),
        },
        'myprofile': {

        },
        'register': {
            'app_name': dict(type='string', allowed=[REELCAM, CITYDOOM, WEDCAM], minlength=6, maxlength=8),
            'email': dict(type='string', regex='[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+', minlength=3, maxlength=128),
            'phone': dict(required=True, type='string', regex='^\+?[1-9]\d{1,14}$'),
            'password': dict(required=True, type='string', minlength=1),
            'fullname': dict(required=True, type='string', minlength=1, maxlength=64),
            'birthday': dict(type='string', regex='\d{2}/\d{2}/\d{4}', minlength=1),
            'country': dict(type='string', minlength=2, maxlength=2),
            'region': dict(type='string', minlength=2, maxlength=32),
            'language': dict(type='string', minlength=2, maxlength=2),
            'language2': dict(type='string', minlength=2, maxlength=2),
            'gender': dict(type='string', allowed=['male', 'female']),
            'gadget': dict(type='boolean'),
            'photo': dict(type='string', minlength=1),
            'build': dict(type='string'),
            'platform': dict(type='string'),
        },
        'restore_pw': {
            'phone': dict(required=True, type='string', regex='^\+?[1-9]\d{1,14}$'),
            'code': dict(required=True, type='string'),
            'password': dict(required=True, type='string', minlength=1),
        },
        'setgps': {
            'lat': dict(required=True, type='float', min=-90, max=90),
            'lon': dict(required=True, type='float', min=-180, max=180),
            'type': dict(required=True, type='integer', allowed=[GPS_TYPES.network, GPS_TYPES.gps]),
            'seconds': dict(required=True, type='integer')
        },
        'setpw': {
            'password': dict(required=True, type='string', minlength=1)
        },
        'settings': {

        },
        'sms_get_code': {
            'phone': dict(required=True, type='string', regex='^\+?[1-9]\d{1,14}$')
        },
        'set_push_token': {
            'token': dict(type='string', required=True, minlength=5, nullable=True),
        },
        'sms_get_code_w_resp': {
            'phone': dict(required=True, type='string', regex='^\+?[1-9]\d{1,14}$')
        },
        'sms_validation': {
            'phone': dict(required=True, type='string', regex='^\+?[1-9]\d{1,14}$'),
            'code': dict(required=True, type='string')
        },
        'social_bind': {
            'provider': dict(type='string', required=True),
            'token': dict(type='string', required=True),
            'token_secret': dict(type='string'),
            'device': dict(type='string', allowed=['gadget', 'desktop'], minlength=1),
        },
        'social_stats_24h': {

        },
        'social_unbind': {
            'provider': dict(type='string', required=True),
        },
        'swiper': {
            'offset': dict(type='integer', min=0),
            'limit': dict(type='integer', min=0)
        },
        'system': {

        },
        'track': {
            'user_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'track_list': {
            'items': dict(type='dict', required=True, keyschema=dict(type='integer', nullable=True)),
        },
        'untrack': {
            'user_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'untrack_list': {
            'items': dict(type='dict', required=True, keyschema=dict(type='integer', nullable=True)),
        },
    }

    @req_auth()
    async def delete(self, data):
        """
        Deletes current user.

        :param: ``str`` **password**: User password.

        Returns

        **403 Forbidden**
            if wrong password

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        user = await User.get_one(_id=self.ctx.request_user.id)

        if not Auth.verify_password(user, data['password']):
            return apiresults.forbidden()

        await user.delete()

        return apiresults.ok()

    @req_auth()
    async def edit(self, data):
        """
        Edit current user profile.

        :param: ``bool`` **follow_approve**: Approve follow requests.
        :param: ``int`` **default_video_permission**: Default video permission.
        :param: ``bool`` **gps_incognito**: GPS incognito flag.
        :param: ``str`` **country**: User country.
        :param: ``str`` **region**: User region.
        :param: ``str`` **language**: User language.
        :param: ``str`` **language2**: Second user language.
        :param: ``str`` **gender**: User gender.
        :param: ``str`` **birthday**: User birthday string in DD/MM/YYYY format.
        :param: ``str`` **photo**: User photo in Base64
        :param: ``str`` **fullname**: User fullname.
        :param: ``str`` **password**: New user password.
        :param: ``str`` **first_name**: User first name.
        :param: ``str`` **last_name**: User last name.

        Emits event **user:gps_incognito** on gps_incognito change

        .. code:: javascript

            {
                "data": {
                    "event_user_id": "<user ID>",
                    "gps_incognito": "<bool>",
                    "id": "<user ID>",
                    "user_id": "<user ID>"
                },
                "event_user_id": "<user ID>",
                "method": "user:gps_incognito"
            }

        Returns

        **403 Forbidden**
            if this fullname string is not allowed with `error` key in `data`

        **400 Bad Request**
            if bad `photo` base64-encoded photo key in `data`

        **304 Found**
            if user with same fullname already exists

        """
        User = s().M.model('User', self.ctx)

        user = await User.get_one(_id=self.ctx.request_user.id)

        first_name = last_name = None
        if 'first_name' in data:
            first_name = User.clean_name(data.pop('first_name').strip())
            data['first_name'] = first_name
        if 'last_name' in data:
            last_name = User.clean_name(data.pop('last_name').strip())
            data['last_name'] = last_name

        if user.app_name == WEDCAM:
            data.pop('fullname', None)
            if first_name and last_name:
                data['fullname'] = "%s %s" % (first_name, last_name)
            elif first_name:
                data['fullname'] = "%s %s" % (first_name, user.last_name)
            elif last_name:
                data['fullname'] = "%s %s" % (user.first_name, last_name)

        if "fullname" in data:
            data["fullname"] = data["fullname"].strip()
            if not User.is_valid_fullname(data["fullname"]):
                # Enable it, when android and ios handled it
                # return apiresults.forbidden(error='invalid_fullname')
                return apiresults.found(fullname=data["fullname"])

            if not User.is_allowed_fullname(data["fullname"]):
                return apiresults.forbidden(error='fullname_not_allowed')

            # check fullname
            exist_fullname = await User.check_exist_fullname(data["fullname"])

            # if fullname exist and its not belongs to exist_user -> return "found fullname" result
            fullname_owner = await User.get_one(phone=user.phone, fullname=data["fullname"].lower())
            if exist_fullname:
                if user.app_name == WEDCAM:
                    if not fullname_owner:
                        data['fullname'] = await User.get_available_fullname(data["fullname"])
                elif not fullname_owner:
                    return apiresults.found(fullname=data["fullname"])

        data = {key: data[key] for key in [field for field in data if field in self._handles['edit']]}

        if 'birthday' in data:
            _day, _month, _year = data['birthday'].split('/')
            try:
                birthday = date(day=int(_day), month=int(_month), year=int(_year))
            except ValueError:
                return apiresults.bad_request()
            else:
                data['birthday'] = birthday

        if 'password' in data:
            data['password'] = Auth.make_password_hash(data['password'])

        userpic = None
        if data.get('photo'):
            from base64 import b64decode
            from api_engine.userpic import Userpic
            try:
                userpic = b64decode(data.pop('photo'))
                if not Userpic.validate_userpic(userpic):
                    raise TypeError
            except TypeError:
                return apiresults.bad_request()

        gps_incognito = data.pop('gps_incognito', None)
        if gps_incognito is not None:
            user = await user.set_gps_incognito(gps_incognito)

        user = await user.update(data)

        if userpic:
            await user.set_userpic(userpic)

        return apiresults.ok()

    async def followed(self, data):
        """
        Returns list of followed users ('i follow') for a user.

        :param: ``str`` **user_id**: User ID.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **404 Not Found**
            if no user with `user_id` found

        **401 Unauthorized**
            if no `user_id` and user unauthorized

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)

        user_id = data.get('user_id', None)
        offset = data.get('offset', 0)
        limit = data.get('limit', 10)

        if user_id is not None:
            user = await User.get_one(_id=user_id)
            if not user:
                return apiresults.not_found()
        elif not self.ctx.request_user:
            return apiresults.unauthorized()
        else:
            user = await User.get_one(_id=self.ctx.request_user.id)

        # followed_users = User.get(_ids=user.followed, _deleted=False, offset=offset, limit=limit) if user.followed else []
        followed_users = await User.get(_ids=user.followed[offset:offset+limit], _deleted=False) \
            if user.followed[offset:offset+limit] else []
        has_more = len(user.followed) > offset + limit

        return apiresults.ok(items=[user.get_idts() for user in followed_users], has_more=has_more)

    async def followers(self, data):
        """
        Returns list of followers for a user. ('follow me')

        :param: ``str`` **user_id**: User ID.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **404 Not Found**
            if no user with `user_id` found

        **401 Unauthorized**
            if no `user_id` and user unathorized

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)

        user_id = data.get('user_id', None)
        offset = data.get('offset', 0)
        limit = data.get('limit', 10)

        if user_id is not None:
            user = await User.get_one(_id=user_id)
            if not user:
                return apiresults.not_found()
        elif not self.ctx.request_user:
            return apiresults.unauthorized()
        else:
            user = await User.get_one(_id=self.ctx.request_user.id)

        if user.user_type is USER_TYPE.system:
            query = User.mapper.get(user_type=USER_TYPE.user,
                                    is_deleted=False,
                                    is_sms_validated=True,
                                    _sorts=['-joined_ts'])
            c = await query.count()
            has_more = c > offset+limit
            raw = await query[offset: offset+limit].execute()
            followers = [User(r) for r in raw]
        else:
            followers = await User.get(_ids=user.followers[offset:offset+limit], _deleted=False) \
                if user.followers[offset:offset+limit] else []
            has_more = len(user.followers) > offset + limit
        return apiresults.ok(items=[user.get_idts() for user in followers], has_more=has_more)

    async def get(self, data):
        """
        Get user data by user ID.

        :param: ``str`` **user_id**: ID of user.

        Returns

        **404 Not Found**
            if no user with `user_id` found

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        user = await User.get_one(_id=data['user_id'])
        if not user:
            return apiresults.not_found()
        else:
            user_data = await user.get_data()
            return apiresults.ok(**user_data)

    async def get_by_phone(self, data):
        """
        Get user by phone number.

        :param: ``str`` **phone**: phone number

        Returns

        **404 Not Found**
            if no user with `user_id` found or user unauthorized

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        user = await User.get_one(phone=data['phone'], _deleted=False)
        if not user:
            return apiresults.not_found()
        else:
            user = await User.get_one(_id=user.id)
            if user.user_type is USER_TYPE.invite:
                return apiresults.not_found()
        user_data = await user.get_data()
        return apiresults.ok(**user_data)

    async def get_online_users(self, data):
        """
        Get first 30 users in online status sorted by rating and located in requested area.

        :param: ``dict`` **location**: lat lon Location.
        :param: ``int`` **radius**: Radius of search in meters.
        :param: ``int`` **type_user**: User type. [3-all, 2-bot, 1-user]
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **200 OK**
            in success with `items` and `has_more` in `data`

        """
        User = s().M.model('User', self.ctx)
        offset = data.pop('offset', 0)
        limit = data.pop('limit', 30)
        query = User.get_online_users(data)[offset: offset+limit]
        raw = await query.execute()
        users = [User(r) for r in raw]
        c = await query.count()
        has_more = c > offset+limit

        items = []
        for u in users:
            r = await u.get_data()
            items.append({u.id: r})

        return apiresults.ok(items=items, has_more=has_more)

    async def login(self, data):
        """
        User login.

        :param: ``str`` **login**: User login.
        :param: ``str`` **pass**: User password.
        :param: ``str`` **app_name**: "reelcam" or "citydoom" string in lowercase.

        Returns

        **423 Locked**
            if wrong `platform` and `build` data received

        **404 Not Found**
            if no user with `user_id` found

        **403 Forbidden**
            if user is not SMS validated or wrong pass

        **200 OK**
            in success with `phone`, `id`, `sessionid` and `auth` in `data`

        """
        if self.ctx.request_user:
            return apiresults.not_modified(id=self.ctx.request_user.id)

        if not self.check_build_version(data.get('platform', None), data.get('build', None)):
            return apiresults.locked()

        User = s().M.model('User', self.ctx)
        data['login'] = User.clean_phone(data['login'])
        user = await User.get_one(phone=data['login'], _deleted=False)

        if not user or user.user_type is USER_TYPE.invite:
            return apiresults.not_found()
        else:
            user = await User.get_one(_id=user.id)

        if not user.is_sms_validated:
            return apiresults.forbidden(is_sms_validated=user.is_sms_validated)

        device = data.get('device', None)
        gadget = (device == 'gadget')

        r = await user.login(data['pass'], gadget=gadget, app_name=data.get('app_name', 'reelcam').lower())
        if not r:
            return apiresults.forbidden()

        return apiresults.ok(phone=user.phone,
                             id=user.id,
                             sessionid=self.ctx.session,
                             auth='normal')

    async def login_social(self, data):
        """
        User login via social networks.

        :param: ``str`` **token**: token in social network.
        :param: ``str`` **provider**: social name.

        Returns

        **403 Forbidden**
            if login failed

        **304 Not Modified**
            if user already authorized

        **200 OK**
            in success with `id`, `fullname_changed`, `sessionid` and `auth` in `data`

        """
        if self.ctx.request_user:
            return apiresults.not_modified()

        Social = s().M.model('Social', self.ctx)
        social_data, err = await Social.get_social_credentials(data)
        if err is not None:
            return err

        provider_uid = "social_auth.%s.uid" % data['provider']

        User = s().M.model('User', self.ctx)
        u = await User.get_one(**{
            provider_uid: social_data['uid'],
            "is_deleted": False
        })

        # if user was found
        if u:
            user = u
            # refresh token for social posting
            if user.social and data['provider'] in user.social:
                user.social[data['provider']]["token"] = data["token"]
            # refresh token for social auth
            user.social_auth[data['provider']]["token"] = data["token"]
            fullname_changed = ""
        # if user was not found -> create new user
        else:
            fullname = social_data.pop("fullname")
            fullname = fullname.strip()
            # if fullname was cleaned to empty
            if fullname == '':
                fullname = "NewUser"
            # check fullname
            exist_fullname = await User.check_exist_fullname(fullname)
            if exist_fullname:
                fullname = await User.get_available_fullname(fullname)
            # get avatar
            photo_file = await Social.get_avatar_from_social(social_data, data['provider'])
            provider = data['provider']

            user_data = {
                'date_joined': dtm.now_dt().date(),
                'joined_ts': dtm.now_YmdHMS(),
                'last_login': dtm.now_YmdHMS(),
                'user_type': USER_TYPE.user,
                'status': USER_STATUS.idle,
                'fullname': fullname,
                'social_auth': {
                    provider: social_data
                },
                'country_code': get_country_code(ip=self.ctx.client_ip),
            }

            # if facebook -> ask posting perms
            if provider == "facebook":
                social_posting_data, err = await Social.get_social_credentials(data, True)
                if not err:
                    user_data['social'] = {provider: social_posting_data}
            else:
                user_data['social'] = {provider: social_data}

            user = User(user_data)
            user = await user.save()
            if photo_file:
                user = await user.set_userpic(photo_file.read())
            fullname_changed = fullname if exist_fullname else ""

        gadget = (data.get('device', None) == 'gadget')
        login = await user.login(gadget=gadget, social_auth=True)
        if not login:
            return apiresults.forbidden()

        # fullname_changed - new fullname if original name was changed
        # during registration procedure, otherwise ""
        return apiresults.ok(**{
            'id': user.id,
            'fullname_changed': fullname_changed,
            'sessionid': self.ctx.session,
            'auth': 'normal'})

    @req_auth(allowed_types=[USER_TYPE.moderator, USER_TYPE.support])
    async def logout(self, data):
        """
        Logouts user.

        Returns

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        user = await User.get_one(_id=self.ctx.request_user.id)
        platform = data.get('platform', None)
        await user.logout()
        return apiresults.ok()

    @req_auth(allowed_types=[USER_TYPE.moderator, USER_TYPE.support])
    async def myprofile(self, data):
        """
        Get user profile data for current user.

        Returns

        **200 OK**
            in success with current user profile data.

        """
        User = s().M.model('User', self.ctx)
        user = await User.get_one(_id=self.ctx.request_user.id)
        user_data = await user.get_data()
        return apiresults.ok(**user_data)

    async def get_list(self, data):
        """
        Get user data for a list of IDs in `items`.

        Returns

        **200 OK**
            in success with items { ``str`` **id1**: ``int`` ts1, **id2**: ts2, ...}

        """
        User = s().M.model('User', self.ctx)
        ids = list(data['items'])
        users = await User.get(_ids=ids, limit=len(ids))
        items = []
        for user in users:
            data = await user.get_data()
            items.append({user.id: data})
        return apiresults.ok(items=items)

    @req_unauth
    async def register(self, data):
        """
        Register a new user.

        :param: ``str`` **birthday**: User birthday, "DD/MM/YYYY"
        :param: ``str`` **password**: User password
        :param: ``str`` **phone**: user phone number
        :param: ``str`` **fullname**: user fullname
        :param: ``str`` **country**: user country
        :param: ``str`` **region**: user region
        :param: ``str`` **language**: user language
        :param: ``str`` **language2**: user language2
        :param: ``str`` **gender**: user gender, ["male", "female"]
        :param: ``bool`` **gadget**: user gadget
        :param: ``str`` **photo**: user photo, base64-encoded string

        Returns

        **403 Forbidden**
            with error="invalid_fullname" in `data` for Android
            with error="fullname_not_allowed" in `data` for iOS

        **302 Found**
            with error="fullname_not_allowed" in `data if user with same fullname already exists

        **200 OK**
            in success with user profile in `data`

        """
        User = s().M.model('User', self.ctx)
        platform = data.get('platform', None)
        if not self.check_build_version(platform, data.get('build', None)):
            return apiresults.locked()

        data["phone"] = User.clean_phone(data["phone"])
        data["fullname"] = data["fullname"].strip()
        if not User.is_valid_fullname(data["fullname"]):
            if platform == self.ANDROID_PLATFORM:
                return apiresults.forbidden(error='invalid_fullname')
            else:
                return apiresults.forbidden(error='fullname_not_allowed')

        if not User.is_allowed_fullname(data["fullname"]):
            return apiresults.forbidden(error='fullname_not_allowed')

        # check fullname
        exist_fullname = await User.check_exist_fullname(data["fullname"])

        # if fullname exist and its not belongs to exist_user -> return "found fullname" result
        u = await User.get_one(phone=data['phone'], fullname=data["fullname"].lower(), _deleted=False)
        if exist_fullname and not u:
            if data.get('app_name', '') != WEDCAM:
                return apiresults.found(fullname=data["fullname"])
            else:
                data['fullname'] = await User.get_available_fullname(data['fullname'])

        exist_user = await User.get_one(phone=data['phone'], _deleted=False)
        if exist_user:
            exist_user = await User.get_one(_id=exist_user.id)
            if exist_user.user_type is not USER_TYPE.invite:
                if not exist_user.is_sms_validated:
                    await exist_user.sms_send_validation_code()
                    exist_user_data = await exist_user.get_data()
                    return apiresults.ok(phone=exist_user.phone, **exist_user_data)
                return apiresults.found(phone=data['phone'])

        if 'birthday' in data:
            _day, _month, _year = data['birthday'].split('/')
            try:
                birthday = date(day=int(_day), month=int(_month), year=int(_year))
            except ValueError:
                return apiresults.bad_request()
            else:
                data['birthday'] = birthday

        userpic = None
        if data.get('photo'):
            from base64 import b64decode
            from api_engine.userpic import Userpic
            try:
                userpic = b64decode(data.pop('photo'))
                if not Userpic.validate_userpic(userpic):
                    raise TypeError
            except TypeError:
                return apiresults.bad_request()
        else:
            if exist_user and exist_user.user_type is USER_TYPE.invite:
                path = 'static/wedcam_default.jpg'
            else:
                path = 'static/default_avatar.jpg'
            try:
                pic = open(path, 'rb')
            except (IOError, OSError):
                pass
            else:
                userpic = pic.read()
                pic.close()

        data.update(
            date_joined=dtm.now_dt().date(),
            joined_ts=dtm.now_YmdHMS(),
            last_login=dtm.now_YmdHMS(),
            password=Auth.make_password_hash(data['password']),
            user_type=USER_TYPE.user,
            status=USER_STATUS.idle,
            country_code=get_country_code(phone=data["phone"]),
        )

        if exist_user:
            user = await exist_user.update(data)
        else:
            user = User(data)
            user = await user.save()
        await user.sms_send_validation_code()

        if userpic:
            user = await user.set_userpic(userpic)

        user_data = await user.get_data()
        return apiresults.ok(**user_data)

    @req_unauth
    async def restore_pw(self, data):
        """
        Changes user password.

        :param: ``string`` **phone**: User phone number.
        :param: ``string`` **code**: Code from SMS.

        Returns

        **404 Not Found**
            if no user with `phone` found

        **403 Forbidden**
            with delete_status=2 if user is deleted
            with error="user_is_not_validated" if user is not validated with SMS
            with error="invalid_sms_code" if SMS code is not valid

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        data['phone'] = data['phone'].lstrip('+0').strip()
        data['code'] = data['code'].strip()

        user = await User.get_one(phone=data['phone'], _deleted=False)
        if not user:
            return apiresults.not_found()

        if user.is_deleted:
            return apiresults.forbidden(delete_status=user.delete_status)

        if not user.is_sms_validated:
            return apiresults.forbidden(error="user_is_not_validated")

        valid_codes = user.get_unexpired_sms_codes()
        if data['code'] not in [c['value'] for c in valid_codes]:
            return apiresults.forbidden(error="invalid_sms_code")

        # sms codes are disposable -> delete used sms
        user.sms_codes = user.delete_sms_code(data['code'])

        # restore password
        user.set_password(data['password'])
        await user.save()

        return apiresults.ok()

    @req_auth()
    async def setgps(self, data):
        """
        Set GPS data.

        :param: ``float`` **lat**: Latitude of GPS position.
        :param: ``float`` **lon**: Longitude of GPS position.
        :param: ``int`` **type**: GPS type (see in utils.enums.GPS_TYPES)
        :param: ``int`` **seconds**: timedelta last coord detection

        Returns

        **200 OK**
            in success

        """
        from api_engine.enums import OFFICE_LOCATION, SAN_FRANCISCO_LOCATION
        from reelcam.utils import distance
        User = s().M.model('User', self.ctx)
        user = await User.get_one(_id=self.ctx.request_user.id)
        gps_location = dict(lat=data['lat'], lon=data['lon'])
        if distance(gps_location, OFFICE_LOCATION) <= 200:
            gps_location = SAN_FRANCISCO_LOCATION
        user.gps_location = gps_location
        user.type = data['type']
        user.gps_ts = dtm.now_ts() - int(data['seconds'])
        await user.save()

        return apiresults.ok()

    @req_auth()
    async def setpw(self, data):
        """
        Changes password for current user.

        :param: ``str`` **password**: New password.

        Returns

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        user = await User.get_one(_id=self.ctx.request_user.id)
        user.password = Auth.make_password_hash(data['password'])

        await user.save()
        return apiresults.ok()

    @req_auth()
    async def set_push_token(self, data):
        """
        Set push_token to current user and invalidate the same push_token to other users

        :param: ``str`` **token**: iOS or Android token for pushing messages

        Returns

        **200 OK**
            in success

        """
        from api_engine.enums import UNKNOWN
        User = s().M.model('User', self.ctx)

        user = await User.get_one(_id=self.ctx.request_user.id)
        if data['token']:
            user.add_token(self.ctx.platform, data.get('app_name', UNKNOWN), data['token'])
            await user.save()
        else:
            token = user.remove_token(self.ctx.platform, data.get('app_name', UNKNOWN))
            if token:
                await user.save()

        return apiresults.ok()

    async def settings(self, data):
        """
        Get user settings.

        Returns

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        from reelcam.uploader import get_uploader
        u = await User.get_system()
        uploadhost = get_uploader().get_nearest_host(self.ctx.client_ip)

        api_settings = {
            'help_email': "support@reelcam.com",
            'site_url': "http://reelcam.com",
            'company_name': "Hellobot OÜ",
            'system_user_id': u.id if u else '',
            'uploadhost': uploadhost,
            'company_tel': "+3726682822",
            'gps_incognito_max_radius': float(s().gps.incognito_max_radius),
            'video_alias': "/#/video/"
        }

        return apiresults.ok(**api_settings)

    async def sms_get_code(self, data):
        """
        Send SMS with code for user SMS-validation

        :param: ``str`` **phone**:

        Returns

        **403 Forbidden**
            with error="sms_send_error" if no SMS sended

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        data["phone"] = User.clean_phone(data['phone'])
        user = await User.get_one(phone=data['phone'], _deleted=False)
        if not user:
            return apiresults.not_found()
        else:
            user = await User.get_one(_id=user.id)

        user = await user.sms_send_validation_code()
        if not user:
            # attempts limit or small next sms time error
            return apiresults.forbidden(error="sms_send_error")

        return apiresults.ok()

    async def sms_get_code_w_resp(self, data):
        """
        Get valid sms-code value.

        :param: ``str`` **phone**:

        Returns

        **404 Not Found**
            if no user with `phone` in REQ `data` found

        **200 OK**
            with error="no_sms_codes" if no SMS codes available for user
            with SMS code in `sms_code` in REP `data`

        """
        User = s().M.model('User', self.ctx)
        data["phone"] = User.clean_phone(data['phone'])
        user = await User.get_one(phone=data['phone'], _deleted=False)
        if not user:
            return apiresults.not_found()

        sms_codes = user.get_unexpired_sms_codes()
        if len(sms_codes):
            return apiresults.ok(sms_code=sms_codes[-1]['value'])
        else:
            return apiresults.ok(error="no_sms_codes")

    async def sms_validation(self, data):
        """
        Validate user via SMS-code.

        :param: ``str`` **phone**:
        :param: ``str`` **code**:

        Returns

        **404 Not Found**
            if no user with `phone` in REQ `data` found

        **403 Forbidden**
            if user validation failed

        **304 Not Modified**
            if user is already validated

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        data["phone"] = User.clean_phone(data['phone'])
        user = await User.get_one(phone=data['phone'], _deleted=False)
        if not user:
            return apiresults.not_found()
        else:
            user = await User.get_one(_id=user.id)

        if user.is_sms_validated:
            return apiresults.not_modified()

        user = await user.sms_validate(data['code'])
        if not user:
            return apiresults.forbidden()

        return apiresults.ok()

    @req_auth()
    async def social_bind(self, data):
        """
        Bind social(for posting) to user.

        :param: ``str`` **token**: Token in social network.
        :param: ``str`` **provider**: Social name.

        Returns

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        user = await User.get_one(_id=self.ctx.request_user.id)
        Social = s().M.model('Social', self.ctx)
        social_data, err = await Social.get_social_credentials(data, True)
        if err is not None:
            return err

        social = {
            data["provider"]: social_data
        }
        if user.social is None:
            user.social = social
        else:
            user.social.update(social)
            user.social = user.social.copy()
        await user.save()

        return apiresults.ok()

    @req_auth()
    async def social_stats_24h(self, data):
        """
        Get stats for all available metrics for past 24 hours.

        Returns

        **200 OK**
            with `bookmarks`, `likes`, `dislikes`, `bans`, `followers`, `views` and `videos` in `data`

        """
        Bookmark = s().M.model('Bookmark', self.ctx)
        Video = s().M.model('Video', self.ctx)
        VideoBan = s().M.model('VideoBan', self.ctx)
        VideoLike = s().M.model('VideoLike', self.ctx)
        VideoView = s().M.model('VideoView', self.ctx)
        from datetime import timedelta

        dt = dtm.YmdHMS_from_dt(dtm.now_dt() - timedelta(hours=24))

        results = await [Bookmark.count_date(dt, "now"),
               VideoLike.like_count_date(dt, "now"),
               VideoLike.dislike_count_date(dt, "now"),
               VideoBan.count_date(dt, "now"),
               Follow.count_date(dt, "now"),
               VideoView.count_date(dt, "now"),
               Video.count_date(dt, "now")]

        return apiresults.ok(bookmarks=results[0],
                                       likes=results[1],
                                       dislikes=results[2],
                                       bans=results[3],
                                       followers=results[4],
                                       views=results[5],
                                       videos=results[6])

    @req_auth()
    async def social_unbind(self, data):
        """
        Unbind social(for posting) from user.

        :param: ``str`` **provider**: Social name.

        Returns

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        user = await User.get_one(_id=self.ctx.request_user.id)
        if data['provider'] in user.social:
            social = user.social
            del social[data['provider']]
            user.social = None
            await user.save()
            if len(social):
                user.social = social
                await user.save()

        return apiresults.ok()

    async def swiper(self, data):
        """
        Returns swiper data for web frontend. It does not populate following/follower.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        users, has_more = await User.get_swiper(offset=data.get('offset', 0), limit=data.get('limit', 10))
        return apiresults.ok(items=users, has_more=has_more)

    async def system(self, data):
        """
        Get system user.

        Returns

        **200 OK**
            with system user profile in data in success

        """
        User = s().M.model('User', self.ctx)
        u = await User.get_system()
        if u:
            user_data = await u.get_data()
            return apiresults.ok(**user_data)
        else:
            return apiresults.ok()

    async def track(self, data):
        """
        Subscribes current connection to requested user events.

        :param: ``str`` **user_id**: ID of user to track.

        Returns

        **404 Not Found**
            if no user with `user_id` found

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        user = await User.get_one(_id=data['user_id'])
        if not user:
            return apiresults.not_found()
        subscriber = self.ctx.request_user if self.ctx.request_user else self.ctx.wsconnection
        subscriber.subscribe(user)
        return apiresults.ok()

    async def track_list(self, data):
        """
        Subscribes current connection to list of requested users events.

        :param: ``dict`` **items**: { ``str`` **id1**: ``int`` ts1, **id2**: ts2, ...}

        Returns

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        ids = list(data.get('items', []))
        users = await User.get(_ids=ids, limit=len(ids))
        subscriber = self.ctx.request_user if self.ctx.request_user else self.ctx.wsconnection
        for u in users:
            subscriber.subscribe(u)
        return apiresults.ok()

    async def untrack(self, data):
        """
        Unsubscribes current connection from requested user events.

        :param: ``str`` **user_id**: ID of user to untrack.

        Returns

        **404 Not Found**
            if no user with `user_id` found

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        user = await User.get_one(_id=data['user_id'])
        if not user:
            return apiresults.not_found()
        subscriber = self.ctx.request_user if self.ctx.request_user else self.ctx.wsconnection
        subscriber.unsubscribe(user)
        return apiresults.ok()

    async def untrack_list(self, data):
        """
        Unsubscribes current connection from list of requested users events.

        :param: ``dict`` **items**: { ``str`` **id1**: ``int`` ts1, **id2**: ts2, ...}

        Returns

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        ids = list(data.get('items', []))
        users = await User.get(_ids=ids, limit=len(ids))
        subscriber = self.ctx.request_user if self.ctx.request_user else self.ctx.wsconnection
        for u in users:
            subscriber.subscribe(u)
        return apiresults.ok()

    @classmethod
    def check_build_version(cls, platform, build_version):
        """
        Checks mobile app build version by platform.

        :param: ``str`` **platform**: Platform name.
        :param: ``int`` **build_version**: App build version number.
        """
        if None in (platform, build_version):
            return True
        try:
            platform = str(platform)
        except (TypeError, ValueError):
            return True
        if platform == s().android.platform:
            try:
                build_version = int(build_version)
            except (TypeError, ValueError):
                return False
            else:
                return build_version >= s().android.build
        else:
            return True
