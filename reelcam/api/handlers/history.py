# -*- coding: utf-8 -*-
from tornado import gen
from tornado.ioloop import IOLoop
from api_engine import apiresults, s
from .base import BaseAPIHandler
from ..decorators import req_auth
from ...models.history import History


class HistoryAPIHandler(BaseAPIHandler):
    """
    History API Handler.
    """

    _handles = {
        'get': {
            'type': dict(type='string'),
            'history_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'get_list': {
            'items': dict(type='dict', required=True, keyschema={'type': 'integer', 'nullable': True},
                          maxlength=100),
            'type': dict(type='string', allowed=History.types),
        },
        'read': {
            'type': dict(type='string', allowed=History.types),
            'history_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'read_list': {
            'items': dict(type='dict', required=True, keyschema={'type': 'integer', 'nullable': True}),
        },
        'read_all': {
            'type': dict(required=True, type='string', allowed=History.types),
        },
        'bookmarks': {
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'likes': {
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'echo': {
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'videos': {
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'followers': {
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'counters': {

        },
        'wedcam': {
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
    }

    @req_auth()
    @gen.coroutine
    def get(self, data):
        """
        Returns history entry by ID and type.

        :param: ``str`` **type**: Type of history to return.
        :param: ``str`` **history_id**: ID of history entry to return.

        Returns

        **200 OK**

        """
        H = s().M.model('H', self.ctx)
        history = yield H.get_one(_id=data.get('history_id'))
        if history is None:
            raise gen.Return(apiresults.not_found())
        raise gen.Return(apiresults.ok(**history.get_data()))

    @req_auth()
    @gen.coroutine
    def get_list(self, data):
        """
        Get multiple history entries by ID.

        :param: ``dict`` **items**: { ``str`` **id1**: ``int`` ts1, **id2**: ts2, ...}

        Returns

        **200 OK**

        """
        H = s().M.model('H', self.ctx)
        ids = list(data.get('items', []))
        histories = yield H.get(_ids=ids)
        items = [{h.id: h.get_data()} for h in histories]
        raise gen.Return(apiresults.ok(items=items))

    @req_auth()
    @gen.coroutine
    def read(self, data):
        """
        Mark history entry as read.

        :param: ``str`` **history_id**: ID of history entry to mark as read.
        :param: ``str`` **type**: history type.

        Returns

        **200 OK**

        """
        H = s().M.model('H', self.ctx)
        history = yield H.get_one(_id=data['history_id'])
        if not history.seen:
            history.seen = True
            yield history.save()
        raise gen.Return(apiresults.ok())

    @req_auth()
    @gen.coroutine
    def read_list(self, data):
        """
        Mark multiple history entries as read.

        :param: ``dict`` **items**: { ``str`` **id1**: ``int`` ts1, **id2**: ts2, ...}

        Returns

        **501 Not Implemented**

        """
        raise gen.Return(apiresults.not_implemented(**data))

    @req_auth()
    @gen.coroutine
    def read_all(self, data):
        """
        Mark all history entries as read.

        :param: ``str`` **type**: Type of history to mark entries.

        Returns

        **200 OK**

        """
        H = s().M.model('H', self.ctx)
        methods = H.get_methods(data.get('type'))
        scope = s().get_scope(data.get('app_name'))
        qs = H.mapper.get(user_id=self.ctx.request_user.id, method__in=methods, scope=scope, seen=False)
        count = yield qs.count()
        res = yield qs[:count].execute()
        histories = [H(r) for r in res]
        for h in histories:
            h.seen = True
            yield h.save()
        raise gen.Return(apiresults.ok())

    @req_auth()
    @gen.coroutine
    def bookmarks(self, data):
        """
        Get list of history by bookmarks.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **200 OK**
            with `items` in `data`

        """
        H = s().M.model('H', self.ctx)
        qs = H.mapper.get(user_id=self.ctx.request_user.id, method__in=H.get_methods('bookmark'),
                          scope=s().get_scope(data.get('app_name')))
        qs = qs.order_by('-create_ts')
        offset = data.get('offset', 0)
        limit = data.get('limit', 10)
        history = yield qs[offset: offset + limit].execute()
        raise gen.Return(apiresults.ok(items=[H(h).get_data() for h in history]))

    @req_auth()
    @gen.coroutine
    def likes(self, data):
        """
        Get list of history by likes.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **200 OK**
            with `items` in `data`

        """
        H = s().M.model('H', self.ctx)
        qs = H.mapper.get(user_id=self.ctx.request_user.id, method__in=H.get_methods('vlike'),
                          scope=s().get_scope(data.get('app_name')))
        qs = qs.order_by('-create_ts')
        offset = data.get('offset', 0)
        limit = data.get('limit', 10)
        history = yield qs[offset: offset + limit].execute()
        raise gen.Return(apiresults.ok(items=[H(h).get_data() for h in history]))

    @req_auth()
    @gen.coroutine
    def echo(self, data):
        """
        Get list of history by likes and comments.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **200 OK**
            with `items` in `data`

        """
        H = s().M.model('H', self.ctx)
        methods = H.get_methods('echo')
        qs = H.mapper.get(user_id=self.ctx.request_user.id, method__in=methods,
                          scope=s().get_scope(data.get('app_name')))
        qs = qs.order_by('-create_ts')
        offset = data.get('offset', 0)
        limit = data.get('limit', 10)
        history = yield qs[offset: offset + limit].execute()
        raise gen.Return(apiresults.ok(items=[H(h).get_data() for h in history]))

    @req_auth()
    @gen.coroutine
    def videos(self, data):
        """
        Get list of history by videos.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **200 OK**
            with `items` in `data`

        """
        H = s().M.model('H', self.ctx)
        qs = H.mapper.get(user_id=self.ctx.request_user.id, method__in=H.get_methods('video'),
                          scope=s().get_scope(data.get('app_name')))
        qs = qs.order_by('-create_ts')
        offset = data.get('offset', 0)
        limit = data.get('limit', 10)
        history = yield qs[offset: offset + limit].execute()
        raise gen.Return(apiresults.ok(items=[H(h).get_data() for h in history]))

    @req_auth()
    @gen.coroutine
    def followers(self, data):
        """
        Get list of history by followers.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **200 OK**
            with `items` in `data`

        """
        H = s().M.model('H', self.ctx)
        scope = s().get_scope(data.get('app_name'))
        qs = H.mapper.get(user_id=self.ctx.request_user.id, method__in=H.get_methods('follow'), scope=scope)
        qs = qs.order_by('-create_ts')
        offset = data.get('offset', 0)
        limit = data.get('limit', 10)
        history = yield qs[offset: offset+limit].execute()
        raise gen.Return(apiresults.ok(items=[H(h).get_data() for h in history]))

    @req_auth()
    @gen.coroutine
    def counters(self, data):
        """
        Get list of history counters.

        Returns

        **200 OK**

        """
        H = s().M.model('H', self.ctx)
        counters = dict(
            bookmarks=dict(count=0, h_type='bookmark'),
            followers=dict(count=0, h_type='follow'),
            likes=dict(count=0, h_type='vlike'),
            videos=dict(count=0, h_type='video'),
            comments=dict(count=0, h_type='comment'),
            echo=dict(count=0, h_type='echo'),
            wedcam=dict(count=0, h_type='wedcam'),
        )
        scope = s().get_scope(data.get('app_name'))
        for d in list(counters.values()):
            d['count'] = yield H.mapper.get(method__in=H.get_methods(d['h_type']),
                                            user_id=self.ctx.request_user.id, seen=False, scope=scope).count()
        result = {k: d['count'] for k, d in list(counters.items())}
        raise gen.Return(apiresults.ok(**result))

    @req_auth()
    @gen.coroutine
    def wedcam(self, data):
        """
        Get wedcam History.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **200 OK**

        """
        H = s().M.model('H', self.ctx)
        limit = data.get('limit', 10)
        offset = data.get('offset', 0)
        methods = H.get_methods('wedcam')
        qs = H.mapper.get(user_id=self.ctx.request_user.id, method__in=methods,
                          scope=s().get_scope("wedcam")).order_by('-create_ts')
        results = yield qs[offset: limit+offset].execute()
        items = [H(r).get_data() for r in results]
        raise gen.Return(apiresults.ok(items=items))
