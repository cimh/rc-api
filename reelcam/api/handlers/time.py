# -*- coding: utf-8 -*- 
from .base import BaseAPIHandler
from utils.datetimemixin import dtm
from api_engine import apiresults
from tornado import gen


class TimeAPIHandler(BaseAPIHandler):
    """
    Time API Handler class.
    """

    _handles = {
        'get': {}
    }

    async def get(self, data):
        """
        Get current timestamp.

        Returns

        **200 OK**
            with current timestamp in `ts` in `data` anytime

        """
        return apiresults.ok(ts=dtm.now_ts())
