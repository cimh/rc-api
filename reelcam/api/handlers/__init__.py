# -*- coding: utf-8 -*-
from .bookmarks import BookmarksAPIHandler
from .chat import ChatAPIHandler
from .feedback import FeedbackApiHandler
from .follow import FollowAPIHandler
from .health import HealthAPIHandler
from .history import HistoryAPIHandler
from .observe import ObserveAPIHandler
from .proxy import ProxyAPIHandler
from .rfi import RFIAPIHandler
from .search import SearchAPIHandler
from .time import TimeAPIHandler
from .user import UserAPIHandler
from .video import VideoAPIHandler
from .upload import UploadApiHandler
from .hbquest.quest import HBQuestAPIHandler
from .hbquest.template import HBQTemplateAPIHandler
from .wedcam.wedcam import WedcamAPIHandler
