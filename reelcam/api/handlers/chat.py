# -*- coding: utf-8 -*-
from tornado import gen
from api_engine import apiresults
from api_engine import s
from api_engine.enums import USER_TYPE
from .base import BaseAPIHandler
from ..decorators import req_auth


class ChatAPIHandler(BaseAPIHandler):
    """
    Chat API Handler class.
    """

    _handles = {
        'get': {
            'chat_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'get_list': {
            'items': dict(type='dict', required=True, keyschema=dict(type='integer', nullable=True),
                          maxlength=BaseAPIHandler.ES_ID_MAX_LENGTH),
        },
        'get_one': {
            'user_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'get_recent': {
            'offset': dict(type='integer'),
            'limit': dict(type='integer'),
        },
        'message_new': {
            'chat_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'body': dict(required=True, type='string', minlength=1),
        },
        'message_read': {
            'message_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'message_delete': {
            'message_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'message_read_all': {
            'chat_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'message_unread_count': {

        },
    }

    @req_auth(allowed_types=[USER_TYPE.moderator, USER_TYPE.support])
    async def get(self, data):
        """
        Get chat data by chat ID.

        :param: ``str`` **chat_id**: Chat ID.

        Returns

        **404 Not Found**
            if no chat with `chat_id` found

        **403 Forbidden**
            if user is not member of chat

        **200 OK**
            with `messages` -> [`items`, `has_more`] in success

        """
        Chat = s().M.model('Chat', self.ctx)

        chat = await Chat.get_one(_id=data['chat_id'])

        if not chat:
            return apiresults.not_found()

        if not chat.is_member(self.ctx.request_user):
            return apiresults.forbidden()

        messages, has_more = await chat.get_messages(
            offset=data.get('offset', 0),
            limit=data.get('limit', 10)
        )

        items = [{msg.id: msg.get_data()} for msg in messages]

        chat_data = await chat.get_data()

        return apiresults.ok(messages=dict(items=items, has_more=has_more), **chat_data)

    @req_auth(allowed_types=[USER_TYPE.moderator, USER_TYPE.support])
    async def get_list(self, data):
        """
        Get chat data for a list of IDs.

        :param: ``dict`` **items**: { ``str`` **id1**: ``int`` ts1, **id2**: ts2, ...}

        Returns

        **200 OK**
            with `items` in `data` in success

        """
        Chat = s().M.model('Chat', self.ctx)

        ids = list(data.get('items', []))
        chats = await Chat.get(_ids=ids, limit=len(ids))

        items = []
        for chat in chats:
            if chat.is_member(self.ctx.request_user):
                data = await chat.get_data()
                items.append({chat.id: data})

        return apiresults.ok(items=items)

    @req_auth(allowed_types=[USER_TYPE.moderator, USER_TYPE.support])
    async def get_one(self, data):
        """
        Get chat from current user to user_id or create new.

        :param: ``str`` **user_id**: User ID.

        Returns

        **404 Not Found**
            if no user with `user_id` found

        **400 Bad Request**
            if user IDs are equal

        **200 OK**
            with `messages` -> [`items`, `has_more`] in success

        """
        User = s().M.model('User', self.ctx)
        Chat = s().M.model('Chat', self.ctx)

        user1 = self.ctx.request_user

        user2 = await User.get_one(_id=data['user_id'], _deleted=False)

        if not user2:
            return apiresults.not_found()

        if user1.id == user2.id:
            return apiresults.bad_request()

        chat = await Chat.get_or_create(user1, user2)

        messages, has_more = await chat.get_messages(
            offset=data.get('offset', 0),
            limit=data.get('limit', 10))

        if messages:
            items = [{msg.id: msg.get_data()} for msg in messages]
        else:
            items, has_more = [], False

        chat_data = await chat.get_data()

        return apiresults.ok(messages=dict(items=items, has_more=has_more), **chat_data)

    @req_auth(allowed_types=[USER_TYPE.moderator, USER_TYPE.support])
    async def get_recent(self, data):
        """
        Get all chats for current user.
        Supports offset and limit arguments.
        Maximum value of limit is MAX_LIMIT, maximum value of offset is MAX_OFFSET.

        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **200 OK**
            with `items` and `has_more` in `data` in success

        """
        Chat = s().M.model('Chat', self.ctx)

        user = self.ctx.request_user
        chats, has_more = await Chat.get_by_user(
            user,
            offset=data.get('offset', 0),
            limit=data.get('limit', 10)
        )

        support_id = user.id if user.user_type is USER_TYPE.support else None

        items = []
        for chat in chats:
            items.append({chat.id: (await chat.get_data(support_id))})

        return apiresults.ok(items=items, has_more=has_more)

    @req_auth(allowed_types=[USER_TYPE.moderator, USER_TYPE.support])
    async def message_new(self, data):
        """
        Publish new message to chat.

        :param: ``str`` **chat_id**: Chat ID.
        :param: ``str`` **body**: Message body.

        Emits event **chat:message_new**

        .. code:: javascript

            {
                "data": {
                    "id": "<chat ID>",
                    "event_user_id": "<user ID>",
                    "message_id": "<message ID>",
                    "ts": "<timestamp>",
                    "user1_id": "<user1 ID>",
                    "user2_id": "<user2 ID>",
                    "user2_last_msg_ts": "<user2 last message timestamp>",
                    "user1_last_msg_ts": "<user1 last message timestamp>",
                    "user1_last_msg_body": "<user1 last message body>",
                    "user2_last_msg_body": "<user2 last message body>",
                    "user1_unread_count": "<user1 unread messages count>",
                    "user2_unread_count": "<user2 unread messages count>"
                },
                "event_user_id": "<user ID>",
                "method": "chat:message_new"
            }

        Returns

        **404 Not Found**
            if no chat with `chat_id` found

        **403 Forbidden**
            if authorized user is not a member of chat

        **200 OK**
            with `id` in success

        """
        Chat = s().M.model('Chat', self.ctx)
        user = self.ctx.request_user
        chat = await Chat.get_one(_id=data['chat_id'])
        if not chat:
            return apiresults.not_found()
        if not chat.is_member(user):
            return apiresults.forbidden()

        mess_id = await chat.message_new(user, data['body'])
        return apiresults.ok(id=mess_id)

    @req_auth(allowed_types=[USER_TYPE.moderator, USER_TYPE.support])
    async def message_read(self, data):
        """
        Marks message as read by ID.

        :param: ``str`` **message_id**: ID of message to read.

        Returns

        **404 Not Found**
            if no message with `message_id` found

        **403 Forbidden**
            @TODO

        **200 OK**
            in success

        """
        Chat = s().M.model('Chat', self.ctx)
        Message = s().M.model('Message', self.ctx)
        user = self.ctx.request_user
        message = await Message.get_one(_id=data['message_id'])
        if not message:
            return apiresults.not_found()

        chat = await Chat.get_one(_id=message.chat_id)
        a = await message.is_author(user)
        if a or message.is_read or not chat or not chat.is_member(user):
            return apiresults.forbidden()

        message.is_read = True
        await message.save()
        await chat.message_read(user)

        return apiresults.ok()

    @req_auth(allowed_types=[USER_TYPE.moderator, USER_TYPE.support])
    async def message_delete(self, data):
        """
        Marks message as delete by ID.

        :param: ``str`` **message_id**: ID of message to read.

        Returns

        **404 Not Found**
            if no message with `message_id` found

        **403 Forbidden**
            if no chat found or authorized user is not member for chat

        **200 OK**
            in success

        """
        Chat = s().M.model('Chat', self.ctx)
        Message = s().M.model('Message', self.ctx)

        user = self.ctx.request_user

        message = await Message.get_one(_id=data['message_id'])
        if not message:
            return apiresults.not_found()

        chat = await Chat.get_one(_id=message.chat_id)
        if not chat or not chat.is_member(user):
            return apiresults.forbidden()

        await chat.message_delete(message, user)

        return apiresults.ok()

    @req_auth(allowed_types=[USER_TYPE.moderator, USER_TYPE.support])
    async def message_read_all(self, data):
        """
        Marks all messages as read by chat ID.

        :param: ``str`` **chat_id**: ID of chat to read.

        Returns

        **404 Not Found**
            if no chat with `chat_id` found

        **403 Forbidden**
            if authorized user is not member for chat

        **200 OK**
            in success

        """
        Chat = s().M.model('Chat', self.ctx)
        user = self.ctx.request_user
        chat_id = data.get('chat_id', None)
        if chat_id is not None:
            chat = await Chat.get_one(_id=chat_id)
            if not chat:
                return apiresults.not_found()
            if not chat.is_member(user):
                return apiresults.forbidden()
            await chat.read(user)
        else:
            await Chat.read_all(user)
        return apiresults.ok()

    @req_auth(allowed_types=[USER_TYPE.moderator, USER_TYPE.support])
    async def message_unread_count(self, data):
        """
        Get unread message count from all chats.

        Returns

        **200 OK**
            with `count` in success

        """
        Message = s().M.model('Message', self.ctx)
        count = await Message.get_unread_count_by_user(self.ctx.request_user)
        return apiresults.ok(count=count)
