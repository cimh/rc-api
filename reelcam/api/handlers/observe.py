# -*- coding: utf-8 -*-
from tornado import gen
from api_engine import apiresults, s
from api_engine.enums import OBSERVE_STATUSES
from .base import BaseAPIHandler
from ..decorators import req_auth


class ObserveAPIHandler(BaseAPIHandler):
    """
    Observe API Handler class.
    """

    _handles = {
        'get_requests': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'request': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'accept': {
            'request_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'reject': {
            'request_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'reject_all': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'begin': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'end': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
    }

    @req_auth()
    async def get_requests(self, data):
        """
        Get Observe requests by Video ID.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **404 Not Found**
            if video or user not found

        **403 Forbidden**
            if user is not reporter for video

        **200 OK**
            in success

        """
        User = s().M.model('User', self.ctx)
        u = await User.get_one(_id=self.ctx.request_user.id, _deleted=False)
        Video = s().M.model('Video', self.ctx)
        v = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not u or not v:
            return apiresults.not_found()
        elif not v.is_reporter(u):
            return apiresults.forbidden()

        Observe = s().M.model('Observe', self.ctx)
        requests = await Observe.get(video_id=v.id, status=OBSERVE_STATUSES.request)
        items = [o.get_data() for o in requests]
        return apiresults.ok(items=items)

    @req_auth()
    async def request(self, data):
        """
        Sends observe request.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **404 Not Found**
            if no video found

        **403 Forbidden**
            if video is not active or already observing

        **302 Found**
            with request status in `status` in `data` if user already sent request

        **200 OK**
            with `status` and `req_count` in `data` in success

        """
        Video = s().M.model('Video', self.ctx)
        Observe = s().M.model('Observe', self.ctx)

        v = await Video.get_one(_id=data['video_id'], _deleted=False)
        if not v:
            return apiresults.not_found()

        o, count = await Observe.request(v, self.ctx.request_user)
        if not o:
            r = apiresults.forbidden()
        elif count is not None:
            r = apiresults.ok(status=o.status, req_count=count)
        else:
            r = apiresults.found(status=o.status)
        return r

    @req_auth()
    async def accept(self, data):
        """
        Accept observe request.

        :param: ``str`` **request_id**: Request ID.

        Returns

        **404 Not Found**
            if no request with `request_id` found

        **403 Forbidden**
            if request was accepted

        **200 OK**
            in success

        """
        Observe = s().M.model('Observe', self.ctx)
        o = await Observe.get_one(_id=data['request_id'], _deleted=False)
        if not o:
            return apiresults.not_found()
        else:
            o = await o.accept(self.ctx.request_user.id)
            if o:
                return apiresults.ok()
            else:
                return apiresults.forbidden()

    @req_auth()
    async def reject(self, data):
        """
        Rejects observe request.

        :param: ``str`` **request_id**: Request ID.

        Returns

        **404 Not Found**
            if no request with `request_id` found

        **403 Forbidden**
            if request already accepted or authorized user is not video reporter

        **200 OK**
            in success

        """
        Observe = s().M.model('Observe', self.ctx)
        o = await Observe.get_one(_id=data['request_id'], _deleted=False)
        if not o:
            return apiresults.not_found()
        else:
            o = await o.reject(self.ctx.request_user.id)
            if o:
                return apiresults.ok()
            else:
                return apiresults.forbidden()

    @req_auth()
    async def reject_all(self, data):
        """
        Rejects all observe requests for a video.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **404 Not Found**
            if no video with `video_id` found

        **403 Forbidden**
            if could not reject video requests for video with `video_id`

        **200 OK**
            in success

        """
        Video = s().M.model('Video', self.ctx)
        v = await Video.get_one(_id=data['video_id'])
        if not v:
            return apiresults.not_found()
        else:
            Observe = s().M.model('Observe', self.ctx)
            r = await Observe.reject_all(data['video_id'], self.ctx.request_user.id)
            if r:
                return apiresults.ok()
            else:
                return apiresults.forbidden()

    @req_auth()
    async def begin(self, data):
        """
        Start observing.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **403 Forbidden**
            if could not start observing

        **200 OK**
            in success

        """
        Observe = s().M.model('Observe', self.ctx)
        r = await Observe.begin(data['video_id'], self.ctx.request_user.id)
        if r:
            return apiresults.ok()
        else:
            return apiresults.forbidden()

    @req_auth()
    async def end(self, data):
        """
        Stop observing.

        :param: ``str`` **video_id**: Video ID.

        Returns

        **403 Forbidden**
            if cound end observing

        **200 OK**
            in success

        """
        Observe = s().M.model('Observe', self.ctx)
        r = await Observe.end(data['video_id'], self.ctx.request_user.id)
        if r:
            return apiresults.ok()
        else:
            return apiresults.forbidden()
