# -*- coding: utf-8 -*-
from tornado import gen
from api_engine import apiresults
from api_engine import s
from api_engine.enums import USER_TYPE, FOLLOW_STATUS
from .base import BaseAPIHandler
from ..decorators import req_auth


class FollowAPIHandler(BaseAPIHandler):
    """
    Follow API Handler class.
    """

    _handles = {
        'followers': {
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'following': {
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'target_user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'accept': {
            'follow_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'discard': {
            'follow_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'count': {
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'disfollow': {
            'user_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'follow': {
            'user_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'followers_count': {
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'is_followed': {
            'user_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'is_follower': {
            'user_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'requests': {
            'limit': dict(type='integer'),
            'offset': dict(type='integer'),
        },
        'undisfollow': {
            'user_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'unfollow': {
            'user_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
    }

    async def followers(self, data):
        """
        Get list of followers for current user when called without args or for user_id if present.

        :param: ``str`` **user_id**: User ID.

        Returns

        **501 Not Implemented**

        """
        return apiresults.not_implemented()

    async def following(self, data):
        """
        Get list of who a current user is following when called without args or for user_id if present.
        If only user_id present, returns 200 OK if you are following user_id or 404 Not Found if not.
        If target_user_id and user_id are present returns 404 if user_id is not following target_user_id and
        200 if following.

        :param: ``str`` **user_id**: User ID.
        :param: ``str`` **target_user_id**: Target user ID.

        Returns

        **501 Not Implemented**

        """
        return apiresults.not_implemented()

    @req_auth()
    async def accept(self, data):
        """
        Accept follow request and update request status.

        :param: ``str`` **follow_id**: Follow ID.

        Emits event **follow:accept**

        .. code:: javascript

            {
                "event_user_id": "<user ID>",
                "data": {
                    "follow_request_id": "<follow ID>",
                    "from": "<follow from ID>",
                    "to": "<follow to ID>",
                    "status": "<follow status>",
                    "ts": "<timestamp>",
                    "event_user_id": "<user ID>",
                    "message": "<follow message body>"
                },
                "method": "follow:accept"
            }

        Returns

        **404 Not Found**
            if no follow with  `follow_id` found

        **403 Forbidden**
            if authorized user is not in `_to` in follow

        **200 OK**
            in success

        """
        Follow = s().M.model('Follow', self.ctx)
        User = s().M.model('User', self.ctx)

        follow = await Follow.get_one(_id=data['follow_id'])
        current_user = self.ctx.request_user

        if not follow:
            return apiresults.not_found()

        if follow._to != current_user.id:
            return apiresults.forbidden()

        if follow.is_request():
            u = await User.get_one(_id=follow._from)
            await follow.accept()

        return apiresults.ok()

    @req_auth()
    async def discard(self, data):
        """
        Discard follow request and update request status.

        :param: ``str`` **follow_id**: Follow ID.

        Emits event **follow:discard**

        .. code:: javascript

            {
                "data": {
                    "follow_request_id": "<follow request ID>",
                    "from": "<follow from user ID>",
                    "to": "<follow to user ID>",
                    "status": "<follow status>",
                    "ts": "<timestamp>",
                    "message": "<follow message>",
                    "event_user_id": "<user ID>"
                },
                "event_user_id": "<user ID>",
                "method": "follow:discard"
            }

        Returns

        **404 Not Found**
            if no follow with `follow_id` found

        **403 Forbidden**
            if authorized user is not in `_to` in follow

        **200 OK**
            in success

        """
        Follow = s().M.model('Follow', self.ctx)

        follow = await Follow.get_one(_id=data['follow_id'])

        if not follow:
            return apiresults.not_found()

        if follow._to != self.ctx.request_user.id:
            return apiresults.forbidden()

        if follow.is_request():
            await follow.discard()

        return apiresults.ok()

    async def count(self, data):
        """
        Get count of followed users ('i follow').

        :param: ``str`` **user_id**: User ID.

        Returns

        **404 Not Found**
            if no user with `user_id` found

        **401 Unauthorized**
            if no user with `user_id` found and user is unauthorized

        **200 OK**
            with `count` in `data` in success

        """
        User = s().M.model('User', self.ctx)

        if data.get('user_id', None):
            user = await User.get_one(_id=data['user_id'])
            if not user:
                return apiresults.not_found()
        else:
            user = self.ctx.request_user
            if not user:
                return apiresults.unauthorized()

        count = len(user.followed)

        return apiresults.ok(count=count)

    async def followers_count(self, data):
        """
        Returns count of followers ('follows me').

        :param: ``str`` **user_id**: User ID.

        Returns

        **404 Not Found**
            if no user with `user_id` found

        **401 Unauthorized**
            if user is unauthorized

        **200 OK**
            with `count` in `data`

        """
        User = s().M.model('User', self.ctx)

        if data.get('user_id', None):
            user = await User.get_one(_id=data['user_id'])
            if not user:
                return apiresults.not_found()
        else:
            # @TODO: replace with @req_auth()?
            user = self.ctx.request_user
            if not user:
                return apiresults.unauthorized()

        if user.user_type == USER_TYPE.system:
            count = await User.count()
        else:
            count = len(user.followers)

        return apiresults.ok(count=count)

    @req_auth()
    async def disfollow(self, data):
        """
        Unfollow user from me (current user) and update follow status.

        :param: ``str`` **user_id**: ID of user to stop follow.

        Emits event **follow:disfollow**

        .. code:: javascript

            {
                "data": {
                    "follow_request_id": "<follow request ID>",
                    "from": "<follow from user ID>",
                    "to": "<follow to user ID>",
                    "status": "<follow status>",
                    "ts": "<timestamp>",
                    "message": "<follow message>",
                    "event_user_id": "<user ID>"
                },
                "event_user_id": "<user ID>",
                "method": "follow:disfollow"
            }

        Returns

        **404 Not Found**
            if no user with `user_id` found or no follow found

        **200 OK**
            in success

        """
        Follow = s().M.model('Follow', self.ctx)
        User = s().M.model('User', self.ctx)

        current_user = self.ctx.request_user

        user = await User.get_one(_id=data['user_id'])

        if not user:
            return apiresults.not_found()

        follow = Follow.new(_from=user.id, _to=current_user.id)
        follow = await Follow.get_one(_id=follow.custom_id)

        if not follow:
            return apiresults.not_found()

        if follow.is_follow():
            await follow.disfollow()

        return apiresults.ok()

    @req_auth()
    async def follow(self, data):
        """
        Follow user by ID.

        :param: ``str`` **user_id**: ID of user to be followed.

        Emits event **follow:follow**

        .. code:: javascript

            {
                "data": {
                    "follow_request_id": "<follow request ID>",
                    "from": "<follow from user ID>",
                    "to": "<follow to user ID>",
                    "status": "<follow status>",
                    "ts": "<timestamp>",
                    "message": "<follow message>",
                    "event_user_id": "<user ID>"
                },
                "event_user_id": "<user ID>",
                "method": "follow:follow"
            }

        Returns

        **404 Not Found**
            if no user with `user_id` found

        **304 Not Modified**
            with `status` in `data` if already following

        **200 OK**
            with follow request status in `status` in `data` in success

        """
        scope = s().get_scope(data.get('app_name'))
        Follow = s().M.model('Follow', self.ctx, scope=scope)
        User = s().M.model('User', self.ctx)

        current_user = self.ctx.request_user

        # Not allowed to follow myself.
        if current_user.id == data['user_id']:
            return apiresults.forbidden()

        followed_user = await User.get_one(_id=data['user_id'])
        if not followed_user:
            return apiresults.not_found()

        f = Follow.new(_from=current_user.id, _to=followed_user.id)
        follow = await Follow.get_one(_id=f.custom_id)

        was_status = None
        if not follow:
            follow = Follow.new(_from=current_user.id, _to=followed_user.id, scope=Follow.default_scope)
            await follow.save()
        elif follow.is_follow():
            return apiresults.not_modified(status=follow.status)
        # If follow record exist and it status is not 'follow' -> remember status.
        else:
            was_status = follow.status

        follow = await follow.follow(follow, current_user, followed_user)

        # If follow record exist with status follow.is_request
        if was_status is not None \
                and follow.is_request() \
                and follow.status == was_status:
            return apiresults.not_modified(status=follow.status)

        return apiresults.ok(status=follow.status)

    @req_auth()
    async def request(self, data):
        """
        Alias for follow().

        :param: ``str`` **user_id**: ID of user to be followed.
        """
        await self.follow(data)

    @req_auth()
    async def is_followed(self, data):
        """
        Returns status of follow if current user is following user_id.

        :param: ``str`` **user_id**: ID of user to be checked.

        Returns

        **404 Not Found**
            if no user with `user_id` found or no follow found

        **200 OK**
            with `status` in `data` in success

        """
        Follow = s().M.model('Follow', self.ctx)
        User = s().M.model('User', self.ctx)

        user = self.ctx.request_user

        followed_user = await User.get_one(_id=data['user_id'])

        if not followed_user:
            return apiresults.not_found()

        follow = Follow.new(_from=user.id, _to=followed_user.id)
        follow = await Follow.get_one(_id=follow.custom_id)

        if not follow:
            return apiresults.not_found()

        return apiresults.ok(status=follow.status)

    @req_auth()
    async def is_follower(self, data):
        """
        Returns status of follow if current user is follower of user_id.

        :param: ``str`` **user_id**: ID of user to check.

        Returns

        **404 Not Found**
            if no user with `user_id` found or no follow found

        **200 OK**
            with `status` in `data` in success

        """
        Follow = s().M.model('Follow', self.ctx)
        User = s().M.model('User', self.ctx)

        user = self.ctx.request_user

        follower_user = await User.get_one(_id=data['user_id'])

        if not follower_user:
            return apiresults.not_found()

        follow = Follow.new(_from=follower_user.id, _to=user.id)
        follow = await Follow.get_one(_id=follow.custom_id)

        if not follow:
            return apiresults.not_found()

        return apiresults.ok(status=follow.status)

    @req_auth()
    async def requests(self, data):
        """
        Returns list of follow requests for current user.
        @TODO: limit + offset

        :param: ``int`` **limit**: Limit.
        :param: ``int`` **offset**: Offset.

        Returns

        **200 OK**
            with `items` in `data` in success

        """
        Follow = s().M.model('Follow', self.ctx)
        follows = await Follow.get(_to=self.ctx.request_user.id, status=FOLLOW_STATUS.request)
        return apiresults.ok(items=[f.get_req_data() for f in follows])

    @req_auth()
    async def undisfollow(self, data):
        """
        Revert disfollow and start follow user again.

        :param: ``str`` **user_id**: ID of user to start follow.

        Emits event **follow:undisfollow**

        .. code:: javascript

            {
                "data": {
                    "follow_request_id": "<follow request ID>",
                    "from": "<follow from user ID>",
                    "to": "<follow to user ID>",
                    "status": "<follow status>",
                    "ts": "<timestamp>",
                    "message": "<follow message>",
                    "event_user_id": "<user ID>"
                },
                "event_user_id": "<user ID>",
                "method": "follow:undisfollow"
            }

        Returns

        **404 Not Found**
            if no user with `user_id` found or no follow found

        **200 OK**
            in success

        """
        Follow = s().M.model('Follow', self.ctx)
        User = s().M.model('User', self.ctx)

        current_user = self.ctx.request_user

        user = await User.get_one(_id=data['user_id'])

        if not user:
            return apiresults.not_found()

        follow = Follow.new(_from=user.id, _to=current_user.id)
        follow = await Follow.get_one(_id=follow.custom_id)

        if not follow:
            return apiresults.not_found()

        if follow.is_disfollow():
            await follow.undisfollow()

        return apiresults.ok()

    @req_auth()
    async def unfollow(self, data):
        """
        Stop follow user and update follow request status.

        :param: ``str`` **user_id**: ID of user to stop follow.

        Emits event **follow:unfollow**

        .. code:: javascript

            {
                "data": {
                    "follow_request_id": "<follow request ID>",
                    "from": "<follow from user ID>",
                    "to": "<follow to user ID>",
                    "status": "<follow status>",
                    "ts": "<timestamp>",
                    "message": "<follow message>",
                    "event_user_id": "<user ID>"
                },
                "event_user_id": "<user ID>",
                "method": "follow:unfollow"
            }

        Returns

        **404 Not Found**
            if no user with `user_id` found or no follow found

        **304 Not Modified**
            with `status` in `data` if already following

        **200 OK**
            in success

        """
        Follow = s().M.model('Follow', self.ctx)
        User = s().M.model('User', self.ctx)

        current_user = self.ctx.request_user

        followed_user = await User.get_one(_id=data['user_id'])

        if not followed_user:
            return apiresults.not_found()

        follow = Follow.new(_from=current_user.id, _to=followed_user.id)
        follow = await Follow.get_one(_id=follow.custom_id)

        if not follow:
            return apiresults.ok()

        if follow.is_follow():
            await follow.unfollow()
            return apiresults.ok()

        return apiresults.not_modified(status=follow.status)
