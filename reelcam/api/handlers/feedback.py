# -*- coding: utf-8 -*-
from tornado import gen
from api_engine import apiresults, s
from api_engine.enums import APP_NAMES
from reelcam.api.handlers.base import BaseAPIHandler
from utils.datetimemixin import dtm
from api_engine.enums import REELCAM, CITYDOOM, WEDCAM


class FeedbackApiHandler(BaseAPIHandler):
    """
    Feedback API Handler
    """

    _handles = {
        'request': {
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH, maxlength=32),
        },
        'contact_us': {
            'app_name': dict(type='string', allowed=[REELCAM, CITYDOOM, WEDCAM], minlength=6, maxlength=8),
            'term': dict(required=True, type='string', minlength=3, maxlength=100),
            'name': dict(required=True, type='string', minlength=3, maxlength=100),
            'email': dict(type='string', regex='[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+', minlength=3, maxlength=128),
            'message': dict(required=True, type='string', minlength=3, maxlength=500),
        },
        'recent': {
            'scope': dict(required=True, type='string', allowed=APP_NAMES),
            'offset': dict(type='integer', min=0),
            'limit': dict(type='integer', min=0, max=100),
        },
        'set_timestamp': {},
        'submit': {
            'video_id': dict(type='string', required=True),
            'error_desc': dict(type='string', required=True),
            'error_type': dict(type='string', required=True),
            'info': dict(type='string', required=True),
            'os_name': dict(type='string', required=True),
            'os_version': dict(type='string', required=True),
            'user_id': dict(type='string', required=True),
        },
    }

    async def request(self, data):
        """
        Push 'feedback:request' event to the client.

        :param: ``str`` **user_id**: User ID.

        Returns

        **404 Not Found**
            if user with `user_id` not exists

        **200 OK**
            on success

        """
        User = s().M.model('User')

        user = await User.get_one(_id=data['user_id'])
        if not user:
            return apiresults.not_found()

        s().e("feedback:request", sender='SYS', users=[user], data={})

        return apiresults.ok()

    async def contact_us(self, data):
        """
        Submit a 'Contact us' form.

        :param: ``str`` **app_name**: "reelcam" or "citydoom" string in lowercase.
        :param: ``str`` **term**: Message subject.
        :param: ``str`` **name**: User name.
        :param: ``str`` **email**: User email.
        :param: ``str`` **message**: Message body.

        Returns

        **200 OK**
            on success

        """
        from api_engine.enums import CITYDOOM, WEDCAM
        Feedback = s().M.model('Feedback', scope=s().get_scope(data.get('app_name')))

        addr = s().contact_us_address
        app_name = data.get('app_name')
        if app_name:
            if app_name == CITYDOOM:
                addr = "support@citydoom.com"
            elif app_name == WEDCAM:
                addr = "support@wedcam.tv"

        feedback = Feedback.new(data['name'], data['term'], data['message'], data.get('email'))
        feedback = await feedback.save()

        recipients = [dict(email=addr)]
        message = {'to': recipients,
                   'from_name': data["name"],
                   'from_email': data.get("email"),
                   'subject': data["term"],
                   'text': data["message"]
        }
        s().email_sender.messages.send(message=message)
        return apiresults.ok(**feedback.get_data())

    async def recent(self, data):
        """
        Get recent Feeds.

        :param: ``str`` **scope**: Data scope.
        :param: ``int`` **offset**: Offset.
        :param: ``int`` **limit**: Limit.

        Returns

        **200 OK**
            with `items` and `has_more` in data

        """
        Feedback = s().M.model('Feedback', scope=s().get_scope(data['scope']))

        offset = data.get('offset', 0)
        limit = data.get('limit', 10)

        recent, has_more = await Feedback.get_recent(offset, limit)
        return apiresults.ok(items=[{feed.id: feed.get_data()} for feed in recent], has_more=has_more)

    async def set_timestamp(self, data):
        """
        Set Feed timestamp.
        Beware: for backend developers only!
        """
        Feedback = s().M.model('Feedback')

        if data:
            try:
                items = dict(
                    ((str(item[0])[:BaseAPIHandler.ES_ID_MIN_LENGTH], int(item[1])) for item in list(data.items())))
            except (TypeError, ValueError):
                return apiresults.bad_request()
            feeds = await Feedback.get(_ids=list(items), _deleted=False)
            for feed in feeds:
                feed.create_ts = dtm.YmdHMS_from_ts(items[feed.id])
                await feed.save()

        return apiresults.ok()

    async def submit(self, data):
        """
        Submit feedback form.

        :param ``str`` **video_id**: Video ID.
        :param ``str`` **error_desc**: Error description.
        :param ``str`` **error_type**: Error type.
        :param ``str`` **info**: User info.
        :param ``str`` **os_name**: OS name.
        :param ``str`` **os_version**: OS version.
        :param ``str`` **user_id**: User ID.

        Returns

        **200 OK**
            in success.

        """
        text = "video_id=%s\nuser_id=%s\ninfo=%s\nerror_type=%s\nos_name=%s\nos_version=%s\n\n%s" % \
               (data['video_id'], data['user_id'], data['info'], data['error_type'], data['os_name'],
                data['os_version'], data['error_desc'])

        emails = ["yaroslav.mitrofanov@seventel.ru",
                  "slava.alennikov@seventel.ru"]

        src = "report-no-reply@reelcam.com"
        name = subject = "report-no-reply"

        for dst in emails:
            s().email.delay(src, dst, text, name, subject, html=False, attachments=data['files'])

        return apiresults.ok()
