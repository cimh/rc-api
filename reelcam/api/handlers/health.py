# -*- coding: utf-8 -*-
from .base import BaseAPIHandler
from api_engine import apiresults
from tornado import gen


class HealthAPIHandler(BaseAPIHandler):
    """
    Health API Handler class.
    """

    _handles = {
        'get': {},
    }

    async def get(self, data):
        """
        Dummy API Handler. Used to check service availability.

        Returns

        **200 OK**
          everytime

        """
        return apiresults.ok()
