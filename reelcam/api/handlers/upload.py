# -*- coding: utf-8 -*-
import logging
from json import dumps

from tornado import gen
from api_engine.enums import USER_TYPE
from api_engine import apiresults, s
from api_engine.request import APIRequest
from reelcam.models.upload import PHOTO, LIVE, RECORD, R, RI, O
from reelcam.uploader import get_uploader
from .base import BaseAPIHandler

log = logging.getLogger(__name__)


class UploadApiHandler(BaseAPIHandler):
    """
    Upload API Handler.
    """

    _handles = {
        'upload': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'mode': dict(required=True, type='string', allowed=[LIVE, RECORD, PHOTO]),
            'target': dict(required=True, type='string', allowed=[R, RI, O]),
            'reporter_token': dict(type='string', minlength=1),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'size': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'mode': dict(required=True, type='string', allowed=[LIVE, RECORD]),
            'target': dict(required=True, type='string', allowed=[R, RI, O]),
            'reporter_token': dict(type='string', minlength=1),
            'user_id': dict(type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
        },
        'duration': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'mode': dict(required=True, type='string', allowed=[LIVE, RECORD]),
            'target': dict(required=True, type='string', allowed=[R, RI, O]),
            'reporter_token': dict(type='string', minlength=1),
            'user_id': dict(type='string', minlength=1),
            'finished': dict(required=True, type='string', allowed=['0', '1']),
            'seconds': dict(required=True, type='string', minlength=1),
        },
        'key': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'target': dict(required=True, type='string', minlength=1, allowed=[R, RI, O]),
        },
        'mediastorage': {},
        'direct': {
            'key': dict(required=True, type='string', minlength=1),
            'mpart_id': dict(required=True, type='string', minlength=1),
            'part_num': dict(required=True, type='integer', min=1),
            'size': dict(required=True, type='integer', min=1),
            'target': dict(required=True, type='string', minlength=1, allowed=[R, RI, O]),
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'token': dict(required=True, type='string', minlength=1),
            'mediastorage': {
                'required': True,
                'type': 'dict',
                'schema': {
                    'type': dict(required=True, type='string', minlength=1),
                    'host': dict(required=True, type='string', minlength=1),
                    'extra': {
                        'required': True,
                        'type': 'dict',
                        'schema': {
                            'bucket': dict(required=True, type='string', minlength=1)
                        },
                    },
                },
            },
        },
        'stream_alive': {
            'video_id': dict(required=True, type='string', minlength=BaseAPIHandler.ES_ID_MIN_LENGTH),
            'target': dict(required=True, type='string', minlength=1, allowed=[R, RI, O]),
            'token': dict(required=True, type='string', minlength=1),
            'num': dict(required=True, type='integer', min=1),
        }
    }

    @gen.coroutine
    def upload(self, data):
        """

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **user_id**: User ID.
        :param: ``str`` **mode**: stream mode('live' | 'record').
        :param: ``str`` **target**: Media target('r' | 'ri' | 'o').
        :param: ``str`` **reporter_token**: Video token.
        :param: ``str`` **offset**: stream offset.
        """
        if self.ctx.request.type == APIRequest.HTTP \
                and self.ctx.request.method != 'POST':
            raise gen.Return('Only POST allowed')

        Video = s().M.model('Video')
        Upload = s().M.model('Upload')

        files = data.pop('files', None)
        if not files:
            log.error('Chunk file missing: %s', dumps(data, indent=3, sort_keys=True))
            raise gen.Return('File not available')
        else:
            http_file = files['data'][0]

        video = yield Video.get_one(_id=data['video_id'])
        if not video:
            log.warning('Video not found: %s', dumps(data, indent=3, sort_keys=True))
            raise gen.Return('Invalid input params: %s' % dumps(data))
        elif video.is_photo:
            raise gen.Return(apiresults.forbidden(is_photo=True))

        if not self._can_upload(video, self.ctx.request_user, data.get('reporter_token')):
            log.error('Token check FAIL: %s', dumps(data, indent=3, sort_keys=True))
            raise gen.Return('Error: tokens mismatch')

        result = "OK"

        if data['mode'] == LIVE:
            target = data['target']
        elif data['mode'] == RECORD:
            target = RI
        elif data['mode'] == PHOTO:
            if http_file.content_type == 'image/jpeg':
                key = video.mediastorage_key('screen1', ext='jpg')
                result = yield self._upload_preview(data['video_id'], key, http_file.body)
            raise gen.Return(result)
        else:
            raise gen.Return(result)

        key = video.mediastorage_key('_'.join([target, 'original']))

        if video.upload_ids[target]:
            upload = yield Upload.get_one(_id=video.upload_ids[target])
            if upload.is_complete:
                if not video.is_reanimated:
                    raise gen.Return(apiresults.forbidden())
        else:
            upload = Upload(dict(video_id=video.id,
                                 target=target,
                                 key=str(key)))
            upload = yield upload.save()
            video.upload_ids[upload.target] = upload.id
            video.upload_ids = video.upload_ids.copy()
            video = yield video.save()

        offset = data.get('offset', None)
        if offset is None and target not in ("r", "o"):
            raise gen.Return('errsize')
        if offset is not None:
            try:
                offset = int(offset)
            except (TypeError, ValueError):
                raise gen.Return('errsize')
            if upload.size != offset:
                data.pop('files', None)
                log.error('Offset check FAIL: %s', dumps(data, indent=3, sort_keys=True))
                raise gen.Return('errsize')
            else:
                if log.isEnabledFor(logging.INFO):
                    data.pop('files', None)
                    log.debug('Offset check OK: %s', dumps(data, indent=3, sort_keys=True))

        upload.next_part(len(http_file.body))
        upload = yield upload.save()

        if upload.mpart_id:
            mpart = get_uploader().resume_multipart(key, upload.mpart_id)
        else:
            mpart = get_uploader().initiate_multipart(key)

        if mpart:
            if not upload.mpart_id:
                upload.mpart_id = mpart.id
                upload = yield upload.save()
            from io import StringIO
            r = mpart.upload_part(StringIO(http_file.body), upload.part_num)
            if not r:
                log.error('Chunk upload FAIL: %s', dumps(data, indent=3, sort_keys=True))
            else:
                if log.isEnabledFor(logging.INFO):
                    log.debug('Chunk upload OK: %s', dumps(data, indent=3, sort_keys=True))

        if target == 'r':
            User = s().M.model('User')
            reporter = yield User.get_one(_id=video.reporter_id)
            del Video
            from tornado.util import ObjectDict
            Video = s().M.model('Video', ctx=ObjectDict(client_ip='127.0.0.1', request_user=reporter))
            video = yield Video.get_one(_id=data['video_id'])
            yield video.r_chunk_uploaded(upload)

        raise gen.Return('OK')

    @classmethod
    @gen.coroutine
    def _upload_preview(cls, video_id, key, preview):
        """

        :param **video_id**: str Video ID.
        :param **key**: str Key.
        :param **preview**: str image binary.

        :return: str.
        """
        from reelcam.uploader import get_uploader
        from api_engine.mediahelper import MediaHelper
        Video = s().M.model('Video')

        video = yield Video.get_one(_id=video_id)

        def callback(key):
            """

            :param key:
            :return:
            """
            from tornado.ioloop import IOLoop
            IOLoop.current().add_callback(video.set_screenshot_keys, dict(screen1=str(key)))
            get_uploader().set_acl(key, 'public-read')
            if log.isEnabledFor(logging.INFO):
                log.debug('Preview upload OK: key=%s', str(key))

        def err(key):
            """

            :param key:
            :return:
            """
            log.error('Preview upload FAIL: key=%s', str(key))

        if MediaHelper.get_image_format(preview):
            cropped = MediaHelper.crop_preview(preview)
            get_uploader().upload(key, cropped, cb=callback, cb_err=err)

        raise gen.Return('OK')

    @gen.coroutine
    def size(self, data):
        """

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **user_id**: User ID.
        :param: ``str`` **mode**: stream mode('live' | 'record').
        :param: ``str`` **target**: Media target('r' | 'ri' | 'o').
        :param: ``str`` **reporter_token**: Video token.
        """
        if self.ctx.request.type == APIRequest.HTTP \
                and self.ctx.request.method not in ['GET', 'POST']:
            raise gen.Return('Only GET or POST allowed')

        Video = s().M.model('Video')
        Upload = s().M.model('Upload')

        video = yield Video.get_one(_id=data['video_id'])

        result = "0"
        if not video:
            raise gen.Return(result)
        if not self._can_upload(video, self.ctx.request_user, data.get('reporter_token')):
            raise gen.Return('Error: tokens mismatch')

        if data['mode'] == LIVE:
            target = data['target']
        elif data['mode'] == RECORD:
            target = RI
        else:
            raise gen.Return(result)

        if video and video.upload_ids[target]:
            upload = yield Upload.get_one(_id=video.upload_ids[target])
            if upload:
                result = str(upload.size)

        raise gen.Return(result)

    @gen.coroutine
    def duration(self, data):
        """

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **user_id**: User ID.
        :param: ``str`` **mode**: stream mode('live' | 'record').
        :param: ``str`` **target**: Media target('r' | 'ri' | 'o').
        :param: ``str`` **reporter_token**: Video token.
        :param: ``str`` **seconds**: current Video duration in seconds.
        :param: ``str`` **finished**: upload finish flag('0' | '1').
        """
        if self.ctx.request.type == APIRequest.HTTP \
                and self.ctx.request.method not in ['GET', 'POST']:
            raise gen.Return('Only GET or POST allowed')

        Video = s().M.model('Video')
        Upload = s().M.model('Upload')

        video = yield Video.get_one(_id=data['video_id'])

        result = "OK"
        if not video:
            data.pop('files', None)
            log.warning('Video not found: %s', dumps(data, indent=3, sort_keys=True))
            raise gen.Return(result)
        if not self._can_upload(video, self.ctx.request_user, data.get('reporter_token')):
            raise gen.Return('Error: tokens mismatch')

        if data['mode'] == LIVE:
            target = data['target']
        elif data['mode'] == RECORD:
            target = RI
        else:
            raise gen.Return(result)

        finished = bool(int(data['finished']))
        if finished and video.upload_ids[target]:

            upload = yield Upload.get_one(_id=video.upload_ids[target])
            if upload.is_complete:
                raise gen.Return(result)

            from reelcam.uploader import get_uploader
            mpart = get_uploader().resume_multipart(upload.key, upload.mpart_id)
            if mpart:
                mpart.complete_upload()
                yield upload.complete(mpart)

            if target in [R, RI]:
                seconds = int(data['seconds'])
                if seconds and not video.meta.duration:
                    meta = video.meta
                    meta.duration = seconds
                    meta.filesize = upload.size
                    yield video.set_meta(meta)

        raise gen.Return(result)

    @gen.coroutine
    def key(self, data):
        """
        Get mediastorage key for video by target.

        :param: ``str`` **video_id**: ID
        :param: ``str`` **target**: media target('r' | 'ri' | 'o')
        """
        Video = s().M.model('Video')

        video = yield Video.get_one(_id=data['video_id'])
        if not video:
            raise gen.Return(apiresults.not_found())

        key = video.mediastorage_key('_'.join([data['target'], 'original']))
        raise gen.Return(apiresults.ok(key=str(key)))

    def mediastorage(self, data):
        """
        Get info about current mediastorage.
        """
        Upload = s().M.model('Upload')
        info = Upload.get_mediastorage_info(self.ctx.client_ip)
        return apiresults.ok(**info)

    @gen.coroutine
    def direct(self, data):
        """
        Finish the direct upload into mediastorage.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **target**: Media target('r' | 'ri' | 'o').
        :param: ``str`` **token**: Video token.
        :param: ``str`` **mediastorage**: MediaStorage type.
        :param: ``int`` **part_num**: number of uploaded parts.
        :param: ``int`` **size**: size of uploaded file.
        :param: ``str`` **key**: mediastorage key.
        """
        Video = s().M.model('Video')
        Upload = s().M.model('Upload')

        video_id = data.pop('video_id')
        video = yield Video.get_one(_id=video_id)
        if not video:
            raise gen.Return(apiresults.not_found())

        mediastorage = data.pop('mediastorage')
        upload = yield Upload.direct(self.ctx.client_ip, mediastorage, video, **data)
        if upload:
            raise gen.Return(apiresults.ok())

        raise gen.Return(apiresults.forbidden())

    @gen.coroutine
    def stream_alive(self, data):
        """
        Notifies that stream of target type is alive.

        :param: ``str`` **video_id**: Video ID.
        :param: ``str`` **target**: Media target('r' | 'ri' | 'o').
        :param: ``str`` **token**: Video token.
        :param: ``int`` **num**: Call number.
        """
        Video = s().M.model('Video')
        Upload = s().M.model('Upload')

        video = yield Video.get_one(_id=data['video_id'])
        if not video:
            raise gen.Return(apiresults.not_found())

        if not Upload.check_token(video, data['token']):
            raise gen.Return(apiresults.forbidden())

        if data['target'] == R:
            video.r_chunk_uploaded(Upload(part_num=data['num']))

        raise gen.Return(apiresults.ok())

    @staticmethod
    def _can_upload(video, current_user=None, token=None):
        """

        :param video:
        :param current_user:
        :param token:
        :return:
        """
        if current_user and (current_user.id == video.reporter_id or current_user.user_type is USER_TYPE.proxy):
            return True
        if token and token in [video.reporter_token, video.observer_token]:
            return True
        return False
