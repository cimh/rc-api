# -*- coding: utf-8 -*-


class Observable(object):
    """
    Observable object interface.

    """
    observe_request_id = None

    def __init__(self, observer_id, observer_close_ts, observe_request_id):
        """

        :param: observer_id: str Observer ID.
        :param: observer_close_ts:
        :param: observe_request_id:
        :return:
        """
        self.observer_id = observer_id
        self.observer_close_ts = observer_close_ts
        self.set_observe_request_id(observe_request_id)

    def set_observe_request_id(self, observe_request_id):
        """

        :param observe_request_id:
        :return:
        """
        self.observe_request_id = observe_request_id

    def can_observe(self, observer_id):
        """

        :param: observer_id: str Observer ID.
        :return:
        """
        raise NotImplementedError()

    def can_stop_observe(self, observer_id):
        """

        :param: observer_id: str Observer ID.
        :return:
        """
        raise NotImplementedError()

    def observe(self, observer_id):
        """

        :param: observer_id: str Observer ID.
        :return:
        """
        raise NotImplementedError()

    def stop_observe(self, observer_id):
        """

        :param: observer_id: str Observer ID.
        :return:
        """
        raise NotImplementedError()
