# -*- coding: utf-8 -*-


class Observer(object):
    """
    Observer object interface class.

    """
    def __init__(self, observer_id):
        """

        :param observer_id:
        :return:
        """
        self.observer_id = observer_id
