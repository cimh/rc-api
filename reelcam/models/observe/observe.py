# -*- coding: utf-8 -*-
from tornado import gen
from tornado.ioloop import IOLoop
from api_engine import s
from api_engine.enums import OBSERVE_STATUSES
from reelcam.mappers.observe import ObserveEntryMappingType
from reelcam.models.base import BaseModel, BaseCustomID


class Observe(BaseCustomID, BaseModel):
    """
    An Observe model.

    """
    mapper = ObserveEntryMappingType
    custom_id_attrs = ['video_id', 'user_id']

    REQUEST_QUEUE_SIZE = 10
    BEGIN_TIMEOUT = 60

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id="",
                    video_id="",
                    user_id="",
                    status=OBSERVE_STATUSES.request)

    @classmethod
    def new(cls, video_id, user_id):
        """

        :param video_id:
        :param user_id:
        :return:
        """
        o = cls(dict(video_id=video_id,
                     user_id=user_id,
                     status=OBSERVE_STATUSES.request,
                     create_ts=cls.now_YmdHMS()))
        return o

    @classmethod
    @gen.coroutine
    def request(cls, video, user):
        """

        :param video:
        :param user:
        :return: list [Observe, int]
        """
        o = count = None
        r = [o, count]  # Observer object, request count.

        if not video.can_observe(user.id):
            raise gen.Return(r)

        o = cls.new(video_id=video.id, user_id=user.id)
        o = yield cls.get_one(_id=o.custom_id)
        if o:
            r[0] = o
            if o.status is OBSERVE_STATUSES.accept:
                IOLoop.current().spawn_callback(o.accept, video.reporter_id)
                raise gen.Return(r)
            elif o.status is OBSERVE_STATUSES.reject:
                raise gen.Return(r)
            else:
                count = yield cls.mapper.get(video_id=video.id).count()
        else:
            count = yield cls.mapper.get(video_id=video.id).count()
            if count >= cls.REQUEST_QUEUE_SIZE:
                raise gen.Return(r)
            o = cls.new(video.id, user.id)
            o = yield o.save()
            count += 1
            r[0], r[1] = o, count

        reporter = yield video.get_reporter()
        if reporter:
            method = 'observe:request'
            data = dict(
                id=o.id,
                request_id=o.id,
                video_id=o.video_id,
                observer_id=o.user_id,
                req_count=count)
            s().e(method, sender=user, users=[user, reporter], data=data)
        raise gen.Return(r)

    @gen.coroutine
    def can_change_status(self, current_user_id):
        """

        :param current_user_id: str current User ID.
        :return:
        """
        Video = s().M.model('Video', self.ctx)
        v = yield Video.get_one(_id=self.video_id, _deleted=False)
        if not v or v.reporter_id != current_user_id:
            return
        else:
            raise gen.Return(v)

    def can_accept(self):
        """

        :return:
        """
        return self.status is OBSERVE_STATUSES.request

    def can_reject(self):
        """

        :return:
        """
        return self.status is OBSERVE_STATUSES.request

    @gen.coroutine
    def accept(self, current_user_id):
        """

        :param current_user_id: str current User ID.
        :return:
        """
        User = s().M.model('User', self.ctx)
        current_user = yield User.get_one(_id=current_user_id, _deleted=False)
        if not current_user:
            return

        v = yield self.can_change_status(current_user.id)
        p = u = None
        if v:
            Proxy = s().M.model('Proxy', self.ctx)
            p = yield Proxy.get_one(_id=v.proxy_id)
            u = yield User.get_one(_id=self.user_id, _deleted=False)

        if not all([v, p, u]) or not v.can_observe(self.user_id):
            return

        o = self
        if self.can_accept():
            self.status = OBSERVE_STATUSES.accept
            o = yield self.save()
            v.set_observe_request_id(o.id)
            v = yield v.save()
            self.set_begin_timeout(self.id)
            IOLoop.current().spawn_callback(o.reject_other, current_user_id)

        method = 'observe:accept'
        data = dict(id=v.id,
                    video_id=v.id,
                    observer_token=v.observer_token,
                    proxy_host=p.host,
                    proxy_port_video=p.port_video,
                    proxy_port_rtmp=p.port_rtmp,
                    owner_id=v.owner_id,
                    status=v.status)
        s().e(method, sender=current_user, users=[current_user, u], data=data)

        raise gen.Return(o)

    @gen.coroutine
    def reject(self, current_user_id):
        """

        :param current_user_id:
        :return:
        """
        v = yield self.can_change_status(current_user_id)
        if v and self.can_reject():
            self.status = OBSERVE_STATUSES.reject
            o = yield self.save()
            raise gen.Return(o)
        else:
            return

    @classmethod
    @gen.coroutine
    def reject_all(cls, video_id, current_user_id):
        """

        :param user:
        :param video_id:
        :return:
        """
        Video = s().M.model('Video', cls.ctx)
        v = yield Video.get_one(_id=video_id)
        if not v or v.reporter_id != current_user_id:
            return
        reqs = yield cls.get(video_id=video_id, status=OBSERVE_STATUSES.request, limit=cls.REQUEST_QUEUE_SIZE)
        for r in reqs:
            IOLoop.current().spawn_callback(r.reject, current_user_id)
        raise gen.Return(v)

    @classmethod
    @gen.coroutine
    def start_stream(cls, switch, video_id, current_user_id):
        """

        :param switch: bool start or stop streaming.
        :param video_id:
        :param current_user_id:
        :return:
        """
        Video = s().M.model('Video', cls.ctx)
        v = yield Video.get_one(_id=video_id, _deleted=False)
        o = None
        if v and v.observe_request_id:
            o = yield cls.get_one(_id=v.observe_request_id)
        User = s().M.model('User', cls.ctx)
        u = yield User.get_one(_id=current_user_id, _deleted=False)
        from api_engine.enums import USER_TYPE
        if not all([v, o, u]) \
                or o.status is not OBSERVE_STATUSES.accept \
                or not (o.user_id == current_user_id or u.user_type is USER_TYPE.proxy):
            return
        if switch:
            v = yield v.observe(o.user_id)
        else:
            v = yield v.stop_observe(o.user_id)
        raise gen.Return(v)

    @classmethod
    @gen.coroutine
    def begin(cls, video_id, current_user_id):
        """

        :param video_id:
        :param current_user_id:
        :return:
        """
        v = yield cls.start_stream(True, video_id, current_user_id)
        raise gen.Return(v)

    @classmethod
    @gen.coroutine
    def end(cls, video_id, current_user_id):
        """

        :param video_id:
        :param current_user_id:
        :return:
        """
        v = yield cls.start_stream(False, video_id, current_user_id)
        if v and v.observe_request_id:
            o = yield cls.get_one(_id=v.observe_request_id)
            if o:
                yield o.delete(o.id)
            v.set_observe_request_id(None)
            v = yield v.save()
        raise gen.Return(v)

    @classmethod
    @gen.coroutine
    def set_begin_timeout(cls, observe_id):
        """
        Check that Observer's stream was started during BEGIN_TIMEOUT, otherwise delete the Observe request.

        :param observe_id:
        :return:
        """
        @gen.coroutine
        def callback(observe_id, ctx):
            Observe = s().M.model('Observe', ctx)
            observe = yield Observe.get_one(_id=observe_id)
            if not observe or observe.status is not OBSERVE_STATUSES.accept:
                return
            Video = s().M.model('Video', ctx)
            video = yield Video.get_one(_id=observe.video_id)
            if not video or not video.observe_request_id:
                return
            elif not video.observer_start_ts or video.observer_close_ts:
                video.set_observe_request_id(None)
                yield video.save()
                yield Observe.delete(observe_id)
        IOLoop.current().call_later(cls.BEGIN_TIMEOUT, callback, observe_id, cls.ctx)

    @gen.coroutine
    def reject_other(self, current_user_id):
        """

        :param current_user_id:
        :return:
        """
        requests = yield self.get(video_id=self.video_id, status=OBSERVE_STATUSES.request, limit=self.REQUEST_QUEUE_SIZE)
        for r in requests:
            if r.id == self.id:
                continue

            @gen.coroutine
            def callback(observe, current_user_id):
                observe = yield observe.reject(current_user_id)
                if observe:
                    yield observe.delete(observe.id)
            IOLoop.current().spawn_callback(callback, r, current_user_id)
