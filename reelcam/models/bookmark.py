# -*- coding: utf-8 -*-
from ..mappers.bookmark import BookmarkEntryMappingType
from .base import BaseModel, BaseCustomID
from tornado import gen
from api_engine import s


class Bookmark(BaseCustomID, BaseModel):
    """
    Bookmark Model.
    """

    mapper = BookmarkEntryMappingType
    custom_id_attrs = ['user_id', 'video_id']

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=str(),
                    user_id=str(),
                    video_id=str(),
                    modify_ts=cls.now_YmdHMS())

    def get_data(self):
        """

        :return:
        """
        return dict(id=self.id,
                    user_id=self.user_id,
                    video_id=self.video_id,
                    ts=self.ts_from_YmdHMS(self.modify_ts))

    @classmethod
    def _validation_schema(cls):
        """

        :return:
        """
        return dict(video_id=dict(required=True, type='string'))

    @gen.coroutine
    def add(self, video):
        """

        :param video:
        :return:
        """
        User = s().M.model('User', self.ctx)
        u, r = yield [User.get_one(_id=video.owner_id, _deleted=False), self.save()]
        s().e('bookmarks:add', sender=self.ctx.request_user, notify_sender=True, users=[u], data=r.get_data())
        s().h('bookmarks:add', r, self.ctx.request_user.id, video.reporter_id)
        raise gen.Return(r)

    @gen.coroutine
    def delete(self, video):
        """

        :param video:
        :return:
        """
        User = s().M.model('User', self.ctx)
        record, r, u = yield [self.get_one(_id=self.id),
                              self.mapper.delete(self.id),
                              User.get_one(_id=video.owner_id, _deleted=False)]
        s().e('bookmarks:delete', sender=self.ctx.request_user, notify_sender=True, users=[u], data=self.get_data())
        H = s().M.model('H')
        yield H.delete(e_object_id=self.id)
        raise gen.Return(record)

    @classmethod
    @gen.coroutine
    def get_mine(cls, current_user_id, limit=10, offset=0):
        """
        Get mine Bookmarks.

        :param current_user_id:
        :param limit:
        :param offset:
        :return: list [Bookmark(), ...], bool has_more.
        """
        qs = cls.mapper.get(user_id=current_user_id)
        if cls.default_scope:
            qs = qs.filter(scope=cls.default_scope)
        count = yield qs.count()
        qs = qs.order_by('-modify_ts')
        results = yield qs[offset: limit+offset].execute()
        bookmarks = [cls(r) for r in results]
        has_more = count > (limit + offset)
        raise gen.Return((bookmarks, has_more))
