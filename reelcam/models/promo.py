# -*- coding: utf-8 -*-
from random import sample
from reelcam.models.base import BaseModel
from reelcam.mappers.promo import PromoEntryMappingType


WCF = "WCF"
WCH = "WCH"
WCQ = "WCQ"
WCS = "WCS"
WCD = "WCD"
WCM = "WCM"

CODE_TYPES = [WCF, WCH, WCQ, WCS, WCD, WCM]


def get_ratio(code_type):
    """

    :param code_type: str Code type.
    :return: float Code Ratio.
    """
    if code_type == WCF:
        ratio = 0.0
    elif code_type == WCH:
        ratio = 0.5
    elif code_type == WCQ:
        ratio = 0.75
    elif code_type == WCS:
        ratio = 0.8
    elif code_type == WCD:
        ratio = 0.9
    elif code_type == WCM:
        ratio = 1.0
    else:
        ratio = None
    return ratio


class Promo(BaseModel):
    """
    Promo model.

    """

    mapper = PromoEntryMappingType

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=None,
                    code=None,
                    code_type=None,
                    start_ts=None,
                    end_ts=None,
                    ratio=None)

    def get_data(self):
        """

        :return:
        """
        return dict(id=self.id,
                    can_activate=self.can_activate(),
                    code=self.code,
                    code_type=self.code_type,
                    create_ts=self.create_ts,
                    start_ts=self.start_ts,
                    end_ts=self.ratio)

    @classmethod
    def new(cls, code, code_type, expire, ratio):
        """

        :param code_type: str Code type.
        :param expire: int seconds.
        :param ratio: float Code Ratio.
        :return: Promo object.
        """
        end_ts = cls.YmdHMS_from_ts(cls.now_ts() + expire)
        new = cls(dict(code=code,
                       code_type=code_type,
                       start_ts=None,
                       end_ts=end_ts,
                       ratio=ratio,
                       scope=cls.default_scope))
        return new

    @staticmethod
    def get_code(code_type):
        """

        :param code_type:
        :return:
        """
        code = code_type + ''.join([str(i) for i in sample(list(range(0, 9)), 7)])
        return code

    def can_activate(self):
        """

        :return:
        """
        if self.start_ts:
            return False
        elif self.now_ts() > self.ts_from_YmdHMS(self.end_ts):
            return False
        else:
            return True
