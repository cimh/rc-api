# -*- coding: utf-8 -*-
import twitter
from .base import BaseModel
from api_engine import s, apiresults
from io import StringIO
from tornado.escape import json_decode
from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from tornado import gen
from urllib.parse import urlencode
from oauth2 import Request, SignatureMethod_HMAC_SHA1, Token, Consumer


class FacebookAPI(object):
    """
    Facebook API class.
    """

    API_URL = "https://graph.facebook.com/"

    def __init__(self, access_token=None):
        self.access_token = access_token

    @gen.coroutine
    def get_object(self, id, **args):
        """
        Fetch the given object from the graph.
        :param id:
        :param args:
        :return:
        """
        res = yield self.request(id, args)
        raise gen.Return(res)

    @gen.coroutine
    def request(self, path, args=None, post_args=None):
        """
        Fetches the given path in the Graph API.

        Translate args to a valid query string. If post_args is
        given, send a POST request to the given path with the given arguments.

        :param path:
        :param args:
        :param post_args:
        :return: response
        """
        args = args or {}
        if self.access_token:
            # Add access_token to 'post' params
            if post_args is not None:
                post_args["access_token"] = self.access_token
            # Otherwise add access_token to 'get' params
            else:
                args["access_token"] = self.access_token

        post_data = None if post_args is None else urlencode(post_args)

        req = HTTPRequest(
            self.API_URL + path + "?" + urlencode(args),
            method="POST" if post_data else "GET",
            body=post_data
        )

        resp = yield AsyncHTTPClient().fetch(req)
        content_type = resp.headers["Content-Type"]

        if 'json' in content_type:
            res = json_decode(resp.body)
        elif 'image' in content_type:
            res = resp.body
        else:
            res = None

        raise gen.Return(res)


class TwitterAPI(object):
    """
    Twitter API class.
    """

    API_URL = 'https://api.twitter.com/1.1'

    def __init__(self, consumer_key=None, consumer_secret=None, access_token_key=None, access_token_secret=None):
        """

        :param consumer_key:
        :param consumer_secret:
        :param access_token_key:
        :param access_token_secret:
        """
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.access_token_key = access_token_key
        self.access_token_secret = access_token_secret
        self.consumer = Consumer(key=self.consumer_key, secret=self.consumer_secret)
        self.token = Token(key=self.access_token_key, secret=self.access_token_secret)
        self.method = SignatureMethod_HMAC_SHA1()

    @gen.coroutine
    def request(self, url, method="GET", body='', headers=None):
        """
        Prepare signed request.
        Make async request to twitter api.

        :param url:
        :param method:
        :param body:
        :param headers:
        :return: response
        """
        if not isinstance(headers, dict):
            headers = {}

        req = Request.from_consumer_and_token(
            self.consumer, token=self.token, http_method=method, http_url=url, body=body)

        req.sign_request(self.method, self.consumer, self.token)
        url = req.to_url()
        req = HTTPRequest(
            url,
            method=method,
            body=body if body else None,
            headers=headers,
        )
        resp = yield AsyncHTTPClient().fetch(req)
        res = json_decode(resp.body)
        raise gen.Return(res)

    @gen.coroutine
    def get_user(self):
        """
        Returns a twitter.User instance if the authenticating user is valid.

        :return:    A twitter.User instance representing that user if the
                    credentials are valid, None otherwise.
        """

        url = '%s/account/verify_credentials.json' % self.API_URL
        resp = yield self.request(url)
        raise gen.Return(resp)


class Social(BaseModel):
    """
    Social model for communication with social networks.
    """

    @classmethod
    @gen.coroutine
    def get_social_credentials(cls, data, check_posting_permission=False):
        """
        Get uid, fullname from social by specified access_token.

        :param data:
        :param check_posting_permission:
        :return: social_credentials, err
        """
        if data["provider"] == "facebook":
            api = FacebookAPI(data["token"])
            profile = yield api.get_object("me")
            try:
                res = {
                    'fullname': profile['name'],
                    'uid': profile['id'],
                    'token': data["token"]
                }, None
            except:
                res = None, apiresults.forbidden()

            if check_posting_permission:
                permissions_path = "%s/permissions" % profile['id']
                perms_data = yield api.request(permissions_path)
                # scope=publish_actions
                perms = [i['permission'] for i in perms_data['data']]
                if 'publish_actions' not in perms:
                    res = None, apiresults.forbidden(error="no_publish_actions_permissions")
                    raise gen.Return(res)

            raise gen.Return(res)
        elif data["provider"] == "twitter":
            api = TwitterAPI(
                consumer_key=s().social_auth.twitter_consumer_key,
                consumer_secret=s().social_auth.twitter_consumer_secret,
                access_token_key=data["token"],
                access_token_secret=data['token_secret']
            )
            profile = yield api.get_user()
            try:
                res = dict(fullname=profile["screen_name"],
                           uid=profile["id"],
                           token=data["token"],
                           token_secret=data["token_secret"],
                           screen_name=profile["screen_name"]), None
            except:
                res = None, apiresults.forbidden()
            raise gen.Return(res)

        res = None, apiresults.forbidden()
        raise gen.Return(res)

    @classmethod
    @gen.coroutine
    def get_avatar_from_social(cls, data, provider):
        """
        Get avatar from social by user id.

        :param data:
        :param provider:
        :return:
        """
        if provider == "facebook":
            api = FacebookAPI(data["token"])
            picture_path = "%s/picture" % data['uid']
            photo = yield api.request(picture_path, dict(width=500, height=500))
            res = StringIO(photo)
            raise gen.Return(res)
        elif provider == "twitter":
            url = "https://twitter.com/%s/profile_image?size=original" % data['screen_name']
            req = HTTPRequest(url)
            resp = yield AsyncHTTPClient().fetch(req)
            res = StringIO(resp.body)
            raise gen.Return(res)

    @classmethod
    @gen.coroutine
    def post_video(cls, user, provider, video):
        """

        :param user:
        :param provider:
        :param video:
        :return:
        """
        if video.screenshots_uploaded():
            screen1 = list(video.screenshot_keys.keys())[0]
            screen_url = video.get_screenshot_urls()[screen1]
        else:
            raise gen.Return((None, "Video have no screenshots uploaded yet;"))

        msg = dict(title="I started a live video",
                   link="http://%s/#/video/%s" % (s().domain, video.id),
                   preview=screen_url)

        res = yield cls.social_post(user, provider, msg)
        raise gen.Return(res)

    @classmethod
    @gen.coroutine
    def social_post(cls, user, provider, msg):
        """
        Post to social network wall.

        :param user: obj Current user
        :param provider: str Social network name
        :param msg: obj Message to post
        :return: bool_success, error_msg
        """
        social = []
        if user.social:
            social = list(user.social.keys())
        if provider not in social:
            raise gen.Return((None, "invalid provider;"))

        if provider == "facebook":
            token = user.social[provider]["token"]
            attach = dict(name=msg['title'],
                          link=msg['link'],
                          picture=msg['preview'],
                          message="")
            api = FacebookAPI(token)
            yield api.request("me/feed", post_args=attach)
        elif provider == "twitter":
            token = user.social[provider]["token"]
            token_secret = user.social[provider]["token_secret"]
            try:
                api = twitter.Api(
                    consumer_key=s().social_auth.twitter_consumer_key,
                    consumer_secret=s().social_auth.twitter_consumer_secret,
                    access_token_key=token,
                    access_token_secret=token_secret
                )
                status = "%s %s" % (msg['title'], msg['link'])
                api.PostMedia(status, msg['preview'])
            except twitter.TwitterError as e:
                raise gen.Return((None, str(e.message)))

        raise gen.Return((True, None))