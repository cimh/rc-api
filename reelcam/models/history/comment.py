# -*- coding: utf-8 -*-
from ..base import BaseModel


class CommentHistory(BaseModel):
    """
    Comment History Model.
    """

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(event=str(),
                    event_user_id=str(),
                    comment_id=str(),
                    user_id=str(),
                    video_id=int(),
                    body=str(),
                    modify_ts=str(),
                    seen=bool(),
                    is_visible=True)

    def get_data(self):
        """

        :return:
        """
        return dict(history_id=self.id,
                    event=self.event,
                    event_user_id=self.event_user_id,
                    comment_id=self.comment_id,
                    user_id=self.user_id,
                    video_id=self.video_id,
                    body=self.body,
                    ts=self.ts_from_YmdHMS(self.modify_ts),
                    seen=self.seen)
