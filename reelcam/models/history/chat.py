# -*- coding: utf-8 -*-
from ..base import BaseModel


class ChatHistory(BaseModel):
    """
    Chat History Model.
    """

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(event=str(),
                    event_user_id=str(),
                    user_id=str(),
                    user_last_message_body=str(),
                    user_last_message_ts=str(),
                    modify_ts=str(),  # @TODO
                    seen=bool(),
                    is_visible=True)

    def get_data(self):
        """

        :return:
        """
        return dict(history_id=self.id,
                    event=self.event,
                    event_user_id=self.event_user_id,
                    user1_id=self.user1_id,
                    user2_id=self.user2_id,
                    user1_unread_count=self.user1_unread_count,
                    user2_unread_count=self.user2_unread_count,
                    user1_last_msg_ts=self.ts_from_YmdHMS(self.user1_last_msg_ts),
                    user2_last_msg_ts=self.ts_from_YmdHMS(self.user2_last_msg_ts),
                    user1_last_msg_body=self.user1_last_msg_body,
                    user2_last_msg_body=self.user2_last_msg_body,
                    ts=self.ts_from_YmdHMS(self.modify_ts))
