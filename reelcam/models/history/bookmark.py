# -*- coding: utf-8 -*-
from ..base import BaseModel
from tornado import gen


class BookmarkHistory(BaseModel):
    """
    Bookmark History Model.
    """
    DEFAULT_SEEN = False

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(event=str(),
                    event_user_id=str(),
                    user_id=str(),
                    video_id=str(),
                    modify_ts=str(),  # @TODO
                    seen=bool(),
                    is_visible=True)

    def get_data(self):
        """

        :return:
        """
        return dict(history_id=self.id,
                    event=self.event,
                    event_user_id=self.event_user_id,
                    user_id=self.user_id,
                    video_id=self.video_id,
                    ts=self.ts_from_YmdHMS(self.modify_ts),
                    seen=self.seen)

    @classmethod
    @gen.coroutine
    def add(cls, bookmark):
        """

        :param bookmark:
        :return:
        """
        data = dict(event=bookmark['event'],
                    user_id=bookmark['owner_id'],
                    video_id=bookmark['id'],
                    status=bookmark['status'],
                    modify_ts=cls.now_YmdHMS(),
                    seen=cls.DEFAULT_SEEN)

        result = yield cls.index(document=data)

        raise gen.Return(result)
