# -*- coding: utf-8 -*-
from ..base import BaseModel


class VideoHistory(BaseModel):
    """
    Video History Model.
    """

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(event=str(),
                    event_user_id=str(),
                    video_id=str(),
                    user_id=str(),
                    status=int(),
                    seen=bool(),
                    modify_ts=str(),  # @TODO
                    is_visible=True)

    def get_data(self):
        """

        :return:
        """
        return dict(history_id=self.id,
                    event=self.event,
                    event_user_id=self.event_user_id,
                    video_id=self.video_id,
                    user_id=self.user_id,
                    status=self.status,
                    seen=self.seen,
                    ts=self.ts_from_YmdHMS(self.modify_ts))
