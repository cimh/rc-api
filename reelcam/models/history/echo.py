# -*- coding: utf-8 -*-
from ..base import BaseModel


class EchoHistory(BaseModel):
    """
    Video Like and Comment History Model.
    """

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(event=str(),
                    event_user_id=str(),
                    video_id=str(),
                    user_id=str(),
                    positive=bool(),
                    seen=bool(),
                    modify_ts=str(),
                    is_visible=True)

    def get_data(self):
        """

        :return:
        """
        return dict(history_id=self.id,
                    event=self.event,
                    event_user_id=self.event_user_id,
                    video_id=self.video_id,
                    user_id=self.user_id,
                    positive=self.positive,
                    seen=self.seen,
                    ts=self.ts_from_YmdHMS(self.modify_ts))
