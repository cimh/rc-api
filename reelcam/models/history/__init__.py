# -*- coding: utf-8 -*-
from ..base import BaseModel
from .chat import ChatHistory
from .comment import CommentHistory
from .observe import ObserveHistory
from ...mappers.history.bookmark import BookmarkHistoryEntryMappingType
from ...mappers.history.chat import ChatHistoryEntryMappingType
from ...mappers.history.comment import CommentHistoryEntryMappingType
from ...mappers.history.follow import FollowHistoryEntryMappingType
from ...mappers.history.video import VideoHistoryEntryMappingType
from ...mappers.history.vlike import VideoLikeHistoryEntryMappingType
from ...mappers.history.observe import ObserveHistoryEntryMappingType
from ...mappers.history.base import BaseHistoryMapper
from tornado import gen
from api_engine import s


class History(BaseModel):
    """
    History Model.
    """

    mapper = None

    types = {
        'bookmark': BookmarkHistoryEntryMappingType,
        'chat': ChatHistoryEntryMappingType,
        'comment': CommentHistoryEntryMappingType,
        'follow': FollowHistoryEntryMappingType,
        'vlike': VideoLikeHistoryEntryMappingType,
        'video': VideoHistoryEntryMappingType,
        'observe': ObserveHistoryEntryMappingType,
        'echo': None
    }

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return cls.mapper._mapped_fields()

    @classmethod
    def add(cls, type, data):
        """

        :param type:
        :param data:
        :return:
        """
        cls.mapper = cls.set_type(type)
        if cls.mapper is None:
            return
        data['is_visible'] = True
        return cls.mapper.add(data)

    @classmethod
    def get_event_data(cls, type, data):
        """

        :param type:
        :param data:
        :return:
        """
        cls.mapper = cls.set_type(type)
        if cls.mapper is None:
            return data
        return cls.mapper.get_event_data(data)

    @classmethod
    @gen.coroutine
    def read(cls, type, _id):
        """

        :param type:
        :param _id:
        :return:
        """
        cls.mapper = cls.set_type(type)
        if cls.mapper is None:
            return
        yield cls.mapper.read(_id)

    @classmethod
    @gen.coroutine
    def read_all(cls, type):
        """

        :param type:
        :return:
        """
        cls.mapper = cls.set_type(type)
        if cls.mapper is None:
            return
        yield cls.mapper.read_all(cls.ctx.request_user.id)

    @classmethod
    def set_type(cls, type):
        """

        :param type:
        :return:
        """
        return cls.types.get(type, None)

    @classmethod
    @gen.coroutine
    def get_videos(cls, user_id, offset=0, limit=10):
        """

        :param user_id:
        :param limit:
        :param offset:
        :return:
        """
        from .video import VideoHistory
        cls.mapper = cls.set_type('video')
        query = cls.mapper.get(user_id=user_id).order_by('-modify_ts')
        raw = yield query[offset:offset+limit].execute()
        raise gen.Return([VideoHistory(r) for r in raw])

    @classmethod
    @gen.coroutine
    def get_likes(cls, user_id, offset=0, limit=10):
        """

        :param user_id:
        :param limit:
        :param offset:
        :return:
        """
        from .vlike import VideoLikeHistory
        cls.mapper = cls.set_type('vlike')
        query = cls.mapper.get(user_id=user_id).order_by('-modify_ts')
        raw = yield query[offset:offset+limit].execute()
        raise gen.Return([VideoLikeHistory(r) for r in raw])

    @classmethod
    @gen.coroutine
    def get_echo(cls, user_id, offset=0, limit=10):
        """
        Get likes and comments history

        :param user_id:
        :param limit:
        :param offset:
        :return:
        """
        from .echo import EchoHistory
        query = BaseHistoryMapper.get_echo(user_id)
        raw = yield query[offset:offset+limit].execute()
        raise gen.Return([EchoHistory(r) for r in raw])

    @classmethod
    @gen.coroutine
    def get_follow(cls, user_id, offset=0, limit=10):
        """

        :param user_id:
        :param limit:
        :param offset:
        :return:
        """
        from .follow import FollowHistory
        cls.mapper = cls.set_type('follow')
        query = cls.mapper.get(user_id=user_id).order_by('-modify_ts')
        raw = yield query[offset:offset+limit].execute()
        raise gen.Return([FollowHistory(r) for r in raw])

    @classmethod
    @gen.coroutine
    def get_bookmarks(cls, user_id, offset=0, limit=10):
        """

        :param user_id:
        :param limit:
        :param offset:
        :return:
        """
        from .bookmark import BookmarkHistory
        cls.mapper = cls.set_type('bookmark')
        query = cls.mapper.get(user_id=user_id).order_by('-modify_ts')
        raw = yield query[offset:offset+limit].execute()
        raise gen.Return([BookmarkHistory(r) for r in raw])

    @classmethod
    @gen.coroutine
    def get_count(cls, type, user_id):
        """
        Returns count of unread history entries.

        :param type:
        :param user_id:
        :return:
        """
        cls.mapper = cls.set_type(type)
        if cls.mapper is None:
            return
        c = yield cls.mapper._get_count(user_id)
        raise gen.Return(c)

    @classmethod
    @gen.coroutine
    def counters(cls, current_user_id):
        """

        :param current_user_id:
        :return:
        """
        counters = dict(
            bookmarks=dict(count=0, type='bookmark'),
            followers=dict(count=0, type='follow'),
            likes=dict(count=0, type='vlike'),
            videos=dict(count=0, type='video'),
            comments=dict(count=0, type='comment'),
        )
        for key in counters:
            counters[key] = yield cls.get_count(counters[key]['type'], current_user_id)
        counters['echo'] = counters['likes'] + counters['comments']
        H = s().M.model('H')
        qs = H.mapper.get(user_id=current_user_id, seen=False, scope=s().get_scope("wedcam"))
        counters['wedcam'] = yield qs.count()
        raise gen.Return(counters)

    @classmethod
    @gen.coroutine
    def hide_by_video_id(cls, video_id):
        """
        Set visible flag to False for each record matching video_id into all video associated history types

        :param video_id:
        :return:
        """
        for t in cls.get_video_associated_types():
            yield t.hide_by_video_id(video_id)
        return

    @classmethod
    @gen.coroutine
    def unhide_by_video_id(cls, video_id):
        """
        Set visible flag to True for each record matching video_id into all video associated history types

        :param video_id:
        :return:
        """
        for t in cls.get_video_associated_types():
            yield t.unhide_by_video_id(video_id)
        return

    @classmethod
    def get_video_associated_types(cls):
        """
        Returns list of history types that has video_id field

        :return:
        """
        return filter(lambda t: t.is_video_associated() if t else False, cls.types.values())
