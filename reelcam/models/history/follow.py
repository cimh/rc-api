# -*- coding: utf-8 -*-
from ..base import BaseModel
from ...mappers.history.follow import FollowHistoryEntryMappingType


class FollowHistory(BaseModel):
    """
    Follow History Model.
    """

    mapper = FollowHistoryEntryMappingType

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(event=str(),
                    event_user_id=str(),
                    _from_id=str(),
                    user_id=str(),
                    status=int(),
                    modify_ts=str(),  # @TODO
                    message=str(),
                    seen=bool(),
                    follow_request_id=str(),
                    is_visible=True)

    def get_data(self):
        """

        :return:
        """
        return dict(history_id=self.id,
                    event=self.event,
                    event_user_id=self.event_user_id,
                    _from_id=self._from_id,
                    user_id=self.user_id,
                    status=self.status,
                    message=self.message,
                    seen=self.seen,
                    follow_request_id=self.follow_request_id,
                    ts=self.ts_from_YmdHMS(self.modify_ts))
