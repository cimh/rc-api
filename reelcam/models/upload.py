# -*- coding: utf-8 -*-
import os
import logging
from uuid import uuid4

from tornado import gen

from api_engine import s
from api_engine.mediahelper import MediaHelper
from reelcam.mappers.upload import UploadEntryMappingType
from reelcam.models.base import BaseModel
from reelcam.scheduler import execute_task
from reelcam.uploader import get_uploader

# Modes.
PHOTO = "photo"
LIVE = "live"
RECORD = "record"

# Targets.
R = "r"  # Live video.
RI = "ri"  # Recorded video.
O = "o"  # Live audio.

log = logging.getLogger(__name__)


class Upload(BaseModel):
    """An upload model class."""

    mapper = UploadEntryMappingType

    targets = (R, RI, O)

    # Stream alive timeout in seconds.
    alive_timeout = 60

    # Types of media allowed to direct upload into mediastorage.
    direct_targets = (R, RI, O)

    @classmethod
    def _mapped_fields(cls):
        """

        :param cls:
        :return:
        """
        return dict(id=None,
                    video_id=None,
                    target=None,
                    part_num=int(),
                    mpart_id=None,
                    size=int(),
                    key=None,
                    is_complete=False,
                    modify_ts=None,
                    task_complete=False,
                    is_reanimated=False)

    def next_part(self, size):
        """

        :param size:
        :return:
        """
        self.part_num += 1
        self.size += size

    @gen.coroutine
    def complete(self, mpart):
        """

        :param mpart:
        :return:
        """
        if self.is_complete:
            return
        get_uploader().set_acl(self.key, 'public-read')

        self.is_complete = True
        yield self.save()
        execute_task('upload_complete', self.id)

    @classmethod
    @gen.coroutine
    def complete_task(cls, upload_id):
        """

        :param upload_id:
        :return:
        """
        upload = yield Upload.get_one(_id=upload_id)
        if not upload:
            return
        # if not upload.is_complete or upload.task_complete:
        #     return
        path = os.path.join(s().upload_path, upload.target, upload.key)
        upload.download(path)
        yield upload.after_download(path)

    @gen.coroutine
    def after_download(self, path):
        """

        :return:
        """
        Video = s().M.model('Video', ctx=self.ctx)
        video = yield Video.get_one(_id=self.video_id)
        if self.is_reanimated:
            # If there is an r.flv or ri.flv already in the DB then the users has continued the video.
            # We just concatenate the two parts together.
            target_key = video.mediastorage_key(self.target)
            target_path = os.path.join(s().upload_path, str(target_key))

            if not os.path.exists(os.path.dirname(target_path)):
                os.makedirs(os.path.dirname(target_path))
            fp = open(target_path, 'wb')
            r = get_uploader().download(target_key, fp)
            if r:
                fp.close()
                if os.path.exists(os.path.dirname(target_path)):
                    with open(path, 'rb') as f1:
                        header = f1.read(len(MediaHelper.FLV))
                        if header != MediaHelper.FLV:
                            f1.seek(0)
                        # f1.seek(13)  - no need to seek for 13 bytes two omit FLV header.
                        # The kernel sends it without the header.
                        new_data = f1.read()
                    with open(target_path, 'ab') as f2:
                        f2.write(new_data)
                    os.remove(path)
                    os.rename(target_path, path)
            else:
                fp.close()
                try:
                    os.remove(fp.name)
                except OSError:
                    log.debug('after_download(): file missing, path=%s', path)
        meta = self.set_meta(path, self.target, video.duration)
        yield self.after_set_meta(path, meta)

    @gen.coroutine
    def after_set_meta(self, path, meta):
        """

        :param path:
        :return:
        """

        if not meta:
            return

        Video = s().M.model('Video', ctx=self.ctx)
        video = yield Video.get_one(_id=self.video_id)

        meta = Video.metadata_class(**meta)

        if self.target == 'ri' or self.target == 'r' and not video.meta.duration:
            video = yield video.set_meta(meta)

        try:
            fp = open(path, 'rb')
        except (OSError, IOError):
            log.debug('after_set_meta(): cannot open file, path=%s', path)

        key = video.mediastorage_key(self.target)
        get_uploader().upload(key, fp)
        fp.close()
        video = yield video.set_media_urls({self.target: str(key)})
        get_uploader().set_acl(key, 'public-read')
        keys = self.make_previews(video.screenshot_keys, path, self.target, video.mediastorage_key)
        if keys:
            yield video.set_screenshot_keys(keys)
        try:
            os.remove(path)
            os.removedirs(os.path.dirname(path))
        except (OSError, IOError):
            log.debug('after_set_meta(): file missing, path=%s', path)
        self.task_complete = True
        yield self.save()

    def download(self, path):
        """

        :return:
        """
        try:
            if not os.path.exists(os.path.dirname(path)):
                os.makedirs(os.path.dirname(path))
            fp = open(path, 'wb')
        except (OSError, IOError):
            log.debug('download(): file missing, path=%s', path)

        get_uploader().download(self.key, fp)
        fp.close()

    @classmethod
    def set_meta(cls, path, target, seconds, cb=None):
        """

        :param path:
        :param target:
        :param seconds:
        :param cb:
        :return:
        """
        from api_engine.mediahelper import MediaHelper
        meta = MediaHelper.set_meta(path, (seconds or 1), target)
        if cb:
            cb(meta)
        return meta

    @classmethod
    def make_previews(cls, screenshot_keys, path, target, key_builder):
        """

        :return:
        """
        if target == 'o' or (target == 'r' and screenshot_keys and len(screenshot_keys) > 1):
            return None
        previews = MediaHelper.make_previews(path, count=3)
        if not previews:
            return None
        keys = {}
        for name, _path in list(previews.items()):
            key = key_builder(uuid4().hex[:16], ext='jpg')
            keys[name] = key
            try:
                fp = open(_path, 'rb')
            except (OSError, IOError):
                log.debug('make_previews(): cannot open file, path=%s', path)
            get_uploader().upload(key, fp)
            fp.close()
            get_uploader().set_acl(key, 'public-read')
            try:
                os.remove(path)
            except (OSError, IOError):
                log.debug('make_previews(): file missing, path=%s', path)
        return keys

    @classmethod
    def get_url(cls, video, target, remote_ip=None):
        """
        Get an url to media file for `video` by `target`.
        If `remote_ip` passed try discover nearest mediastorage host via geoip.

        :param: video: Video
        :param: target: str media target('r' | 'ri' | 'o')
        :param: remote_ip: str remote client ip
        :return: str
        """
        uploader = get_uploader()
        host = uploader.get_nearest_host(remote_ip) if remote_ip else None
        key = video.mediastorage_key(target)
        url = uploader.get_url(key, host)
        return url

    @classmethod
    def check_token(cls, video, token):
        """

        :param video:
        :param token:
        :return:
        """
        return video.check_reporter_token(token) or video.observe_check_token(token)

    @classmethod
    def get_mediastorage(cls):
        """
        Get current mediastorage.

        :return: MediaStorage
        """
        return get_uploader().get_mediastorage()

    @classmethod
    def get_mediastorage_info(cls, remote_ip):
        """
        Get info about current mediastorage.

        :param: remote_ip: str remote client ip
        :return: dict
        """
        mediastorage = cls.get_mediastorage()
        info = mediastorage.get_info(remote_ip)
        info.update(alive_timeout=cls.alive_timeout)
        return info

    @classmethod
    @gen.coroutine
    def direct(cls, remote_ip, mediastorage, video, token, target, key, mpart_id, part_num, size):
        """
        Finish the direct upload into mediastorage.

        :param: remote_ip: str remote client ip
        :param: mediastorage: dict that returned by ``MediaStorage.get_info``.
        :param: video: Video object
        :param: token: str video access token
        :param: target: str media target('r' | 'ri' | 'o')
        :param: key: str mediastorage key
        :param: mpart_id: str ID of S3 multipart upload
        :param: part_num: int number of uploaded parts
        :param: size: int size of uploaded file
        :return:
        """
        # if mediastorage != cls.get_mediastorage().get_info(remote_ip):
        #     return None

        check = False
        if target in cls.direct_targets \
                and cls.check_token(video, token):
            check = not check

        # if not check \
        #         or key != cls.get_original_key(video, target):
        #     return

        if video.upload_ids[target]:
            upload = cls.get_one(_id=video.upload_ids[target])
            upload.is_complete = upload.task_complete = False
        else:
            upload = cls(video_id=video.id, target=target)
            upload = yield upload.save()

        upload.size += size
        upload.part_num += part_num
        upload.mpart_id = mpart_id
        upload.key = str(key)
        upload = yield upload.save()

        video.upload_ids[upload.target] = upload.id
        video.upload_ids = video.upload_ids.copy()

        mpart = get_uploader().resume_multipart(key, mpart_id)
        yield upload.complete(mpart)

        raise gen.Return(upload)
