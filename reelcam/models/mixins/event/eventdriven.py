# -*- coding: utf-8 -*-
from api_engine.lib import eventdriven


class EventDriven(eventdriven.EventDriven):
    """
    EventDriven class.
    """

    def emit_event(self, event):
        """

        :param event:
        :return:
        """
        if self.can_subscribed:
            self._event_emitted(self, event)

    def notify(self, emitter, event):
        """

        :param emitter:
        :param event:
        :return:
        """
        pass

    def get_event_type(self):
        """

        :return:
        """
        from utils import kvs
        return kvs.HS.join([self.__class__.__name__, self.id])

    @classmethod
    def _event_emitted(cls, instance, event):
        """
        Notifies all models(direct subclasses of BaseModel) that ``event`` emitted by ``instance``.

        :param instance:
        :param event:
        :return:
        """
        for m in cls.get_models():
            if m.can_subscribe:
                m.event_emitted(instance, event)
