# -*- coding: utf-8 -*-
from .eventdriven import EventDriven
from .subscriber import Subscriber


class EventMixin(Subscriber, EventDriven):
    """

    """
    pass
