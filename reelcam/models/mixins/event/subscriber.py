# -*- coding: utf-8 -*-
from collections import defaultdict
from api_engine.lib import subscriber as subs
from utils.datetimemixin import dtm
from api_engine import s
from tornado import gen
from utils import kvs


class Subscriber(subs.Subscriber):
    """
    structure
    {
        'User': {
            video@video_id1: set(user_id1, user_id2, ...),
            video@video_id2: set(user_id3, user_id2, ...),
            ...
            user@user_id1: set(user_id5, user_id4, ...),
            user@user_id2: set(user_id1, user_id4, ...),
            ...
        },
        'ws_connection': {
            user@user_id1: set(ws_id1, ws_id2, ...),
            user@user_id2: set(ws_id3, ws_id4, ...),
            ...
            video@video_id1: set(ws_id1, ws_id5, ...),
            video@video_id2: set(ws_id3, ws_id6, ...),
            ...
        }
    }

    Structure in Redis:
    ClassName:Event = set(instances)
    where
    ClassName - (ex 'User')
    Event - ClassName@id (ex 'Video@AU2WNtnj7q__UoPvUBEG')
    instance - ClassName@id (ex 'User@AU2UvGHO7q__UoPvTwx9')
    """

    _subscribes = defaultdict(lambda: defaultdict(set))

    KS = kvs.KS

    def subscribe(self, instance):
        """

        :param instance:
        :return:
        """
        # add instance_id in redis
        # event - instances
        s().kvs.async_call("SADD", self.get_subscriber_key(instance), self.value())
        # instance - events
        s().kvs.async_call("SADD", self.value(), self.get_subscriber_key(instance))

        # add to expiration date set
        s().kvs.async_call("SADD", self.expire_key(dtm.now_Ymd()), self.value())
        s().kvs.async_call("EXPIRE", self.get_subscriber_key(instance), s().ttl.redis_events)
        s().kvs.async_call("EXPIRE", self.value(), s().ttl.redis_events)

    def unsubscribe(self, instance):
        """

        :param instance:
        :return:
        """
        # remove in redis
        # instance id from event
        s().kvs.async_call("SREM", self.get_subscriber_key(instance), self.value())
        # event id from instance
        s().kvs.async_call("SREM", self.value(), self.get_subscriber_key(instance))

        # remove from expiration date set
        s().kvs.async_call("SREM", self.expire_key(dtm.now_Ymd()), self.value())

    @gen.coroutine
    def get_subscribers(self):
        """
        Get Subscribers.

        :return:
        """
        User = s().M.model('User')
        _ids = yield s().kvs.call("SMEMBERS", User.get_subscriber_key(self))
        ids = [i.decode("utf-8").split(":")[-1] for i in _ids]
        users = []
        if ids:
            users = yield User.get(_ids=ids)
        raise gen.Return(users)

    def is_subscribed(self, instance):
        """

        :param instance:
        :return:
        """
        pass

    def clear_subscribes(self):
        """

        :return:
        """
        pass

    @classmethod
    def get_cache_prefix(cls):
        """

        :return:
        """
        return cls.KS.join([s().get_scope(), 'SUBS'])

    @classmethod
    def get_subscriber_key(cls, instance):
        """

        :param instance:
        :return:
        """
        return cls.KS.join([cls.get_cache_prefix(), cls.__name__, instance.get_event_type()])

    def value(self):
        """

        :return:
        """
        return self.KS.join([self.get_cache_prefix(), self.__class__.__name__, self.id])

    def expire_key(self, date):
        """

        :return:
        """
        return self.KS.join([self.get_cache_prefix(), "EXPIRES", date])
