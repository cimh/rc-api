# -*- coding: utf-8 -*-
from tornado import gen
from api_engine import s
from api_engine.auth.sessionmanager import sm, CONN_IDS
from utils import kvs


SESSIONS = 'SESSIONS'


class WsClient(object):
    """
    WS client class.

    """

    client_id = None

    @gen.coroutine
    def get_sessions(self):
        """

        :return:
        """
        ids = yield s().kvs.call("SMEMBERS", self.get_key(SESSIONS))
        raise gen.Return(ids)

    def add_session(self, session):
        """

        :param session: str Session ID.
        """
        session = str(session)
        pp = s().kvspp
        pp.stack_call("SADD", self.get_key(SESSIONS), session)
        pp.stack_call("EXPIRE", self.get_key(SESSIONS), sm.get_ttl())
        s().kvs.async_call(pp)

    def remove_session(self, session):
        """

        :param session: str Session ID.
        """
        session = str(session)
        s().kvs.async_call("SREM", self.get_key(SESSIONS), session)

    def get_key(self, suffix=''):
        """

        :param suffix:
        :return:
        """
        return kvs.get_key('WSCLIENT', self.client_id, suffix, s().get_scope())

    @gen.coroutine
    def try_2_connect(self):
        """

        :return:
        """
        sessions = set()
        connections = set()

        ids = yield self.get_sessions()
        if ids:
            sessions.update([id.decode('utf-8') for id in ids])
        del ids
        sessions.discard(self.ctx.session)

        ids = yield s().kvs.call("SMEMBERS", sm.get_key(self.ctx.session, CONN_IDS))
        if ids:
            connections.update([id.decode('utf-8') for id in ids])
        del ids
        connections.discard(self.ctx.connection)

        raise gen.Return((sessions, connections))
