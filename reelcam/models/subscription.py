# -*- coding: utf-8 -*-
from .base import BaseModel
from ..mappers.subscription import SubscriptionEntryMappingType
from httplib2 import Http
from oauth2client.client import SignedJwtAssertionCredentials
from apiclient.discovery import build
from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from tornado.escape import json_decode, json_encode
from api_engine import s
from tornado import gen
import operator


class Subscription(BaseModel):
    """
    Subscription Model.
    """

    mapper = SubscriptionEntryMappingType
    DEFAULT_NAME = "Basic"
    DEFAULT_LIMIT = 45*60

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=str(),
                    name=str(),
                    limit=int())

    @classmethod
    @gen.coroutine
    def get_info(cls, _id):
        """
        get subscription name and limit

        :param _id: subscription id
        :return (list): name, limit of subscription or defaults
        """
        res = yield cls.get_one(_id=_id)
        if res:
            raise gen.Return((res.name, res.limit))
        else:
            raise gen.Return((cls.DEFAULT_NAME, cls.DEFAULT_LIMIT))

    @classmethod
    def in_app_purchase_android(cls, subscription_id, token):
        """
        Verify android purchase and get purchase details.

        :param subscription_id: str The purchased subscription ID (for example, 'monthly001').
        :param token: str Token provided to the user's device when the subscription was purchased.
        :return: subscription if purchase is valid or False
        """

        with open(s().in_app_purchase.google.private_key_file_path) as f:
            private_key = f.read()

        credentials = SignedJwtAssertionCredentials(
            s().in_app_purchase.google.client_email,
            private_key,
            'https://www.googleapis.com/auth/androidpublisher'
        )
        http = Http()
        credentials.authorize(http)
        try:
            service = build('androidpublisher', 'v2', http=http)
            # Verify transaction
            subscription = service.purchases().subscriptions().get(
                packageName=s().in_app_purchase.google.package_name,
                subscriptionId=subscription_id,
                token=token).execute()
            subscription["expire_ts"] = int(float(subscription["expiryTimeMillis"])/1000)
            return subscription
        except:  # @TODO: too broad exception clause
            return False

    @classmethod
    def in_app_purchase_ios(cls, receipt):
        """
        Verify iOS purchase and get purchase details.

        :param receipt: str Token for purchase verification
        :return:
        """
        data = {
            "password": s().in_app_purchase.itunes.password,
            "receipt-data": receipt
        }

        payload = json_encode(data)
        req = HTTPRequest(s().in_app_purchase.itunes.url,
                          method="POST",
                          body=payload)

        def callback(resp):
            """

            :param resp:
            :return:
            """
            res = json_decode(resp.body)
            # Get all currently-active subscriptions
            all_subscriptions = res["latest_receipt_info"]
            now_ts_ms = cls.now_ts()*1000
            all_unexpired_subscriptions = [el for el in all_subscriptions if now_ts_ms < int(el['expires_date_ms'])]

            if all_unexpired_subscriptions:
                # Get subscription with max expired time.
                last_expired_subs = max(all_unexpired_subscriptions, key=operator.itemgetter('expires_date_ms'))
                # Proceed transaction.
                subscription_name = last_expired_subs["product_id"].split(".")[-1]
                cls.ctx.request_user.proceed_purchase(
                    subscription_name=subscription_name,
                    subscription_expire_ts=int(last_expired_subs['expires_date_ms'])/1000,
                    method='itunes',
                )

        AsyncHTTPClient().fetch(req, callback)
