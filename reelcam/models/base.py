# -*- coding: utf-8 -*-
import datetime
from tornado import gen
from api_engine import s
from api_engine.validator import CustomValidator
from reelcam.utils import default_location, distort_location
from utils.datetimemixin import DatetimeMixin


class BaseModel(DatetimeMixin):
    """
    Base Model.
    """

    validator = CustomValidator()

    mapper = None

    ctx = None

    default_scope = None

    can_subscribe = False
    can_subscribed = False

    @classmethod
    def new(cls, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        return cls(kwargs)

    @classmethod
    def _validation_schema(cls):
        """

        :return:
        """
        raise NotImplemented

    def validate(self, *args):
        """

        :param args:
        :return:
        """
        if not args:
            document = {key: self.__dict__[key] for key in self._validation_schema()}
            return self.validator.validate(document, self._validation_schema())
        for field in args:
            if field not in self._validation_schema():
                return False
        document = {key: self.__dict__[key] for key in args}
        return self.validator.validate(document, self._validation_schema())

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=None,
                    __time_created=cls.now_YmdHMS(),
                    scope=str(),  # @TODO: default scope
                    modify_ts=cls.now_YmdHMS(),
                    create_ts=None,
                    delete_ts=None,
                    is_deleted=False)

    def get_raw_data(self):
        """
        Make up a dict of of modified fields of the model object.
        :return: a dict containing the fields which were modified after getting the document from the storage.
        For a newly created document the method returns all the fields.
        """
        raw_data = dict()
        mapped_fields = self._mapped_fields()
        all_fields = BaseModel._mapped_fields()
        all_fields.update(mapped_fields)

        # We check for 'id' to distinguish between newly created documents and the ones which we get from ES.
        modified_only = self.__dict__.get('id', None)

        for key in all_fields:

            # To use .id in client code and _id in ES mappings
            if key == 'id':
                raw_data['_id'] = self.__dict__[key]
                continue

            if not modified_only or self.__original_data.get(key, None) is not self.__dict__[key]:
                field_mapping = self.mapper.get_mapping()['mappings'][self.mapper.get_mapping_type_name()][
                    'properties'].get(key, None)
                if field_mapping == {'type': 'date', 'format': 'yyyy-MM-dd'}:
                    raw_data[key] = self.__dict__[key].date() \
                        if isinstance(self.__dict__[key], datetime.datetime) \
                        else self.__dict__[key]
                    continue
                raw_data[key] = self.__dict__[key]

        # Add _custom_id to the dict to save document in es with that id
        if getattr(self, 'custom_id', None):
            raw_data['_custom_id'] = self.custom_id

        return raw_data

    def get_idts(self):
        """

        :return:
        """
        return dict(
            id=self.id,
            ts=self.ts_from_YmdHMS(self.modify_ts)
        )

    @classmethod
    @gen.coroutine
    def get(cls, offset=0, limit=10, _sorts=None, ts=0, _deleted=None, _ids=None, **kwargs):
        """
        Returns documents from ES. It uses GET API if only IDs and an optional _deleted parameter are specified.
        Otherwise it uses SEARCH API.
        IMPORTANT: GET API is realtime, but SEARCH API is not! Documents can be retrieved via SEARCH API only
        after refresh.

        :param offset:
        :param limit:
        :param _sorts:
        :param ts:
        :param _deleted:
        :param _ids:
        :param kwargs:
        :return:
        """

        if (_ids and isinstance(_ids, list) and not kwargs
                or not _ids and '_id' in kwargs and len(kwargs) == 1) and not _sorts and ts == 0:

            ids = _ids or [kwargs['_id']]
            ids = list(set(ids))
            ids = [_f for _f in ids if _f]
            if not ids:
                raise gen.Return([])
            result = yield cls._mget(ids)
            result = list(filter(None if _deleted is None else lambda x: x and _deleted == getattr(x, 'is_deleted', False), result))
            raise gen.Return(result)
        qs = cls.mapper.get(_sorts=_sorts, ts=ts, _deleted=_deleted, _ids=_ids, **kwargs)

        result = yield qs[offset:limit+offset].execute()
        raise gen.Return([cls(r) for r in result])

    @classmethod
    @gen.coroutine
    def _mget(cls, ids=None):
        """
        Base multi-get method to make database query which returns multiple rows.

        :param ids:
        :return:
        """
        result = []
        if ids and isinstance(ids, (list, tuple)):
            result = yield cls.mapper._mget(ids=ids)

        raise gen.Return([cls(r) if r else None for r in result])

    @classmethod
    @gen.coroutine
    def get_one(cls, **kwargs):
        """

        :param kwargs:
        :return:
        """
        limit = 1
        r = yield cls.get(limit=limit, **kwargs)
        try:
            raise gen.Return(r[0])
        except IndexError:
            return

    @classmethod
    @gen.coroutine
    def check_exists(cls, **kwargs):
        """
        Check if document exist in es

        :param kwargs:
        :return:
        """
        r = yield cls.get_one(**kwargs)
        raise gen.Return(bool(r))

    @classmethod
    @gen.coroutine
    def count(cls, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        r = yield cls.mapper.get(*args, **kwargs).count()
        raise gen.Return(r)

    @gen.coroutine
    def update(self, data):
        """

        :param data:
        :return:
        """
        self.__dict__.update(data)
        r = yield self.save()
        raise gen.Return(r)

    def __init__(self, args=None, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        data = kwargs if kwargs else args

        mapped_fields = self._mapped_fields()
        all_fields = BaseModel._mapped_fields()
        all_fields.update(mapped_fields)

        # We keep original values in order to be able to find out which fields to update on saving to ES.
        # Copy is required because data may contain compound objects like lists and dicts.
        # We must make copies of them as well.
        self.__original_data = dict()

        if getattr(data, 'es_meta', None):
            setattr(self, 'id', data.es_meta.id)
            self.__original_data['id'] = data.es_meta.id

        for field in data:
            if field in all_fields and field != '_id':
                setattr(self, field, data[field])
                self.__patch_original_data(field, data[field])
            # else:
            #     raise ValueError("Unmapped field '%s' in %s()" % (field, self.__class__.__name__))

        for field in all_fields:
            if not getattr(self, field, None):
                value = data[field] if field in data else all_fields[field]
                setattr(self, field, value)
                self.__original_data[field] = value

        try:
            scope = data['scope']
        except KeyError:
            scope = None
        self.set_scope(scope)

    def __patch_original_data(self, key, value):
        """

        :param key:
        :param value:
        :return:
        """
        if isinstance(value, dict):
            self.__original_data[key] = value.copy()
        elif isinstance(value, list):
            self.__original_data[key] = list(value)
        else:
            self.__original_data[key] = value

    # def __eq__(self, other):
    #     """
    #
    #     :param other:
    #     :return:
    #     """
    #     if type(other) != type(self):
    #         return False
    #
    #     return self.__dict__ == other.__dict__

    @gen.coroutine
    def save(self):
        """

        :return:
        """
        self.modify_ts = self.now_YmdHMS()
        if not self.id:
            self.modify_ts = None
            self.create_ts = self.now_YmdHMS()
        raw_data = self.get_raw_data()
        _id = yield self.mapper.save(raw_data)
        r = yield self.get_one(_id=_id)
        raise gen.Return(r)

    @classmethod
    @gen.coroutine
    def delete(cls, _id):
        """

        :param _id:
        :return:
        """
        record = yield cls.get_one(_id=_id)
        if record:
            yield cls.mapper.delete(_id)
        raise gen.Return(record)

    @classmethod
    @gen.coroutine
    def count_date(cls, date_start, date_end):
        """

        :param date_start:
        :param date_end:
        :return:
        """
        count = yield cls.mapper.count_date(date_start, date_end)
        raise gen.Return(count)

    def set_scope(self, scope=None):
        """

        :param scope:
        :return:
        """
        if scope is None:
            scope = self.default_scope or s().elastic.default_scope
        self.scope = scope

    def __repr__(self):
        """

        :return:
        """
        return '%s( id="%s" )' % (self.__class__.__name__, getattr(self, 'id', None))

    def __del__(self):
        """

        :return:
        """
        self.ctx = None


class BaseGPS:
    """
    Base GPS model.
    """

    def get_gps_location(self):
        """

        :return:
        """
        gps_location = dict(
            lat=self.gps_location.get('lat', default_location()['lat']),
            lon=self.gps_location.get('lon', default_location()['lon'])
        )

        if self.gps_incognito:
            gps_location.update(distort_location(gps_location, s().gps.incognito_max_radius))

        return gps_location


class BaseFeedback(object):
    """
    Base like-dislike-ban model.
    Includes counters for each of feedback type.
    """

    def incr_likes(self):
        """

        :return:
        """
        self.count_likes += 1

    def incr_dislikes(self):
        """

        :return:
        """
        self.count_dislikes += 1

    def incr_bans(self):
        """

        :return:
        """
        self.count_bans += 1

    def decr_likes(self):
        """

        :return:
        """
        self.count_likes -= 1

    def decr_dislikes(self):
        """

        :return:
        """
        self.count_dislikes -= 1

    def decr_bans(self):
        """

        :return:
        """
        self.count_bans -= 1


class BaseCustomID:
    """
    Inherit this to get complex ES IDs.
    """

    SEPARATOR = "_"
    custom_id_attrs = list()

    @property
    def custom_id(self):
        """

        :return:
        """
        if not getattr(self, '_custom_id', ''):
            self._custom_id = self.build_custom_id()
            self.id = self._custom_id
        return self.id

    def build_custom_id(self):
        """
        Build custom ID

        :return:
        """
        return self.SEPARATOR.join(str(getattr(self, f)) for f in self.custom_id_attrs)
