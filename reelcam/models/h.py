# -*- coding: utf-8 -*- 
from reelcam.models.base import BaseModel
from ..mappers.h import HEntryMappingType
from tornado import gen


class H(BaseModel):
    """
    History class.
    """

    mapper = HEntryMappingType

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(method=str(),
                    e_user_id=str(),
                    data=dict(),
                    modify_ts=cls.now_YmdHMS(),
                    e_object_id=str(),
                    seen=False,
                    is_visible=True,
                    user_id=None)

    def get_data(self):
        """

        :return:
        """
        return dict(id=self.id,
                    method=self.method,
                    e_user_id=self.e_user_id,
                    ts=self.ts_from_YmdHMS(self.create_ts or self.modify_ts),
                    seen=self.seen,
                    data=self.data)

    @classmethod
    @gen.coroutine
    def delete(cls, e_object_id):
        """

        :param e_object_id:
        :return:
        """
        existing = yield cls.get(e_object_id=e_object_id)
        yield [cls.mapper.delete(e.id) for e in existing]

    @classmethod
    def get_methods(cls, h_type):
        """

        :param h_type:
        :return:
        """
        types = {
            'bookmark': ["bookmarks:add"],
            'chat': ["follow:follow"],
            'comment': ["video:comment_add"],
            'follow': ["follow:follow"],
            'vlike': ["video:like"],
            'video': ["video:create"],
            'echo': ["video:create", "video:comment_add"],
            'wedcam': ["wedcam:marry", "wedcam:invite", "video:delete"],
        }
        return types.get(h_type, [])
