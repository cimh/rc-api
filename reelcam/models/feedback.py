# -*- coding: utf-8 -*-
from tornado import gen

from reelcam.models import BaseModel
from reelcam.mappers.feedback import FeedbackEntryMappingType


class Feedback(BaseModel):
    """
    Feedback model.

    """

    mapper = FeedbackEntryMappingType

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=None,
                    name=None,
                    subject=None,
                    text=None,
                    email=None)

    def get_data(self):
        """

        :return:
        """
        return dict(id=self.id,
                    name=self.name,
                    subject=self.subject,
                    text=self.text,
                    ts=self.ts_from_YmdHMS(self.create_ts))

    @classmethod
    def new(cls, name, subject, text, email=None):
        """

        :param name:
        :param subject:
        :param text:
        :param email:
        :return:
        """
        return cls(dict(name=name,
                        subject=subject,
                        text=text,
                        email=email,
                        scope=cls.default_scope))

    @classmethod
    @gen.coroutine
    def get_recent(cls, offset=0, limit=10):
        """
        Get recent Feeds.

        :param offset:
        :param limit:
        :return:
        """
        qs = cls.mapper.get(scope=cls.default_scope)
        count = yield qs.count()
        has_more = count > offset+limit
        results = yield qs[offset:offset+limit].order_by('-create_ts').execute()
        recent = [cls(r) for r in results]
        raise gen.Return((recent, has_more))
