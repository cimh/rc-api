# -*- coding: utf-8 -*-
from .base import BaseModel
from ..mappers.comment import CommentEntryMappingType
from tornado import gen
from api_engine import s


class Comment(BaseModel):
    """
    Video Comment Model.
    """

    mapper = CommentEntryMappingType

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=str(),
                    video_id=str(),
                    user_id=str(),
                    body=str(),
                    modify_ts=str(),
                    is_deleted=False)

    def get_data(self):
        """

        :return:
        """
        data = dict(id=self.id,
                    comment_id=self.id,  # TODO: Remove on clients.
                    video_id=self.video_id,
                    user_id=self.user_id,
                    body=self.body,
                    ts=self.ts_from_YmdHMS(self.create_ts))

        if hasattr(self, 'user') and self.user is not None:
            if self.user.fullname == "System":
                self.user.fullname = "CityDOOM"
            data['user'] = dict(fullname=self.user.fullname)

        return data

    @classmethod
    @gen.coroutine
    def get_recent(cls, video_id, offset=0, limit=10):
        """

        :param video_id:
        :param offset:
        :param limit:
        :return:
        """
        query = cls.mapper.get_recent(video_id)
        comments = yield query[offset:limit+offset].execute()  # @TODO: limit+offset+1?)
        if not len(comments):
            raise gen.Return(([], False))
        else:
            raise gen.Return(([cls(r) for r in comments], len(comments) > limit))

    @classmethod
    @gen.coroutine
    def get_multiple(cls, _ids, offset=0, limit=10):
        """

        :param _ids:
        :param offset:
        :param limit:
        :return:
        """
        query = cls.mapper.get_multiple(_ids)
        comments = yield query[offset:limit+offset].execute()
        if not len(comments):
            raise gen.Return([])
        raise gen.Return([cls(r) for r in comments])

    @gen.coroutine
    def delete(self):
        """

        :return:
        """
        self.is_deleted = True
        r = None
        comment = yield self.save()
        if comment:
            r = yield self.mapper.delete(self.id)
        raise gen.Return(r)

    def is_author(self, user):
        """

        :param user:
        :return:
        """
        return self.user_id == user.id
