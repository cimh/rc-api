# -*- coding: utf-8 -*- 
from ..base import BaseModel
from ...mappers.im.chat import ChatEntryMappingType


class Chat(BaseModel):
    """
    Instant Messaging Chat model class.
    """

    mapper = ChatEntryMappingType
