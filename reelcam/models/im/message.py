# -*- coding: utf-8 -*- 
from ..base import BaseModel
from ...mappers.im.msg import MsgEntryMappingType


class Msg(BaseModel):
    """
    Instant Messaging Message model class.
    """

    mapper = MsgEntryMappingType
