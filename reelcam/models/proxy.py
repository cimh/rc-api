# -*- coding: utf-8 -*-
from tornado import gen
from api_engine import s
from api_engine.enums import PROXY_STATUS
from reelcam.utils import location_from_ip
from reelcam.mappers.proxy import ProxyEntryMappingType
from .base import BaseModel


class Proxy(BaseModel):
    """
    Proxy Model.
    """

    mapper = ProxyEntryMappingType

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=str(),
                    host=str(),
                    port_video=int(),
                    port_rtmp=int(),
                    port_web=int(),
                    status=PROXY_STATUS.offline,
                    connections=int(),
                    active=False,  # @TODO: ?? debug
                    gps_location=s().default_gps_point)

    def get_data(self):
        """

        :return:
        """
        return dict(proxy_host=self.host,
                    proxy_port_video=self.port_video,
                    proxy_port_rtmp=self.port_rtmp,
                    proxy_port_web=self.port_web)

    @classmethod
    def new(cls, host, port_rtmp, port_video, port_web):
        """
        Create new Proxy object.

        :param host: str ip address.
        :param port_rtmp: int rtmp port.
        :param port_video: int video(udp) port.
        :param port_web: int web port.
        :return: Proxy object.
        """
        return cls(dict(gps_location=location_from_ip(host),
                        host=host,
                        port_rtmp=port_rtmp,
                        port_video=port_video,
                        port_web=port_web))

    @classmethod
    @gen.coroutine
    def get_by_host_and_ports(cls, data):
        """

        :param data:
        :return:
        """
        r = yield cls.mapper.get_by_host_and_ports(data)
        raise gen.Return(cls(r) if r else None)

    @classmethod
    @gen.coroutine
    def get_closest(cls, client_ip=None):
        """

        :param client_ip:
        :return:
        """
        location = None
        if client_ip is not None:
            location = location_from_ip(client_ip)
        qs = cls.mapper.get_closest(location)
        result = yield qs.execute()
        proxy = None
        if result:
            result = [cls(r) for r in result]
        if result:
            proxy = result[0]
        raise gen.Return(proxy)

    @gen.coroutine
    def set_online(self):
        """

        :return:
        """
        if self.status != PROXY_STATUS.online:
            self.status = PROXY_STATUS.online
            yield self.save()
