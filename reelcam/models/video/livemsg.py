# -*- coding: utf-8 -*- 
from ..base import BaseModel
from ...mappers.video.livemsg import LiveMsgEntryMappingType


class LiveMsg(BaseModel):
    """
    Video Live Message model.
    """

    mapper = LiveMsgEntryMappingType

    @classmethod
    def _mapped_fields(cls):
        """

        :param cls:
        :return:
        """
        return dict(id=str(),
                    video_id=str(),
                    user_id=str(),
                    message=str(),
                    seconds=int())

    def get_data(self):
        """

        :return:
        """
        return dict(user_id=self.user_id,
                    message=self.message,
                    seconds=self.seconds)
