# -*- coding: utf-8 -*-
from ..base import BaseModel, BaseCustomID
from ...mappers.video.like import VideoLikeEntryMappingType
from tornado import gen


class VideoLike(BaseCustomID, BaseModel):
    """
    Video like model.
    """

    mapper = VideoLikeEntryMappingType
    custom_id_attrs = ['user_id', 'video_id']

    def __init__(self, args=None, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        BaseModel.__init__(self, args, **kwargs)
        # setattr(self, "_custom_id", self.build_custom_id(self.user_id, self.video_id))

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=str(),
                    video_id=str(),
                    user_id=str(),
                    modify_ts=str(),
                    positive=bool())

    def get_data(self):
        """

        :return:
        """
        return dict(video_id=self.video_id,
                    user_id=self.user_id,
                    ts=self.ts_from_YmdHMS(self.modify_ts) if self.modify_ts else self.now_YmdHMS(),
                    positive=self.positive)

    @classmethod
    @gen.coroutine
    def check_exists(cls, video_id, user_id):
        """

        :param video_id:
        :param user_id:
        :return:
        """
        like = VideoLike.new(user_id=user_id, video_id=video_id)
        like = yield VideoLike.get_one(_id=like.custom_id, _deleted=False)
        if like is not None:
            raise gen.Return(like)

    @classmethod
    @gen.coroutine
    def like_count_date(cls, date_start, date_end):
        """

        :param date_start:
        :param date_end:
        :return:
        """
        count = yield cls.mapper.like_count_date(date_start, date_end)
        raise gen.Return(count)

    @classmethod
    @gen.coroutine
    def dislike_count_date(cls, date_start, date_end):
        """

        :param date_start:
        :param date_end:
        :return:
        """
        count = yield cls.mapper.dislike_count_date(date_start, date_end)
        raise gen.Return(count)
