# -*- coding: utf-8 -*-
from ..base import BaseModel, BaseCustomID
from ...mappers.video.rtview import RTViewEntryMappingType
from tornado import gen


class RTView(BaseCustomID, BaseModel):
    """
    Realtime View Model.
    """

    mapper = RTViewEntryMappingType
    custom_id_attrs = ['user_id', 'video_id']

    def _mapped_fields(self):
        """

        :return:
        """
        return dict(id=str(),
                    video_id=str(),
                    user_id=str(),
                    modify_ts=str())

    def get_data(self):
        """

        :return:
        """
        return dict(video_id=self.video_id,
                    user_id=self.user_id,
                    ts=self.ts_from_YmdHMS(self.modify_ts))

    @classmethod
    @gen.coroutine
    def count(cls, video_id):
        """

        :param video_id:
        :return:
        """
        r = yield cls.mapper.count(video_id=video_id)
        raise gen.Return(r)

    @classmethod
    @gen.coroutine
    def delete_all_by_video_id(cls, video_id):
        """

        :param video_id:
        :return:
        """
        yield cls.mapper._delete_all_by_video_id(video_id=video_id)
