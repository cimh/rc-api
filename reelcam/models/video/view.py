# -*- coding: utf-8 -*-
from ..base import BaseModel
from ...mappers.video.view import VideoViewEntryMappingType


class VideoView(BaseModel):
    """
    Video view model.
    """

    mapper = VideoViewEntryMappingType

    def _mapped_fields(self):
        """

        :return:
        """
        return dict(id=str(),
                    video_id=str(),
                    user_id=str(),
                    modify_ts=str())

    def get_data(self):
        """

        :return:
        """
        return dict(video_id=self.video_id,
                    user_id=self.user_id,
                    ts=self.ts_from_YmdHMS(self.modify_ts))

    @classmethod
    def _validation_schema(cls):
        """

        :return:
        """
        return dict(video_id=dict(required=True, type='string'),
                    user_id=dict(required=True, type='string'))
