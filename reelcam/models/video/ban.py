# -*- coding: utf-8 -*-
from ..base import BaseModel, BaseCustomID
from ...mappers.video.ban import VideoBanEntryMappingType


class VideoBan(BaseCustomID, BaseModel):
    """
    Video ban model.
    """

    mapper = VideoBanEntryMappingType
    custom_id_attrs = ['user_id', 'video_id']

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(video_id=str(),
                    user_id=str(),
                    modify_ts=str())

    def get_data(self):
        """

        :return:
        """
        return dict(video_id=self.video_id,
                    user_id=self.user_id,
                    ts=self.ts_from_YmdHMS(self.modify_ts) if self.modify_ts else self.now_YmdHMS())
