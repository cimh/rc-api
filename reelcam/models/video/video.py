# -*- coding: utf-8 -*-
import logging
from copy import deepcopy
from datetime import timedelta

from tornado import gen

from api_engine import s
from api_engine.enums import VIDEO_STATUS, VIDEO_PERMISSIONS, VIDEO_DELETE_STATUS, USER_TYPE, WEDCAM
from api_engine.mediahelper import MetaData
from reelcam.utils import default_location, distort_location
from reelcam.models.observe.observable import Observable
from reelcam.uploader import get_uploader
from utils import kvs, check_latlon
from .ban import VideoBan
from .like import VideoLike
from .view import VideoView
from ..base import BaseModel, BaseGPS, BaseFeedback
from ..comment import Comment
from ..mixins.event import EventMixin
from ..user import User
from ...mappers.video.gps import VideoGpsEntryMappingType
from ...mappers.video.users import VideoUsersEntryMappingType
from ...mappers.video.video import VideoEntryMappingType

log = logging.getLogger(__name__)


class Video(Observable, BaseFeedback, BaseGPS, BaseModel, EventMixin):
    """
    Video model.
    """
    mapper = VideoEntryMappingType

    """
    Maximum event recipients count
    """
    MAX_EVENT_RECIPIENTS_COUNT = 1000
    # Video metadata registry class
    metadata_class = MetaData
    WEDCAM_OPERATOR_START_TS = 60 * 60 * 24 * 7  # In seconds.

    can_subscribe = False
    can_subscribed = True

    KS = kvs.KS

    def __init__(self, args=None, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        BaseModel.__init__(self, args, **kwargs)
        Observable.__init__(self, self.observer_id, self.observer_close_ts, self.observe_request_id)

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=str(),
                    subject=str(),
                    visible=True,
                    status=VIDEO_STATUS.paused,
                    duration=float(),
                    filesize=int(),
                    access_observer=True,
                    reporter_id=str(),
                    observer_id=str(),
                    master_id=str(),
                    owner_id=str(),
                    reporter_token=str(),
                    observer_token=str(),
                    reporter_start_ts=None,
                    reporter_close_ts=None,
                    observer_start_ts=None,
                    observer_close_ts=None,
                    observe_request_id=None,
                    count_likes=int(),
                    count_dislikes=int(),
                    count_bans=int(),
                    count_views=int(),
                    count_watchers=int(),
                    count_comments=int(),
                    gps_location=default_location(),
                    record_uploaded=False,
                    tags=str(),
                    permission=int(),
                    is_deleted=False,
                    delete_status=VIDEO_DELETE_STATUS.not_deleted,
                    gps_incognito=False,
                    proxy_id=str(),
                    modify_ts=str(),
                    last_pause_time=None,
                    media_urls=dict(o='', r='', ri=''),
                    shot_urls=str(),
                    screenshot_keys=None,
                    is_reanimated=False,
                    coords=[],
                    country_code=str(),
                    upload_ids=dict(o='', r='', ri=''),
                    parts=[],
                    scope=s().elastic.default_scope,
                    wedding_id=None,
                    is_wedcam_trend=False,
                    is_visible=False,
                    ggg=10,
                    is_photo=False,
                    is_external=False)

    @gen.coroutine
    def get_video_url(self):
        """

        :return:
        """
        if not self.is_finished():
            Proxy = s().M.model('Proxy', self.ctx)
            proxy = yield Proxy.get_one(_id=self.proxy_id)
            if not proxy:
                raise gen.Return('')
            raise gen.Return('http://%s:%d/live/r%s' % (proxy.host, proxy.port_web, self.id))

        else:
            urls = self.get_media_urls()
            if self.record_uploaded:
                raise gen.Return(urls.get('ri', ''))
            else:
                raise gen.Return(urls.get('r', ''))

    @gen.coroutine
    def get_audio_url(self):
        """

        :return:
        """
        if not self.observer_id:
            raise gen.Return('')

        if not self.is_finished():
            Proxy = s().M.model('Proxy', self.ctx)
            proxy = yield Proxy.get_one(_id=self.proxy_id)
            if not proxy:
                raise gen.Return('')
            raise gen.Return('http://%s:%d/live/o%s' % (proxy.host, proxy.port_web, self.id))

        else:
            urls = self.get_media_urls()
            raise gen.Return(urls.get('o', ''))

    @gen.coroutine
    def get_data(self, x_full=False):
        """

        :return:
        """
        gps_location = self.get_gps_location()
        if x_full:
            data = dict(id=self.id,
                        subject=self.subject,
                        status=self.status,
                        duration=self.duration,
                        reporter_id=self.reporter_id,
                        owner_id=self.owner_id,
                        shot_urls=self.get_screenshot_urls(),
                        reporter_start_ts=self.ts_from_YmdHMS(self.reporter_start_ts),
                        count_likes=self.count_likes,
                        count_dislikes=self.count_dislikes,
                        count_bans=self.count_bans,
                        count_views=self.count_views,
                        count_comments=self.count_comments,
                        lat=gps_location['lat'],
                        lon=gps_location['lon'],
                        record_uploaded=self.record_uploaded,
                        tags=self.tags,
                        permission=self.permission,
                        delete_status=self.delete_status,
                        gps_incognito=self.gps_incognito,
                        proxy_id=self.proxy_id,
                        ts=self.ts_from_YmdHMS(self.modify_ts),
                        last_pause_time=self.ts_from_YmdHMS(self.last_pause_time),
                        live=not self.is_finished(),
                        wedding_id=self.wedding_id,
                        is_photo=self.is_photo,
                        is_reanimated=self.is_reanimated,
                        is_external=self.is_external,
                        comments=[])
        else:
            data = dict(id=self.id,
                        video_id=self.id,
                        subject=self.subject,
                        status=self.status,
                        duration=self.duration,
                        reporter_id=self.reporter_id,
                        observer_id=self.observer_id,
                        owner_id=self.owner_id,
                        shot_urls=self.get_screenshot_urls(),
                        reporter_start_ts=self.ts_from_YmdHMS(self.reporter_start_ts),
                        observer_start_ts=self.ts_from_YmdHMS(self.observer_start_ts),
                        count_likes=self.count_likes,
                        count_dislikes=self.count_dislikes,
                        count_bans=self.count_bans,
                        count_views=self.count_views,
                        count_comments=self.count_comments,
                        lat=gps_location['lat'],
                        lon=gps_location['lon'],
                        record_uploaded=self.record_uploaded,
                        tags=self.tags,
                        permission=self.permission,
                        delete_status=self.delete_status,
                        gps_incognito=self.gps_incognito,
                        proxy_id=self.proxy_id,
                        ts=self.ts_from_YmdHMS(self.modify_ts),
                        last_pause_time=self.ts_from_YmdHMS(self.last_pause_time),
                        live=not self.is_finished(),
                        wedding_id=self.wedding_id,
                        is_photo=self.is_photo,
                        is_reanimated=self.is_reanimated,
                        is_external=self.is_external)
        data['video_url'], data['audio_url'] = yield [self.get_video_url(),
                                                      self.get_audio_url()]

        if not x_full:
            data['is_banned'], data['is_bookmarked'] = yield [self.is_banned(),
                                                              self.is_bookmarked()]
            data['is_liked'], data['is_disliked'] = yield self.is_liked_or_disliked()

        if self.is_active():
            data['count_watchers'] = self.count_watchers

        if self.parts:
            parts = deepcopy(self.parts)
            for p in parts:
                p['start_ts'] = self.ts_from_YmdHMS(p['start_ts'])
                p['end_ts'] = self.ts_from_YmdHMS(p['end_ts'])
            data['parts'] = parts

        raise gen.Return(data)

    @classmethod
    @gen.coroutine
    def _join_users_to_comments(cls, comments):
        """

        :param: comments: A Comment instances list
        """
        User = s().M.model('User')
        user_ids = [comment.user_id for comment in comments]
        users = yield User.get(_ids=user_ids, limit=len(user_ids))
        users = {user.id: user for user in users}
        for comment in comments:
            if comment.user_id in users:
                comment.user = users[comment.user_id]

    @gen.coroutine
    def add_part(self, start_ts, duration):
        """

        :param: start_ts: int part start ts.
        :param: duration: int total video duration.
        :return: Video object.
        """
        if start_ts > self.now_ts() or start_ts < self.ts_from_YmdHMS(self.reporter_start_ts) \
                or self.get_part(self.YmdHMS_from_ts(start_ts)):
            return

        parts = deepcopy(self.parts)
        if parts:
            if len(parts) == 1:
                parts[0]['duration'] = duration
            elif duration <= sum([p['duration'] for p in parts[:-1]]):
                return
            else:
                duration = duration - sum([p['duration'] for p in parts[:-1]])

        parts.append(dict(start_ts=self.YmdHMS_from_ts(start_ts), duration=None, end_ts=None))

        if len(parts) > 1:
            parts[-2]['duration'] = duration
            end_ts = self.ts_from_YmdHMS(parts[-2]['start_ts']) + int(duration)
            parts[-2]['end_ts'] = self.YmdHMS_from_ts(end_ts)

        self.parts = parts
        v = yield self.save()
        raise gen.Return(v)

    def get_part(self, start_ts):
        """

        :param start_ts: YmdHMS
        :return:
        """
        return start_ts in [d['start_ts'] for d in self.parts]

    def timeline(self, events, sec_key='seconds'):
        """

        :param events:
        :param sec_key:
        :return:
        """
        parts = deepcopy(self.parts)
        for p in parts:
            p['start_ts'] = self.ts_from_YmdHMS(p['start_ts'])
            p['end_ts'] = self.ts_from_YmdHMS(p['end_ts'])

        for e in events:
            for p in range(0, len(parts)):

                if e['ts'] < parts[p]['start_ts']:
                    e[sec_key] = sum([int(i['duration']) for i in parts[:p]])
                    break
                elif parts[p]['start_ts'] <= e['ts'] <= parts[p]['end_ts']:
                    e[sec_key] = sum([int(i['duration']) for i in parts[:p]]) + \
                                 e['ts'] - parts[p]['start_ts']
                    break
                elif e['ts'] > parts[p]['end_ts']:
                    continue

        events = [d for d in events if (sec_key in d)]

        return events

    @gen.coroutine
    def has_access(self, user=None):
        """

        :param user:
        :return:
        """
        if self.scope == s().get_scope("wedcam"):
            raise gen.Return(True)
        current_user = self.ctx.request_user if user is None else user
        list_users = yield self.users_get()
        followed = False
        owner = yield self.get_owner()
        if user and owner:
            followed = yield owner.is_follower(user)

        _has_access = False

        if self.permission is VIDEO_PERMISSIONS.all:
            _has_access = True

        if current_user and (current_user.user_type is USER_TYPE.moderator or self.is_owner(current_user)):
            _has_access = True

        if current_user and owner and followed and self.permission in [VIDEO_PERMISSIONS.followers,
                                                                       VIDEO_PERMISSIONS.followers_and_list]:
            _has_access = True

        if current_user and current_user in list_users and self.permission in [VIDEO_PERMISSIONS.list,
                                                                               VIDEO_PERMISSIONS.followers_and_list]:
            _has_access = True

        raise gen.Return(_has_access)

    @classmethod
    @gen.coroutine
    def privacy_filter(cls, results, current_user=None):
        """

        :param results:
        :param current_user:
        :return:
        """
        if not current_user:
            results = results.filter(cls.mapper.f_privacy(None))
            raise gen.Return(results)

        if current_user.user_type is USER_TYPE.moderator:
            raise gen.Return(results)

        if cls.default_scope == s().get_scope("wedcam"):
            r = yield cls.wedcam_privacy(results, current_user)
            raise gen.Return(r)

        user_ids = current_user.followed
        video_ids = yield VideoUsersEntryMappingType._get_by_user_id(current_user.id)
        results = results.filter(cls.mapper.f_privacy(current_user.id, video_ids, user_ids))
        raise gen.Return(results)

    @classmethod
    @gen.coroutine
    def wedcam_privacy(cls, qs, current_user):
        """
        Wedcam privacy filter.

        :param qs:
        :param current_user:
        :return:
        """
        from api_engine.enums import INVITE_STATUSES
        Invite = s().M.model('Invite')
        User = s().M.model('User')

        user_ids = []
        wedding_ids = []
        if current_user.followed:
            followed = yield User.get(_ids=current_user.followed, app_name="wedcam")
            if followed:
                user_ids = [u.id for u in followed]
                wedding_ids = list({u.wedding_id for u in followed if u.wedding_id})

        invites = yield Invite.get_user_invites(current_user.id, statuses=[INVITE_STATUSES.ack])
        if invites:
            wedding_ids = set(wedding_ids)
            for inv in invites:
                wedding_ids.add(inv.wedding_id)
            wedding_ids = list(wedding_ids)

        follows = yield Invite.get_user_invites(current_user.id, statuses=[INVITE_STATUSES.ack], link_type="follow")
        if follows:
            wedding_ids = set(wedding_ids)
            for foll in follows:
                wedding_ids.add(foll.wedding_id)
            wedding_ids = list(wedding_ids)

        qs = qs.filter(cls.mapper.wedcam_privacy(current_user.id, current_user.wedding_id, user_ids, wedding_ids))
        raise gen.Return(qs)

    @classmethod
    @gen.coroutine
    def get_i_follow(cls, current_user, offset=0, limit=10):
        """

        :param current_user:
        :param offset:
        :param limit:
        :return:
        """
        Invite = s().M.model('Invite')

        results = cls.mapper.get_i_follow(current_user.id, current_user.followed, scope=cls.default_scope)
        if cls.default_scope == s().get_scope("wedcam"):
            from api_engine.enums import INVITE_STATUSES

            wedding_ids = set()
            wedcam_invites = yield Invite.get_user_invites(current_user.id, statuses=[INVITE_STATUSES.ack])
            if wedcam_invites:
                for inv in wedcam_invites:
                    wedding_ids.add(inv.wedding_id)

            wedcam_follows = yield Invite.get_user_invites(current_user.id, statuses=[INVITE_STATUSES.ack],
                                                           link_type="follow")
            if wedcam_follows:
                for foll in wedcam_follows:
                    wedding_ids.add(foll.wedding_id)

            wedding_ids = list(wedding_ids)
            results = cls.mapper.get_i_follow(current_user.id, current_user.followed, wedding_ids,
                                              scope=cls.default_scope)

        res = []
        has_more = False
        if results is not None:
            results = yield cls.privacy_filter(results, current_user)
            count, res = yield [results.count(),
                                results[offset:offset+limit].execute()]
            has_more = count > limit + offset

        raise gen.Return(([dict(id=key._id, ts=cls.ts_from_YmdHMS(cls(key).modify_ts)) for key in res], has_more))

    @classmethod
    @gen.coroutine
    def get_live(cls, current_user=None, user_id=None, offset=0, limit=10):
        """

        :param current_user:
        :param user_id:
        :param offset:
        :param limit:
        :return:
        """
        results = cls.mapper.get_live(user_id, scope=cls.default_scope)
        results = cls.mapper.f_country_code(results)

        results = yield cls.privacy_filter(results, current_user)

        count, res = yield [results.count(),
                            results[offset:offset+limit].execute()]
        has_more = count > limit + offset

        raise gen.Return(([dict(id=key._id, ts=cls.ts_from_YmdHMS(cls(key).modify_ts))
                           for key in res],
                          has_more))

    @classmethod
    @gen.coroutine
    def x_get_live(cls, current_user=None, user_id=None, offset=0, limit=10):
        """

        :param current_user:
        :param user_id:
        :param offset:
        :param limit:
        :return:
        """
        results = cls.mapper.get_live(user_id, scope=cls.default_scope)
        results = cls.mapper.f_country_code(results)

        results = yield cls.privacy_filter(results, current_user)

        count, res = yield [results.count(),
                            results[offset:offset+limit].execute()]

        videos = [cls(r) for r in res]
        reporter_ids = []
        items = []

        comments = yield Comment.get_multiple(_ids=[v.id for v in videos], limit=30)
        yield cls._join_users_to_comments(comments)

        for v in videos:
            reporter_ids.append(v.reporter_id)

        reporters = yield User.get(_ids=reporter_ids)
        for v in videos:
            reporter = None
            v_data = yield v.get_data(x_full=True)
            for x in reporters:
                if x.id == v.reporter_id:
                    reporter = x
            if reporter:
                v_data['reporter'] = yield reporter.get_data(x_full=True)
            if v.count_comments > 0:
                for c in comments:
                    if c.video_id == v.id:
                        v_data['comments'].append(c.get_data())
            items.append(v_data)
        yield [cls.is_liked_of_disliked_multiple(items), cls.is_bookmarked_multiple(items)]

        has_more = count > offset + limit
        raise gen.Return((items, has_more))

    @classmethod
    @gen.coroutine
    def get_near_me(cls, location, current_user=None, user_id=None, offset=0, limit=10):
        """

        :param location:
        :param current_user:
        :param user_id:
        :param offset:
        :param limit:
        :return:
        """
        platform = cls.ctx.platform if getattr(cls, 'ctx', None) else None
        results = cls.mapper.get_near_me(location, user_id, scope=cls.default_scope, platform=platform)
        results = cls.mapper.f_country_code(results)

        results = yield cls.privacy_filter(results, current_user)

        count, res = yield [results.count(),
                            results[offset:offset+limit].execute()]
        videos = [cls(r) for r in res]
        has_more = count > limit + offset

        raise gen.Return((videos, has_more))

    @classmethod
    @gen.coroutine
    def x_get_near_me(cls, location, current_user=None, user_id=None, offset=0, limit=10):
        """

        :param location:
        :param current_user:
        :param user_id:
        :param offset:
        :param limit:
        :return:
        """
        platform = cls.ctx.platform if getattr(cls, 'ctx', None) else None
        results = cls.mapper.get_near_me(location, user_id, scope=cls.default_scope, platform=platform)
        results = cls.mapper.f_country_code(results)

        results = yield cls.privacy_filter(results, current_user)

        count, res = yield [results.count(),
                            results[offset:offset+limit].execute()]

        videos = [cls(r) for r in res]
        reporter_ids = []
        items = []

        comments = yield Comment.get_multiple(_ids=[v.id for v in videos], limit=30)
        yield cls._join_users_to_comments(comments)

        for v in videos:
            reporter_ids.append(v.reporter_id)
        reporters = yield User.get(_ids=reporter_ids)

        for v in videos:
            reporter = None
            v_data = yield v.get_data(x_full=True)
            for x in reporters:
                if x.id == v.reporter_id:
                    reporter = x
            if reporter:
                v_data['reporter'] = yield reporter.get_data(x_full=True)
            if v.count_comments > 0:
                for c in comments:
                    if c.video_id == v.id:
                        v_data['comments'].append(c.get_data())
            items.append(v_data)
        yield [cls.is_liked_of_disliked_multiple(items), cls.is_bookmarked_multiple(items)]

        has_more = count > offset + limit
        raise gen.Return((items, has_more))

    @classmethod
    @gen.coroutine
    def get_popular(cls, current_user=None, user_id=None, offset=0, limit=10):
        """

        :param current_user:
        :param user_id:
        :param offset:
        :param limit:
        :return:
        """

        # get popular 30 of the week
        query_results_week = cls.mapper.get_popular_week(user_id, scope=cls.default_scope)
        query_results_week = cls.mapper.f_country_code(query_results_week)
        query_results_week = yield cls.privacy_filter(query_results_week, current_user)

        # if results_week.count() >= 30  -> then take only 30 first
        # and if we need more than 30 (offset+limit>30) -> get standard popular except week popular ids
        # or if we need less than 30 (offset+limit<30) -> use results_week

        # if results_week.count() < 30
        # and we need more than results_week.count() results (offset+limit>results_week.count())
        #  -> get standard popular except week popular ids

        # sum results_week and results list

        c = yield query_results_week.count()
        count_results_week = min(c, 30)
        results_week = yield query_results_week[:count_results_week].execute()

        platform = cls.ctx.platform if getattr(cls, 'ctx', None) else None
        results = cls.mapper.get_popular(user_id, scope=cls.default_scope, platform=platform)
        results = cls.mapper.f_country_code(results)
        results = yield cls.privacy_filter(results, current_user)

        if offset + limit >= count_results_week:
            results = cls.mapper.exclude_filter(results, results_week)
            offset_usual = offset - count_results_week if offset > count_results_week else 0
            offset_limit_usual = offset + limit - count_results_week
            res = yield results[offset_usual:offset_limit_usual].execute()
            results_list = [dict(id=key._id,
                                 ts=cls.ts_from_YmdHMS(cls(key).modify_ts))
                            for key in res]

            if offset < count_results_week:
                res = yield query_results_week[offset:count_results_week].execute()
                results_week_list = [dict(id=key._id,
                                          ts=cls.ts_from_YmdHMS(cls(key).modify_ts))
                                     for key in res]
            else:
                results_week_list = []
            res = results_week_list + results_list
            count = yield results.count()
            has_more = count > limit + offset
            raise gen.Return((res, has_more))
        else:
            count, res = yield [results.count(),
                                query_results_week[offset:offset+limit].execute()]
            results_week_list = [dict(id=key._id,
                                      ts=cls.ts_from_YmdHMS(cls(key).modify_ts))
                                 for key in res]
            has_more = count > limit + offset
            raise gen.Return((results_week_list, has_more))

    @classmethod
    @gen.coroutine
    def x_get_popular(cls, current_user=None, user_id=None, offset=0, limit=10):
        """

        :param current_user:
        :param user_id:
        :param offset:
        :param limit:
        :return:
        """

        # get popular 30 of the week
        query_results_week = cls.mapper.get_popular_week(user_id, scope=cls.default_scope)
        query_results_week = cls.mapper.f_country_code(query_results_week)
        query_results_week, c = yield [cls.privacy_filter(query_results_week, current_user),
                                       query_results_week.count()]

        # if results_week.count() >= 30  -> then take only 30 first
        # and if we need more than 30 (offset+limit>30) -> get standard popular except week popular ids
        # or if we need less than 30 (offset+limit<30) -> use results_week

        # if results_week.count() < 30
        # and we need more than results_week.count() results (offset+limit>results_week.count())
        #  -> get standard popular except week popular ids

        # sum results_week and results list
        count_results_week = min(c, 30)

        platform = cls.ctx.platform if getattr(cls, 'ctx', None) else None
        results = cls.mapper.get_popular(user_id, scope=cls.default_scope, platform=platform)
        results = cls.mapper.f_country_code(results)
        results_week, results = yield [query_results_week[:count_results_week].execute(),
                                       cls.privacy_filter(results, current_user)]

        if offset + limit >= count_results_week:
            results = cls.mapper.exclude_filter(results, results_week)
            offset_usual = offset - count_results_week if offset > count_results_week else 0
            offset_limit_usual = offset + limit - count_results_week
            count, res = yield [results.count(),
                                results[offset_usual:offset_limit_usual].execute()]
            results_list = [cls(r) for r in res]
            if offset < count_results_week:
                res = yield query_results_week[offset:count_results_week].execute()
                results_week_list = [cls(r) for r in res]
            else:
                results_week_list = []
            videos = results_week_list + results_list
        else:
            count, res = yield [results.count(),
                                query_results_week[offset:offset+limit].execute()]
            results_week_list = [cls(r) for r in res]
            videos = results_week_list

        reporter_ids = []
        items = []

        comments = yield Comment.get_multiple(_ids=[v.id for v in videos], limit=30)
        yield cls._join_users_to_comments(comments)

        for v in videos:
            reporter_ids.append(v.reporter_id)
        reporters = yield User.get(_ids=reporter_ids)

        for v in videos:
            reporter = None
            v_data = yield v.get_data(x_full=True)
            for x in reporters:
                if x.id == v.reporter_id:
                    reporter = x
            if reporter:
                v_data['reporter'] = yield reporter.get_data(x_full=True)
            if v.count_comments > 0:
                for c in comments:
                    if c.video_id == v.id:
                        v_data['comments'].append(c.get_data())
            items.append(v_data)
        yield [cls.is_liked_of_disliked_multiple(items), cls.is_bookmarked_multiple(items)]

        has_more = count > offset + limit
        raise gen.Return((items, has_more))

    @classmethod
    @gen.coroutine
    def get_recent(cls, current_user=None, user_id=None, offset=0, limit=10):
        """

        :param current_user:
        :param user_id:
        :param offset:
        :param limit:
        :return:
        """
        if user_id:
            User = s().M.model('User')
            user = yield User.get_one(_id=user_id, _deleted=False)
            if user and user.user_type is USER_TYPE.wedcam_trend:
                Wedding = s().M.model('Wedding')
                videos, has_more = yield Wedding.trend_videos(None, offset, limit, user_id)
                raise gen.Return(([v.get_idts() for v in videos], has_more))

        platform = cls.ctx.platform if getattr(cls, 'ctx', None) else None
        results = cls.mapper.get_recent(user_id, scope=cls.default_scope, platform=platform)
        results = cls.mapper.f_country_code(results)
        results = yield cls.privacy_filter(results, current_user)

        count, res = yield [results.count(),
                            results[offset:offset+limit].execute()]
        has_more = count > offset + limit

        raise gen.Return(([dict(id=key._id, ts=cls.ts_from_YmdHMS(cls(key).modify_ts))
                           for key in res],
                          has_more))

    @classmethod
    @gen.coroutine
    def x_get_recent(cls, current_user=None, user_id=None, offset=0, limit=10):
        """

        :param current_user:
        :param user_id:
        :param offset:
        :param limit:
        :return:
        """
        User = s().M.model('User', cls.ctx)
        if user_id:
            user = yield User.get_one(_id=user_id, _deleted=False)
            if user and user.user_type is USER_TYPE.wedcam_trend:
                Wedding = s().M.model('Wedding')
                videos, has_more = yield Wedding.trend_videos(None, offset, limit, user_id)
                raise gen.Return(([v.get_idts() for v in videos], has_more))

        platform = cls.ctx.platform if getattr(cls, 'ctx', None) else None
        results = cls.mapper.get_recent(user_id, scope=cls.default_scope, platform=platform)
        results = cls.mapper.f_country_code(results)
        results = yield cls.privacy_filter(results, current_user)

        count, res = yield [results.count(),
                            results[offset:offset+limit].execute()]
        videos = [cls(r) for r in res]
        reporter_ids = []
        items = []

        comments = yield Comment.get_multiple(_ids=[v.id for v in videos], limit=30)
        yield cls._join_users_to_comments(comments)
        for v in videos:
            reporter_ids.append(v.reporter_id)
        reporters = yield User.get(_ids=reporter_ids)

        for v in videos:
            reporter = None
            v_data = yield v.get_data(x_full=True)
            for x in reporters:
                if x.id == v.reporter_id:
                    reporter = x
            if reporter:
                v_data['reporter'] = yield reporter.get_data(x_full=True)
            if v.count_comments > 0:
                for c in comments:
                    if c.video_id == v.id:
                        v_data['comments'].append(c.get_data())
            items.append(v_data)
        yield [cls.is_liked_of_disliked_multiple(items), cls.is_bookmarked_multiple(items)]

        has_more = count > offset + limit
        raise gen.Return((items, has_more))

    @classmethod
    @gen.coroutine
    def get_wedcam(cls, current_user=None, user_id=None, wedding_id=None, offset=0, limit=10, recent=False):
        """

        :param current_user:
        :param user_id:
        :param wedding_id:
        :param offset:
        :param limit:
        :param recent: bool Recent ordering flag.
        :return:
        """
        platform = cls.ctx.platform if getattr(cls, 'ctx', None) else None
        results = cls.mapper.get_wedcam(user_id, wedding_id, platform=platform)
        results = cls.mapper.f_country_code(results)
        results = yield cls.privacy_filter(results, current_user)

        if recent:
            results = results.order_by('-reporter_start_ts')
        else:
            results = results.order_by('-reporter_start_ts')
            results = results.order_by('-ggg')
        # else:
        #     results = results.order_by('-count_likes', '-count_views', '-reporter_start_ts')

        count, res = yield [results.count(),
                            results[offset:offset+limit].execute()]
        videos = [cls(r) for r in res]
        has_more = count > limit + offset

        raise gen.Return((videos, has_more))

    @classmethod
    @gen.coroutine
    def edit(cls, video_id, data, current_user_id):
        """
        Edit Video.

        :param video_id: str Video ID.
        :param data: dict DATA.
        :param current_user_id: str current User ID.
        :return:
        """
        Wedding = s().M.model('Wedding')

        editable = ('gps_incognito', 'permission', 'subject', 'tags', 'wedding_id')
        data = {field: data[field] for field in editable if field in data}
        wedding_id = data.pop('wedding_id', None)
        video = yield cls.get_one(_id=video_id)

        if not video.wedding_id:
            if current_user_id != video.reporter_id:
                return

            if wedding_id:
                wedding = yield Wedding.get_one(_id=wedding_id)
                if not wedding:
                    return
                video = yield wedding.add_video(video, current_user_id)
                raise gen.Return(video)

            if data:
                video = yield video.update(data)
            raise gen.Return(video)

        else:
            wedding = yield Wedding.get_one(_id=video.wedding_id)
            if not wedding:
                return
            video = yield wedding.edit_video(video, data, current_user_id)
            raise gen.Return(video)

    @classmethod
    @gen.coroutine
    def check_exists(cls, subject, user):
        """
        Check video exists by subject and suggest new subject if exists.

        :param subject:
        :param user:
        :return: (str) new subject, (bool) exists
        """
        import re

        regex = re.compile('(.+)\((\d+)\)$', re.I | re.U)
        res = regex.findall(subject)
        subj, num = res[0] if len(res) else (subject, 0)

        video = yield cls.get_one(subject=subject, owner_id=user.id, _deleted=False, scope=cls.default_scope)

        if not video:
            raise gen.Return((subj, None))

        qs = cls.mapper.get(subject__prefix=subj, owner_id=user.id, _deleted=False, scope=cls.default_scope)
        count = yield qs.count()
        result = yield qs[:count].execute()
        videos = [cls(r) for r in result]

        # videos = cls._get_by_subject_and_user_id_multiple(subj, user.id)

        new_num = int(num) + 1

        nums = set()

        for v in videos:
            _res = regex.findall(v.subject)
            _subj, _num = _res[0] if len(_res) else (v.subject, 0)
            if _num:
                nums.add(int(_num))

        if nums:
            nums_range = set(range(1, max(nums) + 2))
            new_num = sorted(nums_range - nums)[0]

        raise gen.Return(('%s(%d)' % (subj, new_num), video))

    @gen.coroutine
    def ban(self, data):
        """

        :param data:
        :return:
        """
        vban = VideoBan.new(user_id=data['user_id'], video_id=data['video_id'])
        vban = yield VideoBan.get_one(_id=vban.custom_id)
        if not vban:
            vban = VideoBan(data)
            self.incr_bans()
            reporter, subscribers = yield [self.get_reporter(), self.get_subscribers()]
            users = list(subscribers)
            if reporter:
                users.append(reporter)
                reporter.count_bans += 1
                yield reporter.save()
            r, _ = yield [vban.save(), self.save()]
            s().e('video:ban', sender=self.ctx.request_user, notify_sender=True,
                  users=users, data=(yield self.get_data()))
            raise gen.Return(r)
        else:
            raise gen.Return(False)

    @gen.coroutine
    def unban(self, vban_id):
        """

        :param vban_id:
        :return:
        """
        reporter, subscribers = yield [self.get_reporter(), self.get_subscribers()]
        reporter.count_bans -= 1
        self.decr_bans()
        r, data, _, _ = yield [VideoBan.delete(vban_id), self.get_data(), reporter.save(), self.save()]
        users = [reporter] + list(subscribers)
        s().e('video:unban', sender=self.ctx.request_user, notify_sender=True, users=users, data=data)
        raise gen.Return(r)

    @gen.coroutine
    def like(self, data, positive=True):
        """
        increase/decrease like/dislike counter to video owner

        :param data:
        :param positive:
        :return:
        """
        vlike, reporter = yield [VideoLike.check_exists(data['video_id'], data['user_id']), self.get_reporter()]
        if not vlike:
            vlike = VideoLike(data)
            vlike.positive = positive
            if positive:
                if reporter:
                    reporter.count_likes += 1
                self.incr_likes()
            else:
                if reporter:
                    reporter.count_dislikes += 1
                self.incr_dislikes()
        else:
            if vlike.positive == positive:
                return
            else:
                if vlike.positive is False and positive is True:
                    if reporter:
                        reporter.count_dislikes -= 1
                        reporter.count_likes += 1
                    self.decr_dislikes()
                    self.incr_likes()
                elif vlike.positive is True and positive is False:
                    if reporter:
                        reporter.count_dislikes += 1
                        reporter.count_likes -= 1
                    self.incr_dislikes()
                    self.decr_likes()
                vlike.positive = positive
        r, subscribers, video_data, _, _ = yield [vlike.save(), self.get_subscribers(), self.get_data(),
                                                  self.save(), reporter.save()]
        users = list(subscribers)
        if reporter:
            users.append(reporter)
        video_data = yield self.get_data()
        data = dict(video_id=self.id,
                    count_likes=self.count_likes,
                    count_dislikes=self.count_dislikes,
                    count_bans=self.count_bans,
                    is_liked=video_data['is_liked'],
                    is_disliked=video_data['is_disliked'],
                    scope=self.scope)
        method = 'video:like'
        s().e(method, sender=self.ctx.request_user, notify_sender=True, data=data, users=users)
        s().h(method, data, self.ctx.request_user.id, self.reporter_id)
        raise gen.Return(r)

    @gen.coroutine
    def unlike(self, vlike):
        """

        :param vlike:
        :return:
        """
        reporter, subscribers = yield [self.get_reporter(), self.get_subscribers()]
        if vlike.positive:
            if reporter:
                reporter.count_likes -= 1
            self.decr_likes()
        else:
            if reporter:
                reporter.count_dislikes -= 1
            self.decr_dislikes()

        r, video_data, _, _ = yield [VideoLike.delete(vlike.id), self.get_data(), self.save(),
                                     reporter.save() if reporter else gen.moment]
        users = list(subscribers)
        if reporter:
            users.append(reporter)
        data = dict(video_id=self.id,
                    count_likes=self.count_likes,
                    count_dislikes=self.count_dislikes,
                    count_bans=self.count_bans,
                    is_liked=video_data['is_liked'],
                    is_disliked=video_data['is_disliked'])
        s().e('video:unlike', sender=self.ctx.request_user, notify_sender=True, data=data, users=users)
        raise gen.Return(r)

    @gen.coroutine
    def delete(self):
        """

        :return:
        """
        video, reporter = yield [self.set_deleted(), self.get_reporter()]
        # recalculate total video duration for user
        if video.duration:
            reporter.total_video_duration -= video.duration
            yield reporter.save()
        raise gen.Return(video)

    @classmethod
    @gen.coroutine
    def delete_video(cls, video_id, current_user_id, force=False):
        """
        Delete video.

        :param video_id: str Video ID.
        :param current_user_id: str current User ID.
        :param force: bool Forced delete flag.
        :return: Video object
        """
        video = yield cls.get_one(_id=video_id, _deleted=False)
        if not video:
            return

        if force:
            if video.reporter_id == current_user_id:
                video = yield video.delete()
                raise gen.Return(video)
            return

        if not video.wedding_id:
            if video.reporter_id == current_user_id:
                video = yield video.delete()
                raise gen.Return(video)
            else:
                return
        else:
            Wedding = s().M.model('Wedding', cls.ctx)
            wedding = yield Wedding.get_one(_id=video.wedding_id)
            if not wedding:
                return
            video = yield wedding.delete_video(video_id, current_user_id)
            raise gen.Return(video)

    @classmethod
    @gen.coroutine
    def set_permission_for_all_owned(cls, owner):
        """

        :param owner:
        :return:
        """
        owner_id = owner.id
        permission = owner.default_video_permission
        yield cls.mapper.set_permission_for_all_owned(owner_id, permission)

    @gen.coroutine
    def reanimate(self, proxy_id):
        """

        :param proxy_id:
        :return:
        """
        Upload = s().M.model('Upload')
        self.is_reanimated = True
        self.proxy_id = proxy_id
        self.status = VIDEO_STATUS.paused
        self.last_pause_time = self.now_YmdHMS()
        video = yield self.get_one(_id=self.id)
        if not video:
            return
        r = yield self.save()
        for target in self.upload_ids:
            if self.upload_ids[target]:
                upload = yield Upload.get_one(_id=self.upload_ids[target])
                upload.is_complete = False
                upload.task_complete = False
                upload.is_reanimated = True
                yield upload.save()

        reporter, allowed_users, subscribers, data = yield [self.get_reporter(), self.get_allowed_users(),
                                                            self.get_subscribers(), self.get_data()]
        reporter.subscribe(self)
        users = list(allowed_users) + list(subscribers)
        s().e('video:reanimate', sender=reporter, notify_sender=True, users=users, data=data)
        raise gen.Return(r)

    @gen.coroutine
    def get_reporter(self):
        """

        :return:
        """
        User = s().M.model('User')
        reporter = yield User.get_one(_id=self.reporter_id)
        raise gen.Return(reporter)

    @gen.coroutine
    def get_observer(self):
        """

        :return:
        """
        if not self.observer_id:
            return
        User = s().M.model('User')
        observer = yield User.get_one(_id=self.observer_id)
        raise gen.Return(observer)

    @gen.coroutine
    def get_owner(self):
        """

        :return:
        """
        User = s().M.model('User')
        owner = yield User.get_one(_id=self.owner_id)
        raise gen.Return(owner)

    @gen.coroutine
    def start(self):
        """

        :return:
        """
        r = yield self.set_active()
        reporter, data, allowed_users, subscribers = yield [self.get_reporter(), self.get_data(),
                                                            self.get_allowed_users(), self.get_subscribers()]
        users = list(allowed_users + subscribers)
        s().e('video:start', sender=reporter, notify_sender=True, users=users, data=data)
        raise gen.Return(r)

    @gen.coroutine
    def pause(self):
        """

        :return:
        """
        self.status = VIDEO_STATUS.paused
        self.last_pause_time = self.now_YmdHMS()
        reporter, data, r, allowed_users, subscribers = yield [self.get_reporter(), self.get_data(), self.save(),
                                                               self.get_allowed_users(), self.get_subscribers()]
        users = list(subscribers + allowed_users)
        s().e('video:pause', sender=reporter, notify_sender=True, users=users, data=data)
        raise gen.Return(r)

    @gen.coroutine
    def finish(self):
        """

        :return:
        """
        if self.is_finished():
            raise gen.Return(self)
        RTView = s().M.model('RTView', self.ctx)
        self.status = VIDEO_STATUS.finished
        self.reporter_close_ts = self.now_YmdHMS()
        self.count_watchers = 0
        video, _ = yield [self.save(), RTView.delete_all_by_video_id(video_id=self.id)]
        reporter, data, allowed_users, subscribers = yield [self.get_reporter(), self.get_data(),
                                                            self.get_allowed_users(), self.get_subscribers()]
        users = list(subscribers + allowed_users)
        s().e('video:finish', sender=reporter, notify_sender=True, users=users, data=data)
        if self.observer_id and not self.observer_close_ts:
            Observe = s().M.model('Observe')
            from tornado.ioloop import IOLoop
            IOLoop.current().spawn_callback(Observe.end, self.id, self.observer_id)
        raise gen.Return(video)

    def _delay_finish(self):
        """
        :return:
        """

        @gen.coroutine
        def callback(video_id):
            """

            :param video_id:
            :return:
            """
            Video = s().M.model('Video', self.ctx)
            video = yield Video.get_one(_id=video_id)
            if video:
                yield video.finish()

        from tornado.ioloop import IOLoop
        IOLoop.current().spawn_callback(callback, self.id)

    def startcheck(self):
        """
        Checks that live video translation can be resumed.
        It possible that if video model has `paused` or `active` state.
        Note that trying _delay_finish job timeout reset for the `paused` state or set job newly if exists.
        :return:
        """
        if self.is_paused():
            return True
        elif self.is_active():
            return True
        else:
            return False

    def check_reporter_token(self, token):
        """

        :param token:
        :return:
        """
        return self.reporter_token == token

    def ri_uploaded(self):
        """

        :return:
        """
        self.record_uploaded = True
        self.save()

    # state
    def is_active(self):
        """

        :return:
        """
        return self.status == VIDEO_STATUS.active

    @gen.coroutine
    def set_active(self):
        """

        :return:
        """
        self.status = VIDEO_STATUS.active
        self.reporter_start_ts = self.now_YmdHMS()
        r = yield self.save()
        raise gen.Return(r)

    def is_paused(self):
        """

        :return:
        """
        return self.status == VIDEO_STATUS.paused or self.status == VIDEO_STATUS.lost_finished

    def is_finished(self):
        """

        :return:
        """
        return self.status == VIDEO_STATUS.finished

    @gen.coroutine
    def is_viewable(self):
        """

        :return:
        """
        url = yield self.get_video_url()
        raise gen.Return(bool(url))

    # roles
    def is_owner(self, user):
        """

        :param user:
        :return:
        """
        return self.owner_id == user.id or self.reporter_id == user.id

    def is_reporter(self, user):
        """

        :param user:
        :return:
        """
        return self.reporter_id == user.id

    def is_master(self, user):
        """

        :param user:
        :return:
        """
        return self.master_id == user.id

    def is_observer(self, user):
        """

        :param user:
        :return:
        """
        return self.observer_id == user.id

    def observe_check_token(self, token):
        """

        :param token:
        :return:
        """
        return self.observer_token == token

    # observe
    def can_observe(self, observer_id):
        """

        :param observer_id:
        :return:
        """
        if self.observer_id:
            can = self.observer_id == observer_id
        else:
            can = self.reporter_id != observer_id
        if not can:
            return can
        elif not self.is_active():
            can = False
        return can

    def can_stop_observe(self, observer_id):
        """

        :param observer_id:
        :return:
        """
        if not self.observer_id:
            return False
        else:
            can = self.observer_id in (observer_id, self.reporter_id)
        if not can:
            return can
        else:
            can = not self.observer_close_ts
        return can

    @gen.coroutine
    def observe(self, observer_id):
        """
        Start Observe stream.

        :param observer_id: str Observer ID.
        :return:
        """
        if not self.can_observe(observer_id):
            return
        if not self.observer_id:
            self.observer_id = observer_id
            self.observer_start_ts = self.now_YmdHMS()
        self.observer_close_ts = None
        r = yield self.save()
        observer, reporter, data, subscribers = yield [self.get_observer(), self.get_reporter(),
                                                       self.get_data(), self.get_subscribers()]
        users = [reporter] + list(subscribers)
        s().e('video:observe_begin', sender=observer, notify_sender=True, users=users, data=data)
        raise gen.Return(r)

    @gen.coroutine
    def stop_observe(self, observer_id):
        """
        Stop Observe stream.

        :param observer_id: str Observer ID.
        :return:
        """
        if not self.can_stop_observe(observer_id):
            return
        self.observer_close_ts = self.now_YmdHMS()
        r = yield self.save()
        # yield observer.idle(r)
        observer, reporter, data, subscribers = yield [self.get_observer(), self.get_reporter(),
                                                       self.get_data(), self.get_subscribers()]
        users = [reporter] + list(subscribers)
        s().e('video:observe_end', sender=observer, notify_sender=True, users=users, data=data)
        raise gen.Return(r)

    @gen.coroutine
    def set_screenshot_keys(self, screenshot_keys):
        """

        :param screenshot_keys:
        :return:
        """
        if not self.screenshot_keys:
            self.screenshot_keys = screenshot_keys.copy()
        else:
            self.screenshot_keys = dict(self.screenshot_keys, **screenshot_keys)

        video = yield self.save()
        raise gen.Return(video)

    @gen.coroutine
    def set_media_urls(self, media_urls):
        """

        :param media_urls:
        :return:
        """
        if not self.media_urls:
            self.media_urls = media_urls.copy()
        else:
            self.media_urls = dict(self.media_urls, **media_urls)

        if 'ri' in media_urls:
            self.record_uploaded = True

        video = yield self.save()
        raise gen.Return(video)

    def get_screenshot_urls(self):
        """
        @TODO: path in return depends on storage.

        :return:
        """
        uploader = get_uploader()
        screenshot_keys = self.screenshot_keys
        if screenshot_keys is None:
            return {}
        keys = ['screen1', 'screen2', 'screen3']
        host = uploader.get_nearest_host('127.0.0.1')
        return {key: uploader.get_url(screenshot_keys[key], host=host)
                for key in keys if screenshot_keys.get(key)}

    def get_media_urls(self):
        """

        :return:
        """
        uploader = get_uploader()
        host = uploader.get_nearest_host('127.0.0.1')
        return {key: uploader.get_url(self.media_urls[key], host=host)
                for key in self.media_urls if self.media_urls[key]}

    @gen.coroutine
    def comment_add(self, user, body, ts=None):
        """

        :param user:
        :param body:
        :param ts:
        :return:
        """
        can_comment = yield self.can_comment(user.id)
        if not can_comment:
            return

        comment = Comment(dict(video_id=self.id,
                               user_id=user.id,
                               body=body,
                               modify_ts=self.now_YmdHMS() if not ts else ts))
        comment = yield comment.save()
        self.comment = comment
        self.comment.user = user
        self.count_comments += 1
        yield self.save()
        reporter, subscribers = yield [self.get_reporter(), self.get_subscribers()]
        users = [reporter] + list(subscribers)
        method = "video:comment_add"
        data = self.comment.get_data()
        s().e(method, sender=user, notify_sender=True, users=users, data=data)
        s().h(method, data, user.id, reporter.id)
        raise gen.Return(comment)

    @gen.coroutine
    def can_comment(self, current_user_id):
        """

        :param current_user_id: str User ID.
        :return: bool.
        """
        User = s().M.model('User')

        reporter = yield User.get_one(_id=self.reporter_id)
        if not reporter:
            raise gen.Return(False)
        if reporter.user_type is USER_TYPE.system:
            raise gen.Return(True)

        if self.scope == s().get_scope("wedcam"):
            Wedding = s().M.model('Wedding')
            can_comment = yield Wedding.can_comment_video(self, current_user_id)
            raise gen.Return(can_comment)

        raise gen.Return(True)

    @gen.coroutine
    def comment_delete(self, comment_id):
        """

        :param comment_id:
        :return:
        """
        Comment = s().M.model('Comment', self.ctx)
        comment = yield Comment.get_one(_id=comment_id)
        if not comment:
            return
        yield comment.delete()
        self.comment = comment
        self.comment.user = self.ctx.request_user
        self.count_comments -= 1
        yield self.save()
        owner, subscribers = yield [self.get_owner(), self.get_subscribers()]
        users = [owner] + list(subscribers)
        s().e('video:comment_delete', sender=self.comment.user, notify_sender=True, users=users,
              data=self.comment.get_data())
        return

    @gen.coroutine
    def comment_recent(self, offset=0, limit=10):
        """

        :param offset:
        :param limit:
        :return:
        """
        comments, has_more = yield Comment.get_recent(self.id, offset=offset, limit=limit)
        raise gen.Return((comments, has_more))

    @classmethod
    @gen.coroutine
    def comment_multiple(cls, videos=None, offset=0, limit=10):
        """

        :param videos:
        :param offset:
        :param limit:
        :return:
        """
        comment_count = 3
        comments = yield Comment.get_multiple(_ids=[v.id for v in videos], offset=offset, limit=limit)
        users = yield User.get(_ids=list({comment.user_id for comment in comments}))
        fullnames = {u.id: u for u in users}
        items = {}
        for comment in comments:
            if comment.user_id in fullnames:
                comment.user = fullnames[comment.user_id]
            else:
                comment.user = User(dict(fullname='DELETED'))
        for v in videos:
            v_comments = [{comment.id: comment.get_data()}
                          for comment in comments if comment.video_id == v.id][:comment_count]
            items[v.id] = {
                'comments': v_comments,
                'has_more': v.count_comments > len(v_comments)
            }

        raise gen.Return(items)

    @gen.coroutine
    def is_liked_or_disliked(self):
        """
        Checks if the video was liked or disliked by the current context user
        :return:
        """
        if not (self.ctx and self.ctx.request_user):
            raise gen.Return((False, False))
        vlike = yield VideoLike.check_exists(self.id, self.ctx.request_user.id)
        if vlike:
            raise gen.Return((vlike.positive, not vlike.positive))
        raise gen.Return((False, False))

    @classmethod
    @gen.coroutine
    def is_bookmarked_multiple(cls, videos):
        """

        :param videos:
        :return:
        """
        bookmarks = []
        if cls.ctx and cls.ctx.request_user:
            Bookmark = s().M.model('Bookmark', cls.ctx)
            bookmarks = yield Bookmark.get(user_id=cls.ctx.request_user.id, video_id__in=[v['id'] for v in videos])
        for v in videos:
            v['is_bookmarked'] = False
            if len(bookmarks):
                for v in videos:
                    v['is_bookmarked'] = False
                    for b in bookmarks:
                        if v['id'] == b.video_id:
                            v['is_bookmarked'] = True

        raise gen.Return(videos)

    @classmethod
    @gen.coroutine
    def is_liked_of_disliked_multiple(cls, videos):
        """

        :param videos:
        :return:
        """
        vlikes = []
        if cls.ctx and cls.ctx.request_user:
            vlikes = yield VideoLike.get(user_id=cls.ctx.request_user.id, video_id__in=[v['id'] for v in videos])
        for v in videos:
            v['is_liked'], v['is_disliked'] = False, False
            if len(vlikes):
                for v in videos:
                    for vlike in vlikes:
                        if v['id'] == vlike.video_id:
                            v['is_liked'], v['is_disliked'] = vlike.positive, not vlike.positive
        raise gen.Return(videos)

    @gen.coroutine
    def is_banned(self):
        """

        :return:
        """
        if not (self.ctx and self.ctx.request_user):
            raise gen.Return(False)
        ban = VideoBan.new(user_id=self.ctx.request_user.id, video_id=self.id)
        ban = yield VideoBan.get_one(_id=ban.custom_id)
        raise gen.Return(bool(ban))

    @gen.coroutine
    def is_bookmarked(self):
        """

        :return:
        """
        if not (self.ctx and self.ctx.request_user):
            raise gen.Return(False)
        Bookmark = s().M.model('Bookmark', self.ctx)
        b = Bookmark.new(user_id=self.ctx.request_user.id, video_id=self.id)
        b = yield Bookmark.check_exists(_id=b.custom_id)
        raise gen.Return(b)

    @classmethod
    @gen.coroutine
    def set_coords(cls, video_id, lat, lon, time, seconds, gps_type, current_user_id, token=None):
        """
        Set GPS location.

        :param: lat: float latitude.
        :param: lon: float longitude.
        :param: time: int time since last set_gps().
        :param: seconds: int current video duration.
        :param: gps_type: int GPS source type (network or satellite, see ``~.enums.GPS_TYPES``).
        :param: current_user_id: str current User ID.
        :param: token: str reporter token.
        :return: Video object.
        """
        Video = s().M.model('Video', cls.ctx)
        v = yield Video.get_one(_id=video_id, _deleted=False)
        if not v:
            return
        else:
            if token and not v.check_reporter_token(token) or v.reporter_id != current_user_id:
                return
            gps_location = dict(lat=lat, lon=lon)
            from api_engine.enums import OFFICE_LOCATION, SAN_FRANCISCO_LOCATION
            from reelcam.utils import distance
            if distance(gps_location, OFFICE_LOCATION) <= 200:
                gps_location = SAN_FRANCISCO_LOCATION
            v = yield v.set_gps(gps_location, time, seconds, gps_type)
            raise gen.Return(v)

    @gen.coroutine
    def set_gps(self, gps_location, time, seconds, gps_type):
        """
        Set GPS location.

        :param gps_location: dict {'lat': 33.66, 'lon': 66.33}.
        :param time: int time since last set_gps().
        :param seconds: int current video duration.
        :param gps_type: int GPS source type (network or satellite, see ``~.enums.GPS_TYPES``).
        :return: Video object.
        """
        if not check_latlon(**gps_location):
            raise gen.Return(self)
        self.gps_location = gps_location
        coord = dict(
            gps_location=gps_location,
            time=time,
            seconds=seconds,
            gps_type=gps_type,
            modify_ts=self.now_YmdHMS()
        )
        coords = list(self.coords)
        coords.append(coord)
        self.coords = coords
        v, reporter, subscribers = yield [self.save(), self.get_reporter(), self.get_subscribers()]
        users = list(subscribers)
        data = dict(video_id=self.id, **self.get_gps_location())
        s().e('video:set_gps', sender=reporter, notify_sender=True, users=users, data=data)
        raise gen.Return(v)

    @gen.coroutine
    def set_gps_bulk(self, coords):
        """

        :param coords:
        :return:
        """
        list([c.update(gps_location=dict(lat=c.pop('lat'), lon=c.pop('lon')), gps_type=c.pop('type')) for c in coords])
        self.coords.extend(coords)
        yield self.save()

    @gen.coroutine
    def get_coords(self):
        """

        :return:
        """
        coord = lambda: {
            'lat': None,
            'lon': None,
            'second': None,
            'type': None,
            'seconds': None,
            'ts': None,
        }

        coords = []
        video_coords = yield VideoGpsEntryMappingType.get_by_video_id(self.id)
        for c in (self.coords or video_coords):
            coords.append(coord())
            if self.coords:
                coords[-1].update(c.get('gps_location', {}),
                                  second=c.get('time'),
                                  type=c.get('gps_type'),
                                  seconds=c.get('seconds'),
                                  ts=self.ts_from_YmdHMS(c.get('modify_ts')))
            else:
                coords[-1].update(c)

        if not coords:
            coords.append(coord())
            coords[-1].update(self.gps_location, second=0)

        if self.gps_incognito:
            list([c.update(distort_location(c, s().gps.incognito_max_radius)) for c in coords])

        raise gen.Return(coords)

    @gen.coroutine
    def users_get(self):
        """
        Returns users who have been added to the list by owner and NOT deleted.

        :return:
        """
        qs = VideoUsersEntryMappingType.get(video_id=self.id)
        count = yield qs.count()
        result = yield qs[:count].execute()
        ids = [r.user_id for r in result]
        if ids:
            users = yield User.get(_ids=ids, _deleted=False, limit=len(ids))
        else:
            users = []
        raise gen.Return(users)

    def users_add(self, user_id):
        """
        Add user to video users list
        :param user_id:
        :return:
        """
        VideoUsersEntryMappingType.add(user_id, self.id)

    def screenshots_uploaded(self):
        """
        partial checks
        :return:
        """
        return bool(self.screenshot_keys)

    def video_uploaded(self):
        """

        :return:
        """
        return any([self.media_urls.get('r'), self.media_urls.get('ri')])

    @classmethod
    @gen.coroutine
    def search(cls, data):
        """
        Data:
        :param query:
        :param start_ts:
        :param end_ts:
        :param location:
        :param video_scope:
        :param user_scope:
        :param radius:
        :return:
        """
        if 'location' in data:
            location = data['location']
            if 'lng' in location:
                location['lon'] = location.pop('lng')
            if not location.get('lat') or not location.get('lon'):
                raise gen.Return([])
        results = cls.mapper.search(scope=cls.default_scope, **data)
        results = cls.mapper.f_country_code(results)
        current_user = cls.ctx.request_user if cls.ctx else None
        results = yield cls.privacy_filter(results, current_user)
        offset = data.get('offset', 0)
        limit = data.get('limit', 10)
        res = yield results[offset:limit + offset].execute()
        raise gen.Return([cls(r) for r in res])

    @gen.coroutine
    def new_view(self, user_id=None):
        """
        Increase views counter for video.

        :param user_id:
        :return:
        """
        self.count_views += 1
        yield self.save()
        if user_id is None:
            return
        v = yield VideoView.get(user_id=user_id, video_id=self.id)
        if v:
            raise gen.Return(v.pop())
        else:
            v = VideoView(user_id=user_id, video_id=self.id, modify_ts=self.now_YmdHMS())
            yield v.save()
            # self.count_views += 1
            # self.save()
        raise gen.Return(v)

    # kill lost
    @classmethod
    @gen.coroutine
    def get_lost_paused(cls):
        """

        :return:
        """
        qs = cls.mapper.get_lost_paused()
        count = yield qs.count()
        results = yield qs[:count].execute()
        videos = [cls(r) for r in results]
        raise gen.Return(videos)

    @classmethod
    @gen.coroutine
    def get_lost_active(cls):
        """

        :return:
        """
        qs = cls.mapper.get(status=VIDEO_STATUS.active)
        count = yield qs.count()
        results = yield qs[:count].execute()
        videos = [cls(r) for r in results]
        lost_active = []
        _now = cls.now_dt()
        delta = timedelta(minutes=1)
        for v in videos:
            last_r_chunk_upload_time = yield v.get_last_r_chunk_upload_time()
            if (not last_r_chunk_upload_time and cls.dt_from_YmdHMS(v.last_pause_time) < (_now - delta)) \
                    or last_r_chunk_upload_time and last_r_chunk_upload_time < (_now - delta):
                lost_active.append(v)
        raise gen.Return(lost_active)

    @classmethod
    @gen.coroutine
    def kill_lost(cls):
        """
        Finish lost videos.

        :return:
        """
        lost_paused, lost_active = yield [cls.get_lost_paused(), cls.get_lost_active()]
        if lost_paused:
            for video in lost_paused:
                yield video.finish()
                log.info('KILL LOST: \'lost_paused\' VIDEO %s was finished.', video.id)
        if lost_active:
            for video in lost_active:
                yield video.finish()
                log.info('KILL LOST: \'lost_active\' VIDEO %s was finished.', video.id)
        return

    @gen.coroutine
    def r_chunk_uploaded(self, upload):
        """
        Callback when r chunk uploaded
        if it is first chunk of live video -> emit event about it

        :param upload (instance): upload instance
        :return:
        """
        from api_engine.enums import REELCAM
        self.set_last_r_chunk_upload_time(self.now_dt())

        if upload.part_num == 1:
            reporter, data, allowed_users, subscribers = yield [self.get_reporter(), self.get_data(),
                                                                self.get_allowed_users(), self.get_subscribers()]
            push_data = dict(id=self.id, video_id=self.id, subject=self.subject)
            s().e('video:create', sender=reporter, notify_sender=True, users=list(subscribers + allowed_users),
                  data=data)
            s().e('video:create', sender=reporter, users=list(subscribers + allowed_users), data=push_data,
                  push=True, body_loc_key="txt_push_new_video_started", body_loc_args=[reporter.fullname],
                  user_img=reporter.get_avatar_url(size="big"), websocket=False,
                  app_name=getattr(self.ctx, 'app_name', REELCAM))
            h_data = dict(data, scope=self.scope)
            for u in allowed_users:
                s().h('video:create', h_data, self.reporter_id, u.id)
        raise gen.Return(self)

    @gen.coroutine
    def get_last_r_chunk_upload_time(self):
        """
        return datetime or None

        :return:
        """
        cache_key = self.get_last_r_chunk_upload_time_key()
        # get ts and convert to datetime
        ts = yield s().kvs.call("GET", str(cache_key))
        raise gen.Return(self.dt_from_YmdHMS(ts) if ts else None)

    def set_last_r_chunk_upload_time(self, dt):
        """

        :param dt:
        :return:
        """
        cache_key = self.get_last_r_chunk_upload_time_key()
        s().kvs.async_call("SET", cache_key, self.YmdHMS_from_dt(dt))
        s().kvs.async_call("EXPIRE", cache_key, s().ttl.session)

    def get_last_r_chunk_upload_time_key(self):
        """

        :return:
        """
        return self.KS.join([self.get_key_prefix(), 'R_CHUNK_UPLOAD_TIME'])

    def get_key_prefix(self):
        """

        :return:
        """
        return self.KS.join([s().elastic.default_scope, 'VIDEO', self.id])

    @gen.coroutine
    def start_view(self, user_id):
        """

        :param user_id:
        :return:
        """
        RTView = s().M.model('RTView', self.ctx)
        view = RTView(dict(video_id=self.id, user_id=user_id))
        # @TODO: removing id attr to index document instead of updating later in cls.mapper.save()
        view.custom_id
        view.id = None

        v = yield RTView.get_one(_id=view.custom_id)
        if v:
            return
        self.count_watchers += 1
        r, _ = yield [view.save(), self.save()]
        data = {
            'id': self.id,
            'video_id': self.id,
            'count_watchers': self.count_watchers
        }
        reporter, subscribers = yield [self.get_reporter(), self.get_subscribers()]
        users = [reporter] + list(subscribers)
        s().e('video:start_view', sender=self.ctx.request_user, notify_sender=True, users=users, data=data)
        raise gen.Return(r)

    @gen.coroutine
    def stop_view(self, user_id):
        """

        :param user_id:
        :return:
        """
        RTView = s().M.model('RTView', self.ctx)
        view = RTView(dict(video_id=self.id, user_id=user_id))
        view = yield RTView.get_one(_id=view.custom_id)
        r = None
        if view:
            self.count_watchers -= 1
            r, _ = yield [view.delete(view.id), self.save()]
            data = {
                'id': self.id,
                'count_watchers': self.count_watchers
            }
            reporter, subscribers = yield [self.get_reporter(), self.get_subscribers()]
            users = [reporter] + list(subscribers)
            s().e('video:stop_view', sender=self.ctx.request_user, notify_sender=True, users=users, data=data)
        raise gen.Return(r)

    @gen.coroutine
    def live_msg(self, msg):
        """

        :param msg:
        :return:
        """
        LiveMsg = s().M.model('LiveMsg', self.ctx)
        u = self.ctx.request_user
        seconds = self.now_ts() - self.ts_from_YmdHMS(self.reporter_start_ts)
        m = LiveMsg(dict(
            user_id=u.id,
            video_id=self.id,
            message=msg,
            seconds=seconds,
        ))
        reporter, subscribers, _ = yield [self.get_reporter(), self.get_subscribers(), m.save()]
        users = [reporter] + list(subscribers)
        data = dict(message=msg, user=u.get_idts(), video_id=self.id, seconds=seconds)
        s().e('video:live_msg', sender=self.ctx.request_user, notify_sender=True, data=data, users=users)

    @gen.coroutine
    def get_allowed_users(self):
        """
        Returns list of allowed to view users.

        :return: list of User objects.
        """
        User = s().M.model('User')
        Wedding = s().M.model('Wedding')

        users = []
        if self.permission is VIDEO_PERMISSIONS.list:
            raise gen.Return(users)

        reporter = yield self.get_reporter()
        if self.wedding_id:
            wedding = yield Wedding.get_one(_id=self.wedding_id)
            if wedding and self.reporter_id in [wedding.from_id, wedding.to_id]:
                wedcam_followers = yield wedding.get_followers()
                for user in wedcam_followers:
                    if user.id not in reporter.followers:
                        users.append(user)

        if reporter.followers:
            followers = yield User.get(_ids=reporter.followers, _deleted=False)
            users.extend(followers)

        raise gen.Return(users[:self.MAX_EVENT_RECIPIENTS_COUNT])

    @gen.coroutine
    def set_not_deleted(self):
        """

        :return:
        """
        self.visible = True
        self.is_deleted = False
        self.delete_status = VIDEO_DELETE_STATUS.not_deleted
        r = yield self.save()
        raise gen.Return(r)

    @gen.coroutine
    def set_deleted(self):
        """

        :return:
        """
        self.visible = False
        self.is_deleted = True
        self.delete_status = VIDEO_DELETE_STATUS.deleted
        r = yield self.save()
        raise gen.Return(r)

    def hide_in_history(self):
        """

        :return:
        """
        History = s().M.model('History', self.ctx)
        from tornado.ioloop import IOLoop
        IOLoop.current().spawn_callback(History.hide_by_video_id, self.id)

    def unhide_in_history(self):
        """

        :return:
        """
        History = s().M.model('History', self.ctx)
        from tornado.ioloop import IOLoop
        IOLoop.current().spawn_callback(History.unhide_by_video_id, self.id)

    def is_video_visible(self):
        """

        :return:
        """
        checks = (
            self.is_finished() and (
                self.duration < s().video.min_duration or
                self.filesize < s().video.min_filesize or
                not self.video_uploaded()
            ),
            not self.screenshots_uploaded(),
            self.is_deleted
        )
        return not any(checks)

    @classmethod
    @gen.coroutine
    def get_owned_by_count(cls, user_id):
        """

        :param user_id:
        :return:
        """
        f = cls.mapper.f_owner_id(user_id)
        f &= cls.mapper.f_tabs()
        count = yield cls.count(_filter=f)
        raise gen.Return(count)

    def mediastorage_key(self, filename, ext='flv'):
        """

        :param filename:
        :param ext:
        :return:
        """
        return '/'.join(['media', self.reporter_id, self.id, '.'.join([filename, ext])])

    def get_url(self):
        """

        :return:
        """
        return "http://%s/#/video/%s" % (s().domain, self.id)

    @classmethod
    @gen.coroutine
    def finish_previous(cls, user_id, created_video_id):
        """
        Finish previous user videos if it doesn't finished yet.

        :param user_id:
        :param created_video_id:
        :return:
        """
        qs = cls.mapper.get_not_finished(user_id)
        result = yield qs.execute()
        videos = [cls(r) for r in result]
        yield [v.finish() for v in videos if v.id != created_video_id]

    @property
    def meta(self):
        """
        Returns video metadata object.

        :return:
        """
        return self.metadata_class(self.duration,
                                   self.filesize)

    @meta.setter
    def meta(self, meta):
        """
        A video metadata setter.

        :param meta:
        :return:
        """
        assert isinstance(meta, self.metadata_class)
        self.duration = float(meta.duration)
        self.filesize = int(meta.filesize)

    @gen.coroutine
    def set_meta(self, meta):
        """
        Sets up video metadata.

        :param meta:
        :return:
        """
        self.meta = meta
        video, reporter = yield [self.save(), self.get_reporter()]
        reporter.total_video_duration, _ = yield [self.mapper.get_total_duration(reporter.id), reporter.save()]
        raise gen.Return(video)

    def is_public(self):
        """

        :return:
        """
        return self.permission is VIDEO_PERMISSIONS.all

    @classmethod
    @gen.coroutine
    def get_mine(cls, current_user_id, limit=10, offset=0):
        """
        Get mine Videos.

        :param current_user_id: str current User ID.
        :param limit: int results limit.
        :param offset: int results offset.
        :return: dict {"items": [{video.id: video.get_data()}, ...]}.
        """
        User = s().M.model('User')

        current_user = yield User.get_one(_id=current_user_id)

        qs = cls.mapper.get(reporter_id=current_user_id)
        if cls.default_scope:
            qs = cls.mapper.get(reporter_id=current_user_id, scope=cls.default_scope)

        if current_user.user_type is USER_TYPE.wedcam_trend:
            f = cls.mapper.f_tabs(is_photo=True) & cls.mapper.f_finished()
        else:
            platform = cls.ctx.platform if getattr(cls, 'ctx', None) else None
            f = cls.mapper.f_not_live(platform)
        qs = qs.filter(f)

        if cls.default_scope == s().get_scope("wedcam"):
            qs = cls.wedcam_filter(qs, current_user)

        count, results = yield [qs.count(),
                                qs.order_by('-reporter_start_ts')[offset:offset+limit].execute()]
        has_more = count > offset + limit
        videos = [cls(r) for r in results]
        items = [dict(id=vid.id,
                      ts=cls.ts_from_YmdHMS(vid.modify_ts)) for vid in videos]
        raise gen.Return(dict(has_more=has_more, items=items))

    @classmethod
    @gen.coroutine
    def x_get_mine(cls, current_user_id, limit=10, offset=0):
        """
        Get mine Videos.

        :param current_user_id: str current User ID.
        :param limit: int results limit.
        :param offset: int results offset.
        :return: dict {"items": [{video.id: video.get_data()}, ...]}.
        """
        User = s().M.model('User')

        current_user = yield User.get_one(_id=current_user_id)

        qs = cls.mapper.get(reporter_id=current_user_id)
        if cls.default_scope:
            qs = cls.mapper.get(reporter_id=current_user_id, scope=cls.default_scope)

        if current_user.user_type is USER_TYPE.wedcam_trend:
            f = cls.mapper.f_tabs(is_photo=True) & cls.mapper.f_finished()
        else:
            platform = cls.ctx.platform if getattr(cls, 'ctx', None) else None
            f = cls.mapper.f_not_live(platform)
        qs = qs.filter(f)

        if cls.default_scope == s().get_scope("wedcam"):
            qs = cls.wedcam_filter(qs, current_user)

        count, results = yield [qs.count(),
                                qs.order_by('-reporter_start_ts')[offset: offset + limit].execute()]

        videos = [cls(r) for r in results]
        reporter_ids = []
        items = []

        comments = yield Comment.get_multiple(_ids=[v.id for v in videos], offset=offset, limit=30)
        yield cls._join_users_to_comments(comments)

        for v in videos:
            reporter_ids.append(v.reporter_id)
        reporters = yield User.get(_ids=reporter_ids)

        for v in videos:
            reporter = None
            v_data = yield v.get_data(x_full=True)
            for x in reporters:
                if x.id == v.reporter_id:
                    reporter = x
            if reporter:
                v_data['reporter'] = yield reporter.get_data(x_full=True)
            if v.count_comments > 0:
                for c in comments:
                    if c.video_id == v.id:
                        v_data['comments'].append(c.get_data())
            items.append(v_data)
        yield [cls.is_liked_of_disliked_multiple(items), cls.is_bookmarked_multiple(items)]

        has_more = count > offset + limit
        raise gen.Return((items, has_more))

    @classmethod
    @gen.coroutine
    def get_favorites(cls, current_user_id, limit=10, offset=0):
        """
        Get Favorite Videos.

        :param current_user_id: str current User ID.
        :param limit: int results limit.
        :param offset: int results offset.
        :return: dict {"items": [{video.id: video.get_data()}, ...]}.
        """
        Bookmark = s().M.model('Bookmark', cls.ctx, scope=cls.default_scope)

        bookmarks, has_more = yield Bookmark.get_mine(current_user_id, limit, offset)

        ids = [b.video_id for b in bookmarks]
        videos = yield cls.get(_ids=ids)

        items = [v.get_idts() for b in bookmarks for v in videos if (b.video_id == v.id)]

        if cls.default_scope == s().get_scope(WEDCAM) and s().environment == "PRODUCTION":
            wedcam_help = dict(id='AVPMUfbVkZn4AuKbybzv', ts=1459422188)
            if not offset:
                items = [wedcam_help] + items[:-1]
            else:
                items = items[:-1] + [wedcam_help]

        favorites = dict(items=items, has_more=has_more)

        raise gen.Return(favorites)

    @classmethod
    def wedcam_filter(cls, qs, current_user):
        """

        :param qs:
        :param current_user: User object.
        :return:
        """
        from api_engine.enums import USER_TYPE

        if current_user.user_type is USER_TYPE.wedcam_operator:
            from datetime import timedelta
            reporter_start_ts = cls.now_dt() - timedelta(seconds=cls.WEDCAM_OPERATOR_START_TS)
            qs = qs.filter(reporter_start_ts__gte=cls.YmdHMS_from_dt(reporter_start_ts))

        return qs

    @gen.coroutine
    def upload_photo(self, photo):
        """

        :param photo: str binary image
        :return: Video object.
        """
        from api_engine.mediahelper import MediaHelper
        from reelcam.uploader import get_uploader
        if not check_photo(photo):
            return
        fmt = MediaHelper.get_image_format(photo)
        cropped_fp = MediaHelper.crop_preview(photo, fmt)
        if fmt == "PNG":
            ext = "png"
        else:
            ext = "jpg"
        key = self.mediastorage_key('screen1', ext=ext)
        get_uploader().upload(key, cropped_fp)
        get_uploader().set_acl(key, 'public-read')
        video = yield self.set_screenshot_keys(dict(screen1=str(key)))
        raise gen.Return(video)


def check_photo(photo):
    """

    :param photo:
    :return:
    """
    from io import StringIO
    from PIL import Image
    from api_engine.mediahelper import MediaHelper
    fmt = MediaHelper.get_image_format(photo)
    if fmt not in ['PNG', 'JPEG']:
        return False
    img = Image.open(StringIO(photo))
    w, h = img.size
    max_w, max_h = 1920, 1080
    if w > max_w or h > max_h:
        return False
    return True
