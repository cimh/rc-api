# -*- coding: utf-8 -*-
from .base import BaseModel
from .bookmark import Bookmark
from .chat import Chat
from .comment import Comment
from .feedback import Feedback
from .follow import Follow
from .history import History
from .message import Message
from .observe.observe import Observe
from .proxy import Proxy
from .hbquest.quest import Quest
from .subscription import Subscription
from .hbquest.template import Template
from .user import User
from .wsconnection import WSConnection
from .video.video import Video
from .video.ban import VideoBan
from .video.like import VideoLike
from .video.view import VideoView
from .video.livemsg import LiveMsg
from .video.rtview import RTView
from .hbquest.template import Template
from .hbquest.quest import Quest
from .h import H
from .wedcam.wedding import Wedding
from .wedcam.invite import Invite
from .wedcam.confirmation import Confirmation
from .wedcam.trend import Trend
from .promo import Promo
from .upload import Upload
from utils import all_subclasses


class M(object):
    """
    M class.
    """

    _base_model = BaseModel
    # dict of _base_model subclasses.
    _models = {}

    @classmethod
    def model(cls, model_name, ctx=None, scope=None):
        """

        :param model_name:
        :param ctx:
        :param scope:
        :return:
        """
        if not cls._models:
            models = all_subclasses(cls._base_model)
            models = {m.__name__: m for m in models}
            cls._models = models

        cls_dict = dict(ctx=ctx)
        if scope:
            cls_dict.update(default_scope=scope)

        try:
            model = cls._models[model_name]
        except KeyError:
            raise NameError('Unknown model name \'%s\'' % model_name)
        else:
            return type(model.__name__, (model,), cls_dict)
