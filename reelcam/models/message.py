# -*- coding: utf-8 -*-
from tornado import gen
from api_engine import s
from api_engine.enums import USER_TYPE
from .base import BaseModel
from ..mappers.message import MessageEntryMappingType


class Message(BaseModel):
    """
    Message Model.
    """

    mapper = MessageEntryMappingType

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=str(),
                    chat_id=str(),
                    _from=str(),
                    _to=str(),
                    body=str(),
                    created_at=str(),
                    is_read=False,
                    delete_by_from=False,
                    delete_by_to=False)

    def get_data(self):
        """

        :return:
        """
        return dict(id=self.id,
                    chat_id=self.chat_id,
                    user_id=self._from,
                    body=self.body,
                    is_read=self.is_read,
                    ts=self.ts_from_YmdHMS(self.created_at))

    @classmethod
    @gen.coroutine
    def get_recent(cls, chat_id, offset=0, limit=10):
        """

        :param chat_id:
        :param offset:
        :param limit:
        :return:
        """
        User = s().M.model('User', cls.ctx)
        current_user = cls.ctx.request_user
        if cls.ctx.request_user.user_type is USER_TYPE.support:
            u = yield User.get_system()
            system_user_id = u.id
            raw = cls.mapper.get_recent(chat_id, current_user_id=system_user_id)
            result = yield raw[offset: offset+limit].execute()
            messages = [cls(r) for r in result]
            # replace system id with support id
            for i in messages:
                if i._from == system_user_id:
                    i._from = current_user.id
                if i._to == system_user_id:
                    i._to = current_user.id
        else:
            system_user_id = cls.ctx.request_user.id if cls.ctx.request_user else None
            raw = cls.mapper.get_recent(chat_id, current_user_id=system_user_id)
            result = yield raw[offset: offset+limit].execute()
            messages = [cls(r) for r in result]
        count = yield raw.count()
        raise gen.Return((messages, count > offset+limit))

    @classmethod
    @gen.coroutine
    def get_unread_count_by_user(cls, user):
        """

        :param user_id:
        :return:
        """
        User = s().M.model('User', cls.ctx)
        if user.user_type is USER_TYPE.support:
            user = yield User.get_system()
        result = yield cls.mapper.get_unread_count_by_user_id(user.id)
        raise gen.Return(result)

    @gen.coroutine
    def delete(self, user):
        """

        :param user:
        :return:
        """
        User = s().M.model('User', self.ctx)
        if user.user_type is USER_TYPE.support:
            user = yield User.get_system()
        if (yield self.is_author(user)):
            self.delete_by_from = True
        else:
            self.delete_by_to = True
        up = yield self.save()
        raise gen.Return(up)

    @gen.coroutine
    def is_author(self, user):
        """

        :param user:
        :return:
        """
        User = s().M.model('User', self.ctx)
        if user.user_type is USER_TYPE.support:
            user = yield User.get_system()
        raise gen.Return(self._from == user.id)

    @gen.coroutine
    def get_chat(self):
        """

        :return:
        """
        Chat = s().M.model('Chat', self.ctx)
        r = yield Chat.get_one(_id=self.chat_id)
        raise gen.Return(r)

    def get_url(self):
        """

        :return:
        """
        return "http://%s/#/chat/%s" % (s().domain, self.chat_id)
