# -*- coding: utf-8 -*-
import random
import re
from datetime import timedelta
from tornado import gen
from api_engine import s
from api_engine.auth import Auth
from api_engine.userpic import Userpic
from api_engine.enums import VIDEO_PERMISSIONS, USER_DELETE_STATUS, USER_STATUS, USER_TYPE
from reelcam.utils import break_to_name_and_index, default_location
from .mixins.event import EventMixin
from reelcam.uploader import get_uploader
from .base import BaseModel, BaseGPS, BaseFeedback
from .mixins.wsclient import WsClient
from .observe.observer import Observer
from ..mappers.user import UserEntryMappingType


class User(Observer, WsClient, BaseFeedback, BaseGPS, BaseModel, EventMixin):
    """
    User model.
    """
    mapper = UserEntryMappingType

    can_subscribe = True
    can_subscribed = True

    key_prefix = 'USER'

    def __init__(self, args=None, **kwargs):
        """

        :param args:
        :param kwargs:
        :return:
        """
        BaseModel.__init__(self, args, **kwargs)
        Observer.__init__(self, self.id)
        self.client_id = self.id

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(app_name=None,
                    birthday=None,
                    count_bans=int(),
                    count_dislikes=int(),
                    count_likes=int(),
                    country=str(),
                    country_code=str(),
                    date_joined=None,
                    default_video_permission=VIDEO_PERMISSIONS.all,
                    delete_status=USER_DELETE_STATUS.not_deleted,
                    email=str(),
                    first_name=str(),
                    follow_approve=False,
                    followers=[],
                    followed=[],
                    fullname=str(),
                    gadget=False,
                    gender=str(),
                    gift_wedding_id=None,
                    gps_incognito=False,
                    gps_location=default_location(),
                    gps_ts=int(),
                    gps_type=int(),
                    id=str(),
                    is_active=True,
                    is_demo=False,
                    is_deleted=False,
                    is_online=True,
                    is_sms_validated=False,
                    is_staff=False,
                    joined_ts=str("2014-01-01 00:00:00"),
                    joined_wedding_id=None,
                    language=str(),
                    language2=str(),
                    last_login=None,
                    last_name=str(),
                    modify_ts=str(),
                    password=str(),
                    phone=None,
                    photo=str(),
                    photo_keys=None,
                    rating=int(),
                    region=str(),
                    sms_codes=[],
                    social=None,
                    social_auth=None,
                    status=USER_STATUS.idle,
                    subscription=None,
                    total_video_duration=int(),
                    user_type=USER_TYPE.user,
                    citydoom_rating=int(),
                    citydoom_rank=str(),
                    wedding_id=None,
                    gcm_tokens=[])

    @gen.coroutine
    def get_data(self, check_follow=True, x_full=False):
        """

        :return:
        """
        if self.is_deleted or x_full:
            data = dict(id=self.id,
                        fullname=' '.join([self.fullname, '[DELETED]']) if self.is_deleted else self.fullname,
                        photo=self.get_photo_urls(),
                        is_deleted=self.is_deleted)
            if not x_full:
                raise gen.Return(data)
        else:
            gps_location = self.get_gps_location()
            data = dict(app_name=self.app_name,
                        birthday=self.get_birthday(),
                        count_bans=self.count_bans,
                        count_dislikes=self.count_dislikes,
                        count_likes=self.count_likes,
                        country=self.country,
                        date_joined=self.Ymd_from_dt(self.date_joined),
                        follow_approve=self.follow_approve,
                        fullname=self.fullname,
                        gadget=self.gadget,
                        gender=self.gender,
                        gps_incognito=self.gps_incognito,
                        gps_seconds=self.now_ts() - self.gps_ts,
                        gps_type=self.gps_type,
                        id=self.id,
                        is_deleted=self.is_deleted,
                        is_online=self.is_online,
                        language=self.language,
                        language2=self.language2,
                        lat=str(gps_location['lat']),
                        lon=str(gps_location['lon']),
                        rating=self.rating,
                        region=self.region,
                        status=self.status,
                        ts=self.ts_from_YmdHMS(self.modify_ts),
                        user_type=self.user_type,
                        citydoom_rating=self.citydoom_rating,
                        citydoom_rank=self.get_citydoom_rank(),
                        photo=self.get_photo_urls(),
                        wedding_id=self.wedding_id,
                        joined_wedding_id=self.joined_wedding_id,
                        gift_wedding_id=self.gift_wedding_id,
                        first_name=self.first_name,
                        last_name=self.last_name)
        if self.user_type is USER_TYPE.invite:
            data['fullname'] = self.phone
        if self.ctx and self.ctx.request_user and self.user_type != USER_TYPE.system:
            if self.ctx.request_user.id != self.id:
                if check_follow:
                    data['is_followed'], data['is_follower'] = yield [self.is_follower(),
                                                                      self.is_followed()]
            else:
                data['is_demo'] = self.is_demo
                data.update(dict(default_video_permission=self.default_video_permission,
                                 phone=self.phone,
                                 last_login=self.ts_from_YmdHMS(self.joined_ts),
                                 joined_ts=self.ts_from_YmdHMS(self.joined_ts)))
                if self.social:
                    data['social'] = list(self.social.keys())
                data['subscription'] = yield self.get_subscription_info()
        raise gen.Return(data)

    @gen.coroutine
    def login(self, password=None, gadget=False, social_auth=False, app_name=None):
        """

        :param password:
        :param gadget:
        :param social_auth:
        :param app_name:
        :return:
        """
        if not (social_auth or Auth.authenticate(self, password)):
            raise gen.Return(False)
        else:
            if hasattr(self, 'ctx') and self.ctx.session:
                request_user = yield self.get_one(_id=self.id)
                self.ctx.set_user(request_user)
                del request_user
                self.add_session(self.ctx.session)

        if gadget:
            self.gadget = True
            yield self.save()
        if self.ctx.session and s().environment is not 'TESTING':
            from api_engine.enums import TRY2CONNECT
            s().e(TRY2CONNECT, sender='SYS', users=[self], data=dict(data="try 2 connect"))
        raise gen.Return(True)

    @gen.coroutine
    def logout(self):
        """

        :return:
        """
        if hasattr(self, 'ctx') and self.ctx.session:
            self.remove_session(self.ctx.session)
            self.ctx.remove_session()

    def set_password(self, password):
        """

        :param password:
        :return:
        """
        self.password = Auth.make_password_hash(password)

    @classmethod
    @gen.coroutine
    def get_system(cls):
        """

        :return:
        """
        r = yield cls.get_one(user_type=USER_TYPE.system)
        raise gen.Return(r)

    def set_photo_keys(self, photo_keys):
        """

        :param photo_keys:
        :return:
        """
        if self.photo_keys is None:
            self.photo_keys = photo_keys
        else:
            self.photo_keys.update(photo_keys)
            self.photo_keys = self.photo_keys.copy()

    def get_photo_urls(self):
        """

        :return:
        """
        uploader = get_uploader()
        keys = self.photo_keys or {}
        host = None
        if hasattr(self, 'ctx') and self.ctx:
            host = uploader.get_nearest_host(self.ctx.client_ip)
        return {key: uploader.get_url(keys[key], host=host)
                for key in keys if keys[key]}

    @gen.coroutine
    def set_userpic(self, userpic):
        """

        :param userpic: str Userpic.
        :return: User object.
        """
        if self.photo_keys:
            for key in list(self.photo_keys.values()):
                Userpic.remove_userpic(key)

        keys = Userpic(self.id).add_userpic(userpic)
        if keys:
            self.set_photo_keys(keys)
            user = yield self.save()
            raise gen.Return(user)
        raise gen.Return(self)

    @gen.coroutine
    def sms_send_validation_code(self):
        """
        Send validation code to user.
        If user reached sms sending limit -> return False (not success).

        :return (bool): if sms was successfully sent
        """
        codes = self.get_unexpired_sms_codes()
        if self.sms_can_send(codes):
            code = dict(value=self.get_pincode(),
                        send_ts=self.now_YmdHMS())
            codes.append(code)
            self.sms_codes = codes
            user = yield self.save()
            self.invite(self.app_name, 'Code: ' + code['value'])
            raise gen.Return(user)

    def sms_can_send(self, codes):
        """

        :param codes:
        :return:
        """
        if len(codes) >= s().sms_gate.max_times_sent:
            return False
        if codes and (self.now_ts() - self.ts_from_YmdHMS(codes[-1]['send_ts'])) < s().sms_gate.next_send:
            return False
        return True

    @gen.coroutine
    def sms_validate(self, code_value):
        """

        :param code_value:
        :return:
        """
        codes = self.get_unexpired_sms_codes()
        if self.is_sms_validated or code_value not in [c['value'] for c in codes]:
            return
        self.is_sms_validated = True
        self.sms_codes = self.delete_sms_code(code_value)
        user = yield self.save()
        raise gen.Return(user)

    @classmethod
    def get_pincode(cls):
        """
        generate SMS code and save it to user
        :return:
        """
        return str(random.randrange(1000, 9999))

    def delete_sms_code(self, code_value):
        """
        SMS codes are disposable -> delete used SMS
        remove used code from available SMS codes (prevent double usage )
        :return:
        """
        codes = [i for i in self.sms_codes if i['value'] != code_value]
        return codes

    def get_unexpired_sms_codes(self):
        """
        Returns list of unexpired SMS codes
        by the way delete expired codes
        :return:
        """
        # remove expired sms codes
        codes = [i for i in self.sms_codes if
                 (self.dt_from_YmdHMS(i['send_ts']) + timedelta(hours=s().sms_gate.expire)) > self.now_dt()]
        # get unexpired sms codes
        return codes

    @gen.coroutine
    def save(self):
        """

        :return:
        """
        Video = s().M.model('Video', self.ctx)
        videos_count, videos = yield [Video.count(_filter=Video.mapper.f_owner_id(self.id)),
                                      Video.get(owner_id=self.id, _deleted=False)]
        views = sum((v.count_views for v in videos))
        self.rating = 3 * videos_count + views / 100 + \
                      2 * self.count_likes - \
                      self.count_dislikes - \
                      15 * self.count_bans
        self.modify_ts = self.now_YmdHMS()
        user = yield super(User, self).save()
        raise gen.Return(user)

    @gen.coroutine
    def delete(self):
        """

        :return:
        """
        Follow = s().M.model('Follow', self.ctx)
        Video = s().M.model('Video', self.ctx)
        self.set_deleted()
        yield [Follow.delete_user(self.id), Video.mapper._delete_user(self.id), self.save()]

    def set_deleted(self):
        """

        :return:
        """
        self.is_active = False
        self.is_deleted = True
        self.delete_status = USER_DELETE_STATUS.deleted

    def set_blocked(self):
        """

        :return:
        """
        self.is_active = False
        self.is_deleted = True
        self.delete_status = USER_DELETE_STATUS.blocked

    def set_not_deleted(self):
        """

        :return:
        """
        self.is_active = True
        self.is_deleted = False
        self.delete_status = USER_DELETE_STATUS.not_deleted

    def get_birthday(self):
        """

        :return:
        """
        if not self.birthday:
            return None
        d = str(self.birthday.day).zfill(2)
        m = str(self.birthday.month).zfill(2)
        y = str(self.birthday.year)
        return '/'.join((d, m, y))

    @gen.coroutine
    def is_followed(self, current_user=None):
        """

        :param current_user:
        :return:
        """
        if not current_user:
            if getattr(self.ctx, 'request_user'):
                current_user = self.ctx.request_user
            else:
                return
        raise gen.Return(current_user.id in self.followed)

    @gen.coroutine
    def is_follower(self, current_user=None):
        """

        :param current_user:
        :return:
        """
        if not current_user:
            if getattr(self.ctx, 'request_user'):
                current_user = self.ctx.request_user
            else:
                return
        raise gen.Return(current_user.id in self.followers)

    @classmethod
    @gen.coroutine
    def get_swiper(cls, offset=0, limit=10):
        """

        :param offset:
        :param limit:
        :return:
        """
        raw = cls.mapper.get_swiper()
        qs = cls.mapper.f_country_code(raw)
        count, results = yield [qs.count(), qs[offset:offset + limit].execute()]
        users = [cls(r) for r in results]
        items = []
        for u in users:
            user_data = yield u.get_data(check_follow=False)
            items.append({u.id: user_data})
        raise gen.Return((items, count > limit + offset))

    @classmethod
    @gen.coroutine
    def search(cls, data):
        """
        Data:
        :param query:
        :param start_ts:
        :param end_ts:
        :param location:
        :param video_scope:
        :param user_scope:
        :param radius:
        :return:
        """
        raw = cls.mapper.search(**data)
        raw = cls.mapper.f_country_code(raw)
        offset = data.get('offset', 0)
        limit = data.get('limit', 10)
        res = yield raw[offset:limit+offset].execute()
        raise gen.Return([cls(r) for r in res])

    @classmethod
    def is_valid_fullname(cls, fullname):
        """
        Check if fullname is valid
        allowed chars in fullname:
            - letters (any language)
            - digits
            - "-", "_", ".", " "

        :param fullname: user fullname
        :return:
        """
        return re.sub("[\s._-]", '', fullname).isalnum()

    @classmethod
    def is_allowed_fullname(cls, fullname):
        """
        Check if fullname is allowed by length and stop lists

        :param fullname:
        :return (bool): True if allowed
        """
        from utils.stop_list import STOP_LIST_FULL_MATCH #  , STOP_LIST_PARTIAL_MATCH
        # not allowed fullname less then 3 chars length
        if len(fullname) < 3:
            return False

        # fullname from STOP LIST not allowed
        for w in STOP_LIST_FULL_MATCH:
            if w == fullname.lower():
                return False

        # # fullname from STOP LIST not allowed
        # for w in STOP_LIST_PARTIAL_MATCH:
        #     if w in fullname.lower():
        #         return False
        return True

    @classmethod
    @gen.coroutine
    def check_exist_fullname(cls, fullname):
        """
        Check exist fullname

        :param: fullname (str): user fullname.

        :return: boolean: is exist user
        """
        r = yield cls.get(fullname=fullname.lower(), is_deleted=False)
        raise gen.Return(bool(r))

    @classmethod
    @gen.coroutine
    def get_available_fullname(cls, fullname):
        """
        Get available fullname with index like Vasya-Pupkin(1)

        :param: fullname: original user fullname
        :return (str): fullname with index
        """
        base_name, _ = break_to_name_and_index(fullname)

        users = yield cls.get(**{
            "fullname.base": base_name.lower(),
            "is_deleted": False
        })

        # get all postfixes from our db
        nums = set(int(break_to_name_and_index(_user.fullname)[1]) for _user in users)

        # get minimal available postfix
        # example
        # we have nums = {1, 2, 3, 95}
        # available postfix is 4, not 96
        # we need to find it
        nums_range = set(range(1, max(nums) + 2))
        diff = nums_range - nums
        new_num = min(diff)
        r = '%s(%d)' % (base_name, new_num)
        raise gen.Return(r)

    @classmethod
    def clean_phone(cls, phone):
        """
        Remove all non-numeric chars

        :param phone:
        :return:
        """
        phone = ''.join(re.findall(r'\d+', phone))
        phone = phone.lstrip('+0')
        if len(phone) == 11 and phone.startswith('8'):
            phone = '7' + phone[1:]
        return phone

    @staticmethod
    def clean_name(name):
        """

        :param name: str name
        :return:
        """
        return re.sub(r'\s|\d|\W', '', name, flags=re.UNICODE)

    @gen.coroutine
    def get_video_title(self, lat, lon, language):
        """
        Get video title.

        :param lat:
        :param lon:
        :param language:
        :return:
        """
        key = self.get_key("VIDEO_TITLES")
        cached = yield s().kvs.call("LLEN", key)
        if not cached:
            from reelcam.utils.google_api import google_api
            titles = yield google_api.get_street_and_places(lat,  lon, language)
            title = None
            if titles:
                title = titles[0]
                titles = titles[1:]
            if titles:
                s().kvs.async_call("LPUSH", key, *titles)
                s().kvs.async_call("EXPIRE", key, 60)
            raise gen.Return(title)
        else:
            title = yield s().kvs.call("RPOP", key)
            raise gen.Return(title)

    @gen.coroutine
    def get_subscription_info(self):
        """
        get user subscription name
        and seconds remaining

        :return:
        """
        Subscription = s().M.model('Subscription', self.ctx)
        subscription_name, subscription_limit = yield Subscription.get_info(self.subscription)
        remaining = subscription_limit - self.total_video_duration
        if remaining < 0:
            remaining = 0
        raise gen.Return(dict(name=subscription_name,
                              remaining=remaining))

    @gen.coroutine
    def is_reached_subscription_limit(self):
        """

        :return: boolean True if user reached subscription limit
        """
        info = yield self.get_subscription_info()
        raise gen.Return(info['remaining'] == 0)

    @gen.coroutine
    def set_gps_incognito(self, flag):
        """
        Set `gps_incognito` flag.

        :param flag:
        :return:
        """
        if not isinstance(flag, bool) or flag is self.gps_incognito:
            raise gen.Return(self)
        self.gps_incognito = flag
        r = yield self.save()
        data = dict(id=self.id,
                    user_id=self.id,
                    gps_incognito=self.gps_incognito)
        s().e('user:gps_incognito', sender=self, notify_sender=True, users=[self], data=data)
        raise gen.Return(r)

    def get_avatar_url(self, size="small", uploadhost=None):
        """
        get backgrounds urls, shows, expire

        :param size:
        :param uploadhost:
        :return: urls, shows, expire in dict
        """
        urls = self.get_photo_urls()
        return urls.get(size, '')

    @classmethod
    def get_online_users(cls, data):
        """
        Get online users by location and user type

        :param data:
            location (dict): lat lon location.
            radius (int): Radius of search in meters.
            type_user (int): User type. [3-all, 2-bot, 1-user]
        :return:
        """
        raw = cls.mapper.get_online_users(data)
        return cls.mapper.f_country_code(raw)

    def add_citydoom_rating(self, points=None):
        """

        :param points:
        :return:
        """
        if isinstance(points, int):
            self.citydoom_rating += points

    def reduce_citydoom_rating(self, points=None):
        """

        :param points:
        :return:
        """
        if isinstance(points, int):
            self.citydoom_rating -= points

    def get_citydoom_rank(self):
        """

        :return:
        """
        ranks = {
            "Новичок": [0, 15],
            "Следопыт": [16, 30],
            "Усмиритель": [31, 45],
            "Каратель": [46, 60],
            "Генерал": [61, 1000000],
        }
        for k, v in list(ranks.items()):
            if v[0] <= self.citydoom_rating <= v[1]:
                return k

    @classmethod
    @gen.coroutine
    def invite_user(cls, app_name, phone):
        """
        Invite User to Application.

        :param app_name: str Application name ["wedcam", "reelcam", "citydoom"].
        :param phone: str User phone.
        :return: User object.
        """
        from api_engine.enums import APP_NAMES
        if app_name not in APP_NAMES:
            return
        phone = cls.clean_phone(phone)
        user = yield cls.get_one(phone=phone, _deleted=False)
        if not user:
            user = cls.new_invite_user(app_name, phone)
            user = yield user.save()
        raise gen.Return(user)

    def invite(self, app_name, invite_text):
        """
        Send invite message.

        :param app_name: str Application name.
        :param invite_text: unicode invite text.
        """
        from api_engine.lib.msg import sms
        from api_engine.enums import CITYDOOM, WEDCAM
        if app_name == WEDCAM:
            src = sms.WEDCAM
        elif app_name == CITYDOOM:
            src = sms.CITYDOOM
        else:
            src = sms.REELCAM
        if s().sms_gate.on:
            s().sms(src, self.phone, invite_text)

    def send_email(self, app_name, text, subject=None, html=True):
        """

        :param app_name:
        :param text:
        :param subject:
        :param html:
        :return:
        """
        from api_engine.lib.msg import email
        from api_engine.enums import CITYDOOM, WEDCAM
        if app_name == WEDCAM:
            name = email.WEDCAM
            src = "support@wedcam.tv"
        elif app_name == CITYDOOM:
            name = email.CITYDOOM
            src = "support@citydoom.com"
        else:
            name = email.REELCAM
            src = "support@reelcam.com"
        if self.email:
            s().email.delay(src, self.email, text, name, subject, html)

    @classmethod
    def new_invite_user(cls, app_name, phone):
        """

        :param app_name:
        :param phone:
        :return:
        """
        user = cls(dict(app_name=app_name,
                        phone=phone,
                        user_type=USER_TYPE.invite))
        return user

    def get_tokens(self, platform, app_name=None):
        """
        Get gcm-tokens by platform and optionally app_name.

        :param platform: str Platform ID.
        :param app_name: str Application name.
        :return: set of str tokens.
        """
        from api_engine.enums import REELCAM
        if not app_name:
            app_name = REELCAM
        tokens = set()
        if not self.gcm_tokens:
            return tokens
        for gcm_token in self.gcm_tokens:
            if platform == gcm_token['platform'] and (not app_name or gcm_token['app_name'] == app_name):
                tokens.add(gcm_token['token'])
        return tokens

    def add_token(self, platform, app_name, token):
        """
        Add gcm-token.

        :param platform: str Platform ID.
        :param app_name: str Application name.
        :param token: str gcm-token.
        """
        from api_engine.enums import REELCAM
        if not app_name:
            app_name = REELCAM
        if not self.gcm_tokens:
            self.gcm_tokens = []
        for gcm_token in self.gcm_tokens:
            if gcm_token['platform'] == platform and gcm_token['app_name'] == app_name:
                gcm_token['token'] = token
                self.gcm_tokens = list(self.gcm_tokens)
                return
        self.gcm_tokens.append(dict(platform=platform,
                                    app_name=app_name,
                                    token=token))
        self.gcm_tokens = list(self.gcm_tokens)
        return

    def remove_token(self, platform, app_name):
        """
        Remove gcm-token.

        :param platform: str Platform ID.
        :param app_name: str Application name.
        :return: str gcm-token.
        """
        from api_engine.enums import REELCAM
        if not app_name:
            app_name = REELCAM
        if not self.gcm_tokens:
            return
        for gcm_token in self.gcm_tokens:
            if gcm_token['platform'] == platform and gcm_token['app_name'] == app_name:
                if gcm_token['token']:
                    token = gcm_token['token']
                    gcm_token['token'] = None
                    self.gcm_tokens = list(self.gcm_tokens)
                    return token
