# -*- coding: utf-8 -*-
from tornado.escape import json_encode
from api_engine.lib.pubsub import EVENT
from .base import BaseModel
from api_engine import s
from .mixins.event import EventMixin


class WSConnection(BaseModel, EventMixin):
    """
    A websocket connection model
    """

    can_subscribe = True

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=None,
                    session_id=None)

    def notify(self, event):
        """

        :param event:
        :return:
        """
        message = dict(event=event,
                       session_ids=[],
                       connection_ids=[self.id])
        pubsub = s().pubsub
        pubsub.publish(pubsub.get_key(EVENT), json_encode(message))
