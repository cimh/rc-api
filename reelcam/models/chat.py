# -*- coding: utf-8 -*-
from tornado import gen
from api_engine import s
from api_engine.enums import USER_TYPE
from .base import BaseModel
from ..mappers.chat import ChatEntryMappingType
from api_engine.enums import REELCAM, ANDROID


class Chat(BaseModel):
    """
    Chat Model.
    """

    mapper = ChatEntryMappingType

    @classmethod
    def _mapped_fields(cls):
        """

        :return:
        """
        return dict(id=str(),
                    user1_id=str(),
                    user2_id=str(),
                    user1_unread_count=int(),
                    user2_unread_count=int(),
                    user1_last_msg_ts=None,
                    user2_last_msg_ts=None,
                    last_msg_ts=None,
                    user1_last_msg_body=None,
                    user2_last_msg_body=None,
                    modify_ts=None)

    @gen.coroutine
    def get_data(self, support_id=None):
        """

        :param support_id:
        :return:
        """
        User = s().M.model('User', self.ctx)
        data = dict(id=self.id,
                    user1_id=self.user1_id,
                    user2_id=self.user2_id,
                    user1_unread_count=self.user1_unread_count,
                    user2_unread_count=self.user2_unread_count,
                    user1_last_msg_ts=self.ts_from_YmdHMS(self.user1_last_msg_ts),
                    user2_last_msg_ts=self.ts_from_YmdHMS(self.user2_last_msg_ts),
                    user1_last_msg_body=self.user1_last_msg_body,
                    user2_last_msg_body=self.user2_last_msg_body,
                    ts=self.ts_from_YmdHMS(self.modify_ts))
        # Support user case -> replace system id with support id
        # for correct display user name in chat:get_recent
        if support_id:
            system = yield User.get_system()
            if data['user1_id'] == system.id:
                data['user1_id'] = support_id
            if data['user2_id'] == system.id:
                data['user2_id'] = support_id

        raise gen.Return(data)

    @classmethod
    def _validation_schema(cls):
        """

        :return:
        """
        return {}

    @classmethod
    @gen.coroutine
    def mask_support_user(cls, user):
        """
        Replace support user on system

        :param user:
        :return:
        """
        User = s().M.model('User', cls.ctx)
        if user.user_type is USER_TYPE.support:
            u = yield User.get_system()
        else:
            u = user
        raise gen.Return(u)

    @classmethod
    @gen.coroutine
    def get_or_create(cls, user, user2):
        """

        :param user:
        :param user2:
        :return:
        """
        user = yield cls.mask_support_user(user)
        ids = sorted((user.id, user2.id))
        chat = yield cls.get_one(user1_id=ids[0], user2_id=ids[1])
        if chat is not None:
            raise gen.Return(chat)
        chat = cls(dict(user1_id=ids[0],
                        user2_id=ids[1]))
        chat = yield chat.save()
        raise gen.Return(chat)

    @classmethod
    @gen.coroutine
    def get_by_user(cls, user, offset=0, limit=10):
        """

        :param user:
        :param limit:
        :param offset:
        :return:
        """
        user = yield cls.mask_support_user(user)
        qs = cls.mapper.get_by_user_id(user.id, offset=offset, limit=limit+1)
        chats = yield qs.execute()
        raise gen.Return(([cls(r) for r in chats], len(chats) > limit))

    def incr_unread_by_user(self, user):
        """

        :param user:
        :return:
        """
        if user.id == self.user1_id:
            self.user2_unread_count += 1
        elif user.id == self.user2_id:
            self.user1_unread_count += 1

    def decr_unread_for_user(self, user):
        """

        :param user:
        :return:
        """
        if user.id == self.user1_id:
            self.user1_unread_count -= 1
        elif user.id == self.user2_id:
            self.user2_unread_count -= 1

    def reset_unread_for_user(self, user):
        """

        :param user:
        :return:
        """
        if user.id == self.user1_id:
            self.user1_unread_count = 0
        elif user.id == self.user2_id:
            self.user2_unread_count = 0

    def set_last_msg_ts_by_message(self, message):
        """

        :param message:
        :return:
        """
        if message._from == self.user1_id:
            self.user1_last_msg_ts = message.created_at
        elif message._from == self.user2_id:
            self.user2_last_msg_ts = message.created_at
        self.last_msg_ts = message.created_at

    def set_last_msg_body_by_message(self, message):
        """

        :param message:
        :return:
        """
        if message._from == self.user1_id:
            self.user1_last_msg_body = message.body
        elif message._from == self.user2_id:
            self.user2_last_msg_body = message.body

    @gen.coroutine
    def is_member(self, user):
        """

        :param user:
        :return:
        """
        user = yield self.mask_support_user(user)
        raise gen.Return(user.id in [self.user1_id, self.user2_id])

    @gen.coroutine
    def message_new(self, user, body):
        """

        :param user:
        :param body:
        :return:
        """
        Message = s().M.model('Message', self.ctx)
        user = yield self.mask_support_user(user)
        message = Message(dict(chat_id=self.id,
                               _from=user.id,
                               _to=self.user1_id if self.user1_id != user.id else self.user2_id,
                               body=body,
                               created_at=self.now_YmdHMS()))
        self.incr_unread_by_user(user)
        self.set_last_msg_ts_by_message(message)
        self.set_last_msg_body_by_message(message)
        mess, data, members, _ = yield [message.save(), self.get_data(), self.get_members(), self.save()]
        data['message_id'] = mess.id
        notification = dict(badge=data['user1_unread_count']
                            if data['user2_id'] == self.ctx.request_user.id else data['user2_unread_count'])
        s().e('chat:message_new',
              sender=self.ctx.request_user,
              notify_sender=True if self.ctx.platform == ANDROID else False,
              users=list([m for m in members if mess._to == m.id]),
              data=data,
              push=True,
              notification=notification,
              body_loc_key="txt_push_new_chat_message" if self.ctx.platform == ANDROID
              else ": ".join([user.fullname, body]),
              body_loc_args=[user.fullname, body],
              user_img=user.get_avatar_url(size="big"),
              app_name=getattr(self.ctx, 'app_name', REELCAM))
        raise gen.Return(mess.id)

    @gen.coroutine
    def message_read(self, user):
        """

        :param message:
        :param user:
        :return:
        """
        self.decr_unread_for_user(user)
        r = yield self.save()
        raise gen.Return(r)

    @gen.coroutine
    def message_delete(self, message, user):
        """

        :param message:
        :param user:
        :return:
        """
        message, a = yield [message.delete(user), message.is_author(user)]
        if not message.is_read and not a:
            self.decr_unread_for_user(user)
        r = yield self.save()
        raise gen.Return(r)

    @gen.coroutine
    def get_members(self):
        """
        Get chat members to send them event

        :return:
        """
        User = s().M.model('User', self.ctx)
        members = [self.user1_id, self.user2_id]
        # If not support user then add support user in members to send them message
        if self.ctx.request_user.user_type is not USER_TYPE.support:
            system_user = yield User.get_system()
            if system_user and system_user.id in members:
                members.remove(system_user.id)
                all_support_users = yield User.get(_deleted=False, limit=User.mapper.MAX_SIZE, user_type=USER_TYPE.support)
                supports = [u.id for u in all_support_users]
                members = list(set(supports + members))

        members = yield User.get(_ids=members, limit=len(members))
        raise gen.Return(members)

    @gen.coroutine
    def get_messages(self, offset=0, limit=10):
        """

        :param offset:
        :param limit:
        :return:
        """
        Message = s().M.model('Message', self.ctx)
        messages, has_more = yield Message.get_recent(self.id, offset=offset, limit=limit)
        raise gen.Return((messages, has_more))

    @gen.coroutine
    def read(self, user):
        """
        Read all user's incoming messages for this chat.

        :param: user: User object.
        :return: Chat object.
        """
        user = yield self.mask_support_user(user)
        self.reset_unread_for_user(user)
        up = yield self.save()
        Message = s().M.model('Message', self.ctx)
        qs = Message.mapper.get(chat_id=self.id, is_read=False, _to=user.id, delete_by_to=False)
        count = yield qs.count()
        raw = yield qs[:count].execute()
        docs = [dict(r.es_meta.source, _id=r._id) for r in raw]
        for d in docs:
            d.update(is_read=True)
        Message.mapper.bulk_index(docs, id_field='_id')
        raise gen.Return(up)

    @classmethod
    @gen.coroutine
    def read_all(cls, user):
        """
        Read all user's incoming messages for each chats.

        :param: user: User object.
        :return: list of Chat objects.
        """
        user = yield cls.mask_support_user(user)
        qs = cls.mapper.get_all(user.id)
        count = yield qs.count()
        raw = yield qs[:count].execute()
        chats = [cls(r) for r in raw]
        chats = yield [c.read(user) for c in chats]
        raise gen.Return(chats)
